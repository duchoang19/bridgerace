"use strict";
cc._RF.push(module, '56058bsB5JCBYLEHDj4omnN', 'CharacterController');
// Scripts/Controller/CharacterController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var BrickData_1 = require("../Brick/BrickData");
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Utility_1 = require("../Common/Utility");
var CheckMoveDownStairs_1 = require("./CheckMoveDownStairs");
var JoystickFollow_1 = require("./JoystickFollow");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CharacterController = /** @class */ (function (_super) {
    __extends(CharacterController, _super);
    function CharacterController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ArrowDirection = null;
        _this.speedMove = 20;
        _this.timeAnim = 0;
        _this.placeVfxWeapon = null;
        _this.brickHom = null;
        _this.brickStick = null;
        _this.checkMoveDownStairs = null;
        _this.brickType = EnumDefine_1.BrickType.BrickCharacter;
        _this.level = 0;
        _this.x = 0.003;
        _this.lastPositionStickY = 0.013;
        _this.z = -0.002;
        _this.clampTopY = 0;
        _this.listBrickAdd = [];
        _this.boolCanAttack = true;
        _this.boolMoveInFloor = false;
        _this.boolPlaySoundFoot = false;
        _this.boolCheckTypeRotate = false;
        _this.rigidbody = null;
        return _this;
    }
    CharacterController.prototype.onEnable = function () {
        GamePlayInstance_1.eventDispatcher.on(KeyEvent_1.default.increaseStickMyCharacter, this.IncreaseStick, this);
    };
    CharacterController.prototype.onDisable = function () {
        GamePlayInstance_1.eventDispatcher.off(KeyEvent_1.default.increaseStickMyCharacter, this.IncreaseStick, this);
    };
    CharacterController.prototype.start = function () {
        this.rigidbody = this.node.getComponent(cc.RigidBody3D);
        var collider = this.getComponent(cc.Collider3D);
        collider.on('collision-stay', this.onCollisionStay, this);
        this.listBrickAdd.splice(0, this.listBrickAdd.length);
    };
    CharacterController.prototype.onCollisionStay = function (event) {
        if (event.otherCollider.node.group == "Floor") {
            this.boolMoveInFloor = true;
            this.boolCanAttack = true;
        }
        if (event.otherCollider.node.group == "Model") {
            if (event.otherCollider.node.name == "StairsBoxCollider") {
                this.boolCanAttack = false;
            }
        }
    };
    CharacterController.prototype.onCollisionExit = function (event) {
        this.boolMoveInFloor = false;
    };
    CharacterController.prototype.update = function (dt) {
        if (Global_1.default.boolEnableTouch) {
            if (!this.checkMoveDownStairs.getComponent(CheckMoveDownStairs_1.default).boolMoveDown && this.boolMoveInFloor)
                this.MovePhyics(0);
            else
                this.MovePhyics(-20);
        }
        this.ClampTopYByLevel();
    };
    CharacterController.prototype.Attacking = function () {
        var _this = this;
        if (!Global_1.default.boolStartAttacking) {
            if (this.boolCanAttack) {
                Global_1.default.boolStartAttacking = true;
                Global_1.default.boolCheckAttacking = false;
                this.node.getComponent(cc.SkeletonAnimation).play("Attack");
                cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
                this.scheduleOnce(function () {
                    Global_1.default.boolCheckAttacking = true;
                    Global_1.default.boolCheckAttacked = true;
                }, 0.18);
                this.scheduleOnce(function () {
                    Global_1.default.boolStartAttacking = false;
                    Global_1.default.boolFirstTouchJoyStick = false;
                    _this.node.getComponent(cc.SkeletonAnimation).play("Idle");
                }, this.timeAnim);
            }
            else {
                this.node.getComponent(cc.SkeletonAnimation).play("Idle");
                Global_1.default.boolFirstTouchJoyStick = false;
            }
        }
    };
    CharacterController.prototype.MovePhyics = function (z) {
        var _this = this;
        this.node.y = cc.misc.clampf(this.node.y, -26, this.clampTopY);
        if (!this.boolPlaySoundFoot) {
            this.boolPlaySoundFoot = true;
            cc.audioEngine.playEffect(Global_1.default.soundFootStep, false);
            this.scheduleOnce(function () {
                _this.boolPlaySoundFoot = false;
            }, 0.3);
        }
        var degree = Utility_1.default.CaculatorDegree(Global_1.default.touchPos);
        this.node.is3DNode = true;
        if (!this.boolCheckTypeRotate) {
            this.node.eulerAngles = new cc.Vec3(-90, 180, degree);
        }
        else {
            this.node.runAction(cc.sequence(cc.rotate3DTo(0.1, cc.v3(-90, 180, degree)), cc.callFunc(function () {
                _this.boolCheckTypeRotate = false;
            })));
        }
        this.rigidbody.setLinearVelocity(new cc.Vec3(this.speedMove * Global_1.default.touchPos.x, this.speedMove * Global_1.default.touchPos.y, z));
    };
    CharacterController.prototype.StopMove = function () {
        this.rigidbody.setLinearVelocity(new cc.Vec3(0, 0, 0));
    };
    CharacterController.prototype.SpawnerEffectSmoke = function (smoke) {
        var Smoke = cc.instantiate(smoke);
        Smoke.parent = cc.Canvas.instance.node;
        var pos = this.node.convertToWorldSpaceAR(this.placeVfxWeapon.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        Smoke.x = pos.x;
        Smoke.y = pos.y;
        Smoke.z = 0;
    };
    CharacterController.prototype.IncreaseStick = function () {
        var brick = cc.instantiate(this.brickStick);
        brick.parent = this.node.children[2];
        brick.x = this.x;
        brick.y = this.lastPositionStickY + 0.004;
        brick.z = this.z;
        brick.getComponent(BrickData_1.default).setmaterial(EnumDefine_1.BrickType.BrickCharacter);
        this.IncreaseLevel();
        this.lastPositionStickY = this.lastPositionStickY + 0.004;
        this.listBrickAdd.push(brick);
    };
    CharacterController.prototype.DecreaseStick = function () {
        this.listBrickAdd[this.listBrickAdd.length - 1].destroy();
        this.lastPositionStickY = this.lastPositionStickY - 0.004;
        this.listBrickAdd.pop();
    };
    CharacterController.prototype.IncreaseLevel = function () {
        this.node.children[0].scale += 0.1;
        this.level++;
        if (this.level >= 5)
            GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.txtClimb.runAction(cc.fadeIn(0.3));
    };
    CharacterController.prototype.DecreaseLevel = function () {
        this.level--;
    };
    CharacterController.prototype.SpawnerBrickWhenFall = function () {
        if (this.level > 0) {
            for (var i = 0; i < this.listBrickAdd.length; i++) {
                var x = Utility_1.default.RandomRangeFloat(-7, 7);
                var y = Utility_1.default.RandomRangeFloat(-7, 7);
                this.SpawnerBrick(x, y);
            }
            this.DecreaseStickAll();
        }
    };
    CharacterController.prototype.SpawnerBrick = function (x, y) {
        var Brick = cc.instantiate(this.brickHom);
        Brick.parent = cc.Canvas.instance.node;
        Brick.x = this.node.x;
        Brick.y = this.node.y;
        Brick.z = 1;
        var t = new cc.Tween().to(0.3, { position: new cc.Vec3(this.node.x + x, this.node.y + y, 0.5) })
            .target(Brick).start();
    };
    CharacterController.prototype.DecreaseStickAll = function () {
        for (var i = 0; i < this.listBrickAdd.length; i++) {
            this.listBrickAdd[i].destroy();
        }
        this.listBrickAdd.splice(0, this.listBrickAdd.length);
        this.lastPositionStickY = 0.013;
        this.node.children[0].scale = 1;
        this.level = 0;
    };
    CharacterController.prototype.ClampTopYByLevel = function () {
        switch (this.level) {
            case 0:
                this.clampTopY = 32;
                break;
            case 1:
                this.clampTopY = 36;
                break;
            case 2:
                this.clampTopY = 40;
                break;
            case 3:
                this.clampTopY = 44;
                break;
            case 4:
                this.clampTopY = 48;
                break;
            case 5:
                this.clampTopY = 52;
                break;
            case 6:
                this.clampTopY = 56;
                break;
            case 7:
                this.clampTopY = 60;
                break;
            case 8:
                this.clampTopY = 64;
                break;
            default:
                this.clampTopY = 200;
        }
    };
    CharacterController.prototype.CharacterFall = function () {
        var _this = this;
        this.node.getComponent(cc.SkeletonAnimation).play("Fall");
        GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.joyStick.opacity = 0;
        this.SpawnerBrickWhenFall();
        Global_1.default.boolEnableTouch = false;
        this.boolCheckTypeRotate = true;
        Global_1.default.boolCharacterFall = true;
        GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.joyStick.getComponent(JoystickFollow_1.default).joyDot.setPosition(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.joyStick.getComponent(JoystickFollow_1.default).joyRing.getPosition());
        this.StopMove();
        this.scheduleOnce(function () {
            Global_1.default.boolCharacterFall = false;
            _this.node.getComponent(cc.SkeletonAnimation).play("Idle");
            Global_1.default.boolFirstTouchJoyStick = false;
        }, 1.28);
    };
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "ArrowDirection", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "speedMove", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "timeAnim", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "placeVfxWeapon", void 0);
    __decorate([
        property(cc.Prefab)
    ], CharacterController.prototype, "brickHom", void 0);
    __decorate([
        property(cc.Prefab)
    ], CharacterController.prototype, "brickStick", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "checkMoveDownStairs", void 0);
    __decorate([
        property({ type: cc.Enum(EnumDefine_1.BrickType) })
    ], CharacterController.prototype, "brickType", void 0);
    CharacterController = __decorate([
        ccclass
    ], CharacterController);
    return CharacterController;
}(cc.Component));
exports.default = CharacterController;

cc._RF.pop();