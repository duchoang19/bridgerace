"use strict";
cc._RF.push(module, '6e770L2b9VC3KZmrEF0oM/r', 'BrickStairs');
// Scripts/Brick/BrickStairs.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var CharacterController_1 = require("../Controller/CharacterController");
var BrickData_1 = require("./BrickData");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BrickStairs = /** @class */ (function (_super) {
    __extends(BrickStairs, _super);
    function BrickStairs() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.boolAppeared = false;
        return _this;
    }
    BrickStairs.prototype.start = function () {
        var collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-enter', this.onTriggerEnter, this);
    };
    BrickStairs.prototype.onTriggerEnter = function (event) {
        if (event.otherCollider.node.group == "Player") {
            if (!this.boolAppeared) {
                cc.audioEngine.playEffect(Global_1.default.soundUpStairs, false);
                GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.txtClimb.runAction(cc.fadeOut(0.3));
                this.boolAppeared = true;
                this.node.opacity = 255;
                this.node.getComponent(BrickData_1.default).setmaterial(EnumDefine_1.BrickType.BrickCharacter);
                event.otherCollider.node.getComponent(CharacterController_1.default).DecreaseStick();
            }
        }
    };
    BrickStairs = __decorate([
        ccclass
    ], BrickStairs);
    return BrickStairs;
}(cc.Component));
exports.default = BrickStairs;

cc._RF.pop();