"use strict";
cc._RF.push(module, 'c0611MLpDRDp4LPc1DY52go', 'BrickData');
// Scripts/Brick/BrickData.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayController_1 = require("../Common/GamePlayController");
var BrickData = /** @class */ (function (_super) {
    __extends(BrickData, _super);
    function BrickData() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.mesh = null;
        return _this;
    }
    BrickData.prototype.setmaterial = function (materialType) {
        var index = 0;
        if (materialType == EnumDefine_1.BrickType.BrickCharacter)
            this.mesh.setMaterial(0, GamePlayController_1.default.Instance(GamePlayController_1.default).materialBrickCharacter);
        else if (materialType == EnumDefine_1.BrickType.BrickAI1)
            this.mesh.setMaterial(0, GamePlayController_1.default.Instance(GamePlayController_1.default).materialBrickAI[0]);
        else if (materialType == EnumDefine_1.BrickType.BrickAI2)
            this.mesh.setMaterial(0, GamePlayController_1.default.Instance(GamePlayController_1.default).materialBrickAI[1]);
    };
    __decorate([
        property(cc.MeshRenderer)
    ], BrickData.prototype, "mesh", void 0);
    BrickData = __decorate([
        ccclass
    ], BrickData);
    return BrickData;
}(cc.Component));
exports.default = BrickData;

cc._RF.pop();