"use strict";
cc._RF.push(module, 'f512aadc+dGGrR/NUnmdokG', 'EnumDefine');
// Scripts/Common/EnumDefine.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BrickType;
(function (BrickType) {
    BrickType[BrickType["BrickCharacter"] = 0] = "BrickCharacter";
    BrickType[BrickType["BrickAI1"] = 1] = "BrickAI1";
    BrickType[BrickType["BrickAI2"] = 2] = "BrickAI2";
})(BrickType = exports.BrickType || (exports.BrickType = {}));

cc._RF.pop();