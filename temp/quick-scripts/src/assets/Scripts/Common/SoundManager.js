"use strict";
cc._RF.push(module, 'a9858zk9XFHs4LhnNGJENNI', 'SoundManager');
// Scripts/Common/SoundManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Global_1 = require("./Global");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SoundManager = /** @class */ (function (_super) {
    __extends(SoundManager, _super);
    function SoundManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Bg = null;
        _this.footStep = null;
        _this.Intro = null;
        _this.Attack = null;
        _this.collect = null;
        _this.boxBroken = null;
        _this.upStairs = null;
        _this.win = null;
        return _this;
    }
    SoundManager.prototype.onLoad = function () {
        Global_1.default.soundBG = this.Bg;
        Global_1.default.soundIntro = this.Intro;
        Global_1.default.soundFootStep = this.footStep;
        Global_1.default.soundAttack = this.Attack;
        Global_1.default.soundCollect = this.collect;
        Global_1.default.soundBoxBroken = this.boxBroken;
        Global_1.default.soundUpStairs = this.upStairs;
        Global_1.default.soundWin = this.win;
    };
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Bg", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "footStep", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Intro", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Attack", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "collect", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "boxBroken", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "upStairs", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "win", void 0);
    SoundManager = __decorate([
        ccclass
    ], SoundManager);
    return SoundManager;
}(cc.Component));
exports.default = SoundManager;

cc._RF.pop();