
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/__qc_index__.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}
require('./assets/Scripts/Brick/BrickData');
require('./assets/Scripts/Brick/BrickHom');
require('./assets/Scripts/Brick/BrickStairs');
require('./assets/Scripts/Camera/CameraFollow');
require('./assets/Scripts/Common/AdManager');
require('./assets/Scripts/Common/EnableEngine');
require('./assets/Scripts/Common/EnumDefine');
require('./assets/Scripts/Common/GamePlayController');
require('./assets/Scripts/Common/GamePlayInstance');
require('./assets/Scripts/Common/Global');
require('./assets/Scripts/Common/KeyEvent');
require('./assets/Scripts/Common/PlatformBtn');
require('./assets/Scripts/Common/ScrollNew');
require('./assets/Scripts/Common/Singleton');
require('./assets/Scripts/Common/SoundManager');
require('./assets/Scripts/Common/SplineExtend');
require('./assets/Scripts/Common/Utility');
require('./assets/Scripts/Controller/AIController');
require('./assets/Scripts/Controller/BoxController');
require('./assets/Scripts/Controller/CharacterController');
require('./assets/Scripts/Controller/CheckMoveDownStairs');
require('./assets/Scripts/Controller/ColliderAttack');
require('./assets/Scripts/Controller/JoystickFollow');
require('./assets/Scripts/GamePlay/BR1/BR1');

                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();