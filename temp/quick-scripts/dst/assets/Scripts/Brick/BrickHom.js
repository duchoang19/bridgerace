
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Brick/BrickHom.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '532c3wlv65DxqpVK0UVc+yj', 'BrickHom');
// Scripts/Brick/BrickHom.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var AIController_1 = require("../Controller/AIController");
var CharacterController_1 = require("../Controller/CharacterController");
var BrickData_1 = require("./BrickData");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BrickHom = /** @class */ (function (_super) {
    __extends(BrickHom, _super);
    function BrickHom() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.x = 0.003;
        _this.z = -0.002;
        return _this;
    }
    BrickHom.prototype.start = function () {
        var _this = this;
        var collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-enter', this.onTriggerEnter, this);
        this.scheduleOnce(function () {
            _this.node.getComponent(cc.BoxCollider3D).enabled = true;
        }, 1);
    };
    BrickHom.prototype.onTriggerEnter = function (event) {
        var _this = this;
        if (event.otherCollider.node.group == "Player") {
            var otherNode = event.otherCollider.node;
            cc.audioEngine.playEffect(Global_1.default.soundCollect, false);
            var pos1 = cc.Canvas.instance.node.convertToWorldSpaceAR((new cc.Vec3(this.node.x, this.node.y, this.node.z)));
            var pos = otherNode.children[2].convertToNodeSpaceAR(pos1);
            this.node.getComponent(BrickData_1.default).setmaterial(otherNode.getComponent(CharacterController_1.default).brickType);
            this.node.parent = otherNode.children[2];
            this.node.x = pos.x;
            this.node.y = pos.y;
            this.node.z = pos.z;
            this.node.scale = 1;
            var t = new cc.Tween();
            t.to(0.2, { scale: 0, position: new cc.Vec3(this.x, otherNode.getComponent(CharacterController_1.default).lastPositionStickY + 0.004, this.z) })
                .call(function () {
                GamePlayInstance_1.eventDispatcher.emit(KeyEvent_1.default.increaseStickMyCharacter);
                _this.node.destroy();
            }).target(this.node).start();
        }
        if (event.otherCollider.node.group == "Enemy") {
            var otherNode_1 = event.otherCollider.node;
            cc.audioEngine.playEffect(Global_1.default.soundCollect, false);
            var pos1 = cc.Canvas.instance.node.convertToWorldSpaceAR((new cc.Vec3(this.node.x, this.node.y, this.node.z)));
            var pos = otherNode_1.children[2].convertToNodeSpaceAR(pos1);
            this.node.getComponent(BrickData_1.default).setmaterial(otherNode_1.getComponent(AIController_1.default).brickType);
            this.node.parent = otherNode_1.children[2];
            this.node.x = pos.x;
            this.node.y = pos.y;
            this.node.z = pos.z;
            this.node.scale = 1;
            var t = new cc.Tween();
            t.to(0.2, { scale: 0, position: new cc.Vec3(this.x, otherNode_1.getComponent(AIController_1.default).lastPositionStickY + 0.004, this.z) })
                .call(function () {
                GamePlayInstance_1.eventDispatcher.emit(otherNode_1.getComponent(AIController_1.default).stringIncrease);
                _this.node.destroy();
            }).target(this.node).start();
        }
    };
    BrickHom = __decorate([
        ccclass
    ], BrickHom);
    return BrickHom;
}(cc.Component));
exports.default = BrickHom;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQnJpY2tcXEJyaWNrSG9tLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLCtEQUE2RDtBQUM3RCwyQ0FBc0M7QUFDdEMsK0NBQTBDO0FBQzFDLDJEQUFzRDtBQUN0RCx5RUFBb0U7QUFDcEUseUNBQW9DO0FBRTlCLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFHNUM7SUFBc0MsNEJBQVk7SUFEbEQ7UUFBQSxxRUFpREM7UUEvQ0csT0FBQyxHQUFXLEtBQUssQ0FBQztRQUNsQixPQUFDLEdBQVcsQ0FBQyxLQUFLLENBQUM7O0lBOEN2QixDQUFDO0lBN0NHLHdCQUFLLEdBQUw7UUFBQSxpQkFNQztRQUxHLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2hELFFBQVEsQ0FBQyxFQUFFLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNkLEtBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQzVELENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQztJQUNULENBQUM7SUFDRCxpQ0FBYyxHQUFkLFVBQWUsS0FBSztRQUFwQixpQkFxQ0M7UUFwQ0csSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksUUFBUSxFQUFFO1lBQzVDLElBQUksU0FBUyxHQUFHLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO1lBQ3pDLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3RELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvRyxJQUFJLEdBQUcsR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNELElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLG1CQUFTLENBQUMsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3JHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxHQUFHLElBQUksRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLGtCQUFrQixHQUFHLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDakksSUFBSSxDQUFDO2dCQUNGLGtDQUFlLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsd0JBQXdCLENBQUMsQ0FBQztnQkFDeEQsS0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ3BDO1FBQ0QsSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksT0FBTyxFQUFFO1lBQzNDLElBQUksV0FBUyxHQUFHLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO1lBQ3pDLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3RELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvRyxJQUFJLEdBQUcsR0FBRyxXQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNELElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLG1CQUFTLENBQUMsQ0FBQyxXQUFXLENBQUMsV0FBUyxDQUFDLFlBQVksQ0FBQyxzQkFBWSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDOUYsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsV0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDcEIsSUFBSSxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDdkIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxXQUFTLENBQUMsWUFBWSxDQUFDLHNCQUFZLENBQUMsQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7aUJBQzFILElBQUksQ0FBQztnQkFDRixrQ0FBZSxDQUFDLElBQUksQ0FBQyxXQUFTLENBQUMsWUFBWSxDQUFDLHNCQUFZLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDMUUsS0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ3BDO0lBQ0wsQ0FBQztJQS9DZ0IsUUFBUTtRQUQ1QixPQUFPO09BQ2EsUUFBUSxDQWdENUI7SUFBRCxlQUFDO0NBaERELEFBZ0RDLENBaERxQyxFQUFFLENBQUMsU0FBUyxHQWdEakQ7a0JBaERvQixRQUFRIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQnJpY2tUeXBlIH0gZnJvbSBcIi4uL0NvbW1vbi9FbnVtRGVmaW5lXCI7XHJcbmltcG9ydCB7IGV2ZW50RGlzcGF0Y2hlciB9IGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi9Db21tb24vR2xvYmFsXCI7XHJcbmltcG9ydCBLZXlFdmVudCBmcm9tIFwiLi4vQ29tbW9uL0tleUV2ZW50XCI7XHJcbmltcG9ydCBBSUNvbnRyb2xsZXIgZnJvbSBcIi4uL0NvbnRyb2xsZXIvQUlDb250cm9sbGVyXCI7XHJcbmltcG9ydCBDaGFyYWN0ZXJDb250cm9sbGVyIGZyb20gXCIuLi9Db250cm9sbGVyL0NoYXJhY3RlckNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IEJyaWNrRGF0YSBmcm9tIFwiLi9Ccmlja0RhdGFcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBCcmlja0hvbSBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcbiAgICB4OiBudW1iZXIgPSAwLjAwMztcclxuICAgIHo6IG51bWJlciA9IC0wLjAwMjtcclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIGxldCBjb2xsaWRlciA9IHRoaXMuZ2V0Q29tcG9uZW50KGNjLkNvbGxpZGVyM0QpO1xyXG4gICAgICAgIGNvbGxpZGVyLm9uKCd0cmlnZ2VyLWVudGVyJywgdGhpcy5vblRyaWdnZXJFbnRlciwgdGhpcyk7XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLkJveENvbGxpZGVyM0QpLmVuYWJsZWQgPSB0cnVlO1xyXG4gICAgICAgIH0sMSk7XHJcbiAgICB9XHJcbiAgICBvblRyaWdnZXJFbnRlcihldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudC5vdGhlckNvbGxpZGVyLm5vZGUuZ3JvdXAgPT0gXCJQbGF5ZXJcIikge1xyXG4gICAgICAgICAgICBsZXQgb3RoZXJOb2RlID0gZXZlbnQub3RoZXJDb2xsaWRlci5ub2RlO1xyXG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KEdsb2JhbC5zb3VuZENvbGxlY3QsIGZhbHNlKTtcclxuICAgICAgICAgICAgbGV0IHBvczEgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZS5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoKG5ldyBjYy5WZWMzKHRoaXMubm9kZS54LCB0aGlzLm5vZGUueSwgdGhpcy5ub2RlLnopKSk7XHJcbiAgICAgICAgICAgIGxldCBwb3MgPSBvdGhlck5vZGUuY2hpbGRyZW5bMl0uY29udmVydFRvTm9kZVNwYWNlQVIocG9zMSk7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoQnJpY2tEYXRhKS5zZXRtYXRlcmlhbChvdGhlck5vZGUuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLmJyaWNrVHlwZSk7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5wYXJlbnQgPSBvdGhlck5vZGUuY2hpbGRyZW5bMl07XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS54ID0gcG9zLng7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS55ID0gcG9zLnk7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS56ID0gcG9zLno7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5zY2FsZSA9IDE7XHJcbiAgICAgICAgICAgIHZhciB0ID0gbmV3IGNjLlR3ZWVuKCk7XHJcbiAgICAgICAgICAgIHQudG8oMC4yLCB7IHNjYWxlOiAwLCBwb3NpdGlvbjogbmV3IGNjLlZlYzModGhpcy54LCBvdGhlck5vZGUuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLmxhc3RQb3NpdGlvblN0aWNrWSArIDAuMDA0LCB0aGlzLnopIH0pXHJcbiAgICAgICAgICAgICAgICAuY2FsbCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnREaXNwYXRjaGVyLmVtaXQoS2V5RXZlbnQuaW5jcmVhc2VTdGlja015Q2hhcmFjdGVyKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vZGUuZGVzdHJveSgpO1xyXG4gICAgICAgICAgICAgICAgfSkudGFyZ2V0KHRoaXMubm9kZSkuc3RhcnQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGV2ZW50Lm90aGVyQ29sbGlkZXIubm9kZS5ncm91cCA9PSBcIkVuZW15XCIpIHtcclxuICAgICAgICAgICAgbGV0IG90aGVyTm9kZSA9IGV2ZW50Lm90aGVyQ29sbGlkZXIubm9kZTtcclxuICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdChHbG9iYWwuc291bmRDb2xsZWN0LCBmYWxzZSk7XHJcbiAgICAgICAgICAgIGxldCBwb3MxID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGUuY29udmVydFRvV29ybGRTcGFjZUFSKChuZXcgY2MuVmVjMyh0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnksIHRoaXMubm9kZS56KSkpO1xyXG4gICAgICAgICAgICBsZXQgcG9zID0gb3RoZXJOb2RlLmNoaWxkcmVuWzJdLmNvbnZlcnRUb05vZGVTcGFjZUFSKHBvczEpO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KEJyaWNrRGF0YSkuc2V0bWF0ZXJpYWwob3RoZXJOb2RlLmdldENvbXBvbmVudChBSUNvbnRyb2xsZXIpLmJyaWNrVHlwZSk7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5wYXJlbnQgPSBvdGhlck5vZGUuY2hpbGRyZW5bMl07XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS54ID0gcG9zLng7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS55ID0gcG9zLnk7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS56ID0gcG9zLno7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5zY2FsZSA9IDE7XHJcbiAgICAgICAgICAgIHZhciB0ID0gbmV3IGNjLlR3ZWVuKCk7XHJcbiAgICAgICAgICAgIHQudG8oMC4yLCB7IHNjYWxlOiAwLCBwb3NpdGlvbjogbmV3IGNjLlZlYzModGhpcy54LCBvdGhlck5vZGUuZ2V0Q29tcG9uZW50KEFJQ29udHJvbGxlcikubGFzdFBvc2l0aW9uU3RpY2tZICsgMC4wMDQsIHRoaXMueikgfSlcclxuICAgICAgICAgICAgICAgIC5jYWxsKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBldmVudERpc3BhdGNoZXIuZW1pdChvdGhlck5vZGUuZ2V0Q29tcG9uZW50KEFJQ29udHJvbGxlcikuc3RyaW5nSW5jcmVhc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZS5kZXN0cm95KCk7XHJcbiAgICAgICAgICAgICAgICB9KS50YXJnZXQodGhpcy5ub2RlKS5zdGFydCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=