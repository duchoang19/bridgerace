
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Brick/BrickStairs.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6e770L2b9VC3KZmrEF0oM/r', 'BrickStairs');
// Scripts/Brick/BrickStairs.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var CharacterController_1 = require("../Controller/CharacterController");
var BrickData_1 = require("./BrickData");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BrickStairs = /** @class */ (function (_super) {
    __extends(BrickStairs, _super);
    function BrickStairs() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.boolAppeared = false;
        return _this;
    }
    BrickStairs.prototype.start = function () {
        var collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-enter', this.onTriggerEnter, this);
    };
    BrickStairs.prototype.onTriggerEnter = function (event) {
        if (event.otherCollider.node.group == "Player") {
            if (!this.boolAppeared) {
                cc.audioEngine.playEffect(Global_1.default.soundUpStairs, false);
                GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.txtClimb.runAction(cc.fadeOut(0.3));
                this.boolAppeared = true;
                this.node.opacity = 255;
                this.node.getComponent(BrickData_1.default).setmaterial(EnumDefine_1.BrickType.BrickCharacter);
                event.otherCollider.node.getComponent(CharacterController_1.default).DecreaseStick();
            }
        }
    };
    BrickStairs = __decorate([
        ccclass
    ], BrickStairs);
    return BrickStairs;
}(cc.Component));
exports.default = BrickStairs;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQnJpY2tcXEJyaWNrU3RhaXJzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG1EQUFpRDtBQUVqRCwrREFBMEQ7QUFDMUQsMkNBQXNDO0FBQ3RDLHlFQUFvRTtBQUNwRSx5Q0FBb0M7QUFFOUIsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUc1QztJQUF5QywrQkFBWTtJQURyRDtRQUFBLHFFQW1CQztRQWpCRyxrQkFBWSxHQUFZLEtBQUssQ0FBQzs7SUFpQmxDLENBQUM7SUFoQkcsMkJBQUssR0FBTDtRQUNJLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2hELFFBQVEsQ0FBQyxFQUFFLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDNUQsQ0FBQztJQUNELG9DQUFjLEdBQWQsVUFBZSxLQUFLO1FBQ2hCLElBQUksS0FBSyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLFFBQVEsRUFBRTtZQUM1QyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDcEIsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxhQUFhLEVBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3RELDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDekYsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztnQkFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsbUJBQVMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxzQkFBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUN4RSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxhQUFhLEVBQUUsQ0FBQzthQUM5RTtTQUNKO0lBQ0wsQ0FBQztJQWpCZ0IsV0FBVztRQUQvQixPQUFPO09BQ2EsV0FBVyxDQWtCL0I7SUFBRCxrQkFBQztDQWxCRCxBQWtCQyxDQWxCd0MsRUFBRSxDQUFDLFNBQVMsR0FrQnBEO2tCQWxCb0IsV0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJyaWNrVHlwZSB9IGZyb20gXCIuLi9Db21tb24vRW51bURlZmluZVwiO1xyXG5pbXBvcnQgR2FtZVBsYXlDb250cm9sbGVyIGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlDb250cm9sbGVyXCI7XHJcbmltcG9ydCBHYW1lUGxheUluc3RhbmNlIGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi9Db21tb24vR2xvYmFsXCI7XHJcbmltcG9ydCBDaGFyYWN0ZXJDb250cm9sbGVyIGZyb20gXCIuLi9Db250cm9sbGVyL0NoYXJhY3RlckNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IEJyaWNrRGF0YSBmcm9tIFwiLi9Ccmlja0RhdGFcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBCcmlja1N0YWlycyBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcbiAgICBib29sQXBwZWFyZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIGxldCBjb2xsaWRlciA9IHRoaXMuZ2V0Q29tcG9uZW50KGNjLkNvbGxpZGVyM0QpO1xyXG4gICAgICAgIGNvbGxpZGVyLm9uKCd0cmlnZ2VyLWVudGVyJywgdGhpcy5vblRyaWdnZXJFbnRlciwgdGhpcyk7XHJcbiAgICB9XHJcbiAgICBvblRyaWdnZXJFbnRlcihldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudC5vdGhlckNvbGxpZGVyLm5vZGUuZ3JvdXAgPT0gXCJQbGF5ZXJcIikge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuYm9vbEFwcGVhcmVkKSB7XHJcbiAgICAgICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KEdsb2JhbC5zb3VuZFVwU3RhaXJzLGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkudHh0Q2xpbWIucnVuQWN0aW9uKGNjLmZhZGVPdXQoMC4zKSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmJvb2xBcHBlYXJlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUub3BhY2l0eSA9IDI1NTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoQnJpY2tEYXRhKS5zZXRtYXRlcmlhbChCcmlja1R5cGUuQnJpY2tDaGFyYWN0ZXIpO1xyXG4gICAgICAgICAgICAgICAgZXZlbnQub3RoZXJDb2xsaWRlci5ub2RlLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5EZWNyZWFzZVN0aWNrKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19