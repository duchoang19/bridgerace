
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Controller/AIController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '523563QNh5DXpN8/rDUWg9W', 'AIController');
// Scripts/Controller/AIController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var BrickData_1 = require("../Brick/BrickData");
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var Utility_1 = require("../Common/Utility");
var ColliderAttack_1 = require("./ColliderAttack");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var AIController = /** @class */ (function (_super) {
    __extends(AIController, _super);
    function AIController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.clampXLeft = -33;
        _this.clampXRight = 33;
        _this.clampYTop = 21;
        _this.clampYBottom = -18;
        _this.speedMove = 20;
        _this.colliderAttack = null;
        _this.brickHom = null;
        _this.brickStick = null;
        _this.brickType = EnumDefine_1.BrickType.BrickAI1;
        _this.stringIncrease = "";
        _this.moveX = 0;
        _this.moveY = 0;
        _this.level = 0;
        _this.listBrickAdd = [];
        _this.x = 0.003;
        _this.lastPositionStickY = 0.013;
        _this.z = -0.002;
        _this.boolCheckDeath = false;
        _this.boolCheckDeathOne = false;
        _this.boolCheckdelay = false;
        _this.boolEnemyRunningIdle = false;
        _this.boolEnemyFollowBox = false;
        _this.boolEnemyAttacking = false;
        return _this;
    }
    //rigidbody: cc.RigidBody3D;
    AIController.prototype.onEnable = function () {
        GamePlayInstance_1.eventDispatcher.on(this.stringIncrease, this.IncreaseStick, this);
    };
    AIController.prototype.onDisable = function () {
        GamePlayInstance_1.eventDispatcher.off(this.stringIncrease, this.IncreaseStick, this);
    };
    // start() {
    //     this.rigidbody = this.node.getComponent(cc.RigidBody3D);
    // }
    AIController.prototype.update = function () {
        //this.CheckingAttack();
        if (!this.boolCheckDeath) {
            if (Global_1.default.boolStartAttacking && Global_1.default.boolCheckAttacking) {
                var pos1 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[0].getPosition()));
                var pos2 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[1].getPosition()));
                var pos3 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[2].getPosition()));
                var direction1 = cc.v2(pos1.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, pos1.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var direction2 = cc.v2(pos2.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, pos2.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var degree = direction1.signAngle(direction2);
                degree = cc.misc.radiansToDegrees(degree);
                var posEnemy = cc.v2(this.node.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, this.node.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var degreeWithPos1 = posEnemy.signAngle(direction1);
                degreeWithPos1 = cc.misc.radiansToDegrees(degreeWithPos1);
                var degreeWithPos2 = posEnemy.signAngle(direction2);
                degreeWithPos2 = cc.misc.radiansToDegrees(degreeWithPos2);
                var realNeed = 0;
                degreeWithPos1 = Math.abs(degreeWithPos1);
                degreeWithPos2 = Math.abs(degreeWithPos2);
                if (degreeWithPos1 > degreeWithPos2) {
                    realNeed = degreeWithPos1;
                }
                else {
                    realNeed = degreeWithPos2;
                }
                var distance = Utility_1.default.Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y));
                var maxDistance = Utility_1.default.Distance(cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y), cc.v2(pos3.x, pos3.y));
                if (Math.abs(realNeed) < degree) {
                    if (distance < maxDistance) {
                        if (Global_1.default.boolCheckAttacked && !this.boolCheckDeathOne) {
                            this.EnemyFail();
                            this.boolCheckDeath = true;
                            this.boolCheckDeathOne = true;
                        }
                    }
                }
            }
        }
    };
    AIController.prototype.EnemyFail = function () {
        var _this = this;
        this.unscheduleAllCallbacks();
        this.node.stopAllActions();
        this.colliderAttack.getComponent(ColliderAttack_1.default).unscheduleAllCallbacks();
        this.node.getComponent(cc.SkeletonAnimation).play("Fall");
        this.SpawnerBrickWhenFall();
        this.scheduleOnce(function () {
            _this.node.getComponent(cc.SkeletonAnimation).play("Idle");
        }, 1.28);
        this.scheduleOnce(function () {
            _this.boolCheckDeath = false;
            _this.boolCheckDeathOne = false;
            _this.EnemyRunIdle();
        }, 1.6);
    };
    AIController.prototype.StartMove = function () {
        this.EnemyRunIdle();
    };
    AIController.prototype.EnemyRunIdle = function () {
        var _this = this;
        this.node.getComponent(cc.SkeletonAnimation).play("Run");
        this.boolEnemyRunningIdle = true;
        this.boolEnemyFollowBox = false;
        while (Utility_1.default.Distance(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) < 30) {
            this.moveX = Utility_1.default.RandomRangeFloat(this.clampXLeft, this.clampXRight);
            this.moveY = Utility_1.default.RandomRangeFloat(this.clampYBottom, this.clampYTop);
        }
        this.moveX = Utility_1.default.RandomRangeFloat(this.clampXLeft, this.clampXRight);
        this.moveY = Utility_1.default.RandomRangeFloat(this.clampYBottom, this.clampYTop);
        //let Direction = new cc.Vec2(this.moveX - this.node.x, this.moveY - this.node.y).normalize();
        //this.rigidbody.setLinearVelocity(new cc.Vec3(this.speedMove * Direction.x, this.speedMove * Direction.y, -10));
        //let degree = Utility.CaculatorDegree(Direction);
        //this.node.runAction(cc.rotate3DTo(0.2, cc.v3(-90, 180, degree)));
        var tween = new cc.Tween().to(2, { position: cc.v3(this.moveX, this.moveY, 0) }).call(function () {
            _this.EnemyRunIdle();
        });
        tween.target(this.node).start();
        //let degree = Utility.CaculatorDegree(cc.v2(this.moveX, this.moveY));
        var degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) - 90;
        this.node.runAction(cc.rotate3DTo(0.2, cc.v3(-90, -180, -degree)));
        this.scheduleOnce(function () {
            _this.node.stopAllActions();
            _this.node.getComponent(cc.SkeletonAnimation).play("Run");
            _this.EnemyRunIdle();
        }, 2.5);
    };
    AIController.prototype.SpawnerBrickWhenFall = function () {
        if (this.level > 0) {
            for (var i = 0; i < this.listBrickAdd.length; i++) {
                var x = Utility_1.default.RandomRangeFloat(-7, 7);
                var y = Utility_1.default.RandomRangeFloat(-7, 7);
                this.SpawnerBrick(x, y);
            }
            this.DecreaseStickAll();
        }
    };
    AIController.prototype.SpawnerBrick = function (x, y) {
        var Brick = cc.instantiate(this.brickHom);
        Brick.parent = cc.Canvas.instance.node;
        Brick.x = this.node.x;
        Brick.y = this.node.y;
        Brick.z = 1;
        var t = new cc.Tween().to(0.3, { position: new cc.Vec3(this.node.x + x, this.node.y + y, 0.5) })
            .target(Brick).start();
    };
    AIController.prototype.IncreaseStick = function () {
        var brick = cc.instantiate(this.brickStick);
        brick.parent = this.node.children[2];
        brick.x = this.x;
        brick.y = this.lastPositionStickY + 0.004;
        brick.z = this.z;
        brick.getComponent(BrickData_1.default).setmaterial(this.brickType);
        this.IncreaseLevel();
        this.lastPositionStickY = this.lastPositionStickY + 0.004;
        this.listBrickAdd.push(brick);
    };
    AIController.prototype.DecreaseStickAll = function () {
        for (var i = 0; i < this.listBrickAdd.length; i++) {
            this.listBrickAdd[i].destroy();
        }
        this.listBrickAdd.splice(0, this.listBrickAdd.length);
        this.lastPositionStickY = 0.013;
        this.node.children[0].scale = 1;
        this.DecreaseLevel();
    };
    AIController.prototype.IncreaseLevel = function () {
        this.node.children[0].scale += 0.1;
        this.level++;
    };
    AIController.prototype.DecreaseLevel = function () {
        this.level = 0;
    };
    AIController.prototype.betweenDegree = function (comVec, dirVec) {
        var angleDeg = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDeg;
    };
    __decorate([
        property(cc.Integer)
    ], AIController.prototype, "clampXLeft", void 0);
    __decorate([
        property(cc.Integer)
    ], AIController.prototype, "clampXRight", void 0);
    __decorate([
        property(cc.Integer)
    ], AIController.prototype, "clampYTop", void 0);
    __decorate([
        property(cc.Integer)
    ], AIController.prototype, "clampYBottom", void 0);
    __decorate([
        property(cc.Integer)
    ], AIController.prototype, "speedMove", void 0);
    __decorate([
        property(cc.Node)
    ], AIController.prototype, "colliderAttack", void 0);
    __decorate([
        property(cc.Prefab)
    ], AIController.prototype, "brickHom", void 0);
    __decorate([
        property(cc.Prefab)
    ], AIController.prototype, "brickStick", void 0);
    __decorate([
        property({ type: cc.Enum(EnumDefine_1.BrickType) })
    ], AIController.prototype, "brickType", void 0);
    __decorate([
        property(cc.String)
    ], AIController.prototype, "stringIncrease", void 0);
    AIController = __decorate([
        ccclass
    ], AIController);
    return AIController;
}(cc.Component));
exports.default = AIController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29udHJvbGxlclxcQUlDb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLGdEQUEyQztBQUMzQyxtREFBaUQ7QUFDakQsK0RBQStFO0FBQy9FLDJDQUFzQztBQUV0Qyw2Q0FBd0M7QUFFeEMsbURBQThDO0FBRXhDLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFHNUM7SUFBMEMsZ0NBQVk7SUFEdEQ7UUFBQSxxRUFxTEM7UUFsTEcsZ0JBQVUsR0FBVyxDQUFDLEVBQUUsQ0FBQztRQUV6QixpQkFBVyxHQUFXLEVBQUUsQ0FBQztRQUV6QixlQUFTLEdBQVcsRUFBRSxDQUFDO1FBRXZCLGtCQUFZLEdBQVcsQ0FBQyxFQUFFLENBQUM7UUFFM0IsZUFBUyxHQUFXLEVBQUUsQ0FBQztRQUV2QixvQkFBYyxHQUFZLElBQUksQ0FBQztRQUUvQixjQUFRLEdBQWMsSUFBSSxDQUFDO1FBRTNCLGdCQUFVLEdBQWMsSUFBSSxDQUFDO1FBRXRCLGVBQVMsR0FBYyxzQkFBUyxDQUFDLFFBQVEsQ0FBQztRQUVqRCxvQkFBYyxHQUFXLEVBQUUsQ0FBQztRQUM1QixXQUFLLEdBQVcsQ0FBQyxDQUFDO1FBQ2xCLFdBQUssR0FBVyxDQUFDLENBQUM7UUFDbEIsV0FBSyxHQUFXLENBQUMsQ0FBQztRQUNsQixrQkFBWSxHQUFjLEVBQUUsQ0FBQztRQUM3QixPQUFDLEdBQVcsS0FBSyxDQUFDO1FBQ2xCLHdCQUFrQixHQUFXLEtBQUssQ0FBQztRQUNuQyxPQUFDLEdBQVcsQ0FBQyxLQUFLLENBQUM7UUFDbkIsb0JBQWMsR0FBWSxLQUFLLENBQUM7UUFDaEMsdUJBQWlCLEdBQVksS0FBSyxDQUFDO1FBQ25DLG9CQUFjLEdBQVksS0FBSyxDQUFDO1FBQ2hDLDBCQUFvQixHQUFZLEtBQUssQ0FBQztRQUN0Qyx3QkFBa0IsR0FBWSxLQUFLLENBQUM7UUFDcEMsd0JBQWtCLEdBQVksS0FBSyxDQUFDOztJQW1KeEMsQ0FBQztJQWxKRyw0QkFBNEI7SUFDNUIsK0JBQVEsR0FBUjtRQUNJLGtDQUFlLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN0RSxDQUFDO0lBQ0QsZ0NBQVMsR0FBVDtRQUNJLGtDQUFlLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBQ0QsWUFBWTtJQUNaLCtEQUErRDtJQUMvRCxJQUFJO0lBQ0osNkJBQU0sR0FBTjtRQUNJLHdCQUF3QjtRQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN0QixJQUFJLGdCQUFNLENBQUMsa0JBQWtCLElBQUksZ0JBQU0sQ0FBQyxrQkFBa0IsRUFBRTtnQkFDeEQsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BRLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNwUSxJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFFcFEsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakwsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakwsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDOUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzFDLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekwsSUFBSSxjQUFjLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDcEQsY0FBYyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzFELElBQUksY0FBYyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3BELGNBQWMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUMxRCxJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUM7Z0JBQ2pCLGNBQWMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUMxQyxjQUFjLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDMUMsSUFBSSxjQUFjLEdBQUcsY0FBYyxFQUFFO29CQUNqQyxRQUFRLEdBQUcsY0FBYyxDQUFDO2lCQUM3QjtxQkFDSTtvQkFDRCxRQUFRLEdBQUcsY0FBYyxDQUFDO2lCQUM3QjtnQkFDRCxJQUFJLFFBQVEsR0FBRyxpQkFBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSwwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hOLElBQUksV0FBVyxHQUFHLGlCQUFPLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pNLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsR0FBRyxNQUFNLEVBQUU7b0JBQzdCLElBQUksUUFBUSxHQUFHLFdBQVcsRUFBRTt3QkFDeEIsSUFBSSxnQkFBTSxDQUFDLGlCQUFpQixJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFOzRCQUNyRCxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7NEJBQ2pCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDOzRCQUMzQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO3lCQUNqQztxQkFDSjtpQkFDSjthQUNKO1NBQ0o7SUFDTCxDQUFDO0lBQ0QsZ0NBQVMsR0FBVDtRQUFBLGlCQWNDO1FBYkcsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyx3QkFBYyxDQUFDLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUMxRSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNkLEtBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5RCxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDVCxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7WUFDNUIsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztZQUMvQixLQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDeEIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ1osQ0FBQztJQUNELGdDQUFTLEdBQVQ7UUFDSSxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDeEIsQ0FBQztJQUNELG1DQUFZLEdBQVo7UUFBQSxpQkEyQkM7UUExQkcsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUNoQyxPQUFPLGlCQUFPLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBQzFGLElBQUksQ0FBQyxLQUFLLEdBQUcsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN6RSxJQUFJLENBQUMsS0FBSyxHQUFHLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDNUU7UUFDRCxJQUFJLENBQUMsS0FBSyxHQUFHLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLEtBQUssR0FBRyxpQkFBTyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3pFLDhGQUE4RjtRQUM5RixpSEFBaUg7UUFDakgsa0RBQWtEO1FBQ2xELG1FQUFtRTtRQUVuRSxJQUFJLEtBQUssR0FBRyxJQUFJLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDbEYsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDaEMsc0VBQXNFO1FBQ3RFLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDckcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUMzQixLQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDekQsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3hCLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNaLENBQUM7SUFDRCwyQ0FBb0IsR0FBcEI7UUFDSSxJQUFJLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFO1lBQ2hCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDL0MsSUFBSSxDQUFDLEdBQUcsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLEdBQUcsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7YUFDMUI7WUFDRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztTQUMzQjtJQUNMLENBQUM7SUFDRCxtQ0FBWSxHQUFaLFVBQWEsQ0FBUyxFQUFFLENBQVM7UUFDN0IsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDMUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDdkMsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUN0QixLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ3RCLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ1osSUFBSSxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDO2FBQzNGLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBQ0Qsb0NBQWEsR0FBYjtRQUNJLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzVDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDckMsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ2pCLEtBQUssQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUMxQyxLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDakIsS0FBSyxDQUFDLFlBQVksQ0FBQyxtQkFBUyxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMxRCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7UUFDMUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUNELHVDQUFnQixHQUFoQjtRQUNJLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMvQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ2xDO1FBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN6QixDQUFDO0lBQ0Qsb0NBQWEsR0FBYjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxHQUFHLENBQUM7UUFDbkMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2pCLENBQUM7SUFDRCxvQ0FBYSxHQUFiO1FBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7SUFDbkIsQ0FBQztJQUNELG9DQUFhLEdBQWIsVUFBYyxNQUFNLEVBQUUsTUFBTTtRQUN4QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNwRixPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDO0lBakxEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7b0RBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztxREFDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO21EQUNFO0lBRXZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7c0RBQ007SUFFM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQzttREFDRTtJQUV2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3dEQUNhO0lBRS9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7a0RBQ087SUFFM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztvREFDUztJQUU3QjtRQURDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLHNCQUFTLENBQUMsRUFBRSxDQUFDO21EQUNVO0lBRWpEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7d0RBQ1E7SUFwQlgsWUFBWTtRQURoQyxPQUFPO09BQ2EsWUFBWSxDQW9MaEM7SUFBRCxtQkFBQztDQXBMRCxBQW9MQyxDQXBMeUMsRUFBRSxDQUFDLFNBQVMsR0FvTHJEO2tCQXBMb0IsWUFBWSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBCcmlja0RhdGEgZnJvbSBcIi4uL0JyaWNrL0JyaWNrRGF0YVwiO1xyXG5pbXBvcnQgeyBCcmlja1R5cGUgfSBmcm9tIFwiLi4vQ29tbW9uL0VudW1EZWZpbmVcIjtcclxuaW1wb3J0IEdhbWVQbGF5SW5zdGFuY2UsIHsgZXZlbnREaXNwYXRjaGVyIH0gZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFV0aWxpdHkgZnJvbSBcIi4uL0NvbW1vbi9VdGlsaXR5XCI7XHJcbmltcG9ydCBCb3hDb250cm9sbGVyIGZyb20gXCIuL0JveENvbnRyb2xsZXJcIjtcclxuaW1wb3J0IENvbGxpZGVyQXR0YWNrIGZyb20gXCIuL0NvbGxpZGVyQXR0YWNrXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQUlDb250cm9sbGVyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBYTGVmdDogbnVtYmVyID0gLTMzO1xyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjbGFtcFhSaWdodDogbnVtYmVyID0gMzM7XHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIGNsYW1wWVRvcDogbnVtYmVyID0gMjE7XHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIGNsYW1wWUJvdHRvbTogbnVtYmVyID0gLTE4O1xyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBzcGVlZE1vdmU6IG51bWJlciA9IDIwO1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBjb2xsaWRlckF0dGFjazogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgYnJpY2tIb206IGNjLlByZWZhYiA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgYnJpY2tTdGljazogY2MuUHJlZmFiID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLkVudW0oQnJpY2tUeXBlKSB9KVxyXG4gICAgcHVibGljIGJyaWNrVHlwZTogQnJpY2tUeXBlID0gQnJpY2tUeXBlLkJyaWNrQUkxO1xyXG4gICAgQHByb3BlcnR5KGNjLlN0cmluZylcclxuICAgIHN0cmluZ0luY3JlYXNlOiBzdHJpbmcgPSBcIlwiO1xyXG4gICAgbW92ZVg6IG51bWJlciA9IDA7XHJcbiAgICBtb3ZlWTogbnVtYmVyID0gMDtcclxuICAgIGxldmVsOiBudW1iZXIgPSAwO1xyXG4gICAgbGlzdEJyaWNrQWRkOiBjYy5Ob2RlW10gPSBbXTtcclxuICAgIHg6IG51bWJlciA9IDAuMDAzO1xyXG4gICAgbGFzdFBvc2l0aW9uU3RpY2tZOiBudW1iZXIgPSAwLjAxMztcclxuICAgIHo6IG51bWJlciA9IC0wLjAwMjtcclxuICAgIGJvb2xDaGVja0RlYXRoOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBib29sQ2hlY2tEZWF0aE9uZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgYm9vbENoZWNrZGVsYXk6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGJvb2xFbmVteVJ1bm5pbmdJZGxlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBib29sRW5lbXlGb2xsb3dCb3g6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGJvb2xFbmVteUF0dGFja2luZzogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgLy9yaWdpZGJvZHk6IGNjLlJpZ2lkQm9keTNEO1xyXG4gICAgb25FbmFibGUoKSB7XHJcbiAgICAgICAgZXZlbnREaXNwYXRjaGVyLm9uKHRoaXMuc3RyaW5nSW5jcmVhc2UsIHRoaXMuSW5jcmVhc2VTdGljaywgdGhpcyk7XHJcbiAgICB9XHJcbiAgICBvbkRpc2FibGUoKSB7XHJcbiAgICAgICAgZXZlbnREaXNwYXRjaGVyLm9mZih0aGlzLnN0cmluZ0luY3JlYXNlLCB0aGlzLkluY3JlYXNlU3RpY2ssIHRoaXMpO1xyXG4gICAgfVxyXG4gICAgLy8gc3RhcnQoKSB7XHJcbiAgICAvLyAgICAgdGhpcy5yaWdpZGJvZHkgPSB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlJpZ2lkQm9keTNEKTtcclxuICAgIC8vIH1cclxuICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICAvL3RoaXMuQ2hlY2tpbmdBdHRhY2soKTtcclxuICAgICAgICBpZiAoIXRoaXMuYm9vbENoZWNrRGVhdGgpIHtcclxuICAgICAgICAgICAgaWYgKEdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcgJiYgR2xvYmFsLmJvb2xDaGVja0F0dGFja2luZykge1xyXG4gICAgICAgICAgICAgICAgbGV0IHBvczEgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmNoaWxkcmVuWzBdLmNvbnZlcnRUb1dvcmxkU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmNoaWxkcmVuWzBdLmNoaWxkcmVuWzBdLmdldFBvc2l0aW9uKCkpKTtcclxuICAgICAgICAgICAgICAgIGxldCBwb3MyID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGUuY29udmVydFRvTm9kZVNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci5jaGlsZHJlblswXS5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci5jaGlsZHJlblswXS5jaGlsZHJlblsxXS5nZXRQb3NpdGlvbigpKSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgcG9zMyA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuY2hpbGRyZW5bMF0uY29udmVydFRvV29ybGRTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuY2hpbGRyZW5bMF0uY2hpbGRyZW5bMl0uZ2V0UG9zaXRpb24oKSkpO1xyXG5cclxuICAgICAgICAgICAgICAgIGxldCBkaXJlY3Rpb24xID0gY2MudjIocG9zMS54IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci54LCBwb3MxLnkgLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnkpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGRpcmVjdGlvbjIgPSBjYy52Mihwb3MyLnggLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLngsIHBvczIueSAtIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGVncmVlID0gZGlyZWN0aW9uMS5zaWduQW5nbGUoZGlyZWN0aW9uMik7XHJcbiAgICAgICAgICAgICAgICBkZWdyZWUgPSBjYy5taXNjLnJhZGlhbnNUb0RlZ3JlZXMoZGVncmVlKTtcclxuICAgICAgICAgICAgICAgIGxldCBwb3NFbmVteSA9IGNjLnYyKHRoaXMubm9kZS54IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci54LCB0aGlzLm5vZGUueSAtIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGVncmVlV2l0aFBvczEgPSBwb3NFbmVteS5zaWduQW5nbGUoZGlyZWN0aW9uMSk7XHJcbiAgICAgICAgICAgICAgICBkZWdyZWVXaXRoUG9zMSA9IGNjLm1pc2MucmFkaWFuc1RvRGVncmVlcyhkZWdyZWVXaXRoUG9zMSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGVncmVlV2l0aFBvczIgPSBwb3NFbmVteS5zaWduQW5nbGUoZGlyZWN0aW9uMik7XHJcbiAgICAgICAgICAgICAgICBkZWdyZWVXaXRoUG9zMiA9IGNjLm1pc2MucmFkaWFuc1RvRGVncmVlcyhkZWdyZWVXaXRoUG9zMik7XHJcbiAgICAgICAgICAgICAgICBsZXQgcmVhbE5lZWQgPSAwO1xyXG4gICAgICAgICAgICAgICAgZGVncmVlV2l0aFBvczEgPSBNYXRoLmFicyhkZWdyZWVXaXRoUG9zMSk7XHJcbiAgICAgICAgICAgICAgICBkZWdyZWVXaXRoUG9zMiA9IE1hdGguYWJzKGRlZ3JlZVdpdGhQb3MyKTtcclxuICAgICAgICAgICAgICAgIGlmIChkZWdyZWVXaXRoUG9zMSA+IGRlZ3JlZVdpdGhQb3MyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVhbE5lZWQgPSBkZWdyZWVXaXRoUG9zMTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlYWxOZWVkID0gZGVncmVlV2l0aFBvczI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBsZXQgZGlzdGFuY2UgPSBVdGlsaXR5LkRpc3RhbmNlKGNjLnYyKHRoaXMubm9kZS54LCB0aGlzLm5vZGUueSksIGNjLnYyKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueCwgR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci55KSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgbWF4RGlzdGFuY2UgPSBVdGlsaXR5LkRpc3RhbmNlKGNjLnYyKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueCwgR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci55KSwgY2MudjIocG9zMy54LCBwb3MzLnkpKTtcclxuICAgICAgICAgICAgICAgIGlmIChNYXRoLmFicyhyZWFsTmVlZCkgPCBkZWdyZWUpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZGlzdGFuY2UgPCBtYXhEaXN0YW5jZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoR2xvYmFsLmJvb2xDaGVja0F0dGFja2VkICYmICF0aGlzLmJvb2xDaGVja0RlYXRoT25lKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLkVuZW15RmFpbCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ib29sQ2hlY2tEZWF0aCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmJvb2xDaGVja0RlYXRoT25lID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIEVuZW15RmFpbCgpIHtcclxuICAgICAgICB0aGlzLnVuc2NoZWR1bGVBbGxDYWxsYmFja3MoKTtcclxuICAgICAgICB0aGlzLm5vZGUuc3RvcEFsbEFjdGlvbnMoKTtcclxuICAgICAgICB0aGlzLmNvbGxpZGVyQXR0YWNrLmdldENvbXBvbmVudChDb2xsaWRlckF0dGFjaykudW5zY2hlZHVsZUFsbENhbGxiYWNrcygpO1xyXG4gICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJGYWxsXCIpO1xyXG4gICAgICAgIHRoaXMuU3Bhd25lckJyaWNrV2hlbkZhbGwoKTtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJJZGxlXCIpO1xyXG4gICAgICAgIH0sIDEuMjgpO1xyXG4gICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5ib29sQ2hlY2tEZWF0aCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmJvb2xDaGVja0RlYXRoT25lID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuRW5lbXlSdW5JZGxlKCk7XHJcbiAgICAgICAgfSwgMS42KTtcclxuICAgIH1cclxuICAgIFN0YXJ0TW92ZSgpIHtcclxuICAgICAgICB0aGlzLkVuZW15UnVuSWRsZSgpO1xyXG4gICAgfVxyXG4gICAgRW5lbXlSdW5JZGxlKCkge1xyXG4gICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJSdW5cIik7XHJcbiAgICAgICAgdGhpcy5ib29sRW5lbXlSdW5uaW5nSWRsZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5ib29sRW5lbXlGb2xsb3dCb3ggPSBmYWxzZTtcclxuICAgICAgICB3aGlsZSAoVXRpbGl0eS5EaXN0YW5jZShjYy52Mih0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnkpLCBjYy52Mih0aGlzLm1vdmVYLCB0aGlzLm1vdmVZKSkgPCAzMCkge1xyXG4gICAgICAgICAgICB0aGlzLm1vdmVYID0gVXRpbGl0eS5SYW5kb21SYW5nZUZsb2F0KHRoaXMuY2xhbXBYTGVmdCwgdGhpcy5jbGFtcFhSaWdodCk7XHJcbiAgICAgICAgICAgIHRoaXMubW92ZVkgPSBVdGlsaXR5LlJhbmRvbVJhbmdlRmxvYXQodGhpcy5jbGFtcFlCb3R0b20sIHRoaXMuY2xhbXBZVG9wKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5tb3ZlWCA9IFV0aWxpdHkuUmFuZG9tUmFuZ2VGbG9hdCh0aGlzLmNsYW1wWExlZnQsIHRoaXMuY2xhbXBYUmlnaHQpO1xyXG4gICAgICAgIHRoaXMubW92ZVkgPSBVdGlsaXR5LlJhbmRvbVJhbmdlRmxvYXQodGhpcy5jbGFtcFlCb3R0b20sIHRoaXMuY2xhbXBZVG9wKTtcclxuICAgICAgICAvL2xldCBEaXJlY3Rpb24gPSBuZXcgY2MuVmVjMih0aGlzLm1vdmVYIC0gdGhpcy5ub2RlLngsIHRoaXMubW92ZVkgLSB0aGlzLm5vZGUueSkubm9ybWFsaXplKCk7XHJcbiAgICAgICAgLy90aGlzLnJpZ2lkYm9keS5zZXRMaW5lYXJWZWxvY2l0eShuZXcgY2MuVmVjMyh0aGlzLnNwZWVkTW92ZSAqIERpcmVjdGlvbi54LCB0aGlzLnNwZWVkTW92ZSAqIERpcmVjdGlvbi55LCAtMTApKTtcclxuICAgICAgICAvL2xldCBkZWdyZWUgPSBVdGlsaXR5LkNhY3VsYXRvckRlZ3JlZShEaXJlY3Rpb24pO1xyXG4gICAgICAgIC8vdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5yb3RhdGUzRFRvKDAuMiwgY2MudjMoLTkwLCAxODAsIGRlZ3JlZSkpKTtcclxuXHJcbiAgICAgICAgdmFyIHR3ZWVuID0gbmV3IGNjLlR3ZWVuKCkudG8oMiwgeyBwb3NpdGlvbjogY2MudjModGhpcy5tb3ZlWCwgdGhpcy5tb3ZlWSwgMCkgfSkuY2FsbCgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuRW5lbXlSdW5JZGxlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdHdlZW4udGFyZ2V0KHRoaXMubm9kZSkuc3RhcnQoKTtcclxuICAgICAgICAvL2xldCBkZWdyZWUgPSBVdGlsaXR5LkNhY3VsYXRvckRlZ3JlZShjYy52Mih0aGlzLm1vdmVYLCB0aGlzLm1vdmVZKSk7XHJcbiAgICAgICAgbGV0IGRlZ3JlZSA9IHRoaXMuYmV0d2VlbkRlZ3JlZShjYy52Mih0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnkpLCBjYy52Mih0aGlzLm1vdmVYLCB0aGlzLm1vdmVZKSkgLSA5MDtcclxuICAgICAgICB0aGlzLm5vZGUucnVuQWN0aW9uKGNjLnJvdGF0ZTNEVG8oMC4yLCBjYy52MygtOTAsIC0xODAsIC1kZWdyZWUpKSk7XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuc3RvcEFsbEFjdGlvbnMoKTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIlJ1blwiKTtcclxuICAgICAgICAgICAgdGhpcy5FbmVteVJ1bklkbGUoKTtcclxuICAgICAgICB9LCAyLjUpO1xyXG4gICAgfVxyXG4gICAgU3Bhd25lckJyaWNrV2hlbkZhbGwoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMubGV2ZWwgPiAwKSB7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5saXN0QnJpY2tBZGQubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGxldCB4ID0gVXRpbGl0eS5SYW5kb21SYW5nZUZsb2F0KC03LCA3KTtcclxuICAgICAgICAgICAgICAgIGxldCB5ID0gVXRpbGl0eS5SYW5kb21SYW5nZUZsb2F0KC03LCA3KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuU3Bhd25lckJyaWNrKHgsIHkpXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5EZWNyZWFzZVN0aWNrQWxsKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgU3Bhd25lckJyaWNrKHg6IG51bWJlciwgeTogbnVtYmVyKSB7XHJcbiAgICAgICAgbGV0IEJyaWNrID0gY2MuaW5zdGFudGlhdGUodGhpcy5icmlja0hvbSk7XHJcbiAgICAgICAgQnJpY2sucGFyZW50ID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGU7XHJcbiAgICAgICAgQnJpY2sueCA9IHRoaXMubm9kZS54O1xyXG4gICAgICAgIEJyaWNrLnkgPSB0aGlzLm5vZGUueTtcclxuICAgICAgICBCcmljay56ID0gMTtcclxuICAgICAgICBsZXQgdCA9IG5ldyBjYy5Ud2VlbigpLnRvKDAuMywgeyBwb3NpdGlvbjogbmV3IGNjLlZlYzModGhpcy5ub2RlLnggKyB4LCB0aGlzLm5vZGUueSArIHksIDAuNSkgfSlcclxuICAgICAgICAgICAgLnRhcmdldChCcmljaykuc3RhcnQoKTtcclxuICAgIH1cclxuICAgIEluY3JlYXNlU3RpY2soKSB7XHJcbiAgICAgICAgbGV0IGJyaWNrID0gY2MuaW5zdGFudGlhdGUodGhpcy5icmlja1N0aWNrKTtcclxuICAgICAgICBicmljay5wYXJlbnQgPSB0aGlzLm5vZGUuY2hpbGRyZW5bMl07XHJcbiAgICAgICAgYnJpY2sueCA9IHRoaXMueDtcclxuICAgICAgICBicmljay55ID0gdGhpcy5sYXN0UG9zaXRpb25TdGlja1kgKyAwLjAwNDtcclxuICAgICAgICBicmljay56ID0gdGhpcy56O1xyXG4gICAgICAgIGJyaWNrLmdldENvbXBvbmVudChCcmlja0RhdGEpLnNldG1hdGVyaWFsKHRoaXMuYnJpY2tUeXBlKTtcclxuICAgICAgICB0aGlzLkluY3JlYXNlTGV2ZWwoKTtcclxuICAgICAgICB0aGlzLmxhc3RQb3NpdGlvblN0aWNrWSA9IHRoaXMubGFzdFBvc2l0aW9uU3RpY2tZICsgMC4wMDQ7XHJcbiAgICAgICAgdGhpcy5saXN0QnJpY2tBZGQucHVzaChicmljayk7XHJcbiAgICB9XHJcbiAgICBEZWNyZWFzZVN0aWNrQWxsKCkge1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5saXN0QnJpY2tBZGQubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5saXN0QnJpY2tBZGRbaV0uZGVzdHJveSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmxpc3RCcmlja0FkZC5zcGxpY2UoMCwgdGhpcy5saXN0QnJpY2tBZGQubGVuZ3RoKTtcclxuICAgICAgICB0aGlzLmxhc3RQb3NpdGlvblN0aWNrWSA9IDAuMDEzO1xyXG4gICAgICAgIHRoaXMubm9kZS5jaGlsZHJlblswXS5zY2FsZSA9IDE7XHJcbiAgICAgICAgdGhpcy5EZWNyZWFzZUxldmVsKCk7XHJcbiAgICB9XHJcbiAgICBJbmNyZWFzZUxldmVsKCkge1xyXG4gICAgICAgIHRoaXMubm9kZS5jaGlsZHJlblswXS5zY2FsZSArPSAwLjE7XHJcbiAgICAgICAgdGhpcy5sZXZlbCsrO1xyXG4gICAgfVxyXG4gICAgRGVjcmVhc2VMZXZlbCgpIHtcclxuICAgICAgICB0aGlzLmxldmVsID0gMDtcclxuICAgIH1cclxuICAgIGJldHdlZW5EZWdyZWUoY29tVmVjLCBkaXJWZWMpIHtcclxuICAgICAgICBsZXQgYW5nbGVEZWcgPSBNYXRoLmF0YW4yKGRpclZlYy55IC0gY29tVmVjLnksIGRpclZlYy54IC0gY29tVmVjLngpICogMTgwIC8gTWF0aC5QSTtcclxuICAgICAgICByZXR1cm4gYW5nbGVEZWc7XHJcbiAgICB9XHJcbn1cclxuIl19