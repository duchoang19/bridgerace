
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Controller/CharacterController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '56058bsB5JCBYLEHDj4omnN', 'CharacterController');
// Scripts/Controller/CharacterController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var BrickData_1 = require("../Brick/BrickData");
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Utility_1 = require("../Common/Utility");
var CheckMoveDownStairs_1 = require("./CheckMoveDownStairs");
var JoystickFollow_1 = require("./JoystickFollow");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CharacterController = /** @class */ (function (_super) {
    __extends(CharacterController, _super);
    function CharacterController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ArrowDirection = null;
        _this.speedMove = 20;
        _this.timeAnim = 0;
        _this.placeVfxWeapon = null;
        _this.brickHom = null;
        _this.brickStick = null;
        _this.checkMoveDownStairs = null;
        _this.brickType = EnumDefine_1.BrickType.BrickCharacter;
        _this.level = 0;
        _this.x = 0.003;
        _this.lastPositionStickY = 0.013;
        _this.z = -0.002;
        _this.clampTopY = 0;
        _this.listBrickAdd = [];
        _this.boolCanAttack = true;
        _this.boolMoveInFloor = false;
        _this.boolPlaySoundFoot = false;
        _this.boolCheckTypeRotate = false;
        _this.rigidbody = null;
        return _this;
    }
    CharacterController.prototype.onEnable = function () {
        GamePlayInstance_1.eventDispatcher.on(KeyEvent_1.default.increaseStickMyCharacter, this.IncreaseStick, this);
    };
    CharacterController.prototype.onDisable = function () {
        GamePlayInstance_1.eventDispatcher.off(KeyEvent_1.default.increaseStickMyCharacter, this.IncreaseStick, this);
    };
    CharacterController.prototype.start = function () {
        this.rigidbody = this.node.getComponent(cc.RigidBody3D);
        var collider = this.getComponent(cc.Collider3D);
        collider.on('collision-stay', this.onCollisionStay, this);
        this.listBrickAdd.splice(0, this.listBrickAdd.length);
    };
    CharacterController.prototype.onCollisionStay = function (event) {
        if (event.otherCollider.node.group == "Floor") {
            this.boolMoveInFloor = true;
            this.boolCanAttack = true;
        }
        if (event.otherCollider.node.group == "Model") {
            if (event.otherCollider.node.name == "StairsBoxCollider") {
                this.boolCanAttack = false;
            }
        }
    };
    CharacterController.prototype.onCollisionExit = function (event) {
        this.boolMoveInFloor = false;
    };
    CharacterController.prototype.update = function (dt) {
        if (Global_1.default.boolEnableTouch) {
            if (!this.checkMoveDownStairs.getComponent(CheckMoveDownStairs_1.default).boolMoveDown && this.boolMoveInFloor)
                this.MovePhyics(0);
            else
                this.MovePhyics(-20);
        }
        this.ClampTopYByLevel();
    };
    CharacterController.prototype.Attacking = function () {
        var _this = this;
        if (!Global_1.default.boolStartAttacking) {
            if (this.boolCanAttack) {
                Global_1.default.boolStartAttacking = true;
                Global_1.default.boolCheckAttacking = false;
                this.node.getComponent(cc.SkeletonAnimation).play("Attack");
                cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
                this.scheduleOnce(function () {
                    Global_1.default.boolCheckAttacking = true;
                    Global_1.default.boolCheckAttacked = true;
                }, 0.18);
                this.scheduleOnce(function () {
                    Global_1.default.boolStartAttacking = false;
                    Global_1.default.boolFirstTouchJoyStick = false;
                    _this.node.getComponent(cc.SkeletonAnimation).play("Idle");
                }, this.timeAnim);
            }
            else {
                this.node.getComponent(cc.SkeletonAnimation).play("Idle");
                Global_1.default.boolFirstTouchJoyStick = false;
            }
        }
    };
    CharacterController.prototype.MovePhyics = function (z) {
        var _this = this;
        this.node.y = cc.misc.clampf(this.node.y, -26, this.clampTopY);
        if (!this.boolPlaySoundFoot) {
            this.boolPlaySoundFoot = true;
            cc.audioEngine.playEffect(Global_1.default.soundFootStep, false);
            this.scheduleOnce(function () {
                _this.boolPlaySoundFoot = false;
            }, 0.3);
        }
        var degree = Utility_1.default.CaculatorDegree(Global_1.default.touchPos);
        this.node.is3DNode = true;
        if (!this.boolCheckTypeRotate) {
            this.node.eulerAngles = new cc.Vec3(-90, 180, degree);
        }
        else {
            this.node.runAction(cc.sequence(cc.rotate3DTo(0.1, cc.v3(-90, 180, degree)), cc.callFunc(function () {
                _this.boolCheckTypeRotate = false;
            })));
        }
        this.rigidbody.setLinearVelocity(new cc.Vec3(this.speedMove * Global_1.default.touchPos.x, this.speedMove * Global_1.default.touchPos.y, z));
    };
    CharacterController.prototype.StopMove = function () {
        this.rigidbody.setLinearVelocity(new cc.Vec3(0, 0, 0));
    };
    CharacterController.prototype.SpawnerEffectSmoke = function (smoke) {
        var Smoke = cc.instantiate(smoke);
        Smoke.parent = cc.Canvas.instance.node;
        var pos = this.node.convertToWorldSpaceAR(this.placeVfxWeapon.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        Smoke.x = pos.x;
        Smoke.y = pos.y;
        Smoke.z = 0;
    };
    CharacterController.prototype.IncreaseStick = function () {
        var brick = cc.instantiate(this.brickStick);
        brick.parent = this.node.children[2];
        brick.x = this.x;
        brick.y = this.lastPositionStickY + 0.004;
        brick.z = this.z;
        brick.getComponent(BrickData_1.default).setmaterial(EnumDefine_1.BrickType.BrickCharacter);
        this.IncreaseLevel();
        this.lastPositionStickY = this.lastPositionStickY + 0.004;
        this.listBrickAdd.push(brick);
    };
    CharacterController.prototype.DecreaseStick = function () {
        this.listBrickAdd[this.listBrickAdd.length - 1].destroy();
        this.lastPositionStickY = this.lastPositionStickY - 0.004;
        this.listBrickAdd.pop();
    };
    CharacterController.prototype.IncreaseLevel = function () {
        this.node.children[0].scale += 0.1;
        this.level++;
        if (this.level >= 5)
            GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.txtClimb.runAction(cc.fadeIn(0.3));
    };
    CharacterController.prototype.DecreaseLevel = function () {
        this.level--;
    };
    CharacterController.prototype.SpawnerBrickWhenFall = function () {
        if (this.level > 0) {
            for (var i = 0; i < this.listBrickAdd.length; i++) {
                var x = Utility_1.default.RandomRangeFloat(-7, 7);
                var y = Utility_1.default.RandomRangeFloat(-7, 7);
                this.SpawnerBrick(x, y);
            }
            this.DecreaseStickAll();
        }
    };
    CharacterController.prototype.SpawnerBrick = function (x, y) {
        var Brick = cc.instantiate(this.brickHom);
        Brick.parent = cc.Canvas.instance.node;
        Brick.x = this.node.x;
        Brick.y = this.node.y;
        Brick.z = 1;
        var t = new cc.Tween().to(0.3, { position: new cc.Vec3(this.node.x + x, this.node.y + y, 0.5) })
            .target(Brick).start();
    };
    CharacterController.prototype.DecreaseStickAll = function () {
        for (var i = 0; i < this.listBrickAdd.length; i++) {
            this.listBrickAdd[i].destroy();
        }
        this.listBrickAdd.splice(0, this.listBrickAdd.length);
        this.lastPositionStickY = 0.013;
        this.node.children[0].scale = 1;
        this.level = 0;
    };
    CharacterController.prototype.ClampTopYByLevel = function () {
        switch (this.level) {
            case 0:
                this.clampTopY = 32;
                break;
            case 1:
                this.clampTopY = 36;
                break;
            case 2:
                this.clampTopY = 40;
                break;
            case 3:
                this.clampTopY = 44;
                break;
            case 4:
                this.clampTopY = 48;
                break;
            case 5:
                this.clampTopY = 52;
                break;
            case 6:
                this.clampTopY = 56;
                break;
            case 7:
                this.clampTopY = 60;
                break;
            case 8:
                this.clampTopY = 64;
                break;
            default:
                this.clampTopY = 200;
        }
    };
    CharacterController.prototype.CharacterFall = function () {
        var _this = this;
        this.node.getComponent(cc.SkeletonAnimation).play("Fall");
        GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.joyStick.opacity = 0;
        this.SpawnerBrickWhenFall();
        Global_1.default.boolEnableTouch = false;
        this.boolCheckTypeRotate = true;
        Global_1.default.boolCharacterFall = true;
        GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.joyStick.getComponent(JoystickFollow_1.default).joyDot.setPosition(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.joyStick.getComponent(JoystickFollow_1.default).joyRing.getPosition());
        this.StopMove();
        this.scheduleOnce(function () {
            Global_1.default.boolCharacterFall = false;
            _this.node.getComponent(cc.SkeletonAnimation).play("Idle");
            Global_1.default.boolFirstTouchJoyStick = false;
        }, 1.28);
    };
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "ArrowDirection", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "speedMove", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "timeAnim", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "placeVfxWeapon", void 0);
    __decorate([
        property(cc.Prefab)
    ], CharacterController.prototype, "brickHom", void 0);
    __decorate([
        property(cc.Prefab)
    ], CharacterController.prototype, "brickStick", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "checkMoveDownStairs", void 0);
    __decorate([
        property({ type: cc.Enum(EnumDefine_1.BrickType) })
    ], CharacterController.prototype, "brickType", void 0);
    CharacterController = __decorate([
        ccclass
    ], CharacterController);
    return CharacterController;
}(cc.Component));
exports.default = CharacterController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29udHJvbGxlclxcQ2hhcmFjdGVyQ29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxnREFBMkM7QUFDM0MsbURBQWlEO0FBQ2pELCtEQUErRTtBQUMvRSwyQ0FBc0M7QUFDdEMsK0NBQTBDO0FBQzFDLDZDQUF3QztBQUN4Qyw2REFBd0Q7QUFDeEQsbURBQTZDO0FBRXZDLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFHNUM7SUFBaUQsdUNBQVk7SUFEN0Q7UUFBQSxxRUFvTkM7UUFqTkcsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFFL0IsZUFBUyxHQUFXLEVBQUUsQ0FBQztRQUV2QixjQUFRLEdBQVcsQ0FBQyxDQUFDO1FBRXJCLG9CQUFjLEdBQVksSUFBSSxDQUFDO1FBRS9CLGNBQVEsR0FBYyxJQUFJLENBQUM7UUFFM0IsZ0JBQVUsR0FBYyxJQUFJLENBQUM7UUFFN0IseUJBQW1CLEdBQVksSUFBSSxDQUFDO1FBRTdCLGVBQVMsR0FBYyxzQkFBUyxDQUFDLGNBQWMsQ0FBQztRQUN2RCxXQUFLLEdBQVcsQ0FBQyxDQUFDO1FBQ2xCLE9BQUMsR0FBVyxLQUFLLENBQUM7UUFDbEIsd0JBQWtCLEdBQVcsS0FBSyxDQUFDO1FBQ25DLE9BQUMsR0FBVyxDQUFDLEtBQUssQ0FBQztRQUNuQixlQUFTLEdBQVcsQ0FBQyxDQUFDO1FBQ3RCLGtCQUFZLEdBQWMsRUFBRSxDQUFDO1FBQzdCLG1CQUFhLEdBQVksSUFBSSxDQUFDO1FBQzlCLHFCQUFlLEdBQVksS0FBSyxDQUFDO1FBQ2pDLHVCQUFpQixHQUFZLEtBQUssQ0FBQztRQUNuQyx5QkFBbUIsR0FBWSxLQUFLLENBQUM7UUFDOUIsZUFBUyxHQUFtQixJQUFJLENBQUM7O0lBd0w1QyxDQUFDO0lBdkxHLHNDQUFRLEdBQVI7UUFDSSxrQ0FBZSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDcEYsQ0FBQztJQUNELHVDQUFTLEdBQVQ7UUFDSSxrQ0FBZSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDckYsQ0FBQztJQUNELG1DQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN4RCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNoRCxRQUFRLENBQUMsRUFBRSxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUNELDZDQUFlLEdBQWYsVUFBZ0IsS0FBSztRQUNqQixJQUFJLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxPQUFPLEVBQUU7WUFDM0MsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7WUFDNUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7U0FDN0I7UUFDRCxJQUFJLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxPQUFPLEVBQUU7WUFDM0MsSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksbUJBQW1CLEVBQUU7Z0JBQ3RELElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2FBQzlCO1NBQ0o7SUFDTCxDQUFDO0lBQ0QsNkNBQWUsR0FBZixVQUFnQixLQUFLO1FBQ2pCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO0lBQ2pDLENBQUM7SUFDRCxvQ0FBTSxHQUFOLFVBQU8sRUFBRTtRQUNMLElBQUksZ0JBQU0sQ0FBQyxlQUFlLEVBQUU7WUFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLGVBQWU7Z0JBQ2hHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7O2dCQUVuQixJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDNUI7UUFDRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBQ0QsdUNBQVMsR0FBVDtRQUFBLGlCQXNCQztRQXJCRyxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxrQkFBa0IsRUFBRTtZQUM1QixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQ3BCLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO2dCQUNqQyxnQkFBTSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztnQkFDbEMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUM1RCxFQUFFLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxnQkFBTSxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDckQsSUFBSSxDQUFDLFlBQVksQ0FBQztvQkFDZCxnQkFBTSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztvQkFDakMsZ0JBQU0sQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7Z0JBQ3BDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDVCxJQUFJLENBQUMsWUFBWSxDQUFDO29CQUNkLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO29CQUNsQyxnQkFBTSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztvQkFDdEMsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUM5RCxDQUFDLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQ3JCO2lCQUNJO2dCQUNELElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDMUQsZ0JBQU0sQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUM7YUFDekM7U0FDSjtJQUNMLENBQUM7SUFDRCx3Q0FBVSxHQUFWLFVBQVcsQ0FBUztRQUFwQixpQkFvQkM7UUFuQkcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDekIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztZQUM5QixFQUFFLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxnQkFBTSxDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsWUFBWSxDQUFDO2dCQUNkLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7WUFDbkMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ1g7UUFDRCxJQUFJLE1BQU0sR0FBRyxpQkFBTyxDQUFDLGVBQWUsQ0FBQyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDekQ7YUFDSTtZQUNELElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsUUFBUSxDQUFDO2dCQUNyRixLQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO1lBQ3JDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNSO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM3SCxDQUFDO0lBQ0Qsc0NBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBQ0QsZ0RBQWtCLEdBQWxCLFVBQW1CLEtBQWdCO1FBQy9CLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbEMsS0FBSyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDdkMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7UUFDN0UsR0FBRyxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN4RCxLQUFLLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDaEIsS0FBSyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2hCLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2hCLENBQUM7SUFDRCwyQ0FBYSxHQUFiO1FBQ0ksSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDNUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNyQyxLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDakIsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQzFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNqQixLQUFLLENBQUMsWUFBWSxDQUFDLG1CQUFTLENBQUMsQ0FBQyxXQUFXLENBQUMsc0JBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7UUFDMUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUNELDJDQUFhLEdBQWI7UUFDSSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzFELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQzFELElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUNELDJDQUFhLEdBQWI7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksR0FBRyxDQUFDO1FBQ25DLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNiLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDO1lBQ2YsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ2hHLENBQUM7SUFDRCwyQ0FBYSxHQUFiO1FBQ0ksSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2pCLENBQUM7SUFDRCxrREFBb0IsR0FBcEI7UUFDSSxJQUFJLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFO1lBQ2hCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDL0MsSUFBSSxDQUFDLEdBQUcsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLEdBQUcsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7YUFDMUI7WUFDRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztTQUMzQjtJQUNMLENBQUM7SUFDRCwwQ0FBWSxHQUFaLFVBQWEsQ0FBUyxFQUFFLENBQVM7UUFDN0IsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDMUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDdkMsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUN0QixLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ3RCLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ1osSUFBSSxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDO2FBQzNGLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBQ0QsOENBQWdCLEdBQWhCO1FBQ0ksS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQy9DLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDbEM7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7SUFDbkIsQ0FBQztJQUNELDhDQUFnQixHQUFoQjtRQUNJLFFBQVEsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNoQixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVjtnQkFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQztTQUM1QjtJQUNMLENBQUM7SUFDRCwyQ0FBYSxHQUFiO1FBQUEsaUJBY0M7UUFiRyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDMUQsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBQzFFLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQzVCLGdCQUFNLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztRQUMvQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLGdCQUFNLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLHdCQUFhLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLHdCQUFhLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUM5TixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNkLGdCQUFNLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1lBQ2pDLEtBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMxRCxnQkFBTSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztRQUMxQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDYixDQUFDO0lBaE5EO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7K0RBQ2E7SUFFL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQzswREFDRTtJQUV2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3lEQUNBO0lBRXJCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7K0RBQ2E7SUFFL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzt5REFDTztJQUUzQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzJEQUNTO0lBRTdCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7b0VBQ2tCO0lBRXBDO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsc0JBQVMsQ0FBQyxFQUFFLENBQUM7MERBQ2dCO0lBaEJ0QyxtQkFBbUI7UUFEdkMsT0FBTztPQUNhLG1CQUFtQixDQW1OdkM7SUFBRCwwQkFBQztDQW5ORCxBQW1OQyxDQW5OZ0QsRUFBRSxDQUFDLFNBQVMsR0FtTjVEO2tCQW5Ob0IsbUJBQW1CIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEJyaWNrRGF0YSBmcm9tIFwiLi4vQnJpY2svQnJpY2tEYXRhXCI7XHJcbmltcG9ydCB7IEJyaWNrVHlwZSB9IGZyb20gXCIuLi9Db21tb24vRW51bURlZmluZVwiO1xyXG5pbXBvcnQgR2FtZVBsYXlJbnN0YW5jZSwgeyBldmVudERpc3BhdGNoZXIgfSBmcm9tIFwiLi4vQ29tbW9uL0dhbWVQbGF5SW5zdGFuY2VcIjtcclxuaW1wb3J0IEdsb2JhbCBmcm9tIFwiLi4vQ29tbW9uL0dsb2JhbFwiO1xyXG5pbXBvcnQgS2V5RXZlbnQgZnJvbSBcIi4uL0NvbW1vbi9LZXlFdmVudFwiO1xyXG5pbXBvcnQgVXRpbGl0eSBmcm9tIFwiLi4vQ29tbW9uL1V0aWxpdHlcIjtcclxuaW1wb3J0IENoZWNrTW92ZURvd25TdGFpcnMgZnJvbSBcIi4vQ2hlY2tNb3ZlRG93blN0YWlyc1wiO1xyXG5pbXBvcnQgSm95c3RpY0ZvbGxvdyBmcm9tIFwiLi9Kb3lzdGlja0ZvbGxvd1wiO1xyXG5cclxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENoYXJhY3RlckNvbnRyb2xsZXIgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBBcnJvd0RpcmVjdGlvbjogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIHNwZWVkTW92ZTogbnVtYmVyID0gMjA7XHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIHRpbWVBbmltOiBudW1iZXIgPSAwO1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBwbGFjZVZmeFdlYXBvbjogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgYnJpY2tIb206IGNjLlByZWZhYiA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgYnJpY2tTdGljazogY2MuUHJlZmFiID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgY2hlY2tNb3ZlRG93blN0YWlyczogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoeyB0eXBlOiBjYy5FbnVtKEJyaWNrVHlwZSkgfSlcclxuICAgIHB1YmxpYyBicmlja1R5cGU6IEJyaWNrVHlwZSA9IEJyaWNrVHlwZS5Ccmlja0NoYXJhY3RlcjtcclxuICAgIGxldmVsOiBudW1iZXIgPSAwO1xyXG4gICAgeDogbnVtYmVyID0gMC4wMDM7XHJcbiAgICBsYXN0UG9zaXRpb25TdGlja1k6IG51bWJlciA9IDAuMDEzO1xyXG4gICAgejogbnVtYmVyID0gLTAuMDAyO1xyXG4gICAgY2xhbXBUb3BZOiBudW1iZXIgPSAwO1xyXG4gICAgbGlzdEJyaWNrQWRkOiBjYy5Ob2RlW10gPSBbXTtcclxuICAgIGJvb2xDYW5BdHRhY2s6IGJvb2xlYW4gPSB0cnVlO1xyXG4gICAgYm9vbE1vdmVJbkZsb29yOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBib29sUGxheVNvdW5kRm9vdDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgYm9vbENoZWNrVHlwZVJvdGF0ZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgcHVibGljIHJpZ2lkYm9keTogY2MuUmlnaWRCb2R5M0QgPSBudWxsO1xyXG4gICAgb25FbmFibGUoKSB7XHJcbiAgICAgICAgZXZlbnREaXNwYXRjaGVyLm9uKEtleUV2ZW50LmluY3JlYXNlU3RpY2tNeUNoYXJhY3RlciwgdGhpcy5JbmNyZWFzZVN0aWNrLCB0aGlzKTtcclxuICAgIH1cclxuICAgIG9uRGlzYWJsZSgpIHtcclxuICAgICAgICBldmVudERpc3BhdGNoZXIub2ZmKEtleUV2ZW50LmluY3JlYXNlU3RpY2tNeUNoYXJhY3RlciwgdGhpcy5JbmNyZWFzZVN0aWNrLCB0aGlzKTtcclxuICAgIH1cclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIHRoaXMucmlnaWRib2R5ID0gdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5SaWdpZEJvZHkzRCk7XHJcbiAgICAgICAgbGV0IGNvbGxpZGVyID0gdGhpcy5nZXRDb21wb25lbnQoY2MuQ29sbGlkZXIzRCk7XHJcbiAgICAgICAgY29sbGlkZXIub24oJ2NvbGxpc2lvbi1zdGF5JywgdGhpcy5vbkNvbGxpc2lvblN0YXksIHRoaXMpO1xyXG4gICAgICAgIHRoaXMubGlzdEJyaWNrQWRkLnNwbGljZSgwLCB0aGlzLmxpc3RCcmlja0FkZC5sZW5ndGgpO1xyXG4gICAgfVxyXG4gICAgb25Db2xsaXNpb25TdGF5KGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50Lm90aGVyQ29sbGlkZXIubm9kZS5ncm91cCA9PSBcIkZsb29yXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5ib29sTW92ZUluRmxvb3IgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLmJvb2xDYW5BdHRhY2sgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZXZlbnQub3RoZXJDb2xsaWRlci5ub2RlLmdyb3VwID09IFwiTW9kZWxcIikge1xyXG4gICAgICAgICAgICBpZiAoZXZlbnQub3RoZXJDb2xsaWRlci5ub2RlLm5hbWUgPT0gXCJTdGFpcnNCb3hDb2xsaWRlclwiKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmJvb2xDYW5BdHRhY2sgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIG9uQ29sbGlzaW9uRXhpdChldmVudCkge1xyXG4gICAgICAgIHRoaXMuYm9vbE1vdmVJbkZsb29yID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICB1cGRhdGUoZHQpIHtcclxuICAgICAgICBpZiAoR2xvYmFsLmJvb2xFbmFibGVUb3VjaCkge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuY2hlY2tNb3ZlRG93blN0YWlycy5nZXRDb21wb25lbnQoQ2hlY2tNb3ZlRG93blN0YWlycykuYm9vbE1vdmVEb3duICYmIHRoaXMuYm9vbE1vdmVJbkZsb29yKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5Nb3ZlUGh5aWNzKDApO1xyXG4gICAgICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgICAgICB0aGlzLk1vdmVQaHlpY3MoLTIwKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5DbGFtcFRvcFlCeUxldmVsKCk7XHJcbiAgICB9XHJcbiAgICBBdHRhY2tpbmcoKSB7XHJcbiAgICAgICAgaWYgKCFHbG9iYWwuYm9vbFN0YXJ0QXR0YWNraW5nKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmJvb2xDYW5BdHRhY2spIHtcclxuICAgICAgICAgICAgICAgIEdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xDaGVja0F0dGFja2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIkF0dGFja1wiKTtcclxuICAgICAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kQXR0YWNrLCBmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xDaGVja0F0dGFja2luZyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xDaGVja0F0dGFja2VkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH0sIDAuMTgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIEdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICBHbG9iYWwuYm9vbEZpcnN0VG91Y2hKb3lTdGljayA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJJZGxlXCIpO1xyXG4gICAgICAgICAgICAgICAgfSwgdGhpcy50aW1lQW5pbSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiSWRsZVwiKTtcclxuICAgICAgICAgICAgICAgIEdsb2JhbC5ib29sRmlyc3RUb3VjaEpveVN0aWNrID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBNb3ZlUGh5aWNzKHo6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLnksIC0yNiwgdGhpcy5jbGFtcFRvcFkpO1xyXG4gICAgICAgIGlmICghdGhpcy5ib29sUGxheVNvdW5kRm9vdCkge1xyXG4gICAgICAgICAgICB0aGlzLmJvb2xQbGF5U291bmRGb290ID0gdHJ1ZTtcclxuICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdChHbG9iYWwuc291bmRGb290U3RlcCwgZmFsc2UpO1xyXG4gICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmJvb2xQbGF5U291bmRGb290ID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH0sIDAuMyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCBkZWdyZWUgPSBVdGlsaXR5LkNhY3VsYXRvckRlZ3JlZShHbG9iYWwudG91Y2hQb3MpO1xyXG4gICAgICAgIHRoaXMubm9kZS5pczNETm9kZSA9IHRydWU7XHJcbiAgICAgICAgaWYgKCF0aGlzLmJvb2xDaGVja1R5cGVSb3RhdGUpIHtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmV1bGVyQW5nbGVzID0gbmV3IGNjLlZlYzMoLTkwLCAxODAsIGRlZ3JlZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUucnVuQWN0aW9uKGNjLnNlcXVlbmNlKGNjLnJvdGF0ZTNEVG8oMC4xLCBjYy52MygtOTAsIDE4MCwgZGVncmVlKSksIGNjLmNhbGxGdW5jKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYm9vbENoZWNrVHlwZVJvdGF0ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9KSkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnJpZ2lkYm9keS5zZXRMaW5lYXJWZWxvY2l0eShuZXcgY2MuVmVjMyh0aGlzLnNwZWVkTW92ZSAqIEdsb2JhbC50b3VjaFBvcy54LCB0aGlzLnNwZWVkTW92ZSAqIEdsb2JhbC50b3VjaFBvcy55LCB6KSk7XHJcbiAgICB9XHJcbiAgICBTdG9wTW92ZSgpIHtcclxuICAgICAgICB0aGlzLnJpZ2lkYm9keS5zZXRMaW5lYXJWZWxvY2l0eShuZXcgY2MuVmVjMygwLCAwLCAwKSk7XHJcbiAgICB9XHJcbiAgICBTcGF3bmVyRWZmZWN0U21va2Uoc21va2U6IGNjLlByZWZhYikge1xyXG4gICAgICAgIGxldCBTbW9rZSA9IGNjLmluc3RhbnRpYXRlKHNtb2tlKTtcclxuICAgICAgICBTbW9rZS5wYXJlbnQgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZTtcclxuICAgICAgICBsZXQgcG9zID0gdGhpcy5ub2RlLmNvbnZlcnRUb1dvcmxkU3BhY2VBUih0aGlzLnBsYWNlVmZ4V2VhcG9uLmdldFBvc2l0aW9uKCkpO1xyXG4gICAgICAgIHBvcyA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKHBvcyk7XHJcbiAgICAgICAgU21va2UueCA9IHBvcy54O1xyXG4gICAgICAgIFNtb2tlLnkgPSBwb3MueTtcclxuICAgICAgICBTbW9rZS56ID0gMDtcclxuICAgIH1cclxuICAgIEluY3JlYXNlU3RpY2soKSB7XHJcbiAgICAgICAgbGV0IGJyaWNrID0gY2MuaW5zdGFudGlhdGUodGhpcy5icmlja1N0aWNrKTtcclxuICAgICAgICBicmljay5wYXJlbnQgPSB0aGlzLm5vZGUuY2hpbGRyZW5bMl07XHJcbiAgICAgICAgYnJpY2sueCA9IHRoaXMueDtcclxuICAgICAgICBicmljay55ID0gdGhpcy5sYXN0UG9zaXRpb25TdGlja1kgKyAwLjAwNDtcclxuICAgICAgICBicmljay56ID0gdGhpcy56O1xyXG4gICAgICAgIGJyaWNrLmdldENvbXBvbmVudChCcmlja0RhdGEpLnNldG1hdGVyaWFsKEJyaWNrVHlwZS5Ccmlja0NoYXJhY3Rlcik7XHJcbiAgICAgICAgdGhpcy5JbmNyZWFzZUxldmVsKCk7XHJcbiAgICAgICAgdGhpcy5sYXN0UG9zaXRpb25TdGlja1kgPSB0aGlzLmxhc3RQb3NpdGlvblN0aWNrWSArIDAuMDA0O1xyXG4gICAgICAgIHRoaXMubGlzdEJyaWNrQWRkLnB1c2goYnJpY2spO1xyXG4gICAgfVxyXG4gICAgRGVjcmVhc2VTdGljaygpIHtcclxuICAgICAgICB0aGlzLmxpc3RCcmlja0FkZFt0aGlzLmxpc3RCcmlja0FkZC5sZW5ndGggLSAxXS5kZXN0cm95KCk7XHJcbiAgICAgICAgdGhpcy5sYXN0UG9zaXRpb25TdGlja1kgPSB0aGlzLmxhc3RQb3NpdGlvblN0aWNrWSAtIDAuMDA0O1xyXG4gICAgICAgIHRoaXMubGlzdEJyaWNrQWRkLnBvcCgpO1xyXG4gICAgfVxyXG4gICAgSW5jcmVhc2VMZXZlbCgpIHtcclxuICAgICAgICB0aGlzLm5vZGUuY2hpbGRyZW5bMF0uc2NhbGUgKz0gMC4xO1xyXG4gICAgICAgIHRoaXMubGV2ZWwrKztcclxuICAgICAgICBpZiAodGhpcy5sZXZlbCA+PSA1KVxyXG4gICAgICAgICAgICBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5LnR4dENsaW1iLnJ1bkFjdGlvbihjYy5mYWRlSW4oMC4zKSk7XHJcbiAgICB9XHJcbiAgICBEZWNyZWFzZUxldmVsKCkge1xyXG4gICAgICAgIHRoaXMubGV2ZWwtLTtcclxuICAgIH1cclxuICAgIFNwYXduZXJCcmlja1doZW5GYWxsKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmxldmVsID4gMCkge1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMubGlzdEJyaWNrQWRkLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgeCA9IFV0aWxpdHkuUmFuZG9tUmFuZ2VGbG9hdCgtNywgNyk7XHJcbiAgICAgICAgICAgICAgICBsZXQgeSA9IFV0aWxpdHkuUmFuZG9tUmFuZ2VGbG9hdCgtNywgNyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLlNwYXduZXJCcmljayh4LCB5KVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuRGVjcmVhc2VTdGlja0FsbCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFNwYXduZXJCcmljayh4OiBudW1iZXIsIHk6IG51bWJlcikge1xyXG4gICAgICAgIGxldCBCcmljayA9IGNjLmluc3RhbnRpYXRlKHRoaXMuYnJpY2tIb20pO1xyXG4gICAgICAgIEJyaWNrLnBhcmVudCA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlO1xyXG4gICAgICAgIEJyaWNrLnggPSB0aGlzLm5vZGUueDtcclxuICAgICAgICBCcmljay55ID0gdGhpcy5ub2RlLnk7XHJcbiAgICAgICAgQnJpY2sueiA9IDE7XHJcbiAgICAgICAgbGV0IHQgPSBuZXcgY2MuVHdlZW4oKS50bygwLjMsIHsgcG9zaXRpb246IG5ldyBjYy5WZWMzKHRoaXMubm9kZS54ICsgeCwgdGhpcy5ub2RlLnkgKyB5LCAwLjUpIH0pXHJcbiAgICAgICAgICAgIC50YXJnZXQoQnJpY2spLnN0YXJ0KCk7XHJcbiAgICB9XHJcbiAgICBEZWNyZWFzZVN0aWNrQWxsKCkge1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5saXN0QnJpY2tBZGQubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5saXN0QnJpY2tBZGRbaV0uZGVzdHJveSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmxpc3RCcmlja0FkZC5zcGxpY2UoMCwgdGhpcy5saXN0QnJpY2tBZGQubGVuZ3RoKTtcclxuICAgICAgICB0aGlzLmxhc3RQb3NpdGlvblN0aWNrWSA9IDAuMDEzO1xyXG4gICAgICAgIHRoaXMubm9kZS5jaGlsZHJlblswXS5zY2FsZSA9IDE7XHJcbiAgICAgICAgdGhpcy5sZXZlbCA9IDA7XHJcbiAgICB9XHJcbiAgICBDbGFtcFRvcFlCeUxldmVsKCkge1xyXG4gICAgICAgIHN3aXRjaCAodGhpcy5sZXZlbCkge1xyXG4gICAgICAgICAgICBjYXNlIDA6IHRoaXMuY2xhbXBUb3BZID0gMzI7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAxOiB0aGlzLmNsYW1wVG9wWSA9IDM2O1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgMjogdGhpcy5jbGFtcFRvcFkgPSA0MDtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIDM6IHRoaXMuY2xhbXBUb3BZID0gNDQ7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSA0OiB0aGlzLmNsYW1wVG9wWSA9IDQ4O1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgNTogdGhpcy5jbGFtcFRvcFkgPSA1MjtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIDY6IHRoaXMuY2xhbXBUb3BZID0gNTY7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSA3OiB0aGlzLmNsYW1wVG9wWSA9IDYwO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgODogdGhpcy5jbGFtcFRvcFkgPSA2NDtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgdGhpcy5jbGFtcFRvcFkgPSAyMDA7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgQ2hhcmFjdGVyRmFsbCgpIHtcclxuICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiRmFsbFwiKTtcclxuICAgICAgICBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5LmpveVN0aWNrLm9wYWNpdHkgPSAwO1xyXG4gICAgICAgIHRoaXMuU3Bhd25lckJyaWNrV2hlbkZhbGwoKTtcclxuICAgICAgICBHbG9iYWwuYm9vbEVuYWJsZVRvdWNoID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5ib29sQ2hlY2tUeXBlUm90YXRlID0gdHJ1ZTtcclxuICAgICAgICBHbG9iYWwuYm9vbENoYXJhY3RlckZhbGwgPSB0cnVlO1xyXG4gICAgICAgIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuam95U3RpY2suZ2V0Q29tcG9uZW50KEpveXN0aWNGb2xsb3cpLmpveURvdC5zZXRQb3NpdGlvbihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5LmpveVN0aWNrLmdldENvbXBvbmVudChKb3lzdGljRm9sbG93KS5qb3lSaW5nLmdldFBvc2l0aW9uKCkpO1xyXG4gICAgICAgIHRoaXMuU3RvcE1vdmUoKTtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sQ2hhcmFjdGVyRmFsbCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiSWRsZVwiKTtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xGaXJzdFRvdWNoSm95U3RpY2sgPSBmYWxzZTtcclxuICAgICAgICB9LCAxLjI4KTtcclxuICAgIH1cclxufVxyXG4iXX0=