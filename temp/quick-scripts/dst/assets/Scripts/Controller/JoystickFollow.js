
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Controller/JoystickFollow.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'f6068GFEGlHn7TrO7hQLo8v', 'JoystickFollow');
// Scripts/Controller/JoystickFollow.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var CharacterController_1 = require("./CharacterController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var JoysticFollow = /** @class */ (function (_super) {
    __extends(JoysticFollow, _super);
    function JoysticFollow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.joyRing = null;
        _this.joyDot = null;
        _this.stickPos = null;
        _this.touchLocation = null;
        _this.radius = 0;
        return _this;
    }
    JoysticFollow.prototype.onLoad = function () {
        this.radius = this.joyRing.width / 2;
    };
    JoysticFollow.prototype.start = function () {
        this.gamePlayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        this.node.on(cc.Node.EventType.TOUCH_START, this.TouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.TouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.TouchCancel, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.TouchCancel, this);
    };
    JoysticFollow.prototype.TouchStart = function (event) {
        if (!Global_1.default.boolEndGame && Global_1.default.boolStartPlay && !Global_1.default.boolCharacterFall) {
            var mousePosition = event.getLocation();
            var localMousePosition = this.node.convertToNodeSpaceAR(mousePosition);
            this.node.opacity = 255;
            this.stickPos = localMousePosition;
            this.touchLocation = event.getLocation();
            this.joyRing.setPosition(localMousePosition);
            this.joyDot.setPosition(localMousePosition);
            this.gamePlayInstance.gameplay.Guide.active = false;
            this.gamePlayInstance.gameplay.txtCollectBlock.active = false;
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = true;
        }
    };
    JoysticFollow.prototype.TouchMove = function (event) {
        if (!Global_1.default.boolEndGame && Global_1.default.boolStartPlay && !Global_1.default.boolCharacterFall) {
            this.node.opacity = 255;
            Global_1.default.boolEnableTouch = true;
            if (!Global_1.default.boolFirstTouchJoyStick) {
                Global_1.default.boolFirstTouchJoyStick = true;
                this.gamePlayInstance.gameplay.myCharacter.getComponent(cc.SkeletonAnimation).play("Run");
            }
            if (this.touchLocation === event.getLocation()) {
                return false;
            }
            this.gamePlayInstance.gameplay.Guide.active = false;
            var touchPos = this.joyRing.convertToNodeSpaceAR(event.getLocation());
            var distance = touchPos.mag();
            var posX = this.stickPos.x + touchPos.x;
            var posY = this.stickPos.y + touchPos.y;
            var p = cc.v2(posX, posY).sub(this.joyRing.getPosition()).normalize();
            Global_1.default.touchPos = p;
            if (this.radius > distance) {
                this.joyDot.setPosition(cc.v2(posX, posY));
            }
            else {
                var x = this.stickPos.x + p.x * this.radius;
                var y = this.stickPos.y + p.y * this.radius;
                this.joyDot.setPosition(cc.v2(x, y));
            }
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = true;
            this.gamePlayInstance.gameplay.Guide.active = false;
            this.gamePlayInstance.gameplay.txtCollectBlock.active = false;
        }
    };
    JoysticFollow.prototype.TouchCancel = function () {
        if (!Global_1.default.boolEndGame && Global_1.default.boolStartPlay && !Global_1.default.boolCharacterFall) {
            this.joyDot.setPosition(this.joyRing.getPosition());
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).Attacking();
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).StopMove();
            Global_1.default.boolEnableTouch = false;
            this.node.opacity = 0;
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).boolCheckTypeRotate = true;
        }
    };
    __decorate([
        property(cc.Node)
    ], JoysticFollow.prototype, "joyRing", void 0);
    __decorate([
        property(cc.Node)
    ], JoysticFollow.prototype, "joyDot", void 0);
    JoysticFollow = __decorate([
        ccclass
    ], JoysticFollow);
    return JoysticFollow;
}(cc.Component));
exports.default = JoysticFollow;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29udHJvbGxlclxcSm95c3RpY2tGb2xsb3cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsK0RBQTBEO0FBQzFELDJDQUFzQztBQUN0Qyw2REFBd0Q7QUFDbEQsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUU1QztJQUEyQyxpQ0FBWTtJQUR2RDtRQUFBLHFFQTBFQztRQXZFRyxhQUFPLEdBQVksSUFBSSxDQUFDO1FBRXhCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFDdkIsY0FBUSxHQUFZLElBQUksQ0FBQztRQUN6QixtQkFBYSxHQUFZLElBQUksQ0FBQztRQUM5QixZQUFNLEdBQVcsQ0FBQyxDQUFDOztJQWtFdkIsQ0FBQztJQWhFRyw4QkFBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUNELDZCQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDekUsQ0FBQztJQUNELGtDQUFVLEdBQVYsVUFBVyxLQUFLO1FBQ1osSUFBSSxDQUFDLGdCQUFNLENBQUMsV0FBVyxJQUFJLGdCQUFNLENBQUMsYUFBYSxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxpQkFBaUIsRUFBRTtZQUMxRSxJQUFJLGFBQWEsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDeEMsSUFBSSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3ZFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztZQUN4QixJQUFJLENBQUMsUUFBUSxHQUFHLGtCQUFrQixDQUFDO1lBQ25DLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3BELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDOUQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7U0FDN0c7SUFDTCxDQUFDO0lBQ0QsaUNBQVMsR0FBVCxVQUFVLEtBQUs7UUFDWCxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLElBQUksZ0JBQU0sQ0FBQyxhQUFhLElBQUssQ0FBQyxnQkFBTSxDQUFDLGlCQUFpQixFQUFFO1lBQzNFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztZQUN4QixnQkFBTSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7WUFDOUIsSUFBSSxDQUFDLGdCQUFNLENBQUMsc0JBQXNCLEVBQUU7Z0JBQ2hDLGdCQUFNLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDO2dCQUNyQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzdGO1lBQ0QsSUFBSSxJQUFJLENBQUMsYUFBYSxLQUFLLEtBQUssQ0FBQyxXQUFXLEVBQUUsRUFBRTtnQkFDNUMsT0FBTyxLQUFLLENBQUM7YUFDaEI7WUFDRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3BELElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7WUFDdEUsSUFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQzlCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDeEMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN4QyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ3RFLGdCQUFNLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQztZQUNwQixJQUFJLElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxFQUFFO2dCQUN4QixJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO2FBQzlDO2lCQUFNO2dCQUNILElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztnQkFDNUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO2dCQUM1QyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3hDO1lBQ0QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDMUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNwRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1NBQ2pFO0lBQ0wsQ0FBQztJQUNELG1DQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLElBQUksZ0JBQU0sQ0FBQyxhQUFhLElBQUssQ0FBQyxnQkFBTSxDQUFDLGlCQUFpQixFQUFFO1lBQzNFLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUNwRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUN6RixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4RixnQkFBTSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7WUFDL0IsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztTQUMzRztJQUNMLENBQUM7SUF0RUQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDTTtJQUV4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2lEQUNLO0lBSk4sYUFBYTtRQURqQyxPQUFPO09BQ2EsYUFBYSxDQXlFakM7SUFBRCxvQkFBQztDQXpFRCxBQXlFQyxDQXpFMEMsRUFBRSxDQUFDLFNBQVMsR0F5RXREO2tCQXpFb0IsYUFBYSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBHYW1lUGxheUluc3RhbmNlIGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi9Db21tb24vR2xvYmFsXCI7XHJcbmltcG9ydCBDaGFyYWN0ZXJDb250cm9sbGVyIGZyb20gXCIuL0NoYXJhY3RlckNvbnRyb2xsZXJcIjtcclxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSm95c3RpY0ZvbGxvdyBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGpveVJpbmc6IGNjLk5vZGUgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBqb3lEb3Q6IGNjLk5vZGUgPSBudWxsO1xyXG4gICAgc3RpY2tQb3M6IGNjLlZlYzIgPSBudWxsO1xyXG4gICAgdG91Y2hMb2NhdGlvbjogY2MuVmVjMiA9IG51bGw7XHJcbiAgICByYWRpdXM6IG51bWJlciA9IDA7XHJcbiAgICBnYW1lUGxheUluc3RhbmNlOiBHYW1lUGxheUluc3RhbmNlO1xyXG4gICAgb25Mb2FkKCkge1xyXG4gICAgICAgIHRoaXMucmFkaXVzID0gdGhpcy5qb3lSaW5nLndpZHRoIC8gMjtcclxuICAgIH1cclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZSA9IEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSk7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX1NUQVJULCB0aGlzLlRvdWNoU3RhcnQsIHRoaXMpO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbihjYy5Ob2RlLkV2ZW50VHlwZS5UT1VDSF9NT1ZFLCB0aGlzLlRvdWNoTW92ZSwgdGhpcyk7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX0VORCwgdGhpcy5Ub3VjaENhbmNlbCwgdGhpcyk7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX0NBTkNFTCwgdGhpcy5Ub3VjaENhbmNlbCwgdGhpcyk7XHJcbiAgICB9XHJcbiAgICBUb3VjaFN0YXJ0KGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKCFHbG9iYWwuYm9vbEVuZEdhbWUgJiYgR2xvYmFsLmJvb2xTdGFydFBsYXkgJiYgIUdsb2JhbC5ib29sQ2hhcmFjdGVyRmFsbCkge1xyXG4gICAgICAgICAgICB2YXIgbW91c2VQb3NpdGlvbiA9IGV2ZW50LmdldExvY2F0aW9uKCk7XHJcbiAgICAgICAgICAgIGxldCBsb2NhbE1vdXNlUG9zaXRpb24gPSB0aGlzLm5vZGUuY29udmVydFRvTm9kZVNwYWNlQVIobW91c2VQb3NpdGlvbik7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gMjU1O1xyXG4gICAgICAgICAgICB0aGlzLnN0aWNrUG9zID0gbG9jYWxNb3VzZVBvc2l0aW9uO1xyXG4gICAgICAgICAgICB0aGlzLnRvdWNoTG9jYXRpb24gPSBldmVudC5nZXRMb2NhdGlvbigpO1xyXG4gICAgICAgICAgICB0aGlzLmpveVJpbmcuc2V0UG9zaXRpb24obG9jYWxNb3VzZVBvc2l0aW9uKTtcclxuICAgICAgICAgICAgdGhpcy5qb3lEb3Quc2V0UG9zaXRpb24obG9jYWxNb3VzZVBvc2l0aW9uKTtcclxuICAgICAgICAgICAgdGhpcy5nYW1lUGxheUluc3RhbmNlLmdhbWVwbGF5Lkd1aWRlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmdhbWVQbGF5SW5zdGFuY2UuZ2FtZXBsYXkudHh0Q29sbGVjdEJsb2NrLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmdhbWVQbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkFycm93RGlyZWN0aW9uLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgVG91Y2hNb3ZlKGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKCFHbG9iYWwuYm9vbEVuZEdhbWUgJiYgR2xvYmFsLmJvb2xTdGFydFBsYXkgICYmICFHbG9iYWwuYm9vbENoYXJhY3RlckZhbGwpIHtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLm9wYWNpdHkgPSAyNTU7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sRW5hYmxlVG91Y2ggPSB0cnVlO1xyXG4gICAgICAgICAgICBpZiAoIUdsb2JhbC5ib29sRmlyc3RUb3VjaEpveVN0aWNrKSB7XHJcbiAgICAgICAgICAgICAgICBHbG9iYWwuYm9vbEZpcnN0VG91Y2hKb3lTdGljayA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdhbWVQbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiUnVuXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRvdWNoTG9jYXRpb24gPT09IGV2ZW50LmdldExvY2F0aW9uKCkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmdhbWVQbGF5SW5zdGFuY2UuZ2FtZXBsYXkuR3VpZGUuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGxldCB0b3VjaFBvcyA9IHRoaXMuam95UmluZy5jb252ZXJ0VG9Ob2RlU3BhY2VBUihldmVudC5nZXRMb2NhdGlvbigpKTtcclxuICAgICAgICAgICAgbGV0IGRpc3RhbmNlID0gdG91Y2hQb3MubWFnKCk7XHJcbiAgICAgICAgICAgIGxldCBwb3NYID0gdGhpcy5zdGlja1Bvcy54ICsgdG91Y2hQb3MueDtcclxuICAgICAgICAgICAgbGV0IHBvc1kgPSB0aGlzLnN0aWNrUG9zLnkgKyB0b3VjaFBvcy55O1xyXG4gICAgICAgICAgICBsZXQgcCA9IGNjLnYyKHBvc1gsIHBvc1kpLnN1Yih0aGlzLmpveVJpbmcuZ2V0UG9zaXRpb24oKSkubm9ybWFsaXplKCk7XHJcbiAgICAgICAgICAgIEdsb2JhbC50b3VjaFBvcyA9IHA7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnJhZGl1cyA+IGRpc3RhbmNlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmpveURvdC5zZXRQb3NpdGlvbihjYy52Mihwb3NYLCBwb3NZKSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgeCA9IHRoaXMuc3RpY2tQb3MueCArIHAueCAqIHRoaXMucmFkaXVzO1xyXG4gICAgICAgICAgICAgICAgbGV0IHkgPSB0aGlzLnN0aWNrUG9zLnkgKyBwLnkgKiB0aGlzLnJhZGl1cztcclxuICAgICAgICAgICAgICAgIHRoaXMuam95RG90LnNldFBvc2l0aW9uKGNjLnYyKHgsIHkpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmdhbWVQbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkFycm93RGlyZWN0aW9uLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZS5nYW1lcGxheS5HdWlkZS5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5nYW1lUGxheUluc3RhbmNlLmdhbWVwbGF5LnR4dENvbGxlY3RCbG9jay5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBUb3VjaENhbmNlbCgpIHtcclxuICAgICAgICBpZiAoIUdsb2JhbC5ib29sRW5kR2FtZSAmJiBHbG9iYWwuYm9vbFN0YXJ0UGxheSAgJiYgIUdsb2JhbC5ib29sQ2hhcmFjdGVyRmFsbCkge1xyXG4gICAgICAgICAgICB0aGlzLmpveURvdC5zZXRQb3NpdGlvbih0aGlzLmpveVJpbmcuZ2V0UG9zaXRpb24oKSk7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuQXR0YWNraW5nKCk7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuU3RvcE1vdmUoKTtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xFbmFibGVUb3VjaCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUub3BhY2l0eSA9IDA7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuYm9vbENoZWNrVHlwZVJvdGF0ZSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==