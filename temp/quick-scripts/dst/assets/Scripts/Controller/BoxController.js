
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Controller/BoxController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6ea4aqpj3hKabEYvQVlqxyC', 'BoxController');
// Scripts/Controller/BoxController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var Utility_1 = require("../Common/Utility");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BoxController = /** @class */ (function (_super) {
    __extends(BoxController, _super);
    function BoxController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.brickHom = null;
        _this.timeBroken = 1.06;
        _this.boolCheckDeathOne = false;
        _this.boolCheckDeath = false;
        _this.boolCheckdelay = false;
        return _this;
    }
    BoxController.prototype.update = function () {
        var _this = this;
        if (!this.boolCheckDeath) {
            if (Global_1.default.boolStartAttacking && Global_1.default.boolCheckAttacking) {
                var pos1 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[0].getPosition()));
                var pos2 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[1].getPosition()));
                var pos3 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[2].getPosition()));
                var direction1 = cc.v2(pos1.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, pos1.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var direction2 = cc.v2(pos2.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, pos2.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var degree = direction1.signAngle(direction2);
                degree = cc.misc.radiansToDegrees(degree);
                var posEnemy = cc.v2(this.node.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, this.node.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var degreeWithPos1 = posEnemy.signAngle(direction1);
                degreeWithPos1 = cc.misc.radiansToDegrees(degreeWithPos1);
                var degreeWithPos2 = posEnemy.signAngle(direction2);
                degreeWithPos2 = cc.misc.radiansToDegrees(degreeWithPos2);
                var realNeed = 0;
                degreeWithPos1 = Math.abs(degreeWithPos1);
                degreeWithPos2 = Math.abs(degreeWithPos2);
                if (degreeWithPos1 > degreeWithPos2) {
                    realNeed = degreeWithPos1;
                }
                else {
                    realNeed = degreeWithPos2;
                }
                var distance = Utility_1.default.Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y));
                var maxDistance = Utility_1.default.Distance(cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y), cc.v2(pos3.x, pos3.y));
                if (Math.abs(realNeed) < degree) {
                    if (distance < maxDistance) {
                        if (Global_1.default.boolCheckAttacked && !this.boolCheckDeathOne) {
                            this.BrokenBox();
                            this.boolCheckDeath = true;
                            this.boolCheckDeathOne = true;
                            //Global.boolCheckAttacked = false;
                            //Global.boolCheckAttacking = false;
                        }
                    }
                }
                if (!this.boolCheckdelay) {
                    this.boolCheckdelay = true;
                    this.scheduleOnce(function () {
                        Global_1.default.boolCheckAttacking = false;
                        _this.boolCheckdelay = false;
                    }, 0.003);
                }
            }
        }
    };
    BoxController.prototype.DieWhenEnemyAttack = function () {
        this.boolCheckDeath = true;
        this.BrokenBox();
    };
    BoxController.prototype.BrokenBox = function () {
        var _this = this;
        this.node.getComponent(cc.SkeletonAnimation).play("Broken");
        cc.audioEngine.playEffect(Global_1.default.soundBoxBroken, false);
        this.node.getComponent(cc.BoxCollider3D).enabled = false;
        this.scheduleOnce(function () {
            // for( let i = 0; i < 20; i++)
            // {
            //     this.SpawnerBrick(this.node.x - 2, this.node.y + 2);
            // }
            _this.SpawnerBrick(_this.node.x - 2, _this.node.y + 2);
            _this.SpawnerBrick(_this.node.x + 2, _this.node.y + 2);
            _this.SpawnerBrick(_this.node.x - 2, _this.node.y - 2);
            _this.SpawnerBrick(_this.node.x + 2, _this.node.y - 2);
        }, this.timeBroken / 7);
        this.scheduleOnce(function () {
            _this.node.getComponent(cc.SkeletonAnimation).stop();
        }, this.timeBroken);
    };
    BoxController.prototype.SpawnerBrick = function (x, y) {
        var Brick = cc.instantiate(this.brickHom);
        Brick.parent = cc.Canvas.instance.node;
        Brick.x = this.node.x;
        Brick.y = this.node.y;
        Brick.z = 1;
        var t = new cc.Tween().to(0.3, { position: new cc.Vec3(x, y, 0.5) })
            .call(function () {
            Brick.getComponent(cc.BoxCollider3D).enabled = true;
        })
            .target(Brick).start();
    };
    __decorate([
        property(cc.Prefab)
    ], BoxController.prototype, "brickHom", void 0);
    BoxController = __decorate([
        ccclass
    ], BoxController);
    return BoxController;
}(cc.Component));
exports.default = BoxController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29udHJvbGxlclxcQm94Q29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSwrREFBMEQ7QUFDMUQsMkNBQXNDO0FBRXRDLDZDQUF3QztBQUVsQyxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRzVDO0lBQTJDLGlDQUFZO0lBRHZEO1FBQUEscUVBMEZDO1FBdkZHLGNBQVEsR0FBYyxJQUFJLENBQUM7UUFDM0IsZ0JBQVUsR0FBVyxJQUFJLENBQUM7UUFDMUIsdUJBQWlCLEdBQVksS0FBSyxDQUFDO1FBQ25DLG9CQUFjLEdBQVksS0FBSyxDQUFDO1FBQ2hDLG9CQUFjLEdBQVksS0FBSyxDQUFDOztJQW1GcEMsQ0FBQztJQWxGRyw4QkFBTSxHQUFOO1FBQUEsaUJBK0NDO1FBOUNHLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3RCLElBQUksZ0JBQU0sQ0FBQyxrQkFBa0IsSUFBSSxnQkFBTSxDQUFDLGtCQUFrQixFQUFFO2dCQUN4RCxJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDcFEsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BRLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUVwUSxJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqTCxJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqTCxJQUFJLE1BQU0sR0FBRyxVQUFVLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUM5QyxNQUFNLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDMUMsSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN6TCxJQUFJLGNBQWMsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNwRCxjQUFjLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDMUQsSUFBSSxjQUFjLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDcEQsY0FBYyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzFELElBQUksUUFBUSxHQUFHLENBQUMsQ0FBQztnQkFDakIsY0FBYyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzFDLGNBQWMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUMxQyxJQUFJLGNBQWMsR0FBRyxjQUFjLEVBQUU7b0JBQ2pDLFFBQVEsR0FBRyxjQUFjLENBQUM7aUJBQzdCO3FCQUNJO29CQUNELFFBQVEsR0FBRyxjQUFjLENBQUM7aUJBQzdCO2dCQUNELElBQUksUUFBUSxHQUFHLGlCQUFPLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDaE4sSUFBSSxXQUFXLEdBQUcsaUJBQU8sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSwwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDek0sSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHLE1BQU0sRUFBRTtvQkFDN0IsSUFBSSxRQUFRLEdBQUcsV0FBVyxFQUFFO3dCQUN4QixJQUFJLGdCQUFNLENBQUMsaUJBQWlCLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7NEJBQ3JELElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQzs0QkFDakIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7NEJBQzNCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7NEJBQzlCLG1DQUFtQzs0QkFDbkMsb0NBQW9DO3lCQUN2QztxQkFDSjtpQkFDSjtnQkFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtvQkFDdEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7b0JBQzNCLElBQUksQ0FBQyxZQUFZLENBQUM7d0JBQ2QsZ0JBQU0sQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7d0JBQ2xDLEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO29CQUNoQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7aUJBQ2I7YUFDSjtTQUNKO0lBQ0wsQ0FBQztJQUNELDBDQUFrQixHQUFsQjtRQUNJLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1FBQzNCLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBQ0QsaUNBQVMsR0FBVDtRQUFBLGlCQWlCQztRQWhCRyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDNUQsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxjQUFjLEVBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDekQsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNkLCtCQUErQjtZQUMvQixJQUFJO1lBQ0osMkRBQTJEO1lBQzNELElBQUk7WUFDSixLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNwRCxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNwRCxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNwRCxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUN4RCxDQUFDLEVBQUUsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUN4QixJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDeEQsQ0FBQyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBQ0Qsb0NBQVksR0FBWixVQUFhLENBQVMsRUFBRSxDQUFTO1FBQzdCLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO1FBQ3ZDLEtBQUssQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDdEIsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUN0QixLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNaLElBQUksQ0FBQyxHQUFHLElBQUksRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsR0FBRyxDQUFDLEVBQUUsQ0FBQzthQUMvRCxJQUFJLENBQUM7WUFDRixLQUFLLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3hELENBQUMsQ0FBQzthQUNELE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBdEZEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7bURBQ087SUFGVixhQUFhO1FBRGpDLE9BQU87T0FDYSxhQUFhLENBeUZqQztJQUFELG9CQUFDO0NBekZELEFBeUZDLENBekYwQyxFQUFFLENBQUMsU0FBUyxHQXlGdEQ7a0JBekZvQixhQUFhIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEdhbWVQbGF5SW5zdGFuY2UgZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFV0aWxpdHkgZnJvbSBcIi4uL0NvbW1vbi9VdGlsaXR5XCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQm94Q29udHJvbGxlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgYnJpY2tIb206IGNjLlByZWZhYiA9IG51bGw7XHJcbiAgICB0aW1lQnJva2VuOiBudW1iZXIgPSAxLjA2O1xyXG4gICAgYm9vbENoZWNrRGVhdGhPbmU6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGJvb2xDaGVja0RlYXRoOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBib29sQ2hlY2tkZWxheTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgdXBkYXRlKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5ib29sQ2hlY2tEZWF0aCkge1xyXG4gICAgICAgICAgICBpZiAoR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZyAmJiBHbG9iYWwuYm9vbENoZWNrQXR0YWNraW5nKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgcG9zMSA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuY2hpbGRyZW5bMF0uY29udmVydFRvV29ybGRTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuY2hpbGRyZW5bMF0uY2hpbGRyZW5bMF0uZ2V0UG9zaXRpb24oKSkpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHBvczIgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmNoaWxkcmVuWzBdLmNvbnZlcnRUb1dvcmxkU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmNoaWxkcmVuWzBdLmNoaWxkcmVuWzFdLmdldFBvc2l0aW9uKCkpKTtcclxuICAgICAgICAgICAgICAgIGxldCBwb3MzID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGUuY29udmVydFRvTm9kZVNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci5jaGlsZHJlblswXS5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci5jaGlsZHJlblswXS5jaGlsZHJlblsyXS5nZXRQb3NpdGlvbigpKSk7XHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IGRpcmVjdGlvbjEgPSBjYy52Mihwb3MxLnggLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLngsIHBvczEueSAtIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGlyZWN0aW9uMiA9IGNjLnYyKHBvczIueCAtIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueCwgcG9zMi55IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci55KTtcclxuICAgICAgICAgICAgICAgIGxldCBkZWdyZWUgPSBkaXJlY3Rpb24xLnNpZ25BbmdsZShkaXJlY3Rpb24yKTtcclxuICAgICAgICAgICAgICAgIGRlZ3JlZSA9IGNjLm1pc2MucmFkaWFuc1RvRGVncmVlcyhkZWdyZWUpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHBvc0VuZW15ID0gY2MudjIodGhpcy5ub2RlLnggLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLngsIHRoaXMubm9kZS55IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci55KTtcclxuICAgICAgICAgICAgICAgIGxldCBkZWdyZWVXaXRoUG9zMSA9IHBvc0VuZW15LnNpZ25BbmdsZShkaXJlY3Rpb24xKTtcclxuICAgICAgICAgICAgICAgIGRlZ3JlZVdpdGhQb3MxID0gY2MubWlzYy5yYWRpYW5zVG9EZWdyZWVzKGRlZ3JlZVdpdGhQb3MxKTtcclxuICAgICAgICAgICAgICAgIGxldCBkZWdyZWVXaXRoUG9zMiA9IHBvc0VuZW15LnNpZ25BbmdsZShkaXJlY3Rpb24yKTtcclxuICAgICAgICAgICAgICAgIGRlZ3JlZVdpdGhQb3MyID0gY2MubWlzYy5yYWRpYW5zVG9EZWdyZWVzKGRlZ3JlZVdpdGhQb3MyKTtcclxuICAgICAgICAgICAgICAgIGxldCByZWFsTmVlZCA9IDA7XHJcbiAgICAgICAgICAgICAgICBkZWdyZWVXaXRoUG9zMSA9IE1hdGguYWJzKGRlZ3JlZVdpdGhQb3MxKTtcclxuICAgICAgICAgICAgICAgIGRlZ3JlZVdpdGhQb3MyID0gTWF0aC5hYnMoZGVncmVlV2l0aFBvczIpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGRlZ3JlZVdpdGhQb3MxID4gZGVncmVlV2l0aFBvczIpIHtcclxuICAgICAgICAgICAgICAgICAgICByZWFsTmVlZCA9IGRlZ3JlZVdpdGhQb3MxO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVhbE5lZWQgPSBkZWdyZWVXaXRoUG9zMjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGxldCBkaXN0YW5jZSA9IFV0aWxpdHkuRGlzdGFuY2UoY2MudjIodGhpcy5ub2RlLngsIHRoaXMubm9kZS55KSwgY2MudjIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci54LCBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnkpKTtcclxuICAgICAgICAgICAgICAgIGxldCBtYXhEaXN0YW5jZSA9IFV0aWxpdHkuRGlzdGFuY2UoY2MudjIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci54LCBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnkpLCBjYy52Mihwb3MzLngsIHBvczMueSkpO1xyXG4gICAgICAgICAgICAgICAgaWYgKE1hdGguYWJzKHJlYWxOZWVkKSA8IGRlZ3JlZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChkaXN0YW5jZSA8IG1heERpc3RhbmNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChHbG9iYWwuYm9vbENoZWNrQXR0YWNrZWQgJiYgIXRoaXMuYm9vbENoZWNrRGVhdGhPbmUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuQnJva2VuQm94KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmJvb2xDaGVja0RlYXRoID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYm9vbENoZWNrRGVhdGhPbmUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9HbG9iYWwuYm9vbENoZWNrQXR0YWNrZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vR2xvYmFsLmJvb2xDaGVja0F0dGFja2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLmJvb2xDaGVja2RlbGF5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ib29sQ2hlY2tkZWxheSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBHbG9iYWwuYm9vbENoZWNrQXR0YWNraW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYm9vbENoZWNrZGVsYXkgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB9LCAwLjAwMyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBEaWVXaGVuRW5lbXlBdHRhY2soKSB7XHJcbiAgICAgICAgdGhpcy5ib29sQ2hlY2tEZWF0aCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5Ccm9rZW5Cb3goKTtcclxuICAgIH1cclxuICAgIEJyb2tlbkJveCgpIHtcclxuICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiQnJva2VuXCIpO1xyXG4gICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kQm94QnJva2VuLGZhbHNlKTtcclxuICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLkJveENvbGxpZGVyM0QpLmVuYWJsZWQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIGZvciggbGV0IGkgPSAwOyBpIDwgMjA7IGkrKylcclxuICAgICAgICAgICAgLy8ge1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5TcGF3bmVyQnJpY2sodGhpcy5ub2RlLnggLSAyLCB0aGlzLm5vZGUueSArIDIpO1xyXG4gICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgIHRoaXMuU3Bhd25lckJyaWNrKHRoaXMubm9kZS54IC0gMiwgdGhpcy5ub2RlLnkgKyAyKTtcclxuICAgICAgICAgICAgdGhpcy5TcGF3bmVyQnJpY2sodGhpcy5ub2RlLnggKyAyLCB0aGlzLm5vZGUueSArIDIpO1xyXG4gICAgICAgICAgICB0aGlzLlNwYXduZXJCcmljayh0aGlzLm5vZGUueCAtIDIsIHRoaXMubm9kZS55IC0gMik7XHJcbiAgICAgICAgICAgIHRoaXMuU3Bhd25lckJyaWNrKHRoaXMubm9kZS54ICsgMiwgdGhpcy5ub2RlLnkgLSAyKTtcclxuICAgICAgICB9LCB0aGlzLnRpbWVCcm9rZW4gLyA3KTtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnN0b3AoKTtcclxuICAgICAgICB9LCB0aGlzLnRpbWVCcm9rZW4pO1xyXG4gICAgfVxyXG4gICAgU3Bhd25lckJyaWNrKHg6IG51bWJlciwgeTogbnVtYmVyKSB7XHJcbiAgICAgICAgbGV0IEJyaWNrID0gY2MuaW5zdGFudGlhdGUodGhpcy5icmlja0hvbSk7XHJcbiAgICAgICAgQnJpY2sucGFyZW50ID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGU7XHJcbiAgICAgICAgQnJpY2sueCA9IHRoaXMubm9kZS54O1xyXG4gICAgICAgIEJyaWNrLnkgPSB0aGlzLm5vZGUueTtcclxuICAgICAgICBCcmljay56ID0gMTtcclxuICAgICAgICBsZXQgdCA9IG5ldyBjYy5Ud2VlbigpLnRvKDAuMywgeyBwb3NpdGlvbjogbmV3IGNjLlZlYzMoeCwgeSwgMC41KSB9KVxyXG4gICAgICAgICAgICAuY2FsbCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBCcmljay5nZXRDb21wb25lbnQoY2MuQm94Q29sbGlkZXIzRCkuZW5hYmxlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC50YXJnZXQoQnJpY2spLnN0YXJ0KCk7XHJcbiAgICB9XHJcbn1cclxuIl19