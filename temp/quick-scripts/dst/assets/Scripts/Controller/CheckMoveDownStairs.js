
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Controller/CheckMoveDownStairs.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9b8d8xvinNMPYvAQa26/D07', 'CheckMoveDownStairs');
// Scripts/Controller/CheckMoveDownStairs.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CheckMoveDownStairs = /** @class */ (function (_super) {
    __extends(CheckMoveDownStairs, _super);
    function CheckMoveDownStairs() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.boolMoveDown = false;
        return _this;
    }
    CheckMoveDownStairs.prototype.start = function () {
        var collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-stay', this.onTriggerStay, this);
        collider.on('trigger-exit', this.onTriggerExit, this);
    };
    CheckMoveDownStairs.prototype.onTriggerStay = function (event) {
        if (event.otherCollider.node.group == "Model") {
            if (event.otherCollider.node.name == "CheckPlayerMoveBack") {
                this.boolMoveDown = true;
            }
        }
    };
    CheckMoveDownStairs.prototype.onTriggerExit = function (event) {
        this.boolMoveDown = false;
    };
    CheckMoveDownStairs = __decorate([
        ccclass
    ], CheckMoveDownStairs);
    return CheckMoveDownStairs;
}(cc.Component));
exports.default = CheckMoveDownStairs;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29udHJvbGxlclxcQ2hlY2tNb3ZlRG93blN0YWlycy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBTSxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRzVDO0lBQWlELHVDQUFZO0lBRDdEO1FBQUEscUVBa0JDO1FBaEJHLGtCQUFZLEdBQVksS0FBSyxDQUFDOztJQWdCbEMsQ0FBQztJQWZHLG1DQUFLLEdBQUw7UUFDSSxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNoRCxRQUFRLENBQUMsRUFBRSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3RELFFBQVEsQ0FBQyxFQUFFLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUNELDJDQUFhLEdBQWIsVUFBYyxLQUFLO1FBQ2YsSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksT0FBTyxFQUFFO1lBQzNDLElBQUksS0FBSyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLHFCQUFxQixFQUFFO2dCQUN4RCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQzthQUM1QjtTQUNKO0lBQ0wsQ0FBQztJQUNELDJDQUFhLEdBQWIsVUFBYyxLQUFLO1FBQ2YsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7SUFDOUIsQ0FBQztJQWhCZ0IsbUJBQW1CO1FBRHZDLE9BQU87T0FDYSxtQkFBbUIsQ0FpQnZDO0lBQUQsMEJBQUM7Q0FqQkQsQUFpQkMsQ0FqQmdELEVBQUUsQ0FBQyxTQUFTLEdBaUI1RDtrQkFqQm9CLG1CQUFtQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDaGVja01vdmVEb3duU3RhaXJzIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICAgIGJvb2xNb3ZlRG93bjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgbGV0IGNvbGxpZGVyID0gdGhpcy5nZXRDb21wb25lbnQoY2MuQ29sbGlkZXIzRCk7XHJcbiAgICAgICAgY29sbGlkZXIub24oJ3RyaWdnZXItc3RheScsIHRoaXMub25UcmlnZ2VyU3RheSwgdGhpcyk7XHJcbiAgICAgICAgY29sbGlkZXIub24oJ3RyaWdnZXItZXhpdCcsIHRoaXMub25UcmlnZ2VyRXhpdCwgdGhpcyk7XHJcbiAgICB9XHJcbiAgICBvblRyaWdnZXJTdGF5KGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50Lm90aGVyQ29sbGlkZXIubm9kZS5ncm91cCA9PSBcIk1vZGVsXCIpIHtcclxuICAgICAgICAgICAgaWYgKGV2ZW50Lm90aGVyQ29sbGlkZXIubm9kZS5uYW1lID09IFwiQ2hlY2tQbGF5ZXJNb3ZlQmFja1wiKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmJvb2xNb3ZlRG93biA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBvblRyaWdnZXJFeGl0KGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5ib29sTW92ZURvd24gPSBmYWxzZTtcclxuICAgIH1cclxufVxyXG4iXX0=