
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/SplineExtend.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b3890qhPopIIpBUBGAYxovj', 'SplineExtend');
// Scripts/Common/SplineExtend.js

"use strict";

/****************************************************************************
 Copyright (c) 2008 Radu Gruian
 Copyright (c) 2008-2010 Ricardo Quesada
 Copyright (c) 2011 Vit Valentin
 Copyright (c) 2011-2012 cocos2d-x.org
 Copyright (c) 2013-2016 Chukong Technologies Inc.
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 http://www.cocos2d-x.org
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 Orignal code by Radu Gruian: http://www.codeproject.com/Articles/30838/Overhauser-Catmull-Rom-Splines-for-Camera-Animatio.So
 Adapted to cocos2d-x by Vit Valentin
 Adapted from cocos2d-x to cocos2d-iphone by Ricardo Quesada
 ****************************************************************************/

/**
 * @module cc
 */

/*
 * Returns the Cardinal Spline position for a given set of control points, tension and time. <br />
 * CatmullRom Spline formula. <br />
 * s(-ttt + 2tt - t)P1 + s(-ttt + tt)P2 + (2ttt - 3tt + 1)P2 + s(ttt - 2tt + t)P3 + (-2ttt + 3tt)P3 + s(ttt - tt)P4
 *
 * @method cardinalSplineAt
 * @param {Vec2} p0
 * @param {Vec2} p1
 * @param {Vec2} p2
 * @param {Vec2} p3
 * @param {Number} tension
 * @param {Number} t
 * @return {Vec2}
 */
function cardinalSplineAt(p0, p1, p2, p3, tension, t) {
  var t2 = t * t;
  var t3 = t2 * t;
  /*
   * Formula: s(-ttt + 2tt - t)P1 + s(-ttt + tt)P2 + (2ttt - 3tt + 1)P2 + s(ttt - 2tt + t)P3 + (-2ttt + 3tt)P3 + s(ttt - tt)P4
   */

  var s = (1 - tension) / 2;
  var b1 = s * (-t3 + 2 * t2 - t); // s(-t3 + 2 t2 - t)P1

  var b2 = s * (-t3 + t2) + (2 * t3 - 3 * t2 + 1); // s(-t3 + t2)P2 + (2 t3 - 3 t2 + 1)P2

  var b3 = s * (t3 - 2 * t2 + t) + (-2 * t3 + 3 * t2); // s(t3 - 2 t2 + t)P3 + (-2 t3 + 3 t2)P3

  var b4 = s * (t3 - t2); // s(t3 - t2)P4

  var x = p0.x * b1 + p1.x * b2 + p2.x * b3 + p3.x * b4;
  var y = p0.y * b1 + p1.y * b2 + p2.y * b3 + p3.y * b4;
  return cc.v2(x, y);
}

;
/*
 * returns a point from the array
 * @method getControlPointAt
 * @param {Array} controlPoints
 * @param {Number} pos
 * @return {Array}
 */

function getControlPointAt(controlPoints, pos) {
  var p = Math.min(controlPoints.length - 1, Math.max(pos, 0));
  return controlPoints[p];
}

;

function reverseControlPoints(controlPoints) {
  var newArray = [];

  for (var i = controlPoints.length - 1; i >= 0; i--) {
    newArray.push(cc.v2(controlPoints[i].x, controlPoints[i].y));
  }

  return newArray;
}

function cloneControlPoints(controlPoints) {
  var newArray = [];

  for (var i = 0; i < controlPoints.length; i++) {
    newArray.push(cc.v2(controlPoints[i].x, controlPoints[i].y));
  }

  return newArray;
}
/**
 * TODO tối ưu: Thực hiện pre interpolation và precalculate length cho các Spline Action giống nhau
 */

/*
 * Cardinal Spline path. http://en.wikipedia.org/wiki/Cubic_Hermite_spline#Cardinal_spline
 * Absolute coordinates.
 *
 * @class CardinalSplineTo2
 * @extends ActionInterval
 *
 * @param {Number} duration
 * @param {Array} points array of control points
 * @param {Number} tension
 *
 * @example
 * //create a cc.CardinalSplineTo2
 * var action1 = cc.CardinalSplineTo2(3, array, 0);
 * 
 */


cc.CardinalSplineTo2 = cc.Class({
  name: 'cc.CardinalSplineTo2',
  "extends": cc.ActionInterval,
  cacheLengths: null,
  ctor: function ctor(duration, points, angleOffset, tension, speed) {
    /* Array of control points */
    this._points = [];
    this._cacheLengths = [];
    this._totalLength = 0;
    this._deltaT = 0;
    this._tension = 0;
    this._angleOffset = 0;
    this._previousPosition = null;
    this._accumulatedDiff = null;
    tension !== undefined && cc.CardinalSplineTo2.prototype.initWithDuration.call(this, duration, points, angleOffset, tension, speed);
  },
  initWithDuration: function initWithDuration(duration, points, angleOffset, tension, speed) {
    if (!points || points.length === 0) {
      cc.errorID(1024);
      return false;
    }

    this._tension = tension;
    this._angleOffset = angleOffset;
    this.setPoints(points);
    if (speed) duration = this.getLength() / speed;

    if (cc.ActionInterval.prototype.initWithDuration.call(this, duration)) {
      return true;
    }

    return false;
  },
  clone: function clone() {
    var action = new cc.CardinalSplineTo2();
    action.initWithDuration(this._duration, cloneControlPoints(this._points), this._tension);
    return action;
  },
  startWithTarget: function startWithTarget(target) {
    cc.ActionInterval.prototype.startWithTarget.call(this, target); // Issue #1441 from cocos2d-iphone

    this._deltaT = 1 / (this._points.length - 1);
    this._previousPosition = cc.v2(this.target.x, this.target.y);
    this._accumulatedDiff = cc.v2(0, 0);
  },
  getP: function getP(dt) {
    for (var i = 0; i < this._cummutilate.length; i++) {
      if (this._cummutilate[i] > dt) return i;
    }

    return this._cummutilate.length;
  },
  getLt: function getLt(p, dt) {
    if (p == 0) {
      return dt / this._cacheLengths[p];
    } else {
      return (dt - this._cummutilate[p - 1]) / this._cacheLengths[p];
    }
  },
  update: function update(dt) {
    dt = this._computeEaseTime(dt);
    var p, lt;
    var ps = this._points; // eg.
    // p..p..p..p..p..p..p
    // 1..2..3..4..5..6..7
    // want p to be 1, 2, 3, 4, 5, 6

    if (dt === 1) {
      p = ps.length - 1;
      lt = 1;
    } else {
      var locDT = this._deltaT; //p = 0 | (dt / locDT);
      //lt = (dt - locDT * p) / locDT;

      p = this.getP(dt);
      lt = this.getLt(p, dt); // console.log(lt);
    }

    var newPos = cardinalSplineAt(getControlPointAt(ps, p - 1), getControlPointAt(ps, p - 0), getControlPointAt(ps, p + 1), getControlPointAt(ps, p + 2), this._tension, lt);

    if (cc.macro.ENABLE_STACKABLE_ACTIONS) {
      var tempX, tempY;
      tempX = this.target.x - this._previousPosition.x;
      tempY = this.target.y - this._previousPosition.y;

      if (tempX !== 0 || tempY !== 0) {
        var locAccDiff = this._accumulatedDiff;
        tempX = locAccDiff.x + tempX;
        tempY = locAccDiff.y + tempY;
        locAccDiff.x = tempX;
        locAccDiff.y = tempY;
        newPos.x += tempX;
        newPos.y += tempY;
      }
    }

    this.updatePosition(newPos);
  },
  reverse: function reverse() {
    var reversePoints = reverseControlPoints(this._points);
    return cc.CardinalSplineTo2(this._duration, reversePoints, this._tension);
  },

  /*
   * update position of target
   * @method updatePosition
   * @param {Vec2} newPos
   */
  updatePosition: function updatePosition(newPos) {
    var deltaX = newPos.x - this._previousPosition.x;
    var deltaY = newPos.y - this._previousPosition.y;

    if (!this._disableRotate) {
      if (deltaX !== 0 && deltaY !== 0) this.target.angle = Math.atan2(deltaY, deltaX) * 180 / Math.PI + this._angleOffset;
    }

    this.target.setPosition(newPos);
    this._previousPosition = newPos;
  },

  /*
   * Points getter
   * @method getPoints
   * @return {Array}
   */
  getPoints: function getPoints() {
    return this._points;
  },

  /**
   * Points setter
   * @method setPoints
   * @param {Array} points
   */
  setPoints: function setPoints(points) {
    points = this._interpolatatePoints(points, 10);

    this._setPoint(points);
  },
  _setPoint: function _setPoint(points) {
    this._points = points;
    this._cacheLengths = [];
    this._cummutilate = [];

    for (var i = 0; i < points.length - 1; i++) {
      var p1 = points[i];
      var p2 = points[i + 1];
      var length = Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));

      this._cacheLengths.push(length);
    }

    this._totalLength = 0;

    for (var i = 0; i < this._cacheLengths.length; i++) {
      this._totalLength += this._cacheLengths[i];

      this._cummutilate.push(this._totalLength);
    }

    for (var i = 0; i < this._cacheLengths.length; i++) {
      this._cacheLengths[i] = this._cacheLengths[i] / this._totalLength;
      this._cummutilate[i] = this._cummutilate[i] / this._totalLength;
    }
  },
  _setPoint2: function _setPoint2(points) {
    this._points = points;
    this._cacheLengths = [];
    this._cummutilate = [];

    for (var i = 0; i < points.length - 1; i++) {
      var p1 = points[i];
      var p2 = points[i + 1];
      var length = 1;

      this._cacheLengths.push(length);
    }

    this._totalLength = 0;

    for (var i = 0; i < this._cacheLengths.length; i++) {
      this._totalLength += this._cacheLengths[i];

      this._cummutilate.push(this._totalLength);
    }

    for (var i = 0; i < this._cacheLengths.length; i++) {
      this._cacheLengths[i] = this._cacheLengths[i] / this._totalLength;
      this._cummutilate[i] = this._cummutilate[i] / this._totalLength;
    }
  },
  _interpolatatePoints: function _interpolatatePoints(points, d) {
    this._setPoint(points);

    var n = (points.length - 1) * d;
    var newPoints = [];
    var p = 0;
    var lt = 0;
    var ps = points;

    for (var i = 0; i < n; i++) {
      var dt = i * 1 / (n - 1);

      if (dt === 1) {
        p = ps.length - 1;
        lt = 1;
      } else {
        var locDT = 1 / (points.length - 1); //p = 0 | (dt / locDT);
        //lt = (dt - locDT * p) / locDT;

        p = this.getP(dt);
        lt = this.getLt(p, dt);
      }

      var newPos = cardinalSplineAt(getControlPointAt(ps, p - 1), getControlPointAt(ps, p - 0), getControlPointAt(ps, p + 1), getControlPointAt(ps, p + 2), this._tension, lt);
      newPoints.push(newPos);
    }

    return newPoints;
  },
  getLength: function getLength() {
    return this._totalLength;
  },
  disableRotate: function disableRotate() {
    this._disableRotate = true;
  }
});
/**
 * !#en Creates an action with a Cardinal Spline array of points and tension.
 * !#zh 按基数样条曲线轨迹移动到目标位置。
 * @method CardinalSplineTo2
 * @param {Number} duration
 * @param {Array} points array of control points
 * @param {Number} angleOffset
 * @param {Number} tension
 * @return {ActionInterval}
 *
 * @example
 * //create a cc.CardinalSplineTo2
 * var action1 = cc.CardinalSplineTo2(3, array, 0);
 */

cc.cardinalSplineTo2 = function (duration, points, angleOffset, tension) {
  return new cc.CardinalSplineTo2(duration, points, angleOffset, tension);
};
/**
 * !#en Creates an action with a Cardinal Spline array of points and tension.
 * !#zh 按基数样条曲线轨迹移动到目标位置。
 * @method CardinalSplineTo2
 * @param {Number} speed
 * @param {Array} points array of control points
 * @param {Number} angleOffset
 * @param {Number} tension
 * @return {ActionInterval}
 *
 * @example
 * //create a cc.CardinalSplineTo2
 * var action1 = cc.CardinalSplineTo2(3, array, 0);
 */


cc.cardinalSplineTo2_speed = function (speed, points, angleOffset, tension) {
  return new cc.CardinalSplineTo2(1, points, angleOffset, tension, speed);
};
/*
 * Cardinal Spline path. http://en.wikipedia.org/wiki/Cubic_Hermite_spline#Cardinal_spline
 * Relative coordinates.
 *
 * @class CardinalSplineBy2
 * @extends CardinalSplineTo2
 *
 * @param {Number} duration
 * @param {Array} points
 * @param {Number} tension
 *
 * @example
 * //create a cc.CardinalSplineBy2
 * var action1 = cc.CardinalSplineBy2(3, array, 0);
 */


cc.CardinalSplineBy2 = cc.Class({
  name: 'cc.CardinalSplineBy2',
  "extends": cc.CardinalSplineTo2,
  ctor: function ctor(duration, points, angleOffset, tension) {
    this._startPosition = cc.v2(0, 0);
    tension !== undefined && this.initWithDuration(duration, points, angleOffset, tension);
  },
  startWithTarget: function startWithTarget(target) {
    cc.CardinalSplineTo2.prototype.startWithTarget.call(this, target);
    this._startPosition.x = target.x;
    this._startPosition.y = target.y;
  },
  reverse: function reverse() {
    var copyConfig = this._points.slice();

    var current; //
    // convert "absolutes" to "diffs"
    //

    var p = copyConfig[0];

    for (var i = 1; i < copyConfig.length; ++i) {
      current = copyConfig[i];
      copyConfig[i] = current.sub(p);
      p = current;
    } // convert to "diffs" to "reverse absolute"


    var reverseArray = reverseControlPoints(copyConfig); // 1st element (which should be 0,0) should be here too

    p = reverseArray[reverseArray.length - 1];
    reverseArray.pop();
    p.x = -p.x;
    p.y = -p.y;
    reverseArray.unshift(p);

    for (var i = 1; i < reverseArray.length; ++i) {
      current = reverseArray[i];
      current.x = -current.x;
      current.y = -current.y;
      current.x += p.x;
      current.y += p.y;
      reverseArray[i] = current;
      p = current;
    }

    return cc.CardinalSplineBy2(this._duration, reverseArray, this._angleOffset, this._tension);
  },

  /**
   * update position of target
   * @method updatePosition
   * @param {Vec2} newPos
   */
  updatePosition: function updatePosition(newPos) {
    var pos = this._startPosition;
    var posX = newPos.x + pos.x;
    var posY = newPos.y + pos.y;
    var deltaX = posX - this._previousPosition.x;
    var deltaY = posY - this._previousPosition.y;
    if (deltaX !== 0 && deltaY !== 0) this.target.angle = Math.atan2(deltaY, deltaX) * 180 / Math.PI + this._angleOffset;
    this._previousPosition.x = posX;
    this._previousPosition.y = posY;
    this.target.setPosition(posX, posY);
  },
  clone: function clone() {
    var a = new cc.CardinalSplineBy2();
    a.initWithDuration(this._duration, cloneControlPoints(this._points), this._angleOffset, this._tension);
    return a;
  }
});
/**
 * !#en Creates an action with a Cardinal Spline array of points and tension.
 * !#zh 按基数样条曲线轨迹移动指定的距离。
 * @method CardinalSplineBy2
 * @param {Number} duration
 * @param {Array} points
 * @param {Number} angleOffset
 * @param {Number} tension
 *
 * @return {ActionInterval}
 */

cc.cardinalSplineBy2 = function (duration, points, angleOffset, tension) {
  return new cc.CardinalSplineBy2(duration, points, angleOffset, tension);
};
/*
 * An action that moves the target with a CatmullRom curve to a destination point.<br/>
 * A Catmull Rom is a Cardinal Spline with a tension of 0.5.  <br/>
 * http://en.wikipedia.org/wiki/Cubic_Hermite_spline#Catmull.E2.80.93Rom_spline
 * Absolute coordinates.
 *
 * @class CatmullRomTo2
 * @extends CardinalSplineTo2
 *
 * @param {Number} dt
 * @param {Array} points
 *
 * @example
 * var action1 = cc.CatmullRomTo2(3, array);
 */


cc.CatmullRomTo2 = cc.Class({
  name: 'cc.CatmullRomTo2',
  "extends": cc.CardinalSplineTo2,
  ctor: function ctor(dt, points, angleOffset) {
    if (angleOffset === undefined) angleOffset = 0;
    points && this.initWithDuration(dt, points, angleOffset);
  },
  initWithDuration: function initWithDuration(dt, points, angleOffset) {
    return cc.CardinalSplineTo2.prototype.initWithDuration.call(this, dt, points, angleOffset, 0.5);
  },
  clone: function clone() {
    var action = new cc.CatmullRomTo2();
    action.initWithDuration(this._duration, cloneControlPoints(this._points, this._angleOffset));
    return action;
  }
});
/**
 * !#en Creates an action with a Cardinal Spline array of points and tension.
 * !#zh 按 Catmull Rom 样条曲线轨迹移动到目标位置。
 * @method CatmullRomTo2
 * @param {Number} dt
 * @param {Array} points
 * @param {Number} angleOffset
 * @return {ActionInterval}
 *
 * @example
 * var action1 = cc.CatmullRomTo2(3, array);
 */

cc.catmullRomTo2 = function (dt, points, angleOffset) {
  return new cc.CatmullRomTo2(dt, points, angleOffset);
};
/*
 * An action that moves the target with a CatmullRom curve by a certain distance.  <br/>
 * A Catmull Rom is a Cardinal Spline with a tension of 0.5.<br/>
 * http://en.wikipedia.org/wiki/Cubic_Hermite_spline#Catmull.E2.80.93Rom_spline
 * Relative coordinates.
 *
 * @class CatmullRomBy2
 * @extends CardinalSplineBy2
 *
 * @param {Number} dt
 * @param {Array} points
 *
 * @example
 * var action1 = cc.CatmullRomBy2(3, array);
 */


cc.CatmullRomBy2 = cc.Class({
  name: 'cc.CatmullRomBy2',
  "extends": cc.CardinalSplineBy2,
  ctor: function ctor(dt, points, angleOffset) {
    points && this.initWithDuration(dt, points, angleOffset);
  },
  initWithDuration: function initWithDuration(dt, points, angleOffset) {
    return cc.CardinalSplineTo2.prototype.initWithDuration.call(this, dt, points, angleOffset, 0.5);
  },
  clone: function clone() {
    var action = new cc.CatmullRomBy2();
    action.initWithDuration(this._duration, cloneControlPoints(this._points), this._angleOffset);
    return action;
  }
});
/**
 * !#en Creates an action with a Cardinal Spline array of points and tension.
 * !#zh 按 Catmull Rom 样条曲线轨迹移动指定的距离。
 * @method CatmullRomBy2
 * @param {Number} dt
 * @param {Array} points
 * @param {Number} angleOffset
 * @return {ActionInterval}
 * @example
 * var action1 = cc.CatmullRomBy2(3, array, 90);
 */

cc.catmullRomBy2 = function (dt, points, angleOffset) {
  return new cc.CatmullRomBy2(dt, points, angleOffset);
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxTcGxpbmVFeHRlbmQuanMiXSwibmFtZXMiOlsiY2FyZGluYWxTcGxpbmVBdCIsInAwIiwicDEiLCJwMiIsInAzIiwidGVuc2lvbiIsInQiLCJ0MiIsInQzIiwicyIsImIxIiwiYjIiLCJiMyIsImI0IiwieCIsInkiLCJjYyIsInYyIiwiZ2V0Q29udHJvbFBvaW50QXQiLCJjb250cm9sUG9pbnRzIiwicG9zIiwicCIsIk1hdGgiLCJtaW4iLCJsZW5ndGgiLCJtYXgiLCJyZXZlcnNlQ29udHJvbFBvaW50cyIsIm5ld0FycmF5IiwiaSIsInB1c2giLCJjbG9uZUNvbnRyb2xQb2ludHMiLCJDYXJkaW5hbFNwbGluZVRvMiIsIkNsYXNzIiwibmFtZSIsIkFjdGlvbkludGVydmFsIiwiY2FjaGVMZW5ndGhzIiwiY3RvciIsImR1cmF0aW9uIiwicG9pbnRzIiwiYW5nbGVPZmZzZXQiLCJzcGVlZCIsIl9wb2ludHMiLCJfY2FjaGVMZW5ndGhzIiwiX3RvdGFsTGVuZ3RoIiwiX2RlbHRhVCIsIl90ZW5zaW9uIiwiX2FuZ2xlT2Zmc2V0IiwiX3ByZXZpb3VzUG9zaXRpb24iLCJfYWNjdW11bGF0ZWREaWZmIiwidW5kZWZpbmVkIiwicHJvdG90eXBlIiwiaW5pdFdpdGhEdXJhdGlvbiIsImNhbGwiLCJlcnJvcklEIiwic2V0UG9pbnRzIiwiZ2V0TGVuZ3RoIiwiY2xvbmUiLCJhY3Rpb24iLCJfZHVyYXRpb24iLCJzdGFydFdpdGhUYXJnZXQiLCJ0YXJnZXQiLCJnZXRQIiwiZHQiLCJfY3VtbXV0aWxhdGUiLCJnZXRMdCIsInVwZGF0ZSIsIl9jb21wdXRlRWFzZVRpbWUiLCJsdCIsInBzIiwibG9jRFQiLCJuZXdQb3MiLCJtYWNybyIsIkVOQUJMRV9TVEFDS0FCTEVfQUNUSU9OUyIsInRlbXBYIiwidGVtcFkiLCJsb2NBY2NEaWZmIiwidXBkYXRlUG9zaXRpb24iLCJyZXZlcnNlIiwicmV2ZXJzZVBvaW50cyIsImRlbHRhWCIsImRlbHRhWSIsIl9kaXNhYmxlUm90YXRlIiwiYW5nbGUiLCJhdGFuMiIsIlBJIiwic2V0UG9zaXRpb24iLCJnZXRQb2ludHMiLCJfaW50ZXJwb2xhdGF0ZVBvaW50cyIsIl9zZXRQb2ludCIsInNxcnQiLCJfc2V0UG9pbnQyIiwiZCIsIm4iLCJuZXdQb2ludHMiLCJkaXNhYmxlUm90YXRlIiwiY2FyZGluYWxTcGxpbmVUbzIiLCJjYXJkaW5hbFNwbGluZVRvMl9zcGVlZCIsIkNhcmRpbmFsU3BsaW5lQnkyIiwiX3N0YXJ0UG9zaXRpb24iLCJjb3B5Q29uZmlnIiwic2xpY2UiLCJjdXJyZW50Iiwic3ViIiwicmV2ZXJzZUFycmF5IiwicG9wIiwidW5zaGlmdCIsInBvc1giLCJwb3NZIiwiYSIsImNhcmRpbmFsU3BsaW5lQnkyIiwiQ2F0bXVsbFJvbVRvMiIsImNhdG11bGxSb21UbzIiLCJDYXRtdWxsUm9tQnkyIiwiY2F0bXVsbFJvbUJ5MiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTRCQTs7OztBQUlBOzs7Ozs7Ozs7Ozs7OztBQWNBLFNBQVNBLGdCQUFULENBQTJCQyxFQUEzQixFQUErQkMsRUFBL0IsRUFBbUNDLEVBQW5DLEVBQXVDQyxFQUF2QyxFQUEyQ0MsT0FBM0MsRUFBb0RDLENBQXBELEVBQXVEO0FBQ25ELE1BQUlDLEVBQUUsR0FBR0QsQ0FBQyxHQUFHQSxDQUFiO0FBQ0EsTUFBSUUsRUFBRSxHQUFHRCxFQUFFLEdBQUdELENBQWQ7QUFFQTs7OztBQUdBLE1BQUlHLENBQUMsR0FBRyxDQUFDLElBQUlKLE9BQUwsSUFBZ0IsQ0FBeEI7QUFFQSxNQUFJSyxFQUFFLEdBQUdELENBQUMsSUFBSyxDQUFDRCxFQUFELEdBQU8sSUFBSUQsRUFBWixHQUFtQkQsQ0FBdkIsQ0FBVixDQVRtRCxDQVNPOztBQUMxRCxNQUFJSyxFQUFFLEdBQUdGLENBQUMsSUFBSSxDQUFDRCxFQUFELEdBQU1ELEVBQVYsQ0FBRCxJQUFrQixJQUFJQyxFQUFKLEdBQVMsSUFBSUQsRUFBYixHQUFrQixDQUFwQyxDQUFULENBVm1ELENBVU87O0FBQzFELE1BQUlLLEVBQUUsR0FBR0gsQ0FBQyxJQUFJRCxFQUFFLEdBQUcsSUFBSUQsRUFBVCxHQUFjRCxDQUFsQixDQUFELElBQXlCLENBQUMsQ0FBRCxHQUFLRSxFQUFMLEdBQVUsSUFBSUQsRUFBdkMsQ0FBVCxDQVhtRCxDQVdPOztBQUMxRCxNQUFJTSxFQUFFLEdBQUdKLENBQUMsSUFBSUQsRUFBRSxHQUFHRCxFQUFULENBQVYsQ0FabUQsQ0FZTzs7QUFFMUQsTUFBSU8sQ0FBQyxHQUFJYixFQUFFLENBQUNhLENBQUgsR0FBT0osRUFBUCxHQUFZUixFQUFFLENBQUNZLENBQUgsR0FBT0gsRUFBbkIsR0FBd0JSLEVBQUUsQ0FBQ1csQ0FBSCxHQUFPRixFQUEvQixHQUFvQ1IsRUFBRSxDQUFDVSxDQUFILEdBQU9ELEVBQXBEO0FBQ0EsTUFBSUUsQ0FBQyxHQUFJZCxFQUFFLENBQUNjLENBQUgsR0FBT0wsRUFBUCxHQUFZUixFQUFFLENBQUNhLENBQUgsR0FBT0osRUFBbkIsR0FBd0JSLEVBQUUsQ0FBQ1ksQ0FBSCxHQUFPSCxFQUEvQixHQUFvQ1IsRUFBRSxDQUFDVyxDQUFILEdBQU9GLEVBQXBEO0FBQ0EsU0FBT0csRUFBRSxDQUFDQyxFQUFILENBQU1ILENBQU4sRUFBU0MsQ0FBVCxDQUFQO0FBQ0g7O0FBQUE7QUFFRDs7Ozs7Ozs7QUFPQSxTQUFTRyxpQkFBVCxDQUE0QkMsYUFBNUIsRUFBMkNDLEdBQTNDLEVBQWdEO0FBQzVDLE1BQUlDLENBQUMsR0FBR0MsSUFBSSxDQUFDQyxHQUFMLENBQVNKLGFBQWEsQ0FBQ0ssTUFBZCxHQUF1QixDQUFoQyxFQUFtQ0YsSUFBSSxDQUFDRyxHQUFMLENBQVNMLEdBQVQsRUFBYyxDQUFkLENBQW5DLENBQVI7QUFDQSxTQUFPRCxhQUFhLENBQUNFLENBQUQsQ0FBcEI7QUFDSDs7QUFBQTs7QUFFRCxTQUFTSyxvQkFBVCxDQUErQlAsYUFBL0IsRUFBOEM7QUFDMUMsTUFBSVEsUUFBUSxHQUFHLEVBQWY7O0FBQ0EsT0FBSyxJQUFJQyxDQUFDLEdBQUdULGFBQWEsQ0FBQ0ssTUFBZCxHQUF1QixDQUFwQyxFQUF1Q0ksQ0FBQyxJQUFJLENBQTVDLEVBQStDQSxDQUFDLEVBQWhELEVBQW9EO0FBQ2hERCxJQUFBQSxRQUFRLENBQUNFLElBQVQsQ0FBY2IsRUFBRSxDQUFDQyxFQUFILENBQU1FLGFBQWEsQ0FBQ1MsQ0FBRCxDQUFiLENBQWlCZCxDQUF2QixFQUEwQkssYUFBYSxDQUFDUyxDQUFELENBQWIsQ0FBaUJiLENBQTNDLENBQWQ7QUFDSDs7QUFDRCxTQUFPWSxRQUFQO0FBQ0g7O0FBRUQsU0FBU0csa0JBQVQsQ0FBNkJYLGFBQTdCLEVBQTRDO0FBQ3hDLE1BQUlRLFFBQVEsR0FBRyxFQUFmOztBQUNBLE9BQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR1QsYUFBYSxDQUFDSyxNQUFsQyxFQUEwQ0ksQ0FBQyxFQUEzQztBQUNJRCxJQUFBQSxRQUFRLENBQUNFLElBQVQsQ0FBY2IsRUFBRSxDQUFDQyxFQUFILENBQU1FLGFBQWEsQ0FBQ1MsQ0FBRCxDQUFiLENBQWlCZCxDQUF2QixFQUEwQkssYUFBYSxDQUFDUyxDQUFELENBQWIsQ0FBaUJiLENBQTNDLENBQWQ7QUFESjs7QUFFQSxTQUFPWSxRQUFQO0FBQ0g7QUFFRDs7OztBQUlBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkFYLEVBQUUsQ0FBQ2UsaUJBQUgsR0FBdUJmLEVBQUUsQ0FBQ2dCLEtBQUgsQ0FBUztBQUM1QkMsRUFBQUEsSUFBSSxFQUFFLHNCQURzQjtBQUU1QixhQUFTakIsRUFBRSxDQUFDa0IsY0FGZ0I7QUFHNUJDLEVBQUFBLFlBQVksRUFBRSxJQUhjO0FBSzVCQyxFQUFBQSxJQUFJLEVBQUUsY0FBVUMsUUFBVixFQUFvQkMsTUFBcEIsRUFBNEJDLFdBQTVCLEVBQXlDbEMsT0FBekMsRUFBa0RtQyxLQUFsRCxFQUF5RDtBQUMzRDtBQUNBLFNBQUtDLE9BQUwsR0FBZSxFQUFmO0FBQ0EsU0FBS0MsYUFBTCxHQUFxQixFQUFyQjtBQUNBLFNBQUtDLFlBQUwsR0FBb0IsQ0FBcEI7QUFDQSxTQUFLQyxPQUFMLEdBQWUsQ0FBZjtBQUNBLFNBQUtDLFFBQUwsR0FBZ0IsQ0FBaEI7QUFDQSxTQUFLQyxZQUFMLEdBQW9CLENBQXBCO0FBQ0EsU0FBS0MsaUJBQUwsR0FBeUIsSUFBekI7QUFDQSxTQUFLQyxnQkFBTCxHQUF3QixJQUF4QjtBQUNBM0MsSUFBQUEsT0FBTyxLQUFLNEMsU0FBWixJQUF5QmpDLEVBQUUsQ0FBQ2UsaUJBQUgsQ0FBcUJtQixTQUFyQixDQUErQkMsZ0JBQS9CLENBQWdEQyxJQUFoRCxDQUFxRCxJQUFyRCxFQUEyRGYsUUFBM0QsRUFBcUVDLE1BQXJFLEVBQTZFQyxXQUE3RSxFQUEwRmxDLE9BQTFGLEVBQW1HbUMsS0FBbkcsQ0FBekI7QUFDSCxHQWhCMkI7QUFrQjVCVyxFQUFBQSxnQkFBZ0IsRUFBQywwQkFBVWQsUUFBVixFQUFvQkMsTUFBcEIsRUFBNEJDLFdBQTVCLEVBQXlDbEMsT0FBekMsRUFBa0RtQyxLQUFsRCxFQUF5RDtBQUN0RSxRQUFJLENBQUNGLE1BQUQsSUFBV0EsTUFBTSxDQUFDZCxNQUFQLEtBQWtCLENBQWpDLEVBQW9DO0FBQ2hDUixNQUFBQSxFQUFFLENBQUNxQyxPQUFILENBQVcsSUFBWDtBQUNBLGFBQU8sS0FBUDtBQUNIOztBQUVELFNBQUtSLFFBQUwsR0FBZ0J4QyxPQUFoQjtBQUNBLFNBQUt5QyxZQUFMLEdBQW9CUCxXQUFwQjtBQUNBLFNBQUtlLFNBQUwsQ0FBZWhCLE1BQWY7QUFDQSxRQUFHRSxLQUFILEVBQ0lILFFBQVEsR0FBRyxLQUFLa0IsU0FBTCxLQUFpQmYsS0FBNUI7O0FBRUosUUFBSXhCLEVBQUUsQ0FBQ2tCLGNBQUgsQ0FBa0JnQixTQUFsQixDQUE0QkMsZ0JBQTVCLENBQTZDQyxJQUE3QyxDQUFrRCxJQUFsRCxFQUF3RGYsUUFBeEQsQ0FBSixFQUF1RTtBQUNuRSxhQUFPLElBQVA7QUFDSDs7QUFDRCxXQUFPLEtBQVA7QUFDSCxHQWxDMkI7QUFvQzVCbUIsRUFBQUEsS0FBSyxFQUFDLGlCQUFZO0FBQ2QsUUFBSUMsTUFBTSxHQUFHLElBQUl6QyxFQUFFLENBQUNlLGlCQUFQLEVBQWI7QUFDQTBCLElBQUFBLE1BQU0sQ0FBQ04sZ0JBQVAsQ0FBd0IsS0FBS08sU0FBN0IsRUFBd0M1QixrQkFBa0IsQ0FBQyxLQUFLVyxPQUFOLENBQTFELEVBQTBFLEtBQUtJLFFBQS9FO0FBQ0EsV0FBT1ksTUFBUDtBQUNILEdBeEMyQjtBQTBDNUJFLEVBQUFBLGVBQWUsRUFBQyx5QkFBVUMsTUFBVixFQUFrQjtBQUM5QjVDLElBQUFBLEVBQUUsQ0FBQ2tCLGNBQUgsQ0FBa0JnQixTQUFsQixDQUE0QlMsZUFBNUIsQ0FBNENQLElBQTVDLENBQWlELElBQWpELEVBQXVEUSxNQUF2RCxFQUQ4QixDQUU5Qjs7QUFDQSxTQUFLaEIsT0FBTCxHQUFlLEtBQUssS0FBS0gsT0FBTCxDQUFhakIsTUFBYixHQUFzQixDQUEzQixDQUFmO0FBQ0EsU0FBS3VCLGlCQUFMLEdBQXlCL0IsRUFBRSxDQUFDQyxFQUFILENBQU0sS0FBSzJDLE1BQUwsQ0FBWTlDLENBQWxCLEVBQXFCLEtBQUs4QyxNQUFMLENBQVk3QyxDQUFqQyxDQUF6QjtBQUNBLFNBQUtpQyxnQkFBTCxHQUF3QmhDLEVBQUUsQ0FBQ0MsRUFBSCxDQUFNLENBQU4sRUFBUyxDQUFULENBQXhCO0FBQ0gsR0FoRDJCO0FBa0Q1QjRDLEVBQUFBLElBQUksRUFBRSxjQUFTQyxFQUFULEVBQVk7QUFDZCxTQUFJLElBQUlsQyxDQUFDLEdBQUMsQ0FBVixFQUFhQSxDQUFDLEdBQUMsS0FBS21DLFlBQUwsQ0FBa0J2QyxNQUFqQyxFQUF5Q0ksQ0FBQyxFQUExQyxFQUE4QztBQUMxQyxVQUFHLEtBQUttQyxZQUFMLENBQWtCbkMsQ0FBbEIsSUFBdUJrQyxFQUExQixFQUNJLE9BQU9sQyxDQUFQO0FBQ1A7O0FBQ0QsV0FBTyxLQUFLbUMsWUFBTCxDQUFrQnZDLE1BQXpCO0FBQ0gsR0F4RDJCO0FBMEQ1QndDLEVBQUFBLEtBQUssRUFBRSxlQUFTM0MsQ0FBVCxFQUFZeUMsRUFBWixFQUFlO0FBQ2xCLFFBQUd6QyxDQUFDLElBQUUsQ0FBTixFQUFTO0FBQ0wsYUFBT3lDLEVBQUUsR0FBQyxLQUFLcEIsYUFBTCxDQUFtQnJCLENBQW5CLENBQVY7QUFDSCxLQUZELE1BRU87QUFDSCxhQUFPLENBQUN5QyxFQUFFLEdBQUMsS0FBS0MsWUFBTCxDQUFrQjFDLENBQUMsR0FBQyxDQUFwQixDQUFKLElBQTRCLEtBQUtxQixhQUFMLENBQW1CckIsQ0FBbkIsQ0FBbkM7QUFDSDtBQUNKLEdBaEUyQjtBQWtFNUI0QyxFQUFBQSxNQUFNLEVBQUMsZ0JBQVVILEVBQVYsRUFBYztBQUNqQkEsSUFBQUEsRUFBRSxHQUFHLEtBQUtJLGdCQUFMLENBQXNCSixFQUF0QixDQUFMO0FBQ0EsUUFBSXpDLENBQUosRUFBTzhDLEVBQVA7QUFDQSxRQUFJQyxFQUFFLEdBQUcsS0FBSzNCLE9BQWQsQ0FIaUIsQ0FJakI7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsUUFBSXFCLEVBQUUsS0FBSyxDQUFYLEVBQWM7QUFDVnpDLE1BQUFBLENBQUMsR0FBRytDLEVBQUUsQ0FBQzVDLE1BQUgsR0FBWSxDQUFoQjtBQUNBMkMsTUFBQUEsRUFBRSxHQUFHLENBQUw7QUFDSCxLQUhELE1BR087QUFDSCxVQUFJRSxLQUFLLEdBQUcsS0FBS3pCLE9BQWpCLENBREcsQ0FFSDtBQUNBOztBQUNBdkIsTUFBQUEsQ0FBQyxHQUFHLEtBQUt3QyxJQUFMLENBQVVDLEVBQVYsQ0FBSjtBQUNBSyxNQUFBQSxFQUFFLEdBQUcsS0FBS0gsS0FBTCxDQUFXM0MsQ0FBWCxFQUFjeUMsRUFBZCxDQUFMLENBTEcsQ0FNSDtBQUNIOztBQUVELFFBQUlRLE1BQU0sR0FBR3RFLGdCQUFnQixDQUN6QmtCLGlCQUFpQixDQUFDa0QsRUFBRCxFQUFLL0MsQ0FBQyxHQUFHLENBQVQsQ0FEUSxFQUV6QkgsaUJBQWlCLENBQUNrRCxFQUFELEVBQUsvQyxDQUFDLEdBQUcsQ0FBVCxDQUZRLEVBR3pCSCxpQkFBaUIsQ0FBQ2tELEVBQUQsRUFBSy9DLENBQUMsR0FBRyxDQUFULENBSFEsRUFJekJILGlCQUFpQixDQUFDa0QsRUFBRCxFQUFLL0MsQ0FBQyxHQUFHLENBQVQsQ0FKUSxFQUt6QixLQUFLd0IsUUFMb0IsRUFLVnNCLEVBTFUsQ0FBN0I7O0FBT0EsUUFBSW5ELEVBQUUsQ0FBQ3VELEtBQUgsQ0FBU0Msd0JBQWIsRUFBdUM7QUFDbkMsVUFBSUMsS0FBSixFQUFXQyxLQUFYO0FBQ0FELE1BQUFBLEtBQUssR0FBRyxLQUFLYixNQUFMLENBQVk5QyxDQUFaLEdBQWdCLEtBQUtpQyxpQkFBTCxDQUF1QmpDLENBQS9DO0FBQ0E0RCxNQUFBQSxLQUFLLEdBQUcsS0FBS2QsTUFBTCxDQUFZN0MsQ0FBWixHQUFnQixLQUFLZ0MsaUJBQUwsQ0FBdUJoQyxDQUEvQzs7QUFDQSxVQUFJMEQsS0FBSyxLQUFLLENBQVYsSUFBZUMsS0FBSyxLQUFLLENBQTdCLEVBQWdDO0FBQzVCLFlBQUlDLFVBQVUsR0FBRyxLQUFLM0IsZ0JBQXRCO0FBQ0F5QixRQUFBQSxLQUFLLEdBQUdFLFVBQVUsQ0FBQzdELENBQVgsR0FBZTJELEtBQXZCO0FBQ0FDLFFBQUFBLEtBQUssR0FBR0MsVUFBVSxDQUFDNUQsQ0FBWCxHQUFlMkQsS0FBdkI7QUFDQUMsUUFBQUEsVUFBVSxDQUFDN0QsQ0FBWCxHQUFlMkQsS0FBZjtBQUNBRSxRQUFBQSxVQUFVLENBQUM1RCxDQUFYLEdBQWUyRCxLQUFmO0FBQ0FKLFFBQUFBLE1BQU0sQ0FBQ3hELENBQVAsSUFBWTJELEtBQVo7QUFDQUgsUUFBQUEsTUFBTSxDQUFDdkQsQ0FBUCxJQUFZMkQsS0FBWjtBQUNIO0FBQ0o7O0FBQ0QsU0FBS0UsY0FBTCxDQUFvQk4sTUFBcEI7QUFDSCxHQTVHMkI7QUE4RzVCTyxFQUFBQSxPQUFPLEVBQUMsbUJBQVk7QUFDaEIsUUFBSUMsYUFBYSxHQUFHcEQsb0JBQW9CLENBQUMsS0FBS2UsT0FBTixDQUF4QztBQUNBLFdBQU96QixFQUFFLENBQUNlLGlCQUFILENBQXFCLEtBQUsyQixTQUExQixFQUFxQ29CLGFBQXJDLEVBQW9ELEtBQUtqQyxRQUF6RCxDQUFQO0FBQ0gsR0FqSDJCOztBQW1INUI7Ozs7O0FBS0ErQixFQUFBQSxjQUFjLEVBQUMsd0JBQVVOLE1BQVYsRUFBa0I7QUFDN0IsUUFBSVMsTUFBTSxHQUFHVCxNQUFNLENBQUN4RCxDQUFQLEdBQVMsS0FBS2lDLGlCQUFMLENBQXVCakMsQ0FBN0M7QUFDQSxRQUFJa0UsTUFBTSxHQUFHVixNQUFNLENBQUN2RCxDQUFQLEdBQVMsS0FBS2dDLGlCQUFMLENBQXVCaEMsQ0FBN0M7O0FBQ0EsUUFBRyxDQUFDLEtBQUtrRSxjQUFULEVBQXlCO0FBQ3JCLFVBQUdGLE1BQU0sS0FBSyxDQUFYLElBQWdCQyxNQUFNLEtBQUssQ0FBOUIsRUFDSSxLQUFLcEIsTUFBTCxDQUFZc0IsS0FBWixHQUFvQjVELElBQUksQ0FBQzZELEtBQUwsQ0FBV0gsTUFBWCxFQUFtQkQsTUFBbkIsSUFBMkIsR0FBM0IsR0FBZ0N6RCxJQUFJLENBQUM4RCxFQUFyQyxHQUEyQyxLQUFLdEMsWUFBcEU7QUFDUDs7QUFDRCxTQUFLYyxNQUFMLENBQVl5QixXQUFaLENBQXdCZixNQUF4QjtBQUNBLFNBQUt2QixpQkFBTCxHQUF5QnVCLE1BQXpCO0FBQ0gsR0FqSTJCOztBQW1JNUI7Ozs7O0FBS0FnQixFQUFBQSxTQUFTLEVBQUMscUJBQVk7QUFDbEIsV0FBTyxLQUFLN0MsT0FBWjtBQUNILEdBMUkyQjs7QUE0STVCOzs7OztBQUtBYSxFQUFBQSxTQUFTLEVBQUMsbUJBQVVoQixNQUFWLEVBQWtCO0FBQ3hCQSxJQUFBQSxNQUFNLEdBQUcsS0FBS2lELG9CQUFMLENBQTBCakQsTUFBMUIsRUFBa0MsRUFBbEMsQ0FBVDs7QUFDQSxTQUFLa0QsU0FBTCxDQUFlbEQsTUFBZjtBQUNILEdBcEoyQjtBQXNKNUJrRCxFQUFBQSxTQXRKNEIscUJBc0psQmxELE1BdEprQixFQXNKWDtBQUNiLFNBQUtHLE9BQUwsR0FBZUgsTUFBZjtBQUNBLFNBQUtJLGFBQUwsR0FBcUIsRUFBckI7QUFDQSxTQUFLcUIsWUFBTCxHQUFvQixFQUFwQjs7QUFDQSxTQUFJLElBQUluQyxDQUFDLEdBQUMsQ0FBVixFQUFhQSxDQUFDLEdBQUNVLE1BQU0sQ0FBQ2QsTUFBUCxHQUFjLENBQTdCLEVBQWdDSSxDQUFDLEVBQWpDLEVBQXFDO0FBQ2pDLFVBQUkxQixFQUFFLEdBQUdvQyxNQUFNLENBQUNWLENBQUQsQ0FBZjtBQUNBLFVBQUl6QixFQUFFLEdBQUdtQyxNQUFNLENBQUNWLENBQUMsR0FBQyxDQUFILENBQWY7QUFDQSxVQUFJSixNQUFNLEdBQUdGLElBQUksQ0FBQ21FLElBQUwsQ0FBVSxDQUFDdkYsRUFBRSxDQUFDWSxDQUFILEdBQUtYLEVBQUUsQ0FBQ1csQ0FBVCxLQUFhWixFQUFFLENBQUNZLENBQUgsR0FBS1gsRUFBRSxDQUFDVyxDQUFyQixJQUEwQixDQUFDWixFQUFFLENBQUNhLENBQUgsR0FBS1osRUFBRSxDQUFDWSxDQUFULEtBQWFiLEVBQUUsQ0FBQ2EsQ0FBSCxHQUFLWixFQUFFLENBQUNZLENBQXJCLENBQXBDLENBQWI7O0FBQ0EsV0FBSzJCLGFBQUwsQ0FBbUJiLElBQW5CLENBQXdCTCxNQUF4QjtBQUNIOztBQUNELFNBQUttQixZQUFMLEdBQW9CLENBQXBCOztBQUNBLFNBQUksSUFBSWYsQ0FBQyxHQUFDLENBQVYsRUFBYUEsQ0FBQyxHQUFDLEtBQUtjLGFBQUwsQ0FBbUJsQixNQUFsQyxFQUEwQ0ksQ0FBQyxFQUEzQyxFQUErQztBQUMzQyxXQUFLZSxZQUFMLElBQXFCLEtBQUtELGFBQUwsQ0FBbUJkLENBQW5CLENBQXJCOztBQUNBLFdBQUttQyxZQUFMLENBQWtCbEMsSUFBbEIsQ0FBdUIsS0FBS2MsWUFBNUI7QUFDSDs7QUFDRCxTQUFJLElBQUlmLENBQUMsR0FBQyxDQUFWLEVBQWFBLENBQUMsR0FBQyxLQUFLYyxhQUFMLENBQW1CbEIsTUFBbEMsRUFBMENJLENBQUMsRUFBM0MsRUFBK0M7QUFDM0MsV0FBS2MsYUFBTCxDQUFtQmQsQ0FBbkIsSUFBd0IsS0FBS2MsYUFBTCxDQUFtQmQsQ0FBbkIsSUFBc0IsS0FBS2UsWUFBbkQ7QUFDQSxXQUFLb0IsWUFBTCxDQUFrQm5DLENBQWxCLElBQXVCLEtBQUttQyxZQUFMLENBQWtCbkMsQ0FBbEIsSUFBcUIsS0FBS2UsWUFBakQ7QUFDSDtBQUNKLEdBeksyQjtBQTJLNUIrQyxFQUFBQSxVQTNLNEIsc0JBMktqQnBELE1BM0tpQixFQTJLVjtBQUNkLFNBQUtHLE9BQUwsR0FBZUgsTUFBZjtBQUNBLFNBQUtJLGFBQUwsR0FBcUIsRUFBckI7QUFDQSxTQUFLcUIsWUFBTCxHQUFvQixFQUFwQjs7QUFDQSxTQUFJLElBQUluQyxDQUFDLEdBQUMsQ0FBVixFQUFhQSxDQUFDLEdBQUNVLE1BQU0sQ0FBQ2QsTUFBUCxHQUFjLENBQTdCLEVBQWdDSSxDQUFDLEVBQWpDLEVBQXFDO0FBQ2pDLFVBQUkxQixFQUFFLEdBQUdvQyxNQUFNLENBQUNWLENBQUQsQ0FBZjtBQUNBLFVBQUl6QixFQUFFLEdBQUdtQyxNQUFNLENBQUNWLENBQUMsR0FBQyxDQUFILENBQWY7QUFDQSxVQUFJSixNQUFNLEdBQUcsQ0FBYjs7QUFDQSxXQUFLa0IsYUFBTCxDQUFtQmIsSUFBbkIsQ0FBd0JMLE1BQXhCO0FBQ0g7O0FBQ0QsU0FBS21CLFlBQUwsR0FBb0IsQ0FBcEI7O0FBQ0EsU0FBSSxJQUFJZixDQUFDLEdBQUMsQ0FBVixFQUFhQSxDQUFDLEdBQUMsS0FBS2MsYUFBTCxDQUFtQmxCLE1BQWxDLEVBQTBDSSxDQUFDLEVBQTNDLEVBQStDO0FBQzNDLFdBQUtlLFlBQUwsSUFBcUIsS0FBS0QsYUFBTCxDQUFtQmQsQ0FBbkIsQ0FBckI7O0FBQ0EsV0FBS21DLFlBQUwsQ0FBa0JsQyxJQUFsQixDQUF1QixLQUFLYyxZQUE1QjtBQUNIOztBQUNELFNBQUksSUFBSWYsQ0FBQyxHQUFDLENBQVYsRUFBYUEsQ0FBQyxHQUFDLEtBQUtjLGFBQUwsQ0FBbUJsQixNQUFsQyxFQUEwQ0ksQ0FBQyxFQUEzQyxFQUErQztBQUMzQyxXQUFLYyxhQUFMLENBQW1CZCxDQUFuQixJQUF3QixLQUFLYyxhQUFMLENBQW1CZCxDQUFuQixJQUFzQixLQUFLZSxZQUFuRDtBQUNBLFdBQUtvQixZQUFMLENBQWtCbkMsQ0FBbEIsSUFBdUIsS0FBS21DLFlBQUwsQ0FBa0JuQyxDQUFsQixJQUFxQixLQUFLZSxZQUFqRDtBQUNIO0FBQ0osR0E5TDJCO0FBZ001QjRDLEVBQUFBLG9CQUFvQixFQUFFLDhCQUFTakQsTUFBVCxFQUFpQnFELENBQWpCLEVBQW1CO0FBQ3JDLFNBQUtILFNBQUwsQ0FBZWxELE1BQWY7O0FBQ0EsUUFBSXNELENBQUMsR0FBRyxDQUFDdEQsTUFBTSxDQUFDZCxNQUFQLEdBQWMsQ0FBZixJQUFvQm1FLENBQTVCO0FBQ0EsUUFBSUUsU0FBUyxHQUFHLEVBQWhCO0FBQ0EsUUFBSXhFLENBQUMsR0FBRyxDQUFSO0FBQ0EsUUFBSThDLEVBQUUsR0FBRyxDQUFUO0FBQ0EsUUFBSUMsRUFBRSxHQUFHOUIsTUFBVDs7QUFDQSxTQUFJLElBQUlWLENBQUMsR0FBQyxDQUFWLEVBQWFBLENBQUMsR0FBQ2dFLENBQWYsRUFBa0JoRSxDQUFDLEVBQW5CLEVBQXVCO0FBQ25CLFVBQUlrQyxFQUFFLEdBQUdsQyxDQUFDLEdBQUMsQ0FBRixJQUFLZ0UsQ0FBQyxHQUFDLENBQVAsQ0FBVDs7QUFDQSxVQUFJOUIsRUFBRSxLQUFLLENBQVgsRUFBYztBQUNWekMsUUFBQUEsQ0FBQyxHQUFHK0MsRUFBRSxDQUFDNUMsTUFBSCxHQUFZLENBQWhCO0FBQ0EyQyxRQUFBQSxFQUFFLEdBQUcsQ0FBTDtBQUNILE9BSEQsTUFHTztBQUNILFlBQUlFLEtBQUssR0FBRyxLQUFLL0IsTUFBTSxDQUFDZCxNQUFQLEdBQWdCLENBQXJCLENBQVosQ0FERyxDQUVIO0FBQ0E7O0FBQ0FILFFBQUFBLENBQUMsR0FBRyxLQUFLd0MsSUFBTCxDQUFVQyxFQUFWLENBQUo7QUFDQUssUUFBQUEsRUFBRSxHQUFHLEtBQUtILEtBQUwsQ0FBVzNDLENBQVgsRUFBY3lDLEVBQWQsQ0FBTDtBQUNIOztBQUNELFVBQUlRLE1BQU0sR0FBR3RFLGdCQUFnQixDQUN6QmtCLGlCQUFpQixDQUFDa0QsRUFBRCxFQUFLL0MsQ0FBQyxHQUFHLENBQVQsQ0FEUSxFQUV6QkgsaUJBQWlCLENBQUNrRCxFQUFELEVBQUsvQyxDQUFDLEdBQUcsQ0FBVCxDQUZRLEVBR3pCSCxpQkFBaUIsQ0FBQ2tELEVBQUQsRUFBSy9DLENBQUMsR0FBRyxDQUFULENBSFEsRUFJekJILGlCQUFpQixDQUFDa0QsRUFBRCxFQUFLL0MsQ0FBQyxHQUFHLENBQVQsQ0FKUSxFQUt6QixLQUFLd0IsUUFMb0IsRUFLVnNCLEVBTFUsQ0FBN0I7QUFNQTBCLE1BQUFBLFNBQVMsQ0FBQ2hFLElBQVYsQ0FBZXlDLE1BQWY7QUFDSDs7QUFDRCxXQUFPdUIsU0FBUDtBQUNILEdBNU4yQjtBQThONUJ0QyxFQUFBQSxTQUFTLEVBQUUscUJBQVU7QUFDakIsV0FBTyxLQUFLWixZQUFaO0FBQ0gsR0FoTzJCO0FBa081Qm1ELEVBQUFBLGFBQWEsRUFBRSx5QkFBVTtBQUNyQixTQUFLYixjQUFMLEdBQXNCLElBQXRCO0FBQ0g7QUFwTzJCLENBQVQsQ0FBdkI7QUF3T0E7Ozs7Ozs7Ozs7Ozs7OztBQWNBakUsRUFBRSxDQUFDK0UsaUJBQUgsR0FBdUIsVUFBVTFELFFBQVYsRUFBb0JDLE1BQXBCLEVBQTRCQyxXQUE1QixFQUF5Q2xDLE9BQXpDLEVBQWtEO0FBQ3JFLFNBQU8sSUFBSVcsRUFBRSxDQUFDZSxpQkFBUCxDQUF5Qk0sUUFBekIsRUFBbUNDLE1BQW5DLEVBQTJDQyxXQUEzQyxFQUF3RGxDLE9BQXhELENBQVA7QUFDSCxDQUZEO0FBSUE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFjQVcsRUFBRSxDQUFDZ0YsdUJBQUgsR0FBNkIsVUFBVXhELEtBQVYsRUFBaUJGLE1BQWpCLEVBQXlCQyxXQUF6QixFQUFzQ2xDLE9BQXRDLEVBQStDO0FBQ3hFLFNBQU8sSUFBSVcsRUFBRSxDQUFDZSxpQkFBUCxDQUF5QixDQUF6QixFQUE0Qk8sTUFBNUIsRUFBb0NDLFdBQXBDLEVBQWlEbEMsT0FBakQsRUFBMERtQyxLQUExRCxDQUFQO0FBQ0gsQ0FGRDtBQUlBOzs7Ozs7Ozs7Ozs7Ozs7OztBQWVBeEIsRUFBRSxDQUFDaUYsaUJBQUgsR0FBdUJqRixFQUFFLENBQUNnQixLQUFILENBQVM7QUFDNUJDLEVBQUFBLElBQUksRUFBRSxzQkFEc0I7QUFFNUIsYUFBU2pCLEVBQUUsQ0FBQ2UsaUJBRmdCO0FBSTVCSyxFQUFBQSxJQUFJLEVBQUMsY0FBVUMsUUFBVixFQUFvQkMsTUFBcEIsRUFBNEJDLFdBQTVCLEVBQXlDbEMsT0FBekMsRUFBa0Q7QUFDbkQsU0FBSzZGLGNBQUwsR0FBc0JsRixFQUFFLENBQUNDLEVBQUgsQ0FBTSxDQUFOLEVBQVMsQ0FBVCxDQUF0QjtBQUNBWixJQUFBQSxPQUFPLEtBQUs0QyxTQUFaLElBQXlCLEtBQUtFLGdCQUFMLENBQXNCZCxRQUF0QixFQUFnQ0MsTUFBaEMsRUFBd0NDLFdBQXhDLEVBQXFEbEMsT0FBckQsQ0FBekI7QUFDSCxHQVAyQjtBQVM1QnNELEVBQUFBLGVBQWUsRUFBQyx5QkFBVUMsTUFBVixFQUFrQjtBQUM5QjVDLElBQUFBLEVBQUUsQ0FBQ2UsaUJBQUgsQ0FBcUJtQixTQUFyQixDQUErQlMsZUFBL0IsQ0FBK0NQLElBQS9DLENBQW9ELElBQXBELEVBQTBEUSxNQUExRDtBQUNBLFNBQUtzQyxjQUFMLENBQW9CcEYsQ0FBcEIsR0FBd0I4QyxNQUFNLENBQUM5QyxDQUEvQjtBQUNBLFNBQUtvRixjQUFMLENBQW9CbkYsQ0FBcEIsR0FBd0I2QyxNQUFNLENBQUM3QyxDQUEvQjtBQUNILEdBYjJCO0FBZTVCOEQsRUFBQUEsT0FBTyxFQUFDLG1CQUFZO0FBQ2hCLFFBQUlzQixVQUFVLEdBQUcsS0FBSzFELE9BQUwsQ0FBYTJELEtBQWIsRUFBakI7O0FBQ0EsUUFBSUMsT0FBSixDQUZnQixDQUdoQjtBQUNBO0FBQ0E7O0FBQ0EsUUFBSWhGLENBQUMsR0FBRzhFLFVBQVUsQ0FBQyxDQUFELENBQWxCOztBQUNBLFNBQUssSUFBSXZFLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUd1RSxVQUFVLENBQUMzRSxNQUEvQixFQUF1QyxFQUFFSSxDQUF6QyxFQUE0QztBQUN4Q3lFLE1BQUFBLE9BQU8sR0FBR0YsVUFBVSxDQUFDdkUsQ0FBRCxDQUFwQjtBQUNBdUUsTUFBQUEsVUFBVSxDQUFDdkUsQ0FBRCxDQUFWLEdBQWdCeUUsT0FBTyxDQUFDQyxHQUFSLENBQVlqRixDQUFaLENBQWhCO0FBQ0FBLE1BQUFBLENBQUMsR0FBR2dGLE9BQUo7QUFDSCxLQVhlLENBYWhCOzs7QUFDQSxRQUFJRSxZQUFZLEdBQUc3RSxvQkFBb0IsQ0FBQ3lFLFVBQUQsQ0FBdkMsQ0FkZ0IsQ0FnQmhCOztBQUNBOUUsSUFBQUEsQ0FBQyxHQUFHa0YsWUFBWSxDQUFFQSxZQUFZLENBQUMvRSxNQUFiLEdBQXNCLENBQXhCLENBQWhCO0FBQ0ErRSxJQUFBQSxZQUFZLENBQUNDLEdBQWI7QUFFQW5GLElBQUFBLENBQUMsQ0FBQ1AsQ0FBRixHQUFNLENBQUNPLENBQUMsQ0FBQ1AsQ0FBVDtBQUNBTyxJQUFBQSxDQUFDLENBQUNOLENBQUYsR0FBTSxDQUFDTSxDQUFDLENBQUNOLENBQVQ7QUFFQXdGLElBQUFBLFlBQVksQ0FBQ0UsT0FBYixDQUFxQnBGLENBQXJCOztBQUNBLFNBQUssSUFBSU8sQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBRzJFLFlBQVksQ0FBQy9FLE1BQWpDLEVBQXlDLEVBQUVJLENBQTNDLEVBQThDO0FBQzFDeUUsTUFBQUEsT0FBTyxHQUFHRSxZQUFZLENBQUMzRSxDQUFELENBQXRCO0FBQ0F5RSxNQUFBQSxPQUFPLENBQUN2RixDQUFSLEdBQVksQ0FBQ3VGLE9BQU8sQ0FBQ3ZGLENBQXJCO0FBQ0F1RixNQUFBQSxPQUFPLENBQUN0RixDQUFSLEdBQVksQ0FBQ3NGLE9BQU8sQ0FBQ3RGLENBQXJCO0FBQ0FzRixNQUFBQSxPQUFPLENBQUN2RixDQUFSLElBQWFPLENBQUMsQ0FBQ1AsQ0FBZjtBQUNBdUYsTUFBQUEsT0FBTyxDQUFDdEYsQ0FBUixJQUFhTSxDQUFDLENBQUNOLENBQWY7QUFDQXdGLE1BQUFBLFlBQVksQ0FBQzNFLENBQUQsQ0FBWixHQUFrQnlFLE9BQWxCO0FBQ0FoRixNQUFBQSxDQUFDLEdBQUdnRixPQUFKO0FBQ0g7O0FBQ0QsV0FBT3JGLEVBQUUsQ0FBQ2lGLGlCQUFILENBQXFCLEtBQUt2QyxTQUExQixFQUFxQzZDLFlBQXJDLEVBQW1ELEtBQUt6RCxZQUF4RCxFQUFzRSxLQUFLRCxRQUEzRSxDQUFQO0FBQ0gsR0FqRDJCOztBQW1ENUI7Ozs7O0FBS0ErQixFQUFBQSxjQUFjLEVBQUMsd0JBQVVOLE1BQVYsRUFBa0I7QUFDN0IsUUFBSWxELEdBQUcsR0FBRyxLQUFLOEUsY0FBZjtBQUNBLFFBQUlRLElBQUksR0FBR3BDLE1BQU0sQ0FBQ3hELENBQVAsR0FBV00sR0FBRyxDQUFDTixDQUExQjtBQUNBLFFBQUk2RixJQUFJLEdBQUdyQyxNQUFNLENBQUN2RCxDQUFQLEdBQVdLLEdBQUcsQ0FBQ0wsQ0FBMUI7QUFFQSxRQUFJZ0UsTUFBTSxHQUFHMkIsSUFBSSxHQUFDLEtBQUszRCxpQkFBTCxDQUF1QmpDLENBQXpDO0FBQ0EsUUFBSWtFLE1BQU0sR0FBRzJCLElBQUksR0FBQyxLQUFLNUQsaUJBQUwsQ0FBdUJoQyxDQUF6QztBQUNBLFFBQUdnRSxNQUFNLEtBQUssQ0FBWCxJQUFnQkMsTUFBTSxLQUFLLENBQTlCLEVBQ0ksS0FBS3BCLE1BQUwsQ0FBWXNCLEtBQVosR0FBb0I1RCxJQUFJLENBQUM2RCxLQUFMLENBQVdILE1BQVgsRUFBbUJELE1BQW5CLElBQTJCLEdBQTNCLEdBQWdDekQsSUFBSSxDQUFDOEQsRUFBckMsR0FBMkMsS0FBS3RDLFlBQXBFO0FBRUosU0FBS0MsaUJBQUwsQ0FBdUJqQyxDQUF2QixHQUEyQjRGLElBQTNCO0FBQ0EsU0FBSzNELGlCQUFMLENBQXVCaEMsQ0FBdkIsR0FBMkI0RixJQUEzQjtBQUNBLFNBQUsvQyxNQUFMLENBQVl5QixXQUFaLENBQXdCcUIsSUFBeEIsRUFBOEJDLElBQTlCO0FBQ0gsR0FyRTJCO0FBdUU1Qm5ELEVBQUFBLEtBQUssRUFBQyxpQkFBWTtBQUNkLFFBQUlvRCxDQUFDLEdBQUcsSUFBSTVGLEVBQUUsQ0FBQ2lGLGlCQUFQLEVBQVI7QUFDQVcsSUFBQUEsQ0FBQyxDQUFDekQsZ0JBQUYsQ0FBbUIsS0FBS08sU0FBeEIsRUFBbUM1QixrQkFBa0IsQ0FBQyxLQUFLVyxPQUFOLENBQXJELEVBQXFFLEtBQUtLLFlBQTFFLEVBQXdGLEtBQUtELFFBQTdGO0FBQ0EsV0FBTytELENBQVA7QUFDSDtBQTNFMkIsQ0FBVCxDQUF2QjtBQThFQTs7Ozs7Ozs7Ozs7O0FBV0E1RixFQUFFLENBQUM2RixpQkFBSCxHQUF1QixVQUFVeEUsUUFBVixFQUFvQkMsTUFBcEIsRUFBNEJDLFdBQTVCLEVBQXlDbEMsT0FBekMsRUFBa0Q7QUFDckUsU0FBTyxJQUFJVyxFQUFFLENBQUNpRixpQkFBUCxDQUF5QjVELFFBQXpCLEVBQW1DQyxNQUFuQyxFQUEyQ0MsV0FBM0MsRUFBd0RsQyxPQUF4RCxDQUFQO0FBQ0gsQ0FGRDtBQUlBOzs7Ozs7Ozs7Ozs7Ozs7OztBQWVBVyxFQUFFLENBQUM4RixhQUFILEdBQW1COUYsRUFBRSxDQUFDZ0IsS0FBSCxDQUFTO0FBQ3hCQyxFQUFBQSxJQUFJLEVBQUUsa0JBRGtCO0FBRXhCLGFBQVNqQixFQUFFLENBQUNlLGlCQUZZO0FBSXhCSyxFQUFBQSxJQUFJLEVBQUUsY0FBUzBCLEVBQVQsRUFBYXhCLE1BQWIsRUFBcUJDLFdBQXJCLEVBQWtDO0FBQ3BDLFFBQUdBLFdBQVcsS0FBS1UsU0FBbkIsRUFDSVYsV0FBVyxHQUFHLENBQWQ7QUFDSkQsSUFBQUEsTUFBTSxJQUFJLEtBQUthLGdCQUFMLENBQXNCVyxFQUF0QixFQUEwQnhCLE1BQTFCLEVBQWtDQyxXQUFsQyxDQUFWO0FBQ0gsR0FSdUI7QUFVeEJZLEVBQUFBLGdCQUFnQixFQUFDLDBCQUFVVyxFQUFWLEVBQWN4QixNQUFkLEVBQXNCQyxXQUF0QixFQUFtQztBQUNoRCxXQUFPdkIsRUFBRSxDQUFDZSxpQkFBSCxDQUFxQm1CLFNBQXJCLENBQStCQyxnQkFBL0IsQ0FBZ0RDLElBQWhELENBQXFELElBQXJELEVBQTJEVSxFQUEzRCxFQUErRHhCLE1BQS9ELEVBQXVFQyxXQUF2RSxFQUFvRixHQUFwRixDQUFQO0FBQ0gsR0FadUI7QUFjeEJpQixFQUFBQSxLQUFLLEVBQUMsaUJBQVk7QUFDZCxRQUFJQyxNQUFNLEdBQUcsSUFBSXpDLEVBQUUsQ0FBQzhGLGFBQVAsRUFBYjtBQUNBckQsSUFBQUEsTUFBTSxDQUFDTixnQkFBUCxDQUF3QixLQUFLTyxTQUE3QixFQUF3QzVCLGtCQUFrQixDQUFDLEtBQUtXLE9BQU4sRUFBZSxLQUFLSyxZQUFwQixDQUExRDtBQUNBLFdBQU9XLE1BQVA7QUFDSDtBQWxCdUIsQ0FBVCxDQUFuQjtBQXFCQTs7Ozs7Ozs7Ozs7OztBQVlBekMsRUFBRSxDQUFDK0YsYUFBSCxHQUFtQixVQUFVakQsRUFBVixFQUFjeEIsTUFBZCxFQUFzQkMsV0FBdEIsRUFBbUM7QUFDbEQsU0FBTyxJQUFJdkIsRUFBRSxDQUFDOEYsYUFBUCxDQUFxQmhELEVBQXJCLEVBQXlCeEIsTUFBekIsRUFBaUNDLFdBQWpDLENBQVA7QUFDSCxDQUZEO0FBSUE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZUF2QixFQUFFLENBQUNnRyxhQUFILEdBQW1CaEcsRUFBRSxDQUFDZ0IsS0FBSCxDQUFTO0FBQ3hCQyxFQUFBQSxJQUFJLEVBQUUsa0JBRGtCO0FBRXhCLGFBQVNqQixFQUFFLENBQUNpRixpQkFGWTtBQUl4QjdELEVBQUFBLElBQUksRUFBRSxjQUFTMEIsRUFBVCxFQUFheEIsTUFBYixFQUFxQkMsV0FBckIsRUFBa0M7QUFDcENELElBQUFBLE1BQU0sSUFBSSxLQUFLYSxnQkFBTCxDQUFzQlcsRUFBdEIsRUFBMEJ4QixNQUExQixFQUFrQ0MsV0FBbEMsQ0FBVjtBQUNILEdBTnVCO0FBUXhCWSxFQUFBQSxnQkFBZ0IsRUFBQywwQkFBVVcsRUFBVixFQUFjeEIsTUFBZCxFQUFzQkMsV0FBdEIsRUFBbUM7QUFDaEQsV0FBT3ZCLEVBQUUsQ0FBQ2UsaUJBQUgsQ0FBcUJtQixTQUFyQixDQUErQkMsZ0JBQS9CLENBQWdEQyxJQUFoRCxDQUFxRCxJQUFyRCxFQUEyRFUsRUFBM0QsRUFBK0R4QixNQUEvRCxFQUF1RUMsV0FBdkUsRUFBb0YsR0FBcEYsQ0FBUDtBQUNILEdBVnVCO0FBWXhCaUIsRUFBQUEsS0FBSyxFQUFDLGlCQUFZO0FBQ2QsUUFBSUMsTUFBTSxHQUFHLElBQUl6QyxFQUFFLENBQUNnRyxhQUFQLEVBQWI7QUFDQXZELElBQUFBLE1BQU0sQ0FBQ04sZ0JBQVAsQ0FBd0IsS0FBS08sU0FBN0IsRUFBd0M1QixrQkFBa0IsQ0FBQyxLQUFLVyxPQUFOLENBQTFELEVBQTBFLEtBQUtLLFlBQS9FO0FBQ0EsV0FBT1csTUFBUDtBQUNIO0FBaEJ1QixDQUFULENBQW5CO0FBbUJBOzs7Ozs7Ozs7Ozs7QUFXQXpDLEVBQUUsQ0FBQ2lHLGFBQUgsR0FBbUIsVUFBVW5ELEVBQVYsRUFBY3hCLE1BQWQsRUFBc0JDLFdBQXRCLEVBQW1DO0FBQ2xELFNBQU8sSUFBSXZCLEVBQUUsQ0FBQ2dHLGFBQVAsQ0FBcUJsRCxFQUFyQixFQUF5QnhCLE1BQXpCLEVBQWlDQyxXQUFqQyxDQUFQO0FBQ0gsQ0FGRCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcclxuIENvcHlyaWdodCAoYykgMjAwOCBSYWR1IEdydWlhblxyXG4gQ29weXJpZ2h0IChjKSAyMDA4LTIwMTAgUmljYXJkbyBRdWVzYWRhXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTEgVml0IFZhbGVudGluXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTEtMjAxMiBjb2NvczJkLXgub3JnXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTMtMjAxNiBDaHVrb25nIFRlY2hub2xvZ2llcyBJbmMuXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTctMjAxOCBYaWFtZW4gWWFqaSBTb2Z0d2FyZSBDby4sIEx0ZC5cclxuIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZ1xyXG4gUGVybWlzc2lvbiBpcyBoZXJlYnkgZ3JhbnRlZCwgZnJlZSBvZiBjaGFyZ2UsIHRvIGFueSBwZXJzb24gb2J0YWluaW5nIGEgY29weVxyXG4gb2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVzICh0aGUgXCJTb2Z0d2FyZVwiKSwgdG8gZGVhbFxyXG4gaW4gdGhlIFNvZnR3YXJlIHdpdGhvdXQgcmVzdHJpY3Rpb24sIGluY2x1ZGluZyB3aXRob3V0IGxpbWl0YXRpb24gdGhlIHJpZ2h0c1xyXG4gdG8gdXNlLCBjb3B5LCBtb2RpZnksIG1lcmdlLCBwdWJsaXNoLCBkaXN0cmlidXRlLCBzdWJsaWNlbnNlLCBhbmQvb3Igc2VsbFxyXG4gY29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdCBwZXJzb25zIHRvIHdob20gdGhlIFNvZnR3YXJlIGlzXHJcbiBmdXJuaXNoZWQgdG8gZG8gc28sIHN1YmplY3QgdG8gdGhlIGZvbGxvd2luZyBjb25kaXRpb25zOlxyXG4gVGhlIGFib3ZlIGNvcHlyaWdodCBub3RpY2UgYW5kIHRoaXMgcGVybWlzc2lvbiBub3RpY2Ugc2hhbGwgYmUgaW5jbHVkZWQgaW5cclxuIGFsbCBjb3BpZXMgb3Igc3Vic3RhbnRpYWwgcG9ydGlvbnMgb2YgdGhlIFNvZnR3YXJlLlxyXG4gVEhFIFNPRlRXQVJFIElTIFBST1ZJREVEIFwiQVMgSVNcIiwgV0lUSE9VVCBXQVJSQU5UWSBPRiBBTlkgS0lORCwgRVhQUkVTUyBPUlxyXG4gSU1QTElFRCwgSU5DTFVESU5HIEJVVCBOT1QgTElNSVRFRCBUTyBUSEUgV0FSUkFOVElFUyBPRiBNRVJDSEFOVEFCSUxJVFksXHJcbiBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRSBBTkQgTk9OSU5GUklOR0VNRU5ULiBJTiBOTyBFVkVOVCBTSEFMTCBUSEVcclxuIEFVVEhPUlMgT1IgQ09QWVJJR0hUIEhPTERFUlMgQkUgTElBQkxFIEZPUiBBTlkgQ0xBSU0sIERBTUFHRVMgT1IgT1RIRVJcclxuIExJQUJJTElUWSwgV0hFVEhFUiBJTiBBTiBBQ1RJT04gT0YgQ09OVFJBQ1QsIFRPUlQgT1IgT1RIRVJXSVNFLCBBUklTSU5HIEZST00sXHJcbiBPVVQgT0YgT1IgSU4gQ09OTkVDVElPTiBXSVRIIFRIRSBTT0ZUV0FSRSBPUiBUSEUgVVNFIE9SIE9USEVSIERFQUxJTkdTIElOXHJcbiBUSEUgU09GVFdBUkUuXHJcbiBPcmlnbmFsIGNvZGUgYnkgUmFkdSBHcnVpYW46IGh0dHA6Ly93d3cuY29kZXByb2plY3QuY29tL0FydGljbGVzLzMwODM4L092ZXJoYXVzZXItQ2F0bXVsbC1Sb20tU3BsaW5lcy1mb3ItQ2FtZXJhLUFuaW1hdGlvLlNvXHJcbiBBZGFwdGVkIHRvIGNvY29zMmQteCBieSBWaXQgVmFsZW50aW5cclxuIEFkYXB0ZWQgZnJvbSBjb2NvczJkLXggdG8gY29jb3MyZC1pcGhvbmUgYnkgUmljYXJkbyBRdWVzYWRhXHJcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuLyoqXHJcbiAqIEBtb2R1bGUgY2NcclxuICovXHJcblxyXG4vKlxyXG4gKiBSZXR1cm5zIHRoZSBDYXJkaW5hbCBTcGxpbmUgcG9zaXRpb24gZm9yIGEgZ2l2ZW4gc2V0IG9mIGNvbnRyb2wgcG9pbnRzLCB0ZW5zaW9uIGFuZCB0aW1lLiA8YnIgLz5cclxuICogQ2F0bXVsbFJvbSBTcGxpbmUgZm9ybXVsYS4gPGJyIC8+XHJcbiAqIHMoLXR0dCArIDJ0dCAtIHQpUDEgKyBzKC10dHQgKyB0dClQMiArICgydHR0IC0gM3R0ICsgMSlQMiArIHModHR0IC0gMnR0ICsgdClQMyArICgtMnR0dCArIDN0dClQMyArIHModHR0IC0gdHQpUDRcclxuICpcclxuICogQG1ldGhvZCBjYXJkaW5hbFNwbGluZUF0XHJcbiAqIEBwYXJhbSB7VmVjMn0gcDBcclxuICogQHBhcmFtIHtWZWMyfSBwMVxyXG4gKiBAcGFyYW0ge1ZlYzJ9IHAyXHJcbiAqIEBwYXJhbSB7VmVjMn0gcDNcclxuICogQHBhcmFtIHtOdW1iZXJ9IHRlbnNpb25cclxuICogQHBhcmFtIHtOdW1iZXJ9IHRcclxuICogQHJldHVybiB7VmVjMn1cclxuICovXHJcbmZ1bmN0aW9uIGNhcmRpbmFsU3BsaW5lQXQgKHAwLCBwMSwgcDIsIHAzLCB0ZW5zaW9uLCB0KSB7XHJcbiAgICB2YXIgdDIgPSB0ICogdDtcclxuICAgIHZhciB0MyA9IHQyICogdDtcclxuXHJcbiAgICAvKlxyXG4gICAgICogRm9ybXVsYTogcygtdHR0ICsgMnR0IC0gdClQMSArIHMoLXR0dCArIHR0KVAyICsgKDJ0dHQgLSAzdHQgKyAxKVAyICsgcyh0dHQgLSAydHQgKyB0KVAzICsgKC0ydHR0ICsgM3R0KVAzICsgcyh0dHQgLSB0dClQNFxyXG4gICAgICovXHJcbiAgICB2YXIgcyA9ICgxIC0gdGVuc2lvbikgLyAyO1xyXG5cclxuICAgIHZhciBiMSA9IHMgKiAoKC10MyArICgyICogdDIpKSAtIHQpOyAgICAgICAgICAgICAgICAgICAgICAvLyBzKC10MyArIDIgdDIgLSB0KVAxXHJcbiAgICB2YXIgYjIgPSBzICogKC10MyArIHQyKSArICgyICogdDMgLSAzICogdDIgKyAxKTsgICAgICAgICAgLy8gcygtdDMgKyB0MilQMiArICgyIHQzIC0gMyB0MiArIDEpUDJcclxuICAgIHZhciBiMyA9IHMgKiAodDMgLSAyICogdDIgKyB0KSArICgtMiAqIHQzICsgMyAqIHQyKTsgICAgICAvLyBzKHQzIC0gMiB0MiArIHQpUDMgKyAoLTIgdDMgKyAzIHQyKVAzXHJcbiAgICB2YXIgYjQgPSBzICogKHQzIC0gdDIpOyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gcyh0MyAtIHQyKVA0XHJcblxyXG4gICAgdmFyIHggPSAocDAueCAqIGIxICsgcDEueCAqIGIyICsgcDIueCAqIGIzICsgcDMueCAqIGI0KTtcclxuICAgIHZhciB5ID0gKHAwLnkgKiBiMSArIHAxLnkgKiBiMiArIHAyLnkgKiBiMyArIHAzLnkgKiBiNCk7XHJcbiAgICByZXR1cm4gY2MudjIoeCwgeSk7XHJcbn07XHJcblxyXG4vKlxyXG4gKiByZXR1cm5zIGEgcG9pbnQgZnJvbSB0aGUgYXJyYXlcclxuICogQG1ldGhvZCBnZXRDb250cm9sUG9pbnRBdFxyXG4gKiBAcGFyYW0ge0FycmF5fSBjb250cm9sUG9pbnRzXHJcbiAqIEBwYXJhbSB7TnVtYmVyfSBwb3NcclxuICogQHJldHVybiB7QXJyYXl9XHJcbiAqL1xyXG5mdW5jdGlvbiBnZXRDb250cm9sUG9pbnRBdCAoY29udHJvbFBvaW50cywgcG9zKSB7XHJcbiAgICB2YXIgcCA9IE1hdGgubWluKGNvbnRyb2xQb2ludHMubGVuZ3RoIC0gMSwgTWF0aC5tYXgocG9zLCAwKSk7XHJcbiAgICByZXR1cm4gY29udHJvbFBvaW50c1twXTtcclxufTtcclxuXHJcbmZ1bmN0aW9uIHJldmVyc2VDb250cm9sUG9pbnRzIChjb250cm9sUG9pbnRzKSB7XHJcbiAgICB2YXIgbmV3QXJyYXkgPSBbXTtcclxuICAgIGZvciAodmFyIGkgPSBjb250cm9sUG9pbnRzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XHJcbiAgICAgICAgbmV3QXJyYXkucHVzaChjYy52Mihjb250cm9sUG9pbnRzW2ldLngsIGNvbnRyb2xQb2ludHNbaV0ueSkpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG5ld0FycmF5O1xyXG59XHJcblxyXG5mdW5jdGlvbiBjbG9uZUNvbnRyb2xQb2ludHMgKGNvbnRyb2xQb2ludHMpIHtcclxuICAgIHZhciBuZXdBcnJheSA9IFtdO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjb250cm9sUG9pbnRzLmxlbmd0aDsgaSsrKVxyXG4gICAgICAgIG5ld0FycmF5LnB1c2goY2MudjIoY29udHJvbFBvaW50c1tpXS54LCBjb250cm9sUG9pbnRzW2ldLnkpKTtcclxuICAgIHJldHVybiBuZXdBcnJheTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFRPRE8gdOG7kWkgxrB1OiBUaOG7sWMgaGnhu4duIHByZSBpbnRlcnBvbGF0aW9uIHbDoCBwcmVjYWxjdWxhdGUgbGVuZ3RoIGNobyBjw6FjIFNwbGluZSBBY3Rpb24gZ2nhu5FuZyBuaGF1XHJcbiAqL1xyXG5cclxuLypcclxuICogQ2FyZGluYWwgU3BsaW5lIHBhdGguIGh0dHA6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvQ3ViaWNfSGVybWl0ZV9zcGxpbmUjQ2FyZGluYWxfc3BsaW5lXHJcbiAqIEFic29sdXRlIGNvb3JkaW5hdGVzLlxyXG4gKlxyXG4gKiBAY2xhc3MgQ2FyZGluYWxTcGxpbmVUbzJcclxuICogQGV4dGVuZHMgQWN0aW9uSW50ZXJ2YWxcclxuICpcclxuICogQHBhcmFtIHtOdW1iZXJ9IGR1cmF0aW9uXHJcbiAqIEBwYXJhbSB7QXJyYXl9IHBvaW50cyBhcnJheSBvZiBjb250cm9sIHBvaW50c1xyXG4gKiBAcGFyYW0ge051bWJlcn0gdGVuc2lvblxyXG4gKlxyXG4gKiBAZXhhbXBsZVxyXG4gKiAvL2NyZWF0ZSBhIGNjLkNhcmRpbmFsU3BsaW5lVG8yXHJcbiAqIHZhciBhY3Rpb24xID0gY2MuQ2FyZGluYWxTcGxpbmVUbzIoMywgYXJyYXksIDApO1xyXG4gKiBcclxuICovXHJcbmNjLkNhcmRpbmFsU3BsaW5lVG8yID0gY2MuQ2xhc3Moe1xyXG4gICAgbmFtZTogJ2NjLkNhcmRpbmFsU3BsaW5lVG8yJyxcclxuICAgIGV4dGVuZHM6IGNjLkFjdGlvbkludGVydmFsLFxyXG4gICAgY2FjaGVMZW5ndGhzOiBudWxsLFxyXG5cclxuICAgIGN0b3I6IGZ1bmN0aW9uIChkdXJhdGlvbiwgcG9pbnRzLCBhbmdsZU9mZnNldCwgdGVuc2lvbiwgc3BlZWQpIHtcclxuICAgICAgICAvKiBBcnJheSBvZiBjb250cm9sIHBvaW50cyAqL1xyXG4gICAgICAgIHRoaXMuX3BvaW50cyA9IFtdO1xyXG4gICAgICAgIHRoaXMuX2NhY2hlTGVuZ3RocyA9IFtdO1xyXG4gICAgICAgIHRoaXMuX3RvdGFsTGVuZ3RoID0gMDtcclxuICAgICAgICB0aGlzLl9kZWx0YVQgPSAwO1xyXG4gICAgICAgIHRoaXMuX3RlbnNpb24gPSAwO1xyXG4gICAgICAgIHRoaXMuX2FuZ2xlT2Zmc2V0ID0gMDtcclxuICAgICAgICB0aGlzLl9wcmV2aW91c1Bvc2l0aW9uID0gbnVsbDtcclxuICAgICAgICB0aGlzLl9hY2N1bXVsYXRlZERpZmYgPSBudWxsO1xyXG4gICAgICAgIHRlbnNpb24gIT09IHVuZGVmaW5lZCAmJiBjYy5DYXJkaW5hbFNwbGluZVRvMi5wcm90b3R5cGUuaW5pdFdpdGhEdXJhdGlvbi5jYWxsKHRoaXMsIGR1cmF0aW9uLCBwb2ludHMsIGFuZ2xlT2Zmc2V0LCB0ZW5zaW9uLCBzcGVlZCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGluaXRXaXRoRHVyYXRpb246ZnVuY3Rpb24gKGR1cmF0aW9uLCBwb2ludHMsIGFuZ2xlT2Zmc2V0LCB0ZW5zaW9uLCBzcGVlZCkge1xyXG4gICAgICAgIGlmICghcG9pbnRzIHx8IHBvaW50cy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgY2MuZXJyb3JJRCgxMDI0KTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5fdGVuc2lvbiA9IHRlbnNpb247XHJcbiAgICAgICAgdGhpcy5fYW5nbGVPZmZzZXQgPSBhbmdsZU9mZnNldDtcclxuICAgICAgICB0aGlzLnNldFBvaW50cyhwb2ludHMpO1xyXG4gICAgICAgIGlmKHNwZWVkKVxyXG4gICAgICAgICAgICBkdXJhdGlvbiA9IHRoaXMuZ2V0TGVuZ3RoKCkvc3BlZWQ7XHJcblxyXG4gICAgICAgIGlmIChjYy5BY3Rpb25JbnRlcnZhbC5wcm90b3R5cGUuaW5pdFdpdGhEdXJhdGlvbi5jYWxsKHRoaXMsIGR1cmF0aW9uKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfSxcclxuXHJcbiAgICBjbG9uZTpmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIGFjdGlvbiA9IG5ldyBjYy5DYXJkaW5hbFNwbGluZVRvMigpO1xyXG4gICAgICAgIGFjdGlvbi5pbml0V2l0aER1cmF0aW9uKHRoaXMuX2R1cmF0aW9uLCBjbG9uZUNvbnRyb2xQb2ludHModGhpcy5fcG9pbnRzKSwgdGhpcy5fdGVuc2lvbik7XHJcbiAgICAgICAgcmV0dXJuIGFjdGlvbjtcclxuICAgIH0sXHJcblxyXG4gICAgc3RhcnRXaXRoVGFyZ2V0OmZ1bmN0aW9uICh0YXJnZXQpIHtcclxuICAgICAgICBjYy5BY3Rpb25JbnRlcnZhbC5wcm90b3R5cGUuc3RhcnRXaXRoVGFyZ2V0LmNhbGwodGhpcywgdGFyZ2V0KTtcclxuICAgICAgICAvLyBJc3N1ZSAjMTQ0MSBmcm9tIGNvY29zMmQtaXBob25lXHJcbiAgICAgICAgdGhpcy5fZGVsdGFUID0gMSAvICh0aGlzLl9wb2ludHMubGVuZ3RoIC0gMSk7XHJcbiAgICAgICAgdGhpcy5fcHJldmlvdXNQb3NpdGlvbiA9IGNjLnYyKHRoaXMudGFyZ2V0LngsIHRoaXMudGFyZ2V0LnkpO1xyXG4gICAgICAgIHRoaXMuX2FjY3VtdWxhdGVkRGlmZiA9IGNjLnYyKDAsIDApO1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRQOiBmdW5jdGlvbihkdCl7XHJcbiAgICAgICAgZm9yKHZhciBpPTA7IGk8dGhpcy5fY3VtbXV0aWxhdGUubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgaWYodGhpcy5fY3VtbXV0aWxhdGVbaV0gPiBkdClcclxuICAgICAgICAgICAgICAgIHJldHVybiBpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5fY3VtbXV0aWxhdGUubGVuZ3RoO1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRMdDogZnVuY3Rpb24ocCwgZHQpe1xyXG4gICAgICAgIGlmKHA9PTApIHtcclxuICAgICAgICAgICAgcmV0dXJuIGR0L3RoaXMuX2NhY2hlTGVuZ3Roc1twXTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gKGR0LXRoaXMuX2N1bW11dGlsYXRlW3AtMV0pL3RoaXMuX2NhY2hlTGVuZ3Roc1twXTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIHVwZGF0ZTpmdW5jdGlvbiAoZHQpIHtcclxuICAgICAgICBkdCA9IHRoaXMuX2NvbXB1dGVFYXNlVGltZShkdCk7XHJcbiAgICAgICAgdmFyIHAsIGx0O1xyXG4gICAgICAgIHZhciBwcyA9IHRoaXMuX3BvaW50cztcclxuICAgICAgICAvLyBlZy5cclxuICAgICAgICAvLyBwLi5wLi5wLi5wLi5wLi5wLi5wXHJcbiAgICAgICAgLy8gMS4uMi4uMy4uNC4uNS4uNi4uN1xyXG4gICAgICAgIC8vIHdhbnQgcCB0byBiZSAxLCAyLCAzLCA0LCA1LCA2XHJcbiAgICAgICAgaWYgKGR0ID09PSAxKSB7XHJcbiAgICAgICAgICAgIHAgPSBwcy5sZW5ndGggLSAxO1xyXG4gICAgICAgICAgICBsdCA9IDE7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdmFyIGxvY0RUID0gdGhpcy5fZGVsdGFUO1xyXG4gICAgICAgICAgICAvL3AgPSAwIHwgKGR0IC8gbG9jRFQpO1xyXG4gICAgICAgICAgICAvL2x0ID0gKGR0IC0gbG9jRFQgKiBwKSAvIGxvY0RUO1xyXG4gICAgICAgICAgICBwID0gdGhpcy5nZXRQKGR0KTtcclxuICAgICAgICAgICAgbHQgPSB0aGlzLmdldEx0KHAsIGR0KTtcclxuICAgICAgICAgICAgLy8gY29uc29sZS5sb2cobHQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdmFyIG5ld1BvcyA9IGNhcmRpbmFsU3BsaW5lQXQoXHJcbiAgICAgICAgICAgIGdldENvbnRyb2xQb2ludEF0KHBzLCBwIC0gMSksXHJcbiAgICAgICAgICAgIGdldENvbnRyb2xQb2ludEF0KHBzLCBwIC0gMCksXHJcbiAgICAgICAgICAgIGdldENvbnRyb2xQb2ludEF0KHBzLCBwICsgMSksXHJcbiAgICAgICAgICAgIGdldENvbnRyb2xQb2ludEF0KHBzLCBwICsgMiksXHJcbiAgICAgICAgICAgIHRoaXMuX3RlbnNpb24sIGx0KTtcclxuXHJcbiAgICAgICAgaWYgKGNjLm1hY3JvLkVOQUJMRV9TVEFDS0FCTEVfQUNUSU9OUykge1xyXG4gICAgICAgICAgICB2YXIgdGVtcFgsIHRlbXBZO1xyXG4gICAgICAgICAgICB0ZW1wWCA9IHRoaXMudGFyZ2V0LnggLSB0aGlzLl9wcmV2aW91c1Bvc2l0aW9uLng7XHJcbiAgICAgICAgICAgIHRlbXBZID0gdGhpcy50YXJnZXQueSAtIHRoaXMuX3ByZXZpb3VzUG9zaXRpb24ueTtcclxuICAgICAgICAgICAgaWYgKHRlbXBYICE9PSAwIHx8IHRlbXBZICE9PSAwKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgbG9jQWNjRGlmZiA9IHRoaXMuX2FjY3VtdWxhdGVkRGlmZjtcclxuICAgICAgICAgICAgICAgIHRlbXBYID0gbG9jQWNjRGlmZi54ICsgdGVtcFg7XHJcbiAgICAgICAgICAgICAgICB0ZW1wWSA9IGxvY0FjY0RpZmYueSArIHRlbXBZO1xyXG4gICAgICAgICAgICAgICAgbG9jQWNjRGlmZi54ID0gdGVtcFg7XHJcbiAgICAgICAgICAgICAgICBsb2NBY2NEaWZmLnkgPSB0ZW1wWTtcclxuICAgICAgICAgICAgICAgIG5ld1Bvcy54ICs9IHRlbXBYO1xyXG4gICAgICAgICAgICAgICAgbmV3UG9zLnkgKz0gdGVtcFk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy51cGRhdGVQb3NpdGlvbihuZXdQb3MpO1xyXG4gICAgfSxcclxuXHJcbiAgICByZXZlcnNlOmZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgcmV2ZXJzZVBvaW50cyA9IHJldmVyc2VDb250cm9sUG9pbnRzKHRoaXMuX3BvaW50cyk7XHJcbiAgICAgICAgcmV0dXJuIGNjLkNhcmRpbmFsU3BsaW5lVG8yKHRoaXMuX2R1cmF0aW9uLCByZXZlcnNlUG9pbnRzLCB0aGlzLl90ZW5zaW9uKTtcclxuICAgIH0sXHJcblxyXG4gICAgLypcclxuICAgICAqIHVwZGF0ZSBwb3NpdGlvbiBvZiB0YXJnZXRcclxuICAgICAqIEBtZXRob2QgdXBkYXRlUG9zaXRpb25cclxuICAgICAqIEBwYXJhbSB7VmVjMn0gbmV3UG9zXHJcbiAgICAgKi9cclxuICAgIHVwZGF0ZVBvc2l0aW9uOmZ1bmN0aW9uIChuZXdQb3MpIHtcclxuICAgICAgICB2YXIgZGVsdGFYID0gbmV3UG9zLngtdGhpcy5fcHJldmlvdXNQb3NpdGlvbi54O1xyXG4gICAgICAgIHZhciBkZWx0YVkgPSBuZXdQb3MueS10aGlzLl9wcmV2aW91c1Bvc2l0aW9uLnk7XHJcbiAgICAgICAgaWYoIXRoaXMuX2Rpc2FibGVSb3RhdGUpIHtcclxuICAgICAgICAgICAgaWYoZGVsdGFYICE9PSAwICYmIGRlbHRhWSAhPT0gMClcclxuICAgICAgICAgICAgICAgIHRoaXMudGFyZ2V0LmFuZ2xlID0gTWF0aC5hdGFuMihkZWx0YVksIGRlbHRhWCkqMTgwLyhNYXRoLlBJKSArIHRoaXMuX2FuZ2xlT2Zmc2V0O1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnRhcmdldC5zZXRQb3NpdGlvbihuZXdQb3MpO1xyXG4gICAgICAgIHRoaXMuX3ByZXZpb3VzUG9zaXRpb24gPSBuZXdQb3M7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qXHJcbiAgICAgKiBQb2ludHMgZ2V0dGVyXHJcbiAgICAgKiBAbWV0aG9kIGdldFBvaW50c1xyXG4gICAgICogQHJldHVybiB7QXJyYXl9XHJcbiAgICAgKi9cclxuICAgIGdldFBvaW50czpmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3BvaW50cztcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQb2ludHMgc2V0dGVyXHJcbiAgICAgKiBAbWV0aG9kIHNldFBvaW50c1xyXG4gICAgICogQHBhcmFtIHtBcnJheX0gcG9pbnRzXHJcbiAgICAgKi9cclxuICAgIHNldFBvaW50czpmdW5jdGlvbiAocG9pbnRzKSB7XHJcbiAgICAgICAgcG9pbnRzID0gdGhpcy5faW50ZXJwb2xhdGF0ZVBvaW50cyhwb2ludHMsIDEwKTtcclxuICAgICAgICB0aGlzLl9zZXRQb2ludChwb2ludHMpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfc2V0UG9pbnQocG9pbnRzKXtcclxuICAgICAgICB0aGlzLl9wb2ludHMgPSBwb2ludHM7XHJcbiAgICAgICAgdGhpcy5fY2FjaGVMZW5ndGhzID0gW107XHJcbiAgICAgICAgdGhpcy5fY3VtbXV0aWxhdGUgPSBbXTtcclxuICAgICAgICBmb3IodmFyIGk9MDsgaTxwb2ludHMubGVuZ3RoLTE7IGkrKykge1xyXG4gICAgICAgICAgICB2YXIgcDEgPSBwb2ludHNbaV07XHJcbiAgICAgICAgICAgIHZhciBwMiA9IHBvaW50c1tpKzFdO1xyXG4gICAgICAgICAgICB2YXIgbGVuZ3RoID0gTWF0aC5zcXJ0KChwMS54LXAyLngpKihwMS54LXAyLngpICsgKHAxLnktcDIueSkqKHAxLnktcDIueSkpO1xyXG4gICAgICAgICAgICB0aGlzLl9jYWNoZUxlbmd0aHMucHVzaChsZW5ndGgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl90b3RhbExlbmd0aCA9IDA7XHJcbiAgICAgICAgZm9yKHZhciBpPTA7IGk8dGhpcy5fY2FjaGVMZW5ndGhzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3RvdGFsTGVuZ3RoICs9IHRoaXMuX2NhY2hlTGVuZ3Roc1tpXTtcclxuICAgICAgICAgICAgdGhpcy5fY3VtbXV0aWxhdGUucHVzaCh0aGlzLl90b3RhbExlbmd0aCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvcih2YXIgaT0wOyBpPHRoaXMuX2NhY2hlTGVuZ3Rocy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLl9jYWNoZUxlbmd0aHNbaV0gPSB0aGlzLl9jYWNoZUxlbmd0aHNbaV0vdGhpcy5fdG90YWxMZW5ndGg7XHJcbiAgICAgICAgICAgIHRoaXMuX2N1bW11dGlsYXRlW2ldID0gdGhpcy5fY3VtbXV0aWxhdGVbaV0vdGhpcy5fdG90YWxMZW5ndGg7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfc2V0UG9pbnQyKHBvaW50cyl7XHJcbiAgICAgICAgdGhpcy5fcG9pbnRzID0gcG9pbnRzO1xyXG4gICAgICAgIHRoaXMuX2NhY2hlTGVuZ3RocyA9IFtdO1xyXG4gICAgICAgIHRoaXMuX2N1bW11dGlsYXRlID0gW107XHJcbiAgICAgICAgZm9yKHZhciBpPTA7IGk8cG9pbnRzLmxlbmd0aC0xOyBpKyspIHtcclxuICAgICAgICAgICAgdmFyIHAxID0gcG9pbnRzW2ldO1xyXG4gICAgICAgICAgICB2YXIgcDIgPSBwb2ludHNbaSsxXTtcclxuICAgICAgICAgICAgdmFyIGxlbmd0aCA9IDE7XHJcbiAgICAgICAgICAgIHRoaXMuX2NhY2hlTGVuZ3Rocy5wdXNoKGxlbmd0aCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX3RvdGFsTGVuZ3RoID0gMDtcclxuICAgICAgICBmb3IodmFyIGk9MDsgaTx0aGlzLl9jYWNoZUxlbmd0aHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5fdG90YWxMZW5ndGggKz0gdGhpcy5fY2FjaGVMZW5ndGhzW2ldO1xyXG4gICAgICAgICAgICB0aGlzLl9jdW1tdXRpbGF0ZS5wdXNoKHRoaXMuX3RvdGFsTGVuZ3RoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZm9yKHZhciBpPTA7IGk8dGhpcy5fY2FjaGVMZW5ndGhzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2NhY2hlTGVuZ3Roc1tpXSA9IHRoaXMuX2NhY2hlTGVuZ3Roc1tpXS90aGlzLl90b3RhbExlbmd0aDtcclxuICAgICAgICAgICAgdGhpcy5fY3VtbXV0aWxhdGVbaV0gPSB0aGlzLl9jdW1tdXRpbGF0ZVtpXS90aGlzLl90b3RhbExlbmd0aDtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9pbnRlcnBvbGF0YXRlUG9pbnRzOiBmdW5jdGlvbihwb2ludHMsIGQpe1xyXG4gICAgICAgIHRoaXMuX3NldFBvaW50KHBvaW50cyk7XHJcbiAgICAgICAgdmFyIG4gPSAocG9pbnRzLmxlbmd0aC0xKSAqIGQ7XHJcbiAgICAgICAgdmFyIG5ld1BvaW50cyA9IFtdO1xyXG4gICAgICAgIHZhciBwID0gMDtcclxuICAgICAgICB2YXIgbHQgPSAwO1xyXG4gICAgICAgIHZhciBwcyA9IHBvaW50cztcclxuICAgICAgICBmb3IodmFyIGk9MDsgaTxuOyBpKyspIHtcclxuICAgICAgICAgICAgdmFyIGR0ID0gaSoxLyhuLTEpO1xyXG4gICAgICAgICAgICBpZiAoZHQgPT09IDEpIHtcclxuICAgICAgICAgICAgICAgIHAgPSBwcy5sZW5ndGggLSAxO1xyXG4gICAgICAgICAgICAgICAgbHQgPSAxO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdmFyIGxvY0RUID0gMSAvIChwb2ludHMubGVuZ3RoIC0gMSk7XHJcbiAgICAgICAgICAgICAgICAvL3AgPSAwIHwgKGR0IC8gbG9jRFQpO1xyXG4gICAgICAgICAgICAgICAgLy9sdCA9IChkdCAtIGxvY0RUICogcCkgLyBsb2NEVDtcclxuICAgICAgICAgICAgICAgIHAgPSB0aGlzLmdldFAoZHQpO1xyXG4gICAgICAgICAgICAgICAgbHQgPSB0aGlzLmdldEx0KHAsIGR0KTsgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdmFyIG5ld1BvcyA9IGNhcmRpbmFsU3BsaW5lQXQoXHJcbiAgICAgICAgICAgICAgICBnZXRDb250cm9sUG9pbnRBdChwcywgcCAtIDEpLFxyXG4gICAgICAgICAgICAgICAgZ2V0Q29udHJvbFBvaW50QXQocHMsIHAgLSAwKSxcclxuICAgICAgICAgICAgICAgIGdldENvbnRyb2xQb2ludEF0KHBzLCBwICsgMSksXHJcbiAgICAgICAgICAgICAgICBnZXRDb250cm9sUG9pbnRBdChwcywgcCArIDIpLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5fdGVuc2lvbiwgbHQpO1xyXG4gICAgICAgICAgICBuZXdQb2ludHMucHVzaChuZXdQb3MpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbmV3UG9pbnRzO1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRMZW5ndGg6IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3RvdGFsTGVuZ3RoO1xyXG4gICAgfSxcclxuXHJcbiAgICBkaXNhYmxlUm90YXRlOiBmdW5jdGlvbigpe1xyXG4gICAgICAgIHRoaXMuX2Rpc2FibGVSb3RhdGUgPSB0cnVlO1xyXG4gICAgfVxyXG5cclxufSk7XHJcblxyXG4vKipcclxuICogISNlbiBDcmVhdGVzIGFuIGFjdGlvbiB3aXRoIGEgQ2FyZGluYWwgU3BsaW5lIGFycmF5IG9mIHBvaW50cyBhbmQgdGVuc2lvbi5cclxuICogISN6aCDmjInln7rmlbDmoLfmnaHmm7Lnur/ovajov7nnp7vliqjliLDnm67moIfkvY3nva7jgIJcclxuICogQG1ldGhvZCBDYXJkaW5hbFNwbGluZVRvMlxyXG4gKiBAcGFyYW0ge051bWJlcn0gZHVyYXRpb25cclxuICogQHBhcmFtIHtBcnJheX0gcG9pbnRzIGFycmF5IG9mIGNvbnRyb2wgcG9pbnRzXHJcbiAqIEBwYXJhbSB7TnVtYmVyfSBhbmdsZU9mZnNldFxyXG4gKiBAcGFyYW0ge051bWJlcn0gdGVuc2lvblxyXG4gKiBAcmV0dXJuIHtBY3Rpb25JbnRlcnZhbH1cclxuICpcclxuICogQGV4YW1wbGVcclxuICogLy9jcmVhdGUgYSBjYy5DYXJkaW5hbFNwbGluZVRvMlxyXG4gKiB2YXIgYWN0aW9uMSA9IGNjLkNhcmRpbmFsU3BsaW5lVG8yKDMsIGFycmF5LCAwKTtcclxuICovXHJcbmNjLmNhcmRpbmFsU3BsaW5lVG8yID0gZnVuY3Rpb24gKGR1cmF0aW9uLCBwb2ludHMsIGFuZ2xlT2Zmc2V0LCB0ZW5zaW9uKSB7XHJcbiAgICByZXR1cm4gbmV3IGNjLkNhcmRpbmFsU3BsaW5lVG8yKGR1cmF0aW9uLCBwb2ludHMsIGFuZ2xlT2Zmc2V0LCB0ZW5zaW9uKTtcclxufTtcclxuXHJcbi8qKlxyXG4gKiAhI2VuIENyZWF0ZXMgYW4gYWN0aW9uIHdpdGggYSBDYXJkaW5hbCBTcGxpbmUgYXJyYXkgb2YgcG9pbnRzIGFuZCB0ZW5zaW9uLlxyXG4gKiAhI3poIOaMieWfuuaVsOagt+adoeabsue6v+i9qOi/ueenu+WKqOWIsOebruagh+S9jee9ruOAglxyXG4gKiBAbWV0aG9kIENhcmRpbmFsU3BsaW5lVG8yXHJcbiAqIEBwYXJhbSB7TnVtYmVyfSBzcGVlZFxyXG4gKiBAcGFyYW0ge0FycmF5fSBwb2ludHMgYXJyYXkgb2YgY29udHJvbCBwb2ludHNcclxuICogQHBhcmFtIHtOdW1iZXJ9IGFuZ2xlT2Zmc2V0XHJcbiAqIEBwYXJhbSB7TnVtYmVyfSB0ZW5zaW9uXHJcbiAqIEByZXR1cm4ge0FjdGlvbkludGVydmFsfVxyXG4gKlxyXG4gKiBAZXhhbXBsZVxyXG4gKiAvL2NyZWF0ZSBhIGNjLkNhcmRpbmFsU3BsaW5lVG8yXHJcbiAqIHZhciBhY3Rpb24xID0gY2MuQ2FyZGluYWxTcGxpbmVUbzIoMywgYXJyYXksIDApO1xyXG4gKi9cclxuY2MuY2FyZGluYWxTcGxpbmVUbzJfc3BlZWQgPSBmdW5jdGlvbiAoc3BlZWQsIHBvaW50cywgYW5nbGVPZmZzZXQsIHRlbnNpb24pIHtcclxuICAgIHJldHVybiBuZXcgY2MuQ2FyZGluYWxTcGxpbmVUbzIoMSwgcG9pbnRzLCBhbmdsZU9mZnNldCwgdGVuc2lvbiwgc3BlZWQpO1xyXG59O1xyXG5cclxuLypcclxuICogQ2FyZGluYWwgU3BsaW5lIHBhdGguIGh0dHA6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvQ3ViaWNfSGVybWl0ZV9zcGxpbmUjQ2FyZGluYWxfc3BsaW5lXHJcbiAqIFJlbGF0aXZlIGNvb3JkaW5hdGVzLlxyXG4gKlxyXG4gKiBAY2xhc3MgQ2FyZGluYWxTcGxpbmVCeTJcclxuICogQGV4dGVuZHMgQ2FyZGluYWxTcGxpbmVUbzJcclxuICpcclxuICogQHBhcmFtIHtOdW1iZXJ9IGR1cmF0aW9uXHJcbiAqIEBwYXJhbSB7QXJyYXl9IHBvaW50c1xyXG4gKiBAcGFyYW0ge051bWJlcn0gdGVuc2lvblxyXG4gKlxyXG4gKiBAZXhhbXBsZVxyXG4gKiAvL2NyZWF0ZSBhIGNjLkNhcmRpbmFsU3BsaW5lQnkyXHJcbiAqIHZhciBhY3Rpb24xID0gY2MuQ2FyZGluYWxTcGxpbmVCeTIoMywgYXJyYXksIDApO1xyXG4gKi9cclxuY2MuQ2FyZGluYWxTcGxpbmVCeTIgPSBjYy5DbGFzcyh7XHJcbiAgICBuYW1lOiAnY2MuQ2FyZGluYWxTcGxpbmVCeTInLFxyXG4gICAgZXh0ZW5kczogY2MuQ2FyZGluYWxTcGxpbmVUbzIsXHJcblxyXG4gICAgY3RvcjpmdW5jdGlvbiAoZHVyYXRpb24sIHBvaW50cywgYW5nbGVPZmZzZXQsIHRlbnNpb24pIHtcclxuICAgICAgICB0aGlzLl9zdGFydFBvc2l0aW9uID0gY2MudjIoMCwgMCk7XHJcbiAgICAgICAgdGVuc2lvbiAhPT0gdW5kZWZpbmVkICYmIHRoaXMuaW5pdFdpdGhEdXJhdGlvbihkdXJhdGlvbiwgcG9pbnRzLCBhbmdsZU9mZnNldCwgdGVuc2lvbik7XHJcbiAgICB9LFxyXG5cclxuICAgIHN0YXJ0V2l0aFRhcmdldDpmdW5jdGlvbiAodGFyZ2V0KSB7XHJcbiAgICAgICAgY2MuQ2FyZGluYWxTcGxpbmVUbzIucHJvdG90eXBlLnN0YXJ0V2l0aFRhcmdldC5jYWxsKHRoaXMsIHRhcmdldCk7XHJcbiAgICAgICAgdGhpcy5fc3RhcnRQb3NpdGlvbi54ID0gdGFyZ2V0Lng7XHJcbiAgICAgICAgdGhpcy5fc3RhcnRQb3NpdGlvbi55ID0gdGFyZ2V0Lnk7XHJcbiAgICB9LFxyXG5cclxuICAgIHJldmVyc2U6ZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBjb3B5Q29uZmlnID0gdGhpcy5fcG9pbnRzLnNsaWNlKCk7XHJcbiAgICAgICAgdmFyIGN1cnJlbnQ7XHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyBjb252ZXJ0IFwiYWJzb2x1dGVzXCIgdG8gXCJkaWZmc1wiXHJcbiAgICAgICAgLy9cclxuICAgICAgICB2YXIgcCA9IGNvcHlDb25maWdbMF07XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCBjb3B5Q29uZmlnLmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgIGN1cnJlbnQgPSBjb3B5Q29uZmlnW2ldO1xyXG4gICAgICAgICAgICBjb3B5Q29uZmlnW2ldID0gY3VycmVudC5zdWIocCk7XHJcbiAgICAgICAgICAgIHAgPSBjdXJyZW50O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gY29udmVydCB0byBcImRpZmZzXCIgdG8gXCJyZXZlcnNlIGFic29sdXRlXCJcclxuICAgICAgICB2YXIgcmV2ZXJzZUFycmF5ID0gcmV2ZXJzZUNvbnRyb2xQb2ludHMoY29weUNvbmZpZyk7XHJcblxyXG4gICAgICAgIC8vIDFzdCBlbGVtZW50ICh3aGljaCBzaG91bGQgYmUgMCwwKSBzaG91bGQgYmUgaGVyZSB0b29cclxuICAgICAgICBwID0gcmV2ZXJzZUFycmF5WyByZXZlcnNlQXJyYXkubGVuZ3RoIC0gMSBdO1xyXG4gICAgICAgIHJldmVyc2VBcnJheS5wb3AoKTtcclxuXHJcbiAgICAgICAgcC54ID0gLXAueDtcclxuICAgICAgICBwLnkgPSAtcC55O1xyXG5cclxuICAgICAgICByZXZlcnNlQXJyYXkudW5zaGlmdChwKTtcclxuICAgICAgICBmb3IgKHZhciBpID0gMTsgaSA8IHJldmVyc2VBcnJheS5sZW5ndGg7ICsraSkge1xyXG4gICAgICAgICAgICBjdXJyZW50ID0gcmV2ZXJzZUFycmF5W2ldO1xyXG4gICAgICAgICAgICBjdXJyZW50LnggPSAtY3VycmVudC54O1xyXG4gICAgICAgICAgICBjdXJyZW50LnkgPSAtY3VycmVudC55O1xyXG4gICAgICAgICAgICBjdXJyZW50LnggKz0gcC54O1xyXG4gICAgICAgICAgICBjdXJyZW50LnkgKz0gcC55O1xyXG4gICAgICAgICAgICByZXZlcnNlQXJyYXlbaV0gPSBjdXJyZW50O1xyXG4gICAgICAgICAgICBwID0gY3VycmVudDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGNjLkNhcmRpbmFsU3BsaW5lQnkyKHRoaXMuX2R1cmF0aW9uLCByZXZlcnNlQXJyYXksIHRoaXMuX2FuZ2xlT2Zmc2V0LCB0aGlzLl90ZW5zaW9uKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiB1cGRhdGUgcG9zaXRpb24gb2YgdGFyZ2V0XHJcbiAgICAgKiBAbWV0aG9kIHVwZGF0ZVBvc2l0aW9uXHJcbiAgICAgKiBAcGFyYW0ge1ZlYzJ9IG5ld1Bvc1xyXG4gICAgICovXHJcbiAgICB1cGRhdGVQb3NpdGlvbjpmdW5jdGlvbiAobmV3UG9zKSB7XHJcbiAgICAgICAgdmFyIHBvcyA9IHRoaXMuX3N0YXJ0UG9zaXRpb247XHJcbiAgICAgICAgdmFyIHBvc1ggPSBuZXdQb3MueCArIHBvcy54O1xyXG4gICAgICAgIHZhciBwb3NZID0gbmV3UG9zLnkgKyBwb3MueTtcclxuXHJcbiAgICAgICAgdmFyIGRlbHRhWCA9IHBvc1gtdGhpcy5fcHJldmlvdXNQb3NpdGlvbi54O1xyXG4gICAgICAgIHZhciBkZWx0YVkgPSBwb3NZLXRoaXMuX3ByZXZpb3VzUG9zaXRpb24ueTtcclxuICAgICAgICBpZihkZWx0YVggIT09IDAgJiYgZGVsdGFZICE9PSAwKVxyXG4gICAgICAgICAgICB0aGlzLnRhcmdldC5hbmdsZSA9IE1hdGguYXRhbjIoZGVsdGFZLCBkZWx0YVgpKjE4MC8oTWF0aC5QSSkgKyB0aGlzLl9hbmdsZU9mZnNldDtcclxuXHJcbiAgICAgICAgdGhpcy5fcHJldmlvdXNQb3NpdGlvbi54ID0gcG9zWDtcclxuICAgICAgICB0aGlzLl9wcmV2aW91c1Bvc2l0aW9uLnkgPSBwb3NZO1xyXG4gICAgICAgIHRoaXMudGFyZ2V0LnNldFBvc2l0aW9uKHBvc1gsIHBvc1kpO1xyXG4gICAgfSxcclxuXHJcbiAgICBjbG9uZTpmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIGEgPSBuZXcgY2MuQ2FyZGluYWxTcGxpbmVCeTIoKTtcclxuICAgICAgICBhLmluaXRXaXRoRHVyYXRpb24odGhpcy5fZHVyYXRpb24sIGNsb25lQ29udHJvbFBvaW50cyh0aGlzLl9wb2ludHMpLCB0aGlzLl9hbmdsZU9mZnNldCwgdGhpcy5fdGVuc2lvbik7XHJcbiAgICAgICAgcmV0dXJuIGE7XHJcbiAgICB9XHJcbn0pO1xyXG5cclxuLyoqXHJcbiAqICEjZW4gQ3JlYXRlcyBhbiBhY3Rpb24gd2l0aCBhIENhcmRpbmFsIFNwbGluZSBhcnJheSBvZiBwb2ludHMgYW5kIHRlbnNpb24uXHJcbiAqICEjemgg5oyJ5Z+65pWw5qC35p2h5puy57q/6L2o6L+556e75Yqo5oyH5a6a55qE6Led56a744CCXHJcbiAqIEBtZXRob2QgQ2FyZGluYWxTcGxpbmVCeTJcclxuICogQHBhcmFtIHtOdW1iZXJ9IGR1cmF0aW9uXHJcbiAqIEBwYXJhbSB7QXJyYXl9IHBvaW50c1xyXG4gKiBAcGFyYW0ge051bWJlcn0gYW5nbGVPZmZzZXRcclxuICogQHBhcmFtIHtOdW1iZXJ9IHRlbnNpb25cclxuICpcclxuICogQHJldHVybiB7QWN0aW9uSW50ZXJ2YWx9XHJcbiAqL1xyXG5jYy5jYXJkaW5hbFNwbGluZUJ5MiA9IGZ1bmN0aW9uIChkdXJhdGlvbiwgcG9pbnRzLCBhbmdsZU9mZnNldCwgdGVuc2lvbikge1xyXG4gICAgcmV0dXJuIG5ldyBjYy5DYXJkaW5hbFNwbGluZUJ5MihkdXJhdGlvbiwgcG9pbnRzLCBhbmdsZU9mZnNldCwgdGVuc2lvbik7XHJcbn07XHJcblxyXG4vKlxyXG4gKiBBbiBhY3Rpb24gdGhhdCBtb3ZlcyB0aGUgdGFyZ2V0IHdpdGggYSBDYXRtdWxsUm9tIGN1cnZlIHRvIGEgZGVzdGluYXRpb24gcG9pbnQuPGJyLz5cclxuICogQSBDYXRtdWxsIFJvbSBpcyBhIENhcmRpbmFsIFNwbGluZSB3aXRoIGEgdGVuc2lvbiBvZiAwLjUuICA8YnIvPlxyXG4gKiBodHRwOi8vZW4ud2lraXBlZGlhLm9yZy93aWtpL0N1YmljX0hlcm1pdGVfc3BsaW5lI0NhdG11bGwuRTIuODAuOTNSb21fc3BsaW5lXHJcbiAqIEFic29sdXRlIGNvb3JkaW5hdGVzLlxyXG4gKlxyXG4gKiBAY2xhc3MgQ2F0bXVsbFJvbVRvMlxyXG4gKiBAZXh0ZW5kcyBDYXJkaW5hbFNwbGluZVRvMlxyXG4gKlxyXG4gKiBAcGFyYW0ge051bWJlcn0gZHRcclxuICogQHBhcmFtIHtBcnJheX0gcG9pbnRzXHJcbiAqXHJcbiAqIEBleGFtcGxlXHJcbiAqIHZhciBhY3Rpb24xID0gY2MuQ2F0bXVsbFJvbVRvMigzLCBhcnJheSk7XHJcbiAqL1xyXG5jYy5DYXRtdWxsUm9tVG8yID0gY2MuQ2xhc3Moe1xyXG4gICAgbmFtZTogJ2NjLkNhdG11bGxSb21UbzInLFxyXG4gICAgZXh0ZW5kczogY2MuQ2FyZGluYWxTcGxpbmVUbzIsXHJcblxyXG4gICAgY3RvcjogZnVuY3Rpb24oZHQsIHBvaW50cywgYW5nbGVPZmZzZXQpIHtcclxuICAgICAgICBpZihhbmdsZU9mZnNldCA9PT0gdW5kZWZpbmVkKVxyXG4gICAgICAgICAgICBhbmdsZU9mZnNldCA9IDA7XHJcbiAgICAgICAgcG9pbnRzICYmIHRoaXMuaW5pdFdpdGhEdXJhdGlvbihkdCwgcG9pbnRzLCBhbmdsZU9mZnNldCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGluaXRXaXRoRHVyYXRpb246ZnVuY3Rpb24gKGR0LCBwb2ludHMsIGFuZ2xlT2Zmc2V0KSB7XHJcbiAgICAgICAgcmV0dXJuIGNjLkNhcmRpbmFsU3BsaW5lVG8yLnByb3RvdHlwZS5pbml0V2l0aER1cmF0aW9uLmNhbGwodGhpcywgZHQsIHBvaW50cywgYW5nbGVPZmZzZXQsIDAuNSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNsb25lOmZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgYWN0aW9uID0gbmV3IGNjLkNhdG11bGxSb21UbzIoKTtcclxuICAgICAgICBhY3Rpb24uaW5pdFdpdGhEdXJhdGlvbih0aGlzLl9kdXJhdGlvbiwgY2xvbmVDb250cm9sUG9pbnRzKHRoaXMuX3BvaW50cywgdGhpcy5fYW5nbGVPZmZzZXQpKTtcclxuICAgICAgICByZXR1cm4gYWN0aW9uO1xyXG4gICAgfVxyXG59KTtcclxuXHJcbi8qKlxyXG4gKiAhI2VuIENyZWF0ZXMgYW4gYWN0aW9uIHdpdGggYSBDYXJkaW5hbCBTcGxpbmUgYXJyYXkgb2YgcG9pbnRzIGFuZCB0ZW5zaW9uLlxyXG4gKiAhI3poIOaMiSBDYXRtdWxsIFJvbSDmoLfmnaHmm7Lnur/ovajov7nnp7vliqjliLDnm67moIfkvY3nva7jgIJcclxuICogQG1ldGhvZCBDYXRtdWxsUm9tVG8yXHJcbiAqIEBwYXJhbSB7TnVtYmVyfSBkdFxyXG4gKiBAcGFyYW0ge0FycmF5fSBwb2ludHNcclxuICogQHBhcmFtIHtOdW1iZXJ9IGFuZ2xlT2Zmc2V0XHJcbiAqIEByZXR1cm4ge0FjdGlvbkludGVydmFsfVxyXG4gKlxyXG4gKiBAZXhhbXBsZVxyXG4gKiB2YXIgYWN0aW9uMSA9IGNjLkNhdG11bGxSb21UbzIoMywgYXJyYXkpO1xyXG4gKi9cclxuY2MuY2F0bXVsbFJvbVRvMiA9IGZ1bmN0aW9uIChkdCwgcG9pbnRzLCBhbmdsZU9mZnNldCkge1xyXG4gICAgcmV0dXJuIG5ldyBjYy5DYXRtdWxsUm9tVG8yKGR0LCBwb2ludHMsIGFuZ2xlT2Zmc2V0KTtcclxufTtcclxuXHJcbi8qXHJcbiAqIEFuIGFjdGlvbiB0aGF0IG1vdmVzIHRoZSB0YXJnZXQgd2l0aCBhIENhdG11bGxSb20gY3VydmUgYnkgYSBjZXJ0YWluIGRpc3RhbmNlLiAgPGJyLz5cclxuICogQSBDYXRtdWxsIFJvbSBpcyBhIENhcmRpbmFsIFNwbGluZSB3aXRoIGEgdGVuc2lvbiBvZiAwLjUuPGJyLz5cclxuICogaHR0cDovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9DdWJpY19IZXJtaXRlX3NwbGluZSNDYXRtdWxsLkUyLjgwLjkzUm9tX3NwbGluZVxyXG4gKiBSZWxhdGl2ZSBjb29yZGluYXRlcy5cclxuICpcclxuICogQGNsYXNzIENhdG11bGxSb21CeTJcclxuICogQGV4dGVuZHMgQ2FyZGluYWxTcGxpbmVCeTJcclxuICpcclxuICogQHBhcmFtIHtOdW1iZXJ9IGR0XHJcbiAqIEBwYXJhbSB7QXJyYXl9IHBvaW50c1xyXG4gKlxyXG4gKiBAZXhhbXBsZVxyXG4gKiB2YXIgYWN0aW9uMSA9IGNjLkNhdG11bGxSb21CeTIoMywgYXJyYXkpO1xyXG4gKi9cclxuY2MuQ2F0bXVsbFJvbUJ5MiA9IGNjLkNsYXNzKHtcclxuICAgIG5hbWU6ICdjYy5DYXRtdWxsUm9tQnkyJyxcclxuICAgIGV4dGVuZHM6IGNjLkNhcmRpbmFsU3BsaW5lQnkyLFxyXG5cclxuICAgIGN0b3I6IGZ1bmN0aW9uKGR0LCBwb2ludHMsIGFuZ2xlT2Zmc2V0KSB7XHJcbiAgICAgICAgcG9pbnRzICYmIHRoaXMuaW5pdFdpdGhEdXJhdGlvbihkdCwgcG9pbnRzLCBhbmdsZU9mZnNldCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGluaXRXaXRoRHVyYXRpb246ZnVuY3Rpb24gKGR0LCBwb2ludHMsIGFuZ2xlT2Zmc2V0KSB7XHJcbiAgICAgICAgcmV0dXJuIGNjLkNhcmRpbmFsU3BsaW5lVG8yLnByb3RvdHlwZS5pbml0V2l0aER1cmF0aW9uLmNhbGwodGhpcywgZHQsIHBvaW50cywgYW5nbGVPZmZzZXQsIDAuNSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNsb25lOmZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgYWN0aW9uID0gbmV3IGNjLkNhdG11bGxSb21CeTIoKTtcclxuICAgICAgICBhY3Rpb24uaW5pdFdpdGhEdXJhdGlvbih0aGlzLl9kdXJhdGlvbiwgY2xvbmVDb250cm9sUG9pbnRzKHRoaXMuX3BvaW50cyksIHRoaXMuX2FuZ2xlT2Zmc2V0KTtcclxuICAgICAgICByZXR1cm4gYWN0aW9uO1xyXG4gICAgfVxyXG59KTtcclxuXHJcbi8qKlxyXG4gKiAhI2VuIENyZWF0ZXMgYW4gYWN0aW9uIHdpdGggYSBDYXJkaW5hbCBTcGxpbmUgYXJyYXkgb2YgcG9pbnRzIGFuZCB0ZW5zaW9uLlxyXG4gKiAhI3poIOaMiSBDYXRtdWxsIFJvbSDmoLfmnaHmm7Lnur/ovajov7nnp7vliqjmjIflrprnmoTot53nprvjgIJcclxuICogQG1ldGhvZCBDYXRtdWxsUm9tQnkyXHJcbiAqIEBwYXJhbSB7TnVtYmVyfSBkdFxyXG4gKiBAcGFyYW0ge0FycmF5fSBwb2ludHNcclxuICogQHBhcmFtIHtOdW1iZXJ9IGFuZ2xlT2Zmc2V0XHJcbiAqIEByZXR1cm4ge0FjdGlvbkludGVydmFsfVxyXG4gKiBAZXhhbXBsZVxyXG4gKiB2YXIgYWN0aW9uMSA9IGNjLkNhdG11bGxSb21CeTIoMywgYXJyYXksIDkwKTtcclxuICovXHJcbmNjLmNhdG11bGxSb21CeTIgPSBmdW5jdGlvbiAoZHQsIHBvaW50cywgYW5nbGVPZmZzZXQpIHtcclxuICAgIHJldHVybiBuZXcgY2MuQ2F0bXVsbFJvbUJ5MihkdCwgcG9pbnRzLCBhbmdsZU9mZnNldCk7XHJcbn07Il19