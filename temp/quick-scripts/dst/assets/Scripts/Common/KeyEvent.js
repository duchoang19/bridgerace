
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/KeyEvent.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'e49c6ej8eJKEZmMHqGedjtb', 'KeyEvent');
// Scripts/Common/KeyEvent.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var KeyEvent = {
    increaseStickMyCharacter: "increaseStickMyCharacter",
    increaseStickAI1: "increaseStickAI1",
    increaseStickAI2: "increaseStickAI2"
};
exports.default = KeyEvent;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxLZXlFdmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUtBLElBQUksUUFBUSxHQUNaO0lBQ0ksd0JBQXdCLEVBQUUsMEJBQTBCO0lBQ3BELGdCQUFnQixFQUFFLGtCQUFrQjtJQUNwQyxnQkFBZ0IsRUFBRSxrQkFBa0I7Q0FDdkMsQ0FBQTtBQUNELGtCQUFlLFFBQVEsQ0FBQSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImludGVyZmFjZSBLZXlFdmVudCB7XHJcbiAgICBpbmNyZWFzZVN0aWNrTXlDaGFyYWN0ZXI6IHN0cmluZyxcclxuICAgIGluY3JlYXNlU3RpY2tBSTE6IHN0cmluZyxcclxuICAgIGluY3JlYXNlU3RpY2tBSTI6IHN0cmluZyxcclxufVxyXG5sZXQgS2V5RXZlbnQ6IEtleUV2ZW50ID1cclxue1xyXG4gICAgaW5jcmVhc2VTdGlja015Q2hhcmFjdGVyOiBcImluY3JlYXNlU3RpY2tNeUNoYXJhY3RlclwiLFxyXG4gICAgaW5jcmVhc2VTdGlja0FJMTogXCJpbmNyZWFzZVN0aWNrQUkxXCIsXHJcbiAgICBpbmNyZWFzZVN0aWNrQUkyOiBcImluY3JlYXNlU3RpY2tBSTJcIlxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IEtleUV2ZW50Il19