
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/GamePlayController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '91971i5LkpN2r+KQEdz49a3', 'GamePlayController');
// Scripts/Common/GamePlayController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Singleton_1 = require("./Singleton");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GamePlayController = /** @class */ (function (_super) {
    __extends(GamePlayController, _super);
    function GamePlayController() {
        var _this = _super.call(this) || this;
        _this.materialBrickCharacter = null;
        _this.materialBrickAI = [];
        GamePlayController_1._instance = _this;
        return _this;
    }
    GamePlayController_1 = GamePlayController;
    var GamePlayController_1;
    __decorate([
        property(cc.Material)
    ], GamePlayController.prototype, "materialBrickCharacter", void 0);
    __decorate([
        property(cc.Material)
    ], GamePlayController.prototype, "materialBrickAI", void 0);
    GamePlayController = GamePlayController_1 = __decorate([
        ccclass
    ], GamePlayController);
    return GamePlayController;
}(Singleton_1.default));
exports.default = GamePlayController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxHYW1lUGxheUNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EseUNBQW9DO0FBQzlCLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFFNUM7SUFBZ0Qsc0NBQTZCO0lBS3pFO1FBQUEsWUFDSSxpQkFBTyxTQUVWO1FBTkQsNEJBQXNCLEdBQWdCLElBQUksQ0FBQztRQUUzQyxxQkFBZSxHQUFrQixFQUFFLENBQUM7UUFHaEMsb0JBQWtCLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQzs7SUFDeEMsQ0FBQzsyQkFSZ0Isa0JBQWtCOztJQUVuQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDO3NFQUNxQjtJQUUzQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDOytEQUNjO0lBSm5CLGtCQUFrQjtRQUR0QyxPQUFPO09BQ2Esa0JBQWtCLENBU3RDO0lBQUQseUJBQUM7Q0FURCxBQVNDLENBVCtDLG1CQUFTLEdBU3hEO2tCQVRvQixrQkFBa0IiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQlIxIGZyb20gXCIuLi9HYW1lUGxheS9CUjEvQlIxXCI7XHJcbmltcG9ydCBTaW5nbGV0b24gZnJvbSBcIi4vU2luZ2xldG9uXCI7XHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEdhbWVQbGF5Q29udHJvbGxlciBleHRlbmRzIFNpbmdsZXRvbjxHYW1lUGxheUNvbnRyb2xsZXI+IHtcclxuICAgIEBwcm9wZXJ0eShjYy5NYXRlcmlhbClcclxuICAgIG1hdGVyaWFsQnJpY2tDaGFyYWN0ZXI6IGNjLk1hdGVyaWFsID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5NYXRlcmlhbClcclxuICAgIG1hdGVyaWFsQnJpY2tBSTogY2MuTWF0ZXJpYWxbXSA9IFtdO1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBHYW1lUGxheUNvbnRyb2xsZXIuX2luc3RhbmNlID0gdGhpcztcclxuICAgIH1cclxufVxyXG4iXX0=