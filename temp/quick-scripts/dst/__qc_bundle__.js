
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/__qc_index__.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}
require('./assets/Scripts/Brick/BrickData');
require('./assets/Scripts/Brick/BrickHom');
require('./assets/Scripts/Brick/BrickStairs');
require('./assets/Scripts/Camera/CameraFollow');
require('./assets/Scripts/Common/AdManager');
require('./assets/Scripts/Common/EnableEngine');
require('./assets/Scripts/Common/EnumDefine');
require('./assets/Scripts/Common/GamePlayController');
require('./assets/Scripts/Common/GamePlayInstance');
require('./assets/Scripts/Common/Global');
require('./assets/Scripts/Common/KeyEvent');
require('./assets/Scripts/Common/PlatformBtn');
require('./assets/Scripts/Common/ScrollNew');
require('./assets/Scripts/Common/Singleton');
require('./assets/Scripts/Common/SoundManager');
require('./assets/Scripts/Common/SplineExtend');
require('./assets/Scripts/Common/Utility');
require('./assets/Scripts/Controller/AIController');
require('./assets/Scripts/Controller/BoxController');
require('./assets/Scripts/Controller/CharacterController');
require('./assets/Scripts/Controller/CheckMoveDownStairs');
require('./assets/Scripts/Controller/ColliderAttack');
require('./assets/Scripts/Controller/JoystickFollow');
require('./assets/Scripts/GamePlay/BR1/BR1');

                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Controller/AIController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '523563QNh5DXpN8/rDUWg9W', 'AIController');
// Scripts/Controller/AIController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var BrickData_1 = require("../Brick/BrickData");
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var Utility_1 = require("../Common/Utility");
var ColliderAttack_1 = require("./ColliderAttack");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var AIController = /** @class */ (function (_super) {
    __extends(AIController, _super);
    function AIController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.clampXLeft = -33;
        _this.clampXRight = 33;
        _this.clampYTop = 21;
        _this.clampYBottom = -18;
        _this.speedMove = 20;
        _this.colliderAttack = null;
        _this.brickHom = null;
        _this.brickStick = null;
        _this.brickType = EnumDefine_1.BrickType.BrickAI1;
        _this.stringIncrease = "";
        _this.moveX = 0;
        _this.moveY = 0;
        _this.level = 0;
        _this.listBrickAdd = [];
        _this.x = 0.003;
        _this.lastPositionStickY = 0.013;
        _this.z = -0.002;
        _this.boolCheckDeath = false;
        _this.boolCheckDeathOne = false;
        _this.boolCheckdelay = false;
        _this.boolEnemyRunningIdle = false;
        _this.boolEnemyFollowBox = false;
        _this.boolEnemyAttacking = false;
        return _this;
    }
    //rigidbody: cc.RigidBody3D;
    AIController.prototype.onEnable = function () {
        GamePlayInstance_1.eventDispatcher.on(this.stringIncrease, this.IncreaseStick, this);
    };
    AIController.prototype.onDisable = function () {
        GamePlayInstance_1.eventDispatcher.off(this.stringIncrease, this.IncreaseStick, this);
    };
    // start() {
    //     this.rigidbody = this.node.getComponent(cc.RigidBody3D);
    // }
    AIController.prototype.update = function () {
        //this.CheckingAttack();
        if (!this.boolCheckDeath) {
            if (Global_1.default.boolStartAttacking && Global_1.default.boolCheckAttacking) {
                var pos1 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[0].getPosition()));
                var pos2 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[1].getPosition()));
                var pos3 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[2].getPosition()));
                var direction1 = cc.v2(pos1.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, pos1.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var direction2 = cc.v2(pos2.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, pos2.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var degree = direction1.signAngle(direction2);
                degree = cc.misc.radiansToDegrees(degree);
                var posEnemy = cc.v2(this.node.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, this.node.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var degreeWithPos1 = posEnemy.signAngle(direction1);
                degreeWithPos1 = cc.misc.radiansToDegrees(degreeWithPos1);
                var degreeWithPos2 = posEnemy.signAngle(direction2);
                degreeWithPos2 = cc.misc.radiansToDegrees(degreeWithPos2);
                var realNeed = 0;
                degreeWithPos1 = Math.abs(degreeWithPos1);
                degreeWithPos2 = Math.abs(degreeWithPos2);
                if (degreeWithPos1 > degreeWithPos2) {
                    realNeed = degreeWithPos1;
                }
                else {
                    realNeed = degreeWithPos2;
                }
                var distance = Utility_1.default.Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y));
                var maxDistance = Utility_1.default.Distance(cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y), cc.v2(pos3.x, pos3.y));
                if (Math.abs(realNeed) < degree) {
                    if (distance < maxDistance) {
                        if (Global_1.default.boolCheckAttacked && !this.boolCheckDeathOne) {
                            this.EnemyFail();
                            this.boolCheckDeath = true;
                            this.boolCheckDeathOne = true;
                        }
                    }
                }
            }
        }
    };
    AIController.prototype.EnemyFail = function () {
        var _this = this;
        this.unscheduleAllCallbacks();
        this.node.stopAllActions();
        this.colliderAttack.getComponent(ColliderAttack_1.default).unscheduleAllCallbacks();
        this.node.getComponent(cc.SkeletonAnimation).play("Fall");
        this.SpawnerBrickWhenFall();
        this.scheduleOnce(function () {
            _this.node.getComponent(cc.SkeletonAnimation).play("Idle");
        }, 1.28);
        this.scheduleOnce(function () {
            _this.boolCheckDeath = false;
            _this.boolCheckDeathOne = false;
            _this.EnemyRunIdle();
        }, 1.6);
    };
    AIController.prototype.StartMove = function () {
        this.EnemyRunIdle();
    };
    AIController.prototype.EnemyRunIdle = function () {
        var _this = this;
        this.node.getComponent(cc.SkeletonAnimation).play("Run");
        this.boolEnemyRunningIdle = true;
        this.boolEnemyFollowBox = false;
        while (Utility_1.default.Distance(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) < 30) {
            this.moveX = Utility_1.default.RandomRangeFloat(this.clampXLeft, this.clampXRight);
            this.moveY = Utility_1.default.RandomRangeFloat(this.clampYBottom, this.clampYTop);
        }
        this.moveX = Utility_1.default.RandomRangeFloat(this.clampXLeft, this.clampXRight);
        this.moveY = Utility_1.default.RandomRangeFloat(this.clampYBottom, this.clampYTop);
        //let Direction = new cc.Vec2(this.moveX - this.node.x, this.moveY - this.node.y).normalize();
        //this.rigidbody.setLinearVelocity(new cc.Vec3(this.speedMove * Direction.x, this.speedMove * Direction.y, -10));
        //let degree = Utility.CaculatorDegree(Direction);
        //this.node.runAction(cc.rotate3DTo(0.2, cc.v3(-90, 180, degree)));
        var tween = new cc.Tween().to(2, { position: cc.v3(this.moveX, this.moveY, 0) }).call(function () {
            _this.EnemyRunIdle();
        });
        tween.target(this.node).start();
        //let degree = Utility.CaculatorDegree(cc.v2(this.moveX, this.moveY));
        var degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) - 90;
        this.node.runAction(cc.rotate3DTo(0.2, cc.v3(-90, -180, -degree)));
        this.scheduleOnce(function () {
            _this.node.stopAllActions();
            _this.node.getComponent(cc.SkeletonAnimation).play("Run");
            _this.EnemyRunIdle();
        }, 2.5);
    };
    AIController.prototype.SpawnerBrickWhenFall = function () {
        if (this.level > 0) {
            for (var i = 0; i < this.listBrickAdd.length; i++) {
                var x = Utility_1.default.RandomRangeFloat(-7, 7);
                var y = Utility_1.default.RandomRangeFloat(-7, 7);
                this.SpawnerBrick(x, y);
            }
            this.DecreaseStickAll();
        }
    };
    AIController.prototype.SpawnerBrick = function (x, y) {
        var Brick = cc.instantiate(this.brickHom);
        Brick.parent = cc.Canvas.instance.node;
        Brick.x = this.node.x;
        Brick.y = this.node.y;
        Brick.z = 1;
        var t = new cc.Tween().to(0.3, { position: new cc.Vec3(this.node.x + x, this.node.y + y, 0.5) })
            .target(Brick).start();
    };
    AIController.prototype.IncreaseStick = function () {
        var brick = cc.instantiate(this.brickStick);
        brick.parent = this.node.children[2];
        brick.x = this.x;
        brick.y = this.lastPositionStickY + 0.004;
        brick.z = this.z;
        brick.getComponent(BrickData_1.default).setmaterial(this.brickType);
        this.IncreaseLevel();
        this.lastPositionStickY = this.lastPositionStickY + 0.004;
        this.listBrickAdd.push(brick);
    };
    AIController.prototype.DecreaseStickAll = function () {
        for (var i = 0; i < this.listBrickAdd.length; i++) {
            this.listBrickAdd[i].destroy();
        }
        this.listBrickAdd.splice(0, this.listBrickAdd.length);
        this.lastPositionStickY = 0.013;
        this.node.children[0].scale = 1;
        this.DecreaseLevel();
    };
    AIController.prototype.IncreaseLevel = function () {
        this.node.children[0].scale += 0.1;
        this.level++;
    };
    AIController.prototype.DecreaseLevel = function () {
        this.level = 0;
    };
    AIController.prototype.betweenDegree = function (comVec, dirVec) {
        var angleDeg = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDeg;
    };
    __decorate([
        property(cc.Integer)
    ], AIController.prototype, "clampXLeft", void 0);
    __decorate([
        property(cc.Integer)
    ], AIController.prototype, "clampXRight", void 0);
    __decorate([
        property(cc.Integer)
    ], AIController.prototype, "clampYTop", void 0);
    __decorate([
        property(cc.Integer)
    ], AIController.prototype, "clampYBottom", void 0);
    __decorate([
        property(cc.Integer)
    ], AIController.prototype, "speedMove", void 0);
    __decorate([
        property(cc.Node)
    ], AIController.prototype, "colliderAttack", void 0);
    __decorate([
        property(cc.Prefab)
    ], AIController.prototype, "brickHom", void 0);
    __decorate([
        property(cc.Prefab)
    ], AIController.prototype, "brickStick", void 0);
    __decorate([
        property({ type: cc.Enum(EnumDefine_1.BrickType) })
    ], AIController.prototype, "brickType", void 0);
    __decorate([
        property(cc.String)
    ], AIController.prototype, "stringIncrease", void 0);
    AIController = __decorate([
        ccclass
    ], AIController);
    return AIController;
}(cc.Component));
exports.default = AIController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29udHJvbGxlclxcQUlDb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLGdEQUEyQztBQUMzQyxtREFBaUQ7QUFDakQsK0RBQStFO0FBQy9FLDJDQUFzQztBQUV0Qyw2Q0FBd0M7QUFFeEMsbURBQThDO0FBRXhDLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFHNUM7SUFBMEMsZ0NBQVk7SUFEdEQ7UUFBQSxxRUFxTEM7UUFsTEcsZ0JBQVUsR0FBVyxDQUFDLEVBQUUsQ0FBQztRQUV6QixpQkFBVyxHQUFXLEVBQUUsQ0FBQztRQUV6QixlQUFTLEdBQVcsRUFBRSxDQUFDO1FBRXZCLGtCQUFZLEdBQVcsQ0FBQyxFQUFFLENBQUM7UUFFM0IsZUFBUyxHQUFXLEVBQUUsQ0FBQztRQUV2QixvQkFBYyxHQUFZLElBQUksQ0FBQztRQUUvQixjQUFRLEdBQWMsSUFBSSxDQUFDO1FBRTNCLGdCQUFVLEdBQWMsSUFBSSxDQUFDO1FBRXRCLGVBQVMsR0FBYyxzQkFBUyxDQUFDLFFBQVEsQ0FBQztRQUVqRCxvQkFBYyxHQUFXLEVBQUUsQ0FBQztRQUM1QixXQUFLLEdBQVcsQ0FBQyxDQUFDO1FBQ2xCLFdBQUssR0FBVyxDQUFDLENBQUM7UUFDbEIsV0FBSyxHQUFXLENBQUMsQ0FBQztRQUNsQixrQkFBWSxHQUFjLEVBQUUsQ0FBQztRQUM3QixPQUFDLEdBQVcsS0FBSyxDQUFDO1FBQ2xCLHdCQUFrQixHQUFXLEtBQUssQ0FBQztRQUNuQyxPQUFDLEdBQVcsQ0FBQyxLQUFLLENBQUM7UUFDbkIsb0JBQWMsR0FBWSxLQUFLLENBQUM7UUFDaEMsdUJBQWlCLEdBQVksS0FBSyxDQUFDO1FBQ25DLG9CQUFjLEdBQVksS0FBSyxDQUFDO1FBQ2hDLDBCQUFvQixHQUFZLEtBQUssQ0FBQztRQUN0Qyx3QkFBa0IsR0FBWSxLQUFLLENBQUM7UUFDcEMsd0JBQWtCLEdBQVksS0FBSyxDQUFDOztJQW1KeEMsQ0FBQztJQWxKRyw0QkFBNEI7SUFDNUIsK0JBQVEsR0FBUjtRQUNJLGtDQUFlLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN0RSxDQUFDO0lBQ0QsZ0NBQVMsR0FBVDtRQUNJLGtDQUFlLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBQ0QsWUFBWTtJQUNaLCtEQUErRDtJQUMvRCxJQUFJO0lBQ0osNkJBQU0sR0FBTjtRQUNJLHdCQUF3QjtRQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN0QixJQUFJLGdCQUFNLENBQUMsa0JBQWtCLElBQUksZ0JBQU0sQ0FBQyxrQkFBa0IsRUFBRTtnQkFDeEQsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BRLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNwUSxJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFFcFEsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakwsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakwsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDOUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzFDLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekwsSUFBSSxjQUFjLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDcEQsY0FBYyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzFELElBQUksY0FBYyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3BELGNBQWMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUMxRCxJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUM7Z0JBQ2pCLGNBQWMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUMxQyxjQUFjLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDMUMsSUFBSSxjQUFjLEdBQUcsY0FBYyxFQUFFO29CQUNqQyxRQUFRLEdBQUcsY0FBYyxDQUFDO2lCQUM3QjtxQkFDSTtvQkFDRCxRQUFRLEdBQUcsY0FBYyxDQUFDO2lCQUM3QjtnQkFDRCxJQUFJLFFBQVEsR0FBRyxpQkFBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSwwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hOLElBQUksV0FBVyxHQUFHLGlCQUFPLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pNLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsR0FBRyxNQUFNLEVBQUU7b0JBQzdCLElBQUksUUFBUSxHQUFHLFdBQVcsRUFBRTt3QkFDeEIsSUFBSSxnQkFBTSxDQUFDLGlCQUFpQixJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFOzRCQUNyRCxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7NEJBQ2pCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDOzRCQUMzQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO3lCQUNqQztxQkFDSjtpQkFDSjthQUNKO1NBQ0o7SUFDTCxDQUFDO0lBQ0QsZ0NBQVMsR0FBVDtRQUFBLGlCQWNDO1FBYkcsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyx3QkFBYyxDQUFDLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUMxRSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNkLEtBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5RCxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDVCxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7WUFDNUIsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztZQUMvQixLQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDeEIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ1osQ0FBQztJQUNELGdDQUFTLEdBQVQ7UUFDSSxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDeEIsQ0FBQztJQUNELG1DQUFZLEdBQVo7UUFBQSxpQkEyQkM7UUExQkcsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUNoQyxPQUFPLGlCQUFPLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBQzFGLElBQUksQ0FBQyxLQUFLLEdBQUcsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN6RSxJQUFJLENBQUMsS0FBSyxHQUFHLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDNUU7UUFDRCxJQUFJLENBQUMsS0FBSyxHQUFHLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLEtBQUssR0FBRyxpQkFBTyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3pFLDhGQUE4RjtRQUM5RixpSEFBaUg7UUFDakgsa0RBQWtEO1FBQ2xELG1FQUFtRTtRQUVuRSxJQUFJLEtBQUssR0FBRyxJQUFJLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDbEYsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDaEMsc0VBQXNFO1FBQ3RFLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDckcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUMzQixLQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDekQsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3hCLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNaLENBQUM7SUFDRCwyQ0FBb0IsR0FBcEI7UUFDSSxJQUFJLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFO1lBQ2hCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDL0MsSUFBSSxDQUFDLEdBQUcsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLEdBQUcsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7YUFDMUI7WUFDRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztTQUMzQjtJQUNMLENBQUM7SUFDRCxtQ0FBWSxHQUFaLFVBQWEsQ0FBUyxFQUFFLENBQVM7UUFDN0IsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDMUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDdkMsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUN0QixLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ3RCLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ1osSUFBSSxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDO2FBQzNGLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBQ0Qsb0NBQWEsR0FBYjtRQUNJLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzVDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDckMsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ2pCLEtBQUssQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUMxQyxLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDakIsS0FBSyxDQUFDLFlBQVksQ0FBQyxtQkFBUyxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMxRCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7UUFDMUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUNELHVDQUFnQixHQUFoQjtRQUNJLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMvQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ2xDO1FBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN6QixDQUFDO0lBQ0Qsb0NBQWEsR0FBYjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxHQUFHLENBQUM7UUFDbkMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2pCLENBQUM7SUFDRCxvQ0FBYSxHQUFiO1FBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7SUFDbkIsQ0FBQztJQUNELG9DQUFhLEdBQWIsVUFBYyxNQUFNLEVBQUUsTUFBTTtRQUN4QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNwRixPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDO0lBakxEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7b0RBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztxREFDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO21EQUNFO0lBRXZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7c0RBQ007SUFFM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQzttREFDRTtJQUV2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3dEQUNhO0lBRS9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7a0RBQ087SUFFM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztvREFDUztJQUU3QjtRQURDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLHNCQUFTLENBQUMsRUFBRSxDQUFDO21EQUNVO0lBRWpEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7d0RBQ1E7SUFwQlgsWUFBWTtRQURoQyxPQUFPO09BQ2EsWUFBWSxDQW9MaEM7SUFBRCxtQkFBQztDQXBMRCxBQW9MQyxDQXBMeUMsRUFBRSxDQUFDLFNBQVMsR0FvTHJEO2tCQXBMb0IsWUFBWSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBCcmlja0RhdGEgZnJvbSBcIi4uL0JyaWNrL0JyaWNrRGF0YVwiO1xyXG5pbXBvcnQgeyBCcmlja1R5cGUgfSBmcm9tIFwiLi4vQ29tbW9uL0VudW1EZWZpbmVcIjtcclxuaW1wb3J0IEdhbWVQbGF5SW5zdGFuY2UsIHsgZXZlbnREaXNwYXRjaGVyIH0gZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFV0aWxpdHkgZnJvbSBcIi4uL0NvbW1vbi9VdGlsaXR5XCI7XHJcbmltcG9ydCBCb3hDb250cm9sbGVyIGZyb20gXCIuL0JveENvbnRyb2xsZXJcIjtcclxuaW1wb3J0IENvbGxpZGVyQXR0YWNrIGZyb20gXCIuL0NvbGxpZGVyQXR0YWNrXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQUlDb250cm9sbGVyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBYTGVmdDogbnVtYmVyID0gLTMzO1xyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjbGFtcFhSaWdodDogbnVtYmVyID0gMzM7XHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIGNsYW1wWVRvcDogbnVtYmVyID0gMjE7XHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIGNsYW1wWUJvdHRvbTogbnVtYmVyID0gLTE4O1xyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBzcGVlZE1vdmU6IG51bWJlciA9IDIwO1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBjb2xsaWRlckF0dGFjazogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgYnJpY2tIb206IGNjLlByZWZhYiA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgYnJpY2tTdGljazogY2MuUHJlZmFiID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLkVudW0oQnJpY2tUeXBlKSB9KVxyXG4gICAgcHVibGljIGJyaWNrVHlwZTogQnJpY2tUeXBlID0gQnJpY2tUeXBlLkJyaWNrQUkxO1xyXG4gICAgQHByb3BlcnR5KGNjLlN0cmluZylcclxuICAgIHN0cmluZ0luY3JlYXNlOiBzdHJpbmcgPSBcIlwiO1xyXG4gICAgbW92ZVg6IG51bWJlciA9IDA7XHJcbiAgICBtb3ZlWTogbnVtYmVyID0gMDtcclxuICAgIGxldmVsOiBudW1iZXIgPSAwO1xyXG4gICAgbGlzdEJyaWNrQWRkOiBjYy5Ob2RlW10gPSBbXTtcclxuICAgIHg6IG51bWJlciA9IDAuMDAzO1xyXG4gICAgbGFzdFBvc2l0aW9uU3RpY2tZOiBudW1iZXIgPSAwLjAxMztcclxuICAgIHo6IG51bWJlciA9IC0wLjAwMjtcclxuICAgIGJvb2xDaGVja0RlYXRoOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBib29sQ2hlY2tEZWF0aE9uZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgYm9vbENoZWNrZGVsYXk6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGJvb2xFbmVteVJ1bm5pbmdJZGxlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBib29sRW5lbXlGb2xsb3dCb3g6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGJvb2xFbmVteUF0dGFja2luZzogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgLy9yaWdpZGJvZHk6IGNjLlJpZ2lkQm9keTNEO1xyXG4gICAgb25FbmFibGUoKSB7XHJcbiAgICAgICAgZXZlbnREaXNwYXRjaGVyLm9uKHRoaXMuc3RyaW5nSW5jcmVhc2UsIHRoaXMuSW5jcmVhc2VTdGljaywgdGhpcyk7XHJcbiAgICB9XHJcbiAgICBvbkRpc2FibGUoKSB7XHJcbiAgICAgICAgZXZlbnREaXNwYXRjaGVyLm9mZih0aGlzLnN0cmluZ0luY3JlYXNlLCB0aGlzLkluY3JlYXNlU3RpY2ssIHRoaXMpO1xyXG4gICAgfVxyXG4gICAgLy8gc3RhcnQoKSB7XHJcbiAgICAvLyAgICAgdGhpcy5yaWdpZGJvZHkgPSB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlJpZ2lkQm9keTNEKTtcclxuICAgIC8vIH1cclxuICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICAvL3RoaXMuQ2hlY2tpbmdBdHRhY2soKTtcclxuICAgICAgICBpZiAoIXRoaXMuYm9vbENoZWNrRGVhdGgpIHtcclxuICAgICAgICAgICAgaWYgKEdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcgJiYgR2xvYmFsLmJvb2xDaGVja0F0dGFja2luZykge1xyXG4gICAgICAgICAgICAgICAgbGV0IHBvczEgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmNoaWxkcmVuWzBdLmNvbnZlcnRUb1dvcmxkU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmNoaWxkcmVuWzBdLmNoaWxkcmVuWzBdLmdldFBvc2l0aW9uKCkpKTtcclxuICAgICAgICAgICAgICAgIGxldCBwb3MyID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGUuY29udmVydFRvTm9kZVNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci5jaGlsZHJlblswXS5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci5jaGlsZHJlblswXS5jaGlsZHJlblsxXS5nZXRQb3NpdGlvbigpKSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgcG9zMyA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuY2hpbGRyZW5bMF0uY29udmVydFRvV29ybGRTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuY2hpbGRyZW5bMF0uY2hpbGRyZW5bMl0uZ2V0UG9zaXRpb24oKSkpO1xyXG5cclxuICAgICAgICAgICAgICAgIGxldCBkaXJlY3Rpb24xID0gY2MudjIocG9zMS54IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci54LCBwb3MxLnkgLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnkpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGRpcmVjdGlvbjIgPSBjYy52Mihwb3MyLnggLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLngsIHBvczIueSAtIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGVncmVlID0gZGlyZWN0aW9uMS5zaWduQW5nbGUoZGlyZWN0aW9uMik7XHJcbiAgICAgICAgICAgICAgICBkZWdyZWUgPSBjYy5taXNjLnJhZGlhbnNUb0RlZ3JlZXMoZGVncmVlKTtcclxuICAgICAgICAgICAgICAgIGxldCBwb3NFbmVteSA9IGNjLnYyKHRoaXMubm9kZS54IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci54LCB0aGlzLm5vZGUueSAtIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGVncmVlV2l0aFBvczEgPSBwb3NFbmVteS5zaWduQW5nbGUoZGlyZWN0aW9uMSk7XHJcbiAgICAgICAgICAgICAgICBkZWdyZWVXaXRoUG9zMSA9IGNjLm1pc2MucmFkaWFuc1RvRGVncmVlcyhkZWdyZWVXaXRoUG9zMSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGVncmVlV2l0aFBvczIgPSBwb3NFbmVteS5zaWduQW5nbGUoZGlyZWN0aW9uMik7XHJcbiAgICAgICAgICAgICAgICBkZWdyZWVXaXRoUG9zMiA9IGNjLm1pc2MucmFkaWFuc1RvRGVncmVlcyhkZWdyZWVXaXRoUG9zMik7XHJcbiAgICAgICAgICAgICAgICBsZXQgcmVhbE5lZWQgPSAwO1xyXG4gICAgICAgICAgICAgICAgZGVncmVlV2l0aFBvczEgPSBNYXRoLmFicyhkZWdyZWVXaXRoUG9zMSk7XHJcbiAgICAgICAgICAgICAgICBkZWdyZWVXaXRoUG9zMiA9IE1hdGguYWJzKGRlZ3JlZVdpdGhQb3MyKTtcclxuICAgICAgICAgICAgICAgIGlmIChkZWdyZWVXaXRoUG9zMSA+IGRlZ3JlZVdpdGhQb3MyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVhbE5lZWQgPSBkZWdyZWVXaXRoUG9zMTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlYWxOZWVkID0gZGVncmVlV2l0aFBvczI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBsZXQgZGlzdGFuY2UgPSBVdGlsaXR5LkRpc3RhbmNlKGNjLnYyKHRoaXMubm9kZS54LCB0aGlzLm5vZGUueSksIGNjLnYyKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueCwgR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci55KSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgbWF4RGlzdGFuY2UgPSBVdGlsaXR5LkRpc3RhbmNlKGNjLnYyKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueCwgR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci55KSwgY2MudjIocG9zMy54LCBwb3MzLnkpKTtcclxuICAgICAgICAgICAgICAgIGlmIChNYXRoLmFicyhyZWFsTmVlZCkgPCBkZWdyZWUpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZGlzdGFuY2UgPCBtYXhEaXN0YW5jZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoR2xvYmFsLmJvb2xDaGVja0F0dGFja2VkICYmICF0aGlzLmJvb2xDaGVja0RlYXRoT25lKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLkVuZW15RmFpbCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ib29sQ2hlY2tEZWF0aCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmJvb2xDaGVja0RlYXRoT25lID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIEVuZW15RmFpbCgpIHtcclxuICAgICAgICB0aGlzLnVuc2NoZWR1bGVBbGxDYWxsYmFja3MoKTtcclxuICAgICAgICB0aGlzLm5vZGUuc3RvcEFsbEFjdGlvbnMoKTtcclxuICAgICAgICB0aGlzLmNvbGxpZGVyQXR0YWNrLmdldENvbXBvbmVudChDb2xsaWRlckF0dGFjaykudW5zY2hlZHVsZUFsbENhbGxiYWNrcygpO1xyXG4gICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJGYWxsXCIpO1xyXG4gICAgICAgIHRoaXMuU3Bhd25lckJyaWNrV2hlbkZhbGwoKTtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJJZGxlXCIpO1xyXG4gICAgICAgIH0sIDEuMjgpO1xyXG4gICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5ib29sQ2hlY2tEZWF0aCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmJvb2xDaGVja0RlYXRoT25lID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuRW5lbXlSdW5JZGxlKCk7XHJcbiAgICAgICAgfSwgMS42KTtcclxuICAgIH1cclxuICAgIFN0YXJ0TW92ZSgpIHtcclxuICAgICAgICB0aGlzLkVuZW15UnVuSWRsZSgpO1xyXG4gICAgfVxyXG4gICAgRW5lbXlSdW5JZGxlKCkge1xyXG4gICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJSdW5cIik7XHJcbiAgICAgICAgdGhpcy5ib29sRW5lbXlSdW5uaW5nSWRsZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5ib29sRW5lbXlGb2xsb3dCb3ggPSBmYWxzZTtcclxuICAgICAgICB3aGlsZSAoVXRpbGl0eS5EaXN0YW5jZShjYy52Mih0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnkpLCBjYy52Mih0aGlzLm1vdmVYLCB0aGlzLm1vdmVZKSkgPCAzMCkge1xyXG4gICAgICAgICAgICB0aGlzLm1vdmVYID0gVXRpbGl0eS5SYW5kb21SYW5nZUZsb2F0KHRoaXMuY2xhbXBYTGVmdCwgdGhpcy5jbGFtcFhSaWdodCk7XHJcbiAgICAgICAgICAgIHRoaXMubW92ZVkgPSBVdGlsaXR5LlJhbmRvbVJhbmdlRmxvYXQodGhpcy5jbGFtcFlCb3R0b20sIHRoaXMuY2xhbXBZVG9wKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5tb3ZlWCA9IFV0aWxpdHkuUmFuZG9tUmFuZ2VGbG9hdCh0aGlzLmNsYW1wWExlZnQsIHRoaXMuY2xhbXBYUmlnaHQpO1xyXG4gICAgICAgIHRoaXMubW92ZVkgPSBVdGlsaXR5LlJhbmRvbVJhbmdlRmxvYXQodGhpcy5jbGFtcFlCb3R0b20sIHRoaXMuY2xhbXBZVG9wKTtcclxuICAgICAgICAvL2xldCBEaXJlY3Rpb24gPSBuZXcgY2MuVmVjMih0aGlzLm1vdmVYIC0gdGhpcy5ub2RlLngsIHRoaXMubW92ZVkgLSB0aGlzLm5vZGUueSkubm9ybWFsaXplKCk7XHJcbiAgICAgICAgLy90aGlzLnJpZ2lkYm9keS5zZXRMaW5lYXJWZWxvY2l0eShuZXcgY2MuVmVjMyh0aGlzLnNwZWVkTW92ZSAqIERpcmVjdGlvbi54LCB0aGlzLnNwZWVkTW92ZSAqIERpcmVjdGlvbi55LCAtMTApKTtcclxuICAgICAgICAvL2xldCBkZWdyZWUgPSBVdGlsaXR5LkNhY3VsYXRvckRlZ3JlZShEaXJlY3Rpb24pO1xyXG4gICAgICAgIC8vdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5yb3RhdGUzRFRvKDAuMiwgY2MudjMoLTkwLCAxODAsIGRlZ3JlZSkpKTtcclxuXHJcbiAgICAgICAgdmFyIHR3ZWVuID0gbmV3IGNjLlR3ZWVuKCkudG8oMiwgeyBwb3NpdGlvbjogY2MudjModGhpcy5tb3ZlWCwgdGhpcy5tb3ZlWSwgMCkgfSkuY2FsbCgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuRW5lbXlSdW5JZGxlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdHdlZW4udGFyZ2V0KHRoaXMubm9kZSkuc3RhcnQoKTtcclxuICAgICAgICAvL2xldCBkZWdyZWUgPSBVdGlsaXR5LkNhY3VsYXRvckRlZ3JlZShjYy52Mih0aGlzLm1vdmVYLCB0aGlzLm1vdmVZKSk7XHJcbiAgICAgICAgbGV0IGRlZ3JlZSA9IHRoaXMuYmV0d2VlbkRlZ3JlZShjYy52Mih0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnkpLCBjYy52Mih0aGlzLm1vdmVYLCB0aGlzLm1vdmVZKSkgLSA5MDtcclxuICAgICAgICB0aGlzLm5vZGUucnVuQWN0aW9uKGNjLnJvdGF0ZTNEVG8oMC4yLCBjYy52MygtOTAsIC0xODAsIC1kZWdyZWUpKSk7XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuc3RvcEFsbEFjdGlvbnMoKTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIlJ1blwiKTtcclxuICAgICAgICAgICAgdGhpcy5FbmVteVJ1bklkbGUoKTtcclxuICAgICAgICB9LCAyLjUpO1xyXG4gICAgfVxyXG4gICAgU3Bhd25lckJyaWNrV2hlbkZhbGwoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMubGV2ZWwgPiAwKSB7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5saXN0QnJpY2tBZGQubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGxldCB4ID0gVXRpbGl0eS5SYW5kb21SYW5nZUZsb2F0KC03LCA3KTtcclxuICAgICAgICAgICAgICAgIGxldCB5ID0gVXRpbGl0eS5SYW5kb21SYW5nZUZsb2F0KC03LCA3KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuU3Bhd25lckJyaWNrKHgsIHkpXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5EZWNyZWFzZVN0aWNrQWxsKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgU3Bhd25lckJyaWNrKHg6IG51bWJlciwgeTogbnVtYmVyKSB7XHJcbiAgICAgICAgbGV0IEJyaWNrID0gY2MuaW5zdGFudGlhdGUodGhpcy5icmlja0hvbSk7XHJcbiAgICAgICAgQnJpY2sucGFyZW50ID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGU7XHJcbiAgICAgICAgQnJpY2sueCA9IHRoaXMubm9kZS54O1xyXG4gICAgICAgIEJyaWNrLnkgPSB0aGlzLm5vZGUueTtcclxuICAgICAgICBCcmljay56ID0gMTtcclxuICAgICAgICBsZXQgdCA9IG5ldyBjYy5Ud2VlbigpLnRvKDAuMywgeyBwb3NpdGlvbjogbmV3IGNjLlZlYzModGhpcy5ub2RlLnggKyB4LCB0aGlzLm5vZGUueSArIHksIDAuNSkgfSlcclxuICAgICAgICAgICAgLnRhcmdldChCcmljaykuc3RhcnQoKTtcclxuICAgIH1cclxuICAgIEluY3JlYXNlU3RpY2soKSB7XHJcbiAgICAgICAgbGV0IGJyaWNrID0gY2MuaW5zdGFudGlhdGUodGhpcy5icmlja1N0aWNrKTtcclxuICAgICAgICBicmljay5wYXJlbnQgPSB0aGlzLm5vZGUuY2hpbGRyZW5bMl07XHJcbiAgICAgICAgYnJpY2sueCA9IHRoaXMueDtcclxuICAgICAgICBicmljay55ID0gdGhpcy5sYXN0UG9zaXRpb25TdGlja1kgKyAwLjAwNDtcclxuICAgICAgICBicmljay56ID0gdGhpcy56O1xyXG4gICAgICAgIGJyaWNrLmdldENvbXBvbmVudChCcmlja0RhdGEpLnNldG1hdGVyaWFsKHRoaXMuYnJpY2tUeXBlKTtcclxuICAgICAgICB0aGlzLkluY3JlYXNlTGV2ZWwoKTtcclxuICAgICAgICB0aGlzLmxhc3RQb3NpdGlvblN0aWNrWSA9IHRoaXMubGFzdFBvc2l0aW9uU3RpY2tZICsgMC4wMDQ7XHJcbiAgICAgICAgdGhpcy5saXN0QnJpY2tBZGQucHVzaChicmljayk7XHJcbiAgICB9XHJcbiAgICBEZWNyZWFzZVN0aWNrQWxsKCkge1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5saXN0QnJpY2tBZGQubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5saXN0QnJpY2tBZGRbaV0uZGVzdHJveSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmxpc3RCcmlja0FkZC5zcGxpY2UoMCwgdGhpcy5saXN0QnJpY2tBZGQubGVuZ3RoKTtcclxuICAgICAgICB0aGlzLmxhc3RQb3NpdGlvblN0aWNrWSA9IDAuMDEzO1xyXG4gICAgICAgIHRoaXMubm9kZS5jaGlsZHJlblswXS5zY2FsZSA9IDE7XHJcbiAgICAgICAgdGhpcy5EZWNyZWFzZUxldmVsKCk7XHJcbiAgICB9XHJcbiAgICBJbmNyZWFzZUxldmVsKCkge1xyXG4gICAgICAgIHRoaXMubm9kZS5jaGlsZHJlblswXS5zY2FsZSArPSAwLjE7XHJcbiAgICAgICAgdGhpcy5sZXZlbCsrO1xyXG4gICAgfVxyXG4gICAgRGVjcmVhc2VMZXZlbCgpIHtcclxuICAgICAgICB0aGlzLmxldmVsID0gMDtcclxuICAgIH1cclxuICAgIGJldHdlZW5EZWdyZWUoY29tVmVjLCBkaXJWZWMpIHtcclxuICAgICAgICBsZXQgYW5nbGVEZWcgPSBNYXRoLmF0YW4yKGRpclZlYy55IC0gY29tVmVjLnksIGRpclZlYy54IC0gY29tVmVjLngpICogMTgwIC8gTWF0aC5QSTtcclxuICAgICAgICByZXR1cm4gYW5nbGVEZWc7XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Brick/BrickData.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c0611MLpDRDp4LPc1DY52go', 'BrickData');
// Scripts/Brick/BrickData.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayController_1 = require("../Common/GamePlayController");
var BrickData = /** @class */ (function (_super) {
    __extends(BrickData, _super);
    function BrickData() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.mesh = null;
        return _this;
    }
    BrickData.prototype.setmaterial = function (materialType) {
        var index = 0;
        if (materialType == EnumDefine_1.BrickType.BrickCharacter)
            this.mesh.setMaterial(0, GamePlayController_1.default.Instance(GamePlayController_1.default).materialBrickCharacter);
        else if (materialType == EnumDefine_1.BrickType.BrickAI1)
            this.mesh.setMaterial(0, GamePlayController_1.default.Instance(GamePlayController_1.default).materialBrickAI[0]);
        else if (materialType == EnumDefine_1.BrickType.BrickAI2)
            this.mesh.setMaterial(0, GamePlayController_1.default.Instance(GamePlayController_1.default).materialBrickAI[1]);
    };
    __decorate([
        property(cc.MeshRenderer)
    ], BrickData.prototype, "mesh", void 0);
    BrickData = __decorate([
        ccclass
    ], BrickData);
    return BrickData;
}(cc.Component));
exports.default = BrickData;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQnJpY2tcXEJyaWNrRGF0YS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBTSxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBQzVDLG1EQUFpRDtBQUNqRCxtRUFBOEQ7QUFFOUQ7SUFBdUMsNkJBQVk7SUFEbkQ7UUFBQSxxRUFjQztRQVhHLFVBQUksR0FBb0IsSUFBSSxDQUFDOztJQVdqQyxDQUFDO0lBVkcsK0JBQVcsR0FBWCxVQUFZLFlBQXVCO1FBQy9CLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNkLElBQUksWUFBWSxJQUFJLHNCQUFTLENBQUMsY0FBYztZQUN4QyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsNEJBQWtCLENBQUMsUUFBUSxDQUFDLDRCQUFrQixDQUFDLENBQUMsc0JBQXNCLENBQUMsQ0FBQzthQUNoRyxJQUFJLFlBQVksSUFBSSxzQkFBUyxDQUFDLFFBQVE7WUFDdkMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLDRCQUFrQixDQUFDLFFBQVEsQ0FBQyw0QkFBa0IsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzVGLElBQUksWUFBWSxJQUFJLHNCQUFTLENBQUMsUUFBUTtZQUN2QyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsNEJBQWtCLENBQUMsUUFBUSxDQUFDLDRCQUFrQixDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDckcsQ0FBQztJQVREO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUM7MkNBQ0c7SUFGWixTQUFTO1FBRDdCLE9BQU87T0FDYSxTQUFTLENBYTdCO0lBQUQsZ0JBQUM7Q0FiRCxBQWFDLENBYnNDLEVBQUUsQ0FBQyxTQUFTLEdBYWxEO2tCQWJvQixTQUFTIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcclxuaW1wb3J0IHsgQnJpY2tUeXBlIH0gZnJvbSBcIi4uL0NvbW1vbi9FbnVtRGVmaW5lXCI7XHJcbmltcG9ydCBHYW1lUGxheUNvbnRyb2xsZXIgZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUNvbnRyb2xsZXJcIjtcclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQnJpY2tEYXRhIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICAgIEBwcm9wZXJ0eShjYy5NZXNoUmVuZGVyZXIpXHJcbiAgICBtZXNoOiBjYy5NZXNoUmVuZGVyZXIgPSBudWxsO1xyXG4gICAgc2V0bWF0ZXJpYWwobWF0ZXJpYWxUeXBlOiBCcmlja1R5cGUpIHtcclxuICAgICAgICBsZXQgaW5kZXggPSAwO1xyXG4gICAgICAgIGlmIChtYXRlcmlhbFR5cGUgPT0gQnJpY2tUeXBlLkJyaWNrQ2hhcmFjdGVyKVxyXG4gICAgICAgICAgICB0aGlzLm1lc2guc2V0TWF0ZXJpYWwoMCwgR2FtZVBsYXlDb250cm9sbGVyLkluc3RhbmNlKEdhbWVQbGF5Q29udHJvbGxlcikubWF0ZXJpYWxCcmlja0NoYXJhY3Rlcik7XHJcbiAgICAgICAgZWxzZSBpZiAobWF0ZXJpYWxUeXBlID09IEJyaWNrVHlwZS5Ccmlja0FJMSlcclxuICAgICAgICAgICAgdGhpcy5tZXNoLnNldE1hdGVyaWFsKDAsIEdhbWVQbGF5Q29udHJvbGxlci5JbnN0YW5jZShHYW1lUGxheUNvbnRyb2xsZXIpLm1hdGVyaWFsQnJpY2tBSVswXSk7XHJcbiAgICAgICAgZWxzZSBpZiAobWF0ZXJpYWxUeXBlID09IEJyaWNrVHlwZS5Ccmlja0FJMilcclxuICAgICAgICAgICAgdGhpcy5tZXNoLnNldE1hdGVyaWFsKDAsIEdhbWVQbGF5Q29udHJvbGxlci5JbnN0YW5jZShHYW1lUGxheUNvbnRyb2xsZXIpLm1hdGVyaWFsQnJpY2tBSVsxXSk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Controller/CharacterController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '56058bsB5JCBYLEHDj4omnN', 'CharacterController');
// Scripts/Controller/CharacterController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var BrickData_1 = require("../Brick/BrickData");
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Utility_1 = require("../Common/Utility");
var CheckMoveDownStairs_1 = require("./CheckMoveDownStairs");
var JoystickFollow_1 = require("./JoystickFollow");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CharacterController = /** @class */ (function (_super) {
    __extends(CharacterController, _super);
    function CharacterController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ArrowDirection = null;
        _this.speedMove = 20;
        _this.timeAnim = 0;
        _this.placeVfxWeapon = null;
        _this.brickHom = null;
        _this.brickStick = null;
        _this.checkMoveDownStairs = null;
        _this.brickType = EnumDefine_1.BrickType.BrickCharacter;
        _this.level = 0;
        _this.x = 0.003;
        _this.lastPositionStickY = 0.013;
        _this.z = -0.002;
        _this.clampTopY = 0;
        _this.listBrickAdd = [];
        _this.boolCanAttack = true;
        _this.boolMoveInFloor = false;
        _this.boolPlaySoundFoot = false;
        _this.boolCheckTypeRotate = false;
        _this.rigidbody = null;
        return _this;
    }
    CharacterController.prototype.onEnable = function () {
        GamePlayInstance_1.eventDispatcher.on(KeyEvent_1.default.increaseStickMyCharacter, this.IncreaseStick, this);
    };
    CharacterController.prototype.onDisable = function () {
        GamePlayInstance_1.eventDispatcher.off(KeyEvent_1.default.increaseStickMyCharacter, this.IncreaseStick, this);
    };
    CharacterController.prototype.start = function () {
        this.rigidbody = this.node.getComponent(cc.RigidBody3D);
        var collider = this.getComponent(cc.Collider3D);
        collider.on('collision-stay', this.onCollisionStay, this);
        this.listBrickAdd.splice(0, this.listBrickAdd.length);
    };
    CharacterController.prototype.onCollisionStay = function (event) {
        if (event.otherCollider.node.group == "Floor") {
            this.boolMoveInFloor = true;
            this.boolCanAttack = true;
        }
        if (event.otherCollider.node.group == "Model") {
            if (event.otherCollider.node.name == "StairsBoxCollider") {
                this.boolCanAttack = false;
            }
        }
    };
    CharacterController.prototype.onCollisionExit = function (event) {
        this.boolMoveInFloor = false;
    };
    CharacterController.prototype.update = function (dt) {
        if (Global_1.default.boolEnableTouch) {
            if (!this.checkMoveDownStairs.getComponent(CheckMoveDownStairs_1.default).boolMoveDown && this.boolMoveInFloor)
                this.MovePhyics(0);
            else
                this.MovePhyics(-20);
        }
        this.ClampTopYByLevel();
    };
    CharacterController.prototype.Attacking = function () {
        var _this = this;
        if (!Global_1.default.boolStartAttacking) {
            if (this.boolCanAttack) {
                Global_1.default.boolStartAttacking = true;
                Global_1.default.boolCheckAttacking = false;
                this.node.getComponent(cc.SkeletonAnimation).play("Attack");
                cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
                this.scheduleOnce(function () {
                    Global_1.default.boolCheckAttacking = true;
                    Global_1.default.boolCheckAttacked = true;
                }, 0.18);
                this.scheduleOnce(function () {
                    Global_1.default.boolStartAttacking = false;
                    Global_1.default.boolFirstTouchJoyStick = false;
                    _this.node.getComponent(cc.SkeletonAnimation).play("Idle");
                }, this.timeAnim);
            }
            else {
                this.node.getComponent(cc.SkeletonAnimation).play("Idle");
                Global_1.default.boolFirstTouchJoyStick = false;
            }
        }
    };
    CharacterController.prototype.MovePhyics = function (z) {
        var _this = this;
        this.node.y = cc.misc.clampf(this.node.y, -26, this.clampTopY);
        if (!this.boolPlaySoundFoot) {
            this.boolPlaySoundFoot = true;
            cc.audioEngine.playEffect(Global_1.default.soundFootStep, false);
            this.scheduleOnce(function () {
                _this.boolPlaySoundFoot = false;
            }, 0.3);
        }
        var degree = Utility_1.default.CaculatorDegree(Global_1.default.touchPos);
        this.node.is3DNode = true;
        if (!this.boolCheckTypeRotate) {
            this.node.eulerAngles = new cc.Vec3(-90, 180, degree);
        }
        else {
            this.node.runAction(cc.sequence(cc.rotate3DTo(0.1, cc.v3(-90, 180, degree)), cc.callFunc(function () {
                _this.boolCheckTypeRotate = false;
            })));
        }
        this.rigidbody.setLinearVelocity(new cc.Vec3(this.speedMove * Global_1.default.touchPos.x, this.speedMove * Global_1.default.touchPos.y, z));
    };
    CharacterController.prototype.StopMove = function () {
        this.rigidbody.setLinearVelocity(new cc.Vec3(0, 0, 0));
    };
    CharacterController.prototype.SpawnerEffectSmoke = function (smoke) {
        var Smoke = cc.instantiate(smoke);
        Smoke.parent = cc.Canvas.instance.node;
        var pos = this.node.convertToWorldSpaceAR(this.placeVfxWeapon.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        Smoke.x = pos.x;
        Smoke.y = pos.y;
        Smoke.z = 0;
    };
    CharacterController.prototype.IncreaseStick = function () {
        var brick = cc.instantiate(this.brickStick);
        brick.parent = this.node.children[2];
        brick.x = this.x;
        brick.y = this.lastPositionStickY + 0.004;
        brick.z = this.z;
        brick.getComponent(BrickData_1.default).setmaterial(EnumDefine_1.BrickType.BrickCharacter);
        this.IncreaseLevel();
        this.lastPositionStickY = this.lastPositionStickY + 0.004;
        this.listBrickAdd.push(brick);
    };
    CharacterController.prototype.DecreaseStick = function () {
        this.listBrickAdd[this.listBrickAdd.length - 1].destroy();
        this.lastPositionStickY = this.lastPositionStickY - 0.004;
        this.listBrickAdd.pop();
    };
    CharacterController.prototype.IncreaseLevel = function () {
        this.node.children[0].scale += 0.1;
        this.level++;
        if (this.level >= 5)
            GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.txtClimb.runAction(cc.fadeIn(0.3));
    };
    CharacterController.prototype.DecreaseLevel = function () {
        this.level--;
    };
    CharacterController.prototype.SpawnerBrickWhenFall = function () {
        if (this.level > 0) {
            for (var i = 0; i < this.listBrickAdd.length; i++) {
                var x = Utility_1.default.RandomRangeFloat(-7, 7);
                var y = Utility_1.default.RandomRangeFloat(-7, 7);
                this.SpawnerBrick(x, y);
            }
            this.DecreaseStickAll();
        }
    };
    CharacterController.prototype.SpawnerBrick = function (x, y) {
        var Brick = cc.instantiate(this.brickHom);
        Brick.parent = cc.Canvas.instance.node;
        Brick.x = this.node.x;
        Brick.y = this.node.y;
        Brick.z = 1;
        var t = new cc.Tween().to(0.3, { position: new cc.Vec3(this.node.x + x, this.node.y + y, 0.5) })
            .target(Brick).start();
    };
    CharacterController.prototype.DecreaseStickAll = function () {
        for (var i = 0; i < this.listBrickAdd.length; i++) {
            this.listBrickAdd[i].destroy();
        }
        this.listBrickAdd.splice(0, this.listBrickAdd.length);
        this.lastPositionStickY = 0.013;
        this.node.children[0].scale = 1;
        this.level = 0;
    };
    CharacterController.prototype.ClampTopYByLevel = function () {
        switch (this.level) {
            case 0:
                this.clampTopY = 32;
                break;
            case 1:
                this.clampTopY = 36;
                break;
            case 2:
                this.clampTopY = 40;
                break;
            case 3:
                this.clampTopY = 44;
                break;
            case 4:
                this.clampTopY = 48;
                break;
            case 5:
                this.clampTopY = 52;
                break;
            case 6:
                this.clampTopY = 56;
                break;
            case 7:
                this.clampTopY = 60;
                break;
            case 8:
                this.clampTopY = 64;
                break;
            default:
                this.clampTopY = 200;
        }
    };
    CharacterController.prototype.CharacterFall = function () {
        var _this = this;
        this.node.getComponent(cc.SkeletonAnimation).play("Fall");
        GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.joyStick.opacity = 0;
        this.SpawnerBrickWhenFall();
        Global_1.default.boolEnableTouch = false;
        this.boolCheckTypeRotate = true;
        Global_1.default.boolCharacterFall = true;
        GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.joyStick.getComponent(JoystickFollow_1.default).joyDot.setPosition(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.joyStick.getComponent(JoystickFollow_1.default).joyRing.getPosition());
        this.StopMove();
        this.scheduleOnce(function () {
            Global_1.default.boolCharacterFall = false;
            _this.node.getComponent(cc.SkeletonAnimation).play("Idle");
            Global_1.default.boolFirstTouchJoyStick = false;
        }, 1.28);
    };
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "ArrowDirection", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "speedMove", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "timeAnim", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "placeVfxWeapon", void 0);
    __decorate([
        property(cc.Prefab)
    ], CharacterController.prototype, "brickHom", void 0);
    __decorate([
        property(cc.Prefab)
    ], CharacterController.prototype, "brickStick", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "checkMoveDownStairs", void 0);
    __decorate([
        property({ type: cc.Enum(EnumDefine_1.BrickType) })
    ], CharacterController.prototype, "brickType", void 0);
    CharacterController = __decorate([
        ccclass
    ], CharacterController);
    return CharacterController;
}(cc.Component));
exports.default = CharacterController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29udHJvbGxlclxcQ2hhcmFjdGVyQ29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxnREFBMkM7QUFDM0MsbURBQWlEO0FBQ2pELCtEQUErRTtBQUMvRSwyQ0FBc0M7QUFDdEMsK0NBQTBDO0FBQzFDLDZDQUF3QztBQUN4Qyw2REFBd0Q7QUFDeEQsbURBQTZDO0FBRXZDLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFHNUM7SUFBaUQsdUNBQVk7SUFEN0Q7UUFBQSxxRUFvTkM7UUFqTkcsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFFL0IsZUFBUyxHQUFXLEVBQUUsQ0FBQztRQUV2QixjQUFRLEdBQVcsQ0FBQyxDQUFDO1FBRXJCLG9CQUFjLEdBQVksSUFBSSxDQUFDO1FBRS9CLGNBQVEsR0FBYyxJQUFJLENBQUM7UUFFM0IsZ0JBQVUsR0FBYyxJQUFJLENBQUM7UUFFN0IseUJBQW1CLEdBQVksSUFBSSxDQUFDO1FBRTdCLGVBQVMsR0FBYyxzQkFBUyxDQUFDLGNBQWMsQ0FBQztRQUN2RCxXQUFLLEdBQVcsQ0FBQyxDQUFDO1FBQ2xCLE9BQUMsR0FBVyxLQUFLLENBQUM7UUFDbEIsd0JBQWtCLEdBQVcsS0FBSyxDQUFDO1FBQ25DLE9BQUMsR0FBVyxDQUFDLEtBQUssQ0FBQztRQUNuQixlQUFTLEdBQVcsQ0FBQyxDQUFDO1FBQ3RCLGtCQUFZLEdBQWMsRUFBRSxDQUFDO1FBQzdCLG1CQUFhLEdBQVksSUFBSSxDQUFDO1FBQzlCLHFCQUFlLEdBQVksS0FBSyxDQUFDO1FBQ2pDLHVCQUFpQixHQUFZLEtBQUssQ0FBQztRQUNuQyx5QkFBbUIsR0FBWSxLQUFLLENBQUM7UUFDOUIsZUFBUyxHQUFtQixJQUFJLENBQUM7O0lBd0w1QyxDQUFDO0lBdkxHLHNDQUFRLEdBQVI7UUFDSSxrQ0FBZSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDcEYsQ0FBQztJQUNELHVDQUFTLEdBQVQ7UUFDSSxrQ0FBZSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDckYsQ0FBQztJQUNELG1DQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN4RCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNoRCxRQUFRLENBQUMsRUFBRSxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUNELDZDQUFlLEdBQWYsVUFBZ0IsS0FBSztRQUNqQixJQUFJLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxPQUFPLEVBQUU7WUFDM0MsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7WUFDNUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7U0FDN0I7UUFDRCxJQUFJLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxPQUFPLEVBQUU7WUFDM0MsSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksbUJBQW1CLEVBQUU7Z0JBQ3RELElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2FBQzlCO1NBQ0o7SUFDTCxDQUFDO0lBQ0QsNkNBQWUsR0FBZixVQUFnQixLQUFLO1FBQ2pCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO0lBQ2pDLENBQUM7SUFDRCxvQ0FBTSxHQUFOLFVBQU8sRUFBRTtRQUNMLElBQUksZ0JBQU0sQ0FBQyxlQUFlLEVBQUU7WUFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLGVBQWU7Z0JBQ2hHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7O2dCQUVuQixJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDNUI7UUFDRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBQ0QsdUNBQVMsR0FBVDtRQUFBLGlCQXNCQztRQXJCRyxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxrQkFBa0IsRUFBRTtZQUM1QixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQ3BCLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO2dCQUNqQyxnQkFBTSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztnQkFDbEMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUM1RCxFQUFFLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxnQkFBTSxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDckQsSUFBSSxDQUFDLFlBQVksQ0FBQztvQkFDZCxnQkFBTSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztvQkFDakMsZ0JBQU0sQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7Z0JBQ3BDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDVCxJQUFJLENBQUMsWUFBWSxDQUFDO29CQUNkLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO29CQUNsQyxnQkFBTSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztvQkFDdEMsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUM5RCxDQUFDLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQ3JCO2lCQUNJO2dCQUNELElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDMUQsZ0JBQU0sQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUM7YUFDekM7U0FDSjtJQUNMLENBQUM7SUFDRCx3Q0FBVSxHQUFWLFVBQVcsQ0FBUztRQUFwQixpQkFvQkM7UUFuQkcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDekIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztZQUM5QixFQUFFLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxnQkFBTSxDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsWUFBWSxDQUFDO2dCQUNkLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7WUFDbkMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ1g7UUFDRCxJQUFJLE1BQU0sR0FBRyxpQkFBTyxDQUFDLGVBQWUsQ0FBQyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDekQ7YUFDSTtZQUNELElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsUUFBUSxDQUFDO2dCQUNyRixLQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO1lBQ3JDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNSO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM3SCxDQUFDO0lBQ0Qsc0NBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBQ0QsZ0RBQWtCLEdBQWxCLFVBQW1CLEtBQWdCO1FBQy9CLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbEMsS0FBSyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDdkMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7UUFDN0UsR0FBRyxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN4RCxLQUFLLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDaEIsS0FBSyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2hCLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2hCLENBQUM7SUFDRCwyQ0FBYSxHQUFiO1FBQ0ksSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDNUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNyQyxLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDakIsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQzFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNqQixLQUFLLENBQUMsWUFBWSxDQUFDLG1CQUFTLENBQUMsQ0FBQyxXQUFXLENBQUMsc0JBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7UUFDMUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUNELDJDQUFhLEdBQWI7UUFDSSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzFELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQzFELElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUNELDJDQUFhLEdBQWI7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksR0FBRyxDQUFDO1FBQ25DLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNiLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDO1lBQ2YsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ2hHLENBQUM7SUFDRCwyQ0FBYSxHQUFiO1FBQ0ksSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2pCLENBQUM7SUFDRCxrREFBb0IsR0FBcEI7UUFDSSxJQUFJLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFO1lBQ2hCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDL0MsSUFBSSxDQUFDLEdBQUcsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLEdBQUcsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7YUFDMUI7WUFDRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztTQUMzQjtJQUNMLENBQUM7SUFDRCwwQ0FBWSxHQUFaLFVBQWEsQ0FBUyxFQUFFLENBQVM7UUFDN0IsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDMUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDdkMsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUN0QixLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ3RCLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ1osSUFBSSxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDO2FBQzNGLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBQ0QsOENBQWdCLEdBQWhCO1FBQ0ksS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQy9DLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDbEM7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7SUFDbkIsQ0FBQztJQUNELDhDQUFnQixHQUFoQjtRQUNJLFFBQVEsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNoQixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVixLQUFLLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE1BQU07WUFDVjtnQkFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQztTQUM1QjtJQUNMLENBQUM7SUFDRCwyQ0FBYSxHQUFiO1FBQUEsaUJBY0M7UUFiRyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDMUQsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBQzFFLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQzVCLGdCQUFNLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztRQUMvQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLGdCQUFNLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLHdCQUFhLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLHdCQUFhLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUM5TixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNkLGdCQUFNLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1lBQ2pDLEtBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMxRCxnQkFBTSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztRQUMxQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDYixDQUFDO0lBaE5EO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7K0RBQ2E7SUFFL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQzswREFDRTtJQUV2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3lEQUNBO0lBRXJCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7K0RBQ2E7SUFFL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzt5REFDTztJQUUzQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzJEQUNTO0lBRTdCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7b0VBQ2tCO0lBRXBDO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsc0JBQVMsQ0FBQyxFQUFFLENBQUM7MERBQ2dCO0lBaEJ0QyxtQkFBbUI7UUFEdkMsT0FBTztPQUNhLG1CQUFtQixDQW1OdkM7SUFBRCwwQkFBQztDQW5ORCxBQW1OQyxDQW5OZ0QsRUFBRSxDQUFDLFNBQVMsR0FtTjVEO2tCQW5Ob0IsbUJBQW1CIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEJyaWNrRGF0YSBmcm9tIFwiLi4vQnJpY2svQnJpY2tEYXRhXCI7XHJcbmltcG9ydCB7IEJyaWNrVHlwZSB9IGZyb20gXCIuLi9Db21tb24vRW51bURlZmluZVwiO1xyXG5pbXBvcnQgR2FtZVBsYXlJbnN0YW5jZSwgeyBldmVudERpc3BhdGNoZXIgfSBmcm9tIFwiLi4vQ29tbW9uL0dhbWVQbGF5SW5zdGFuY2VcIjtcclxuaW1wb3J0IEdsb2JhbCBmcm9tIFwiLi4vQ29tbW9uL0dsb2JhbFwiO1xyXG5pbXBvcnQgS2V5RXZlbnQgZnJvbSBcIi4uL0NvbW1vbi9LZXlFdmVudFwiO1xyXG5pbXBvcnQgVXRpbGl0eSBmcm9tIFwiLi4vQ29tbW9uL1V0aWxpdHlcIjtcclxuaW1wb3J0IENoZWNrTW92ZURvd25TdGFpcnMgZnJvbSBcIi4vQ2hlY2tNb3ZlRG93blN0YWlyc1wiO1xyXG5pbXBvcnQgSm95c3RpY0ZvbGxvdyBmcm9tIFwiLi9Kb3lzdGlja0ZvbGxvd1wiO1xyXG5cclxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENoYXJhY3RlckNvbnRyb2xsZXIgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBBcnJvd0RpcmVjdGlvbjogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIHNwZWVkTW92ZTogbnVtYmVyID0gMjA7XHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIHRpbWVBbmltOiBudW1iZXIgPSAwO1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBwbGFjZVZmeFdlYXBvbjogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgYnJpY2tIb206IGNjLlByZWZhYiA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgYnJpY2tTdGljazogY2MuUHJlZmFiID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgY2hlY2tNb3ZlRG93blN0YWlyczogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoeyB0eXBlOiBjYy5FbnVtKEJyaWNrVHlwZSkgfSlcclxuICAgIHB1YmxpYyBicmlja1R5cGU6IEJyaWNrVHlwZSA9IEJyaWNrVHlwZS5Ccmlja0NoYXJhY3RlcjtcclxuICAgIGxldmVsOiBudW1iZXIgPSAwO1xyXG4gICAgeDogbnVtYmVyID0gMC4wMDM7XHJcbiAgICBsYXN0UG9zaXRpb25TdGlja1k6IG51bWJlciA9IDAuMDEzO1xyXG4gICAgejogbnVtYmVyID0gLTAuMDAyO1xyXG4gICAgY2xhbXBUb3BZOiBudW1iZXIgPSAwO1xyXG4gICAgbGlzdEJyaWNrQWRkOiBjYy5Ob2RlW10gPSBbXTtcclxuICAgIGJvb2xDYW5BdHRhY2s6IGJvb2xlYW4gPSB0cnVlO1xyXG4gICAgYm9vbE1vdmVJbkZsb29yOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBib29sUGxheVNvdW5kRm9vdDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgYm9vbENoZWNrVHlwZVJvdGF0ZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgcHVibGljIHJpZ2lkYm9keTogY2MuUmlnaWRCb2R5M0QgPSBudWxsO1xyXG4gICAgb25FbmFibGUoKSB7XHJcbiAgICAgICAgZXZlbnREaXNwYXRjaGVyLm9uKEtleUV2ZW50LmluY3JlYXNlU3RpY2tNeUNoYXJhY3RlciwgdGhpcy5JbmNyZWFzZVN0aWNrLCB0aGlzKTtcclxuICAgIH1cclxuICAgIG9uRGlzYWJsZSgpIHtcclxuICAgICAgICBldmVudERpc3BhdGNoZXIub2ZmKEtleUV2ZW50LmluY3JlYXNlU3RpY2tNeUNoYXJhY3RlciwgdGhpcy5JbmNyZWFzZVN0aWNrLCB0aGlzKTtcclxuICAgIH1cclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIHRoaXMucmlnaWRib2R5ID0gdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5SaWdpZEJvZHkzRCk7XHJcbiAgICAgICAgbGV0IGNvbGxpZGVyID0gdGhpcy5nZXRDb21wb25lbnQoY2MuQ29sbGlkZXIzRCk7XHJcbiAgICAgICAgY29sbGlkZXIub24oJ2NvbGxpc2lvbi1zdGF5JywgdGhpcy5vbkNvbGxpc2lvblN0YXksIHRoaXMpO1xyXG4gICAgICAgIHRoaXMubGlzdEJyaWNrQWRkLnNwbGljZSgwLCB0aGlzLmxpc3RCcmlja0FkZC5sZW5ndGgpO1xyXG4gICAgfVxyXG4gICAgb25Db2xsaXNpb25TdGF5KGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50Lm90aGVyQ29sbGlkZXIubm9kZS5ncm91cCA9PSBcIkZsb29yXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5ib29sTW92ZUluRmxvb3IgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLmJvb2xDYW5BdHRhY2sgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZXZlbnQub3RoZXJDb2xsaWRlci5ub2RlLmdyb3VwID09IFwiTW9kZWxcIikge1xyXG4gICAgICAgICAgICBpZiAoZXZlbnQub3RoZXJDb2xsaWRlci5ub2RlLm5hbWUgPT0gXCJTdGFpcnNCb3hDb2xsaWRlclwiKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmJvb2xDYW5BdHRhY2sgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIG9uQ29sbGlzaW9uRXhpdChldmVudCkge1xyXG4gICAgICAgIHRoaXMuYm9vbE1vdmVJbkZsb29yID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICB1cGRhdGUoZHQpIHtcclxuICAgICAgICBpZiAoR2xvYmFsLmJvb2xFbmFibGVUb3VjaCkge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuY2hlY2tNb3ZlRG93blN0YWlycy5nZXRDb21wb25lbnQoQ2hlY2tNb3ZlRG93blN0YWlycykuYm9vbE1vdmVEb3duICYmIHRoaXMuYm9vbE1vdmVJbkZsb29yKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5Nb3ZlUGh5aWNzKDApO1xyXG4gICAgICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgICAgICB0aGlzLk1vdmVQaHlpY3MoLTIwKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5DbGFtcFRvcFlCeUxldmVsKCk7XHJcbiAgICB9XHJcbiAgICBBdHRhY2tpbmcoKSB7XHJcbiAgICAgICAgaWYgKCFHbG9iYWwuYm9vbFN0YXJ0QXR0YWNraW5nKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmJvb2xDYW5BdHRhY2spIHtcclxuICAgICAgICAgICAgICAgIEdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xDaGVja0F0dGFja2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIkF0dGFja1wiKTtcclxuICAgICAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kQXR0YWNrLCBmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xDaGVja0F0dGFja2luZyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xDaGVja0F0dGFja2VkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH0sIDAuMTgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIEdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICBHbG9iYWwuYm9vbEZpcnN0VG91Y2hKb3lTdGljayA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJJZGxlXCIpO1xyXG4gICAgICAgICAgICAgICAgfSwgdGhpcy50aW1lQW5pbSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiSWRsZVwiKTtcclxuICAgICAgICAgICAgICAgIEdsb2JhbC5ib29sRmlyc3RUb3VjaEpveVN0aWNrID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBNb3ZlUGh5aWNzKHo6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLnksIC0yNiwgdGhpcy5jbGFtcFRvcFkpO1xyXG4gICAgICAgIGlmICghdGhpcy5ib29sUGxheVNvdW5kRm9vdCkge1xyXG4gICAgICAgICAgICB0aGlzLmJvb2xQbGF5U291bmRGb290ID0gdHJ1ZTtcclxuICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdChHbG9iYWwuc291bmRGb290U3RlcCwgZmFsc2UpO1xyXG4gICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmJvb2xQbGF5U291bmRGb290ID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH0sIDAuMyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCBkZWdyZWUgPSBVdGlsaXR5LkNhY3VsYXRvckRlZ3JlZShHbG9iYWwudG91Y2hQb3MpO1xyXG4gICAgICAgIHRoaXMubm9kZS5pczNETm9kZSA9IHRydWU7XHJcbiAgICAgICAgaWYgKCF0aGlzLmJvb2xDaGVja1R5cGVSb3RhdGUpIHtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmV1bGVyQW5nbGVzID0gbmV3IGNjLlZlYzMoLTkwLCAxODAsIGRlZ3JlZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUucnVuQWN0aW9uKGNjLnNlcXVlbmNlKGNjLnJvdGF0ZTNEVG8oMC4xLCBjYy52MygtOTAsIDE4MCwgZGVncmVlKSksIGNjLmNhbGxGdW5jKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYm9vbENoZWNrVHlwZVJvdGF0ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9KSkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnJpZ2lkYm9keS5zZXRMaW5lYXJWZWxvY2l0eShuZXcgY2MuVmVjMyh0aGlzLnNwZWVkTW92ZSAqIEdsb2JhbC50b3VjaFBvcy54LCB0aGlzLnNwZWVkTW92ZSAqIEdsb2JhbC50b3VjaFBvcy55LCB6KSk7XHJcbiAgICB9XHJcbiAgICBTdG9wTW92ZSgpIHtcclxuICAgICAgICB0aGlzLnJpZ2lkYm9keS5zZXRMaW5lYXJWZWxvY2l0eShuZXcgY2MuVmVjMygwLCAwLCAwKSk7XHJcbiAgICB9XHJcbiAgICBTcGF3bmVyRWZmZWN0U21va2Uoc21va2U6IGNjLlByZWZhYikge1xyXG4gICAgICAgIGxldCBTbW9rZSA9IGNjLmluc3RhbnRpYXRlKHNtb2tlKTtcclxuICAgICAgICBTbW9rZS5wYXJlbnQgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZTtcclxuICAgICAgICBsZXQgcG9zID0gdGhpcy5ub2RlLmNvbnZlcnRUb1dvcmxkU3BhY2VBUih0aGlzLnBsYWNlVmZ4V2VhcG9uLmdldFBvc2l0aW9uKCkpO1xyXG4gICAgICAgIHBvcyA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKHBvcyk7XHJcbiAgICAgICAgU21va2UueCA9IHBvcy54O1xyXG4gICAgICAgIFNtb2tlLnkgPSBwb3MueTtcclxuICAgICAgICBTbW9rZS56ID0gMDtcclxuICAgIH1cclxuICAgIEluY3JlYXNlU3RpY2soKSB7XHJcbiAgICAgICAgbGV0IGJyaWNrID0gY2MuaW5zdGFudGlhdGUodGhpcy5icmlja1N0aWNrKTtcclxuICAgICAgICBicmljay5wYXJlbnQgPSB0aGlzLm5vZGUuY2hpbGRyZW5bMl07XHJcbiAgICAgICAgYnJpY2sueCA9IHRoaXMueDtcclxuICAgICAgICBicmljay55ID0gdGhpcy5sYXN0UG9zaXRpb25TdGlja1kgKyAwLjAwNDtcclxuICAgICAgICBicmljay56ID0gdGhpcy56O1xyXG4gICAgICAgIGJyaWNrLmdldENvbXBvbmVudChCcmlja0RhdGEpLnNldG1hdGVyaWFsKEJyaWNrVHlwZS5Ccmlja0NoYXJhY3Rlcik7XHJcbiAgICAgICAgdGhpcy5JbmNyZWFzZUxldmVsKCk7XHJcbiAgICAgICAgdGhpcy5sYXN0UG9zaXRpb25TdGlja1kgPSB0aGlzLmxhc3RQb3NpdGlvblN0aWNrWSArIDAuMDA0O1xyXG4gICAgICAgIHRoaXMubGlzdEJyaWNrQWRkLnB1c2goYnJpY2spO1xyXG4gICAgfVxyXG4gICAgRGVjcmVhc2VTdGljaygpIHtcclxuICAgICAgICB0aGlzLmxpc3RCcmlja0FkZFt0aGlzLmxpc3RCcmlja0FkZC5sZW5ndGggLSAxXS5kZXN0cm95KCk7XHJcbiAgICAgICAgdGhpcy5sYXN0UG9zaXRpb25TdGlja1kgPSB0aGlzLmxhc3RQb3NpdGlvblN0aWNrWSAtIDAuMDA0O1xyXG4gICAgICAgIHRoaXMubGlzdEJyaWNrQWRkLnBvcCgpO1xyXG4gICAgfVxyXG4gICAgSW5jcmVhc2VMZXZlbCgpIHtcclxuICAgICAgICB0aGlzLm5vZGUuY2hpbGRyZW5bMF0uc2NhbGUgKz0gMC4xO1xyXG4gICAgICAgIHRoaXMubGV2ZWwrKztcclxuICAgICAgICBpZiAodGhpcy5sZXZlbCA+PSA1KVxyXG4gICAgICAgICAgICBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5LnR4dENsaW1iLnJ1bkFjdGlvbihjYy5mYWRlSW4oMC4zKSk7XHJcbiAgICB9XHJcbiAgICBEZWNyZWFzZUxldmVsKCkge1xyXG4gICAgICAgIHRoaXMubGV2ZWwtLTtcclxuICAgIH1cclxuICAgIFNwYXduZXJCcmlja1doZW5GYWxsKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmxldmVsID4gMCkge1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMubGlzdEJyaWNrQWRkLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgeCA9IFV0aWxpdHkuUmFuZG9tUmFuZ2VGbG9hdCgtNywgNyk7XHJcbiAgICAgICAgICAgICAgICBsZXQgeSA9IFV0aWxpdHkuUmFuZG9tUmFuZ2VGbG9hdCgtNywgNyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLlNwYXduZXJCcmljayh4LCB5KVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuRGVjcmVhc2VTdGlja0FsbCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFNwYXduZXJCcmljayh4OiBudW1iZXIsIHk6IG51bWJlcikge1xyXG4gICAgICAgIGxldCBCcmljayA9IGNjLmluc3RhbnRpYXRlKHRoaXMuYnJpY2tIb20pO1xyXG4gICAgICAgIEJyaWNrLnBhcmVudCA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlO1xyXG4gICAgICAgIEJyaWNrLnggPSB0aGlzLm5vZGUueDtcclxuICAgICAgICBCcmljay55ID0gdGhpcy5ub2RlLnk7XHJcbiAgICAgICAgQnJpY2sueiA9IDE7XHJcbiAgICAgICAgbGV0IHQgPSBuZXcgY2MuVHdlZW4oKS50bygwLjMsIHsgcG9zaXRpb246IG5ldyBjYy5WZWMzKHRoaXMubm9kZS54ICsgeCwgdGhpcy5ub2RlLnkgKyB5LCAwLjUpIH0pXHJcbiAgICAgICAgICAgIC50YXJnZXQoQnJpY2spLnN0YXJ0KCk7XHJcbiAgICB9XHJcbiAgICBEZWNyZWFzZVN0aWNrQWxsKCkge1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5saXN0QnJpY2tBZGQubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5saXN0QnJpY2tBZGRbaV0uZGVzdHJveSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmxpc3RCcmlja0FkZC5zcGxpY2UoMCwgdGhpcy5saXN0QnJpY2tBZGQubGVuZ3RoKTtcclxuICAgICAgICB0aGlzLmxhc3RQb3NpdGlvblN0aWNrWSA9IDAuMDEzO1xyXG4gICAgICAgIHRoaXMubm9kZS5jaGlsZHJlblswXS5zY2FsZSA9IDE7XHJcbiAgICAgICAgdGhpcy5sZXZlbCA9IDA7XHJcbiAgICB9XHJcbiAgICBDbGFtcFRvcFlCeUxldmVsKCkge1xyXG4gICAgICAgIHN3aXRjaCAodGhpcy5sZXZlbCkge1xyXG4gICAgICAgICAgICBjYXNlIDA6IHRoaXMuY2xhbXBUb3BZID0gMzI7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAxOiB0aGlzLmNsYW1wVG9wWSA9IDM2O1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgMjogdGhpcy5jbGFtcFRvcFkgPSA0MDtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIDM6IHRoaXMuY2xhbXBUb3BZID0gNDQ7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSA0OiB0aGlzLmNsYW1wVG9wWSA9IDQ4O1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgNTogdGhpcy5jbGFtcFRvcFkgPSA1MjtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIDY6IHRoaXMuY2xhbXBUb3BZID0gNTY7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSA3OiB0aGlzLmNsYW1wVG9wWSA9IDYwO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgODogdGhpcy5jbGFtcFRvcFkgPSA2NDtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgdGhpcy5jbGFtcFRvcFkgPSAyMDA7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgQ2hhcmFjdGVyRmFsbCgpIHtcclxuICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiRmFsbFwiKTtcclxuICAgICAgICBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5LmpveVN0aWNrLm9wYWNpdHkgPSAwO1xyXG4gICAgICAgIHRoaXMuU3Bhd25lckJyaWNrV2hlbkZhbGwoKTtcclxuICAgICAgICBHbG9iYWwuYm9vbEVuYWJsZVRvdWNoID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5ib29sQ2hlY2tUeXBlUm90YXRlID0gdHJ1ZTtcclxuICAgICAgICBHbG9iYWwuYm9vbENoYXJhY3RlckZhbGwgPSB0cnVlO1xyXG4gICAgICAgIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuam95U3RpY2suZ2V0Q29tcG9uZW50KEpveXN0aWNGb2xsb3cpLmpveURvdC5zZXRQb3NpdGlvbihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5LmpveVN0aWNrLmdldENvbXBvbmVudChKb3lzdGljRm9sbG93KS5qb3lSaW5nLmdldFBvc2l0aW9uKCkpO1xyXG4gICAgICAgIHRoaXMuU3RvcE1vdmUoKTtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sQ2hhcmFjdGVyRmFsbCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiSWRsZVwiKTtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xGaXJzdFRvdWNoSm95U3RpY2sgPSBmYWxzZTtcclxuICAgICAgICB9LCAxLjI4KTtcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/SplineExtend.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b3890qhPopIIpBUBGAYxovj', 'SplineExtend');
// Scripts/Common/SplineExtend.js

"use strict";

/****************************************************************************
 Copyright (c) 2008 Radu Gruian
 Copyright (c) 2008-2010 Ricardo Quesada
 Copyright (c) 2011 Vit Valentin
 Copyright (c) 2011-2012 cocos2d-x.org
 Copyright (c) 2013-2016 Chukong Technologies Inc.
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 http://www.cocos2d-x.org
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 Orignal code by Radu Gruian: http://www.codeproject.com/Articles/30838/Overhauser-Catmull-Rom-Splines-for-Camera-Animatio.So
 Adapted to cocos2d-x by Vit Valentin
 Adapted from cocos2d-x to cocos2d-iphone by Ricardo Quesada
 ****************************************************************************/

/**
 * @module cc
 */

/*
 * Returns the Cardinal Spline position for a given set of control points, tension and time. <br />
 * CatmullRom Spline formula. <br />
 * s(-ttt + 2tt - t)P1 + s(-ttt + tt)P2 + (2ttt - 3tt + 1)P2 + s(ttt - 2tt + t)P3 + (-2ttt + 3tt)P3 + s(ttt - tt)P4
 *
 * @method cardinalSplineAt
 * @param {Vec2} p0
 * @param {Vec2} p1
 * @param {Vec2} p2
 * @param {Vec2} p3
 * @param {Number} tension
 * @param {Number} t
 * @return {Vec2}
 */
function cardinalSplineAt(p0, p1, p2, p3, tension, t) {
  var t2 = t * t;
  var t3 = t2 * t;
  /*
   * Formula: s(-ttt + 2tt - t)P1 + s(-ttt + tt)P2 + (2ttt - 3tt + 1)P2 + s(ttt - 2tt + t)P3 + (-2ttt + 3tt)P3 + s(ttt - tt)P4
   */

  var s = (1 - tension) / 2;
  var b1 = s * (-t3 + 2 * t2 - t); // s(-t3 + 2 t2 - t)P1

  var b2 = s * (-t3 + t2) + (2 * t3 - 3 * t2 + 1); // s(-t3 + t2)P2 + (2 t3 - 3 t2 + 1)P2

  var b3 = s * (t3 - 2 * t2 + t) + (-2 * t3 + 3 * t2); // s(t3 - 2 t2 + t)P3 + (-2 t3 + 3 t2)P3

  var b4 = s * (t3 - t2); // s(t3 - t2)P4

  var x = p0.x * b1 + p1.x * b2 + p2.x * b3 + p3.x * b4;
  var y = p0.y * b1 + p1.y * b2 + p2.y * b3 + p3.y * b4;
  return cc.v2(x, y);
}

;
/*
 * returns a point from the array
 * @method getControlPointAt
 * @param {Array} controlPoints
 * @param {Number} pos
 * @return {Array}
 */

function getControlPointAt(controlPoints, pos) {
  var p = Math.min(controlPoints.length - 1, Math.max(pos, 0));
  return controlPoints[p];
}

;

function reverseControlPoints(controlPoints) {
  var newArray = [];

  for (var i = controlPoints.length - 1; i >= 0; i--) {
    newArray.push(cc.v2(controlPoints[i].x, controlPoints[i].y));
  }

  return newArray;
}

function cloneControlPoints(controlPoints) {
  var newArray = [];

  for (var i = 0; i < controlPoints.length; i++) {
    newArray.push(cc.v2(controlPoints[i].x, controlPoints[i].y));
  }

  return newArray;
}
/**
 * TODO tối ưu: Thực hiện pre interpolation và precalculate length cho các Spline Action giống nhau
 */

/*
 * Cardinal Spline path. http://en.wikipedia.org/wiki/Cubic_Hermite_spline#Cardinal_spline
 * Absolute coordinates.
 *
 * @class CardinalSplineTo2
 * @extends ActionInterval
 *
 * @param {Number} duration
 * @param {Array} points array of control points
 * @param {Number} tension
 *
 * @example
 * //create a cc.CardinalSplineTo2
 * var action1 = cc.CardinalSplineTo2(3, array, 0);
 * 
 */


cc.CardinalSplineTo2 = cc.Class({
  name: 'cc.CardinalSplineTo2',
  "extends": cc.ActionInterval,
  cacheLengths: null,
  ctor: function ctor(duration, points, angleOffset, tension, speed) {
    /* Array of control points */
    this._points = [];
    this._cacheLengths = [];
    this._totalLength = 0;
    this._deltaT = 0;
    this._tension = 0;
    this._angleOffset = 0;
    this._previousPosition = null;
    this._accumulatedDiff = null;
    tension !== undefined && cc.CardinalSplineTo2.prototype.initWithDuration.call(this, duration, points, angleOffset, tension, speed);
  },
  initWithDuration: function initWithDuration(duration, points, angleOffset, tension, speed) {
    if (!points || points.length === 0) {
      cc.errorID(1024);
      return false;
    }

    this._tension = tension;
    this._angleOffset = angleOffset;
    this.setPoints(points);
    if (speed) duration = this.getLength() / speed;

    if (cc.ActionInterval.prototype.initWithDuration.call(this, duration)) {
      return true;
    }

    return false;
  },
  clone: function clone() {
    var action = new cc.CardinalSplineTo2();
    action.initWithDuration(this._duration, cloneControlPoints(this._points), this._tension);
    return action;
  },
  startWithTarget: function startWithTarget(target) {
    cc.ActionInterval.prototype.startWithTarget.call(this, target); // Issue #1441 from cocos2d-iphone

    this._deltaT = 1 / (this._points.length - 1);
    this._previousPosition = cc.v2(this.target.x, this.target.y);
    this._accumulatedDiff = cc.v2(0, 0);
  },
  getP: function getP(dt) {
    for (var i = 0; i < this._cummutilate.length; i++) {
      if (this._cummutilate[i] > dt) return i;
    }

    return this._cummutilate.length;
  },
  getLt: function getLt(p, dt) {
    if (p == 0) {
      return dt / this._cacheLengths[p];
    } else {
      return (dt - this._cummutilate[p - 1]) / this._cacheLengths[p];
    }
  },
  update: function update(dt) {
    dt = this._computeEaseTime(dt);
    var p, lt;
    var ps = this._points; // eg.
    // p..p..p..p..p..p..p
    // 1..2..3..4..5..6..7
    // want p to be 1, 2, 3, 4, 5, 6

    if (dt === 1) {
      p = ps.length - 1;
      lt = 1;
    } else {
      var locDT = this._deltaT; //p = 0 | (dt / locDT);
      //lt = (dt - locDT * p) / locDT;

      p = this.getP(dt);
      lt = this.getLt(p, dt); // console.log(lt);
    }

    var newPos = cardinalSplineAt(getControlPointAt(ps, p - 1), getControlPointAt(ps, p - 0), getControlPointAt(ps, p + 1), getControlPointAt(ps, p + 2), this._tension, lt);

    if (cc.macro.ENABLE_STACKABLE_ACTIONS) {
      var tempX, tempY;
      tempX = this.target.x - this._previousPosition.x;
      tempY = this.target.y - this._previousPosition.y;

      if (tempX !== 0 || tempY !== 0) {
        var locAccDiff = this._accumulatedDiff;
        tempX = locAccDiff.x + tempX;
        tempY = locAccDiff.y + tempY;
        locAccDiff.x = tempX;
        locAccDiff.y = tempY;
        newPos.x += tempX;
        newPos.y += tempY;
      }
    }

    this.updatePosition(newPos);
  },
  reverse: function reverse() {
    var reversePoints = reverseControlPoints(this._points);
    return cc.CardinalSplineTo2(this._duration, reversePoints, this._tension);
  },

  /*
   * update position of target
   * @method updatePosition
   * @param {Vec2} newPos
   */
  updatePosition: function updatePosition(newPos) {
    var deltaX = newPos.x - this._previousPosition.x;
    var deltaY = newPos.y - this._previousPosition.y;

    if (!this._disableRotate) {
      if (deltaX !== 0 && deltaY !== 0) this.target.angle = Math.atan2(deltaY, deltaX) * 180 / Math.PI + this._angleOffset;
    }

    this.target.setPosition(newPos);
    this._previousPosition = newPos;
  },

  /*
   * Points getter
   * @method getPoints
   * @return {Array}
   */
  getPoints: function getPoints() {
    return this._points;
  },

  /**
   * Points setter
   * @method setPoints
   * @param {Array} points
   */
  setPoints: function setPoints(points) {
    points = this._interpolatatePoints(points, 10);

    this._setPoint(points);
  },
  _setPoint: function _setPoint(points) {
    this._points = points;
    this._cacheLengths = [];
    this._cummutilate = [];

    for (var i = 0; i < points.length - 1; i++) {
      var p1 = points[i];
      var p2 = points[i + 1];
      var length = Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));

      this._cacheLengths.push(length);
    }

    this._totalLength = 0;

    for (var i = 0; i < this._cacheLengths.length; i++) {
      this._totalLength += this._cacheLengths[i];

      this._cummutilate.push(this._totalLength);
    }

    for (var i = 0; i < this._cacheLengths.length; i++) {
      this._cacheLengths[i] = this._cacheLengths[i] / this._totalLength;
      this._cummutilate[i] = this._cummutilate[i] / this._totalLength;
    }
  },
  _setPoint2: function _setPoint2(points) {
    this._points = points;
    this._cacheLengths = [];
    this._cummutilate = [];

    for (var i = 0; i < points.length - 1; i++) {
      var p1 = points[i];
      var p2 = points[i + 1];
      var length = 1;

      this._cacheLengths.push(length);
    }

    this._totalLength = 0;

    for (var i = 0; i < this._cacheLengths.length; i++) {
      this._totalLength += this._cacheLengths[i];

      this._cummutilate.push(this._totalLength);
    }

    for (var i = 0; i < this._cacheLengths.length; i++) {
      this._cacheLengths[i] = this._cacheLengths[i] / this._totalLength;
      this._cummutilate[i] = this._cummutilate[i] / this._totalLength;
    }
  },
  _interpolatatePoints: function _interpolatatePoints(points, d) {
    this._setPoint(points);

    var n = (points.length - 1) * d;
    var newPoints = [];
    var p = 0;
    var lt = 0;
    var ps = points;

    for (var i = 0; i < n; i++) {
      var dt = i * 1 / (n - 1);

      if (dt === 1) {
        p = ps.length - 1;
        lt = 1;
      } else {
        var locDT = 1 / (points.length - 1); //p = 0 | (dt / locDT);
        //lt = (dt - locDT * p) / locDT;

        p = this.getP(dt);
        lt = this.getLt(p, dt);
      }

      var newPos = cardinalSplineAt(getControlPointAt(ps, p - 1), getControlPointAt(ps, p - 0), getControlPointAt(ps, p + 1), getControlPointAt(ps, p + 2), this._tension, lt);
      newPoints.push(newPos);
    }

    return newPoints;
  },
  getLength: function getLength() {
    return this._totalLength;
  },
  disableRotate: function disableRotate() {
    this._disableRotate = true;
  }
});
/**
 * !#en Creates an action with a Cardinal Spline array of points and tension.
 * !#zh 按基数样条曲线轨迹移动到目标位置。
 * @method CardinalSplineTo2
 * @param {Number} duration
 * @param {Array} points array of control points
 * @param {Number} angleOffset
 * @param {Number} tension
 * @return {ActionInterval}
 *
 * @example
 * //create a cc.CardinalSplineTo2
 * var action1 = cc.CardinalSplineTo2(3, array, 0);
 */

cc.cardinalSplineTo2 = function (duration, points, angleOffset, tension) {
  return new cc.CardinalSplineTo2(duration, points, angleOffset, tension);
};
/**
 * !#en Creates an action with a Cardinal Spline array of points and tension.
 * !#zh 按基数样条曲线轨迹移动到目标位置。
 * @method CardinalSplineTo2
 * @param {Number} speed
 * @param {Array} points array of control points
 * @param {Number} angleOffset
 * @param {Number} tension
 * @return {ActionInterval}
 *
 * @example
 * //create a cc.CardinalSplineTo2
 * var action1 = cc.CardinalSplineTo2(3, array, 0);
 */


cc.cardinalSplineTo2_speed = function (speed, points, angleOffset, tension) {
  return new cc.CardinalSplineTo2(1, points, angleOffset, tension, speed);
};
/*
 * Cardinal Spline path. http://en.wikipedia.org/wiki/Cubic_Hermite_spline#Cardinal_spline
 * Relative coordinates.
 *
 * @class CardinalSplineBy2
 * @extends CardinalSplineTo2
 *
 * @param {Number} duration
 * @param {Array} points
 * @param {Number} tension
 *
 * @example
 * //create a cc.CardinalSplineBy2
 * var action1 = cc.CardinalSplineBy2(3, array, 0);
 */


cc.CardinalSplineBy2 = cc.Class({
  name: 'cc.CardinalSplineBy2',
  "extends": cc.CardinalSplineTo2,
  ctor: function ctor(duration, points, angleOffset, tension) {
    this._startPosition = cc.v2(0, 0);
    tension !== undefined && this.initWithDuration(duration, points, angleOffset, tension);
  },
  startWithTarget: function startWithTarget(target) {
    cc.CardinalSplineTo2.prototype.startWithTarget.call(this, target);
    this._startPosition.x = target.x;
    this._startPosition.y = target.y;
  },
  reverse: function reverse() {
    var copyConfig = this._points.slice();

    var current; //
    // convert "absolutes" to "diffs"
    //

    var p = copyConfig[0];

    for (var i = 1; i < copyConfig.length; ++i) {
      current = copyConfig[i];
      copyConfig[i] = current.sub(p);
      p = current;
    } // convert to "diffs" to "reverse absolute"


    var reverseArray = reverseControlPoints(copyConfig); // 1st element (which should be 0,0) should be here too

    p = reverseArray[reverseArray.length - 1];
    reverseArray.pop();
    p.x = -p.x;
    p.y = -p.y;
    reverseArray.unshift(p);

    for (var i = 1; i < reverseArray.length; ++i) {
      current = reverseArray[i];
      current.x = -current.x;
      current.y = -current.y;
      current.x += p.x;
      current.y += p.y;
      reverseArray[i] = current;
      p = current;
    }

    return cc.CardinalSplineBy2(this._duration, reverseArray, this._angleOffset, this._tension);
  },

  /**
   * update position of target
   * @method updatePosition
   * @param {Vec2} newPos
   */
  updatePosition: function updatePosition(newPos) {
    var pos = this._startPosition;
    var posX = newPos.x + pos.x;
    var posY = newPos.y + pos.y;
    var deltaX = posX - this._previousPosition.x;
    var deltaY = posY - this._previousPosition.y;
    if (deltaX !== 0 && deltaY !== 0) this.target.angle = Math.atan2(deltaY, deltaX) * 180 / Math.PI + this._angleOffset;
    this._previousPosition.x = posX;
    this._previousPosition.y = posY;
    this.target.setPosition(posX, posY);
  },
  clone: function clone() {
    var a = new cc.CardinalSplineBy2();
    a.initWithDuration(this._duration, cloneControlPoints(this._points), this._angleOffset, this._tension);
    return a;
  }
});
/**
 * !#en Creates an action with a Cardinal Spline array of points and tension.
 * !#zh 按基数样条曲线轨迹移动指定的距离。
 * @method CardinalSplineBy2
 * @param {Number} duration
 * @param {Array} points
 * @param {Number} angleOffset
 * @param {Number} tension
 *
 * @return {ActionInterval}
 */

cc.cardinalSplineBy2 = function (duration, points, angleOffset, tension) {
  return new cc.CardinalSplineBy2(duration, points, angleOffset, tension);
};
/*
 * An action that moves the target with a CatmullRom curve to a destination point.<br/>
 * A Catmull Rom is a Cardinal Spline with a tension of 0.5.  <br/>
 * http://en.wikipedia.org/wiki/Cubic_Hermite_spline#Catmull.E2.80.93Rom_spline
 * Absolute coordinates.
 *
 * @class CatmullRomTo2
 * @extends CardinalSplineTo2
 *
 * @param {Number} dt
 * @param {Array} points
 *
 * @example
 * var action1 = cc.CatmullRomTo2(3, array);
 */


cc.CatmullRomTo2 = cc.Class({
  name: 'cc.CatmullRomTo2',
  "extends": cc.CardinalSplineTo2,
  ctor: function ctor(dt, points, angleOffset) {
    if (angleOffset === undefined) angleOffset = 0;
    points && this.initWithDuration(dt, points, angleOffset);
  },
  initWithDuration: function initWithDuration(dt, points, angleOffset) {
    return cc.CardinalSplineTo2.prototype.initWithDuration.call(this, dt, points, angleOffset, 0.5);
  },
  clone: function clone() {
    var action = new cc.CatmullRomTo2();
    action.initWithDuration(this._duration, cloneControlPoints(this._points, this._angleOffset));
    return action;
  }
});
/**
 * !#en Creates an action with a Cardinal Spline array of points and tension.
 * !#zh 按 Catmull Rom 样条曲线轨迹移动到目标位置。
 * @method CatmullRomTo2
 * @param {Number} dt
 * @param {Array} points
 * @param {Number} angleOffset
 * @return {ActionInterval}
 *
 * @example
 * var action1 = cc.CatmullRomTo2(3, array);
 */

cc.catmullRomTo2 = function (dt, points, angleOffset) {
  return new cc.CatmullRomTo2(dt, points, angleOffset);
};
/*
 * An action that moves the target with a CatmullRom curve by a certain distance.  <br/>
 * A Catmull Rom is a Cardinal Spline with a tension of 0.5.<br/>
 * http://en.wikipedia.org/wiki/Cubic_Hermite_spline#Catmull.E2.80.93Rom_spline
 * Relative coordinates.
 *
 * @class CatmullRomBy2
 * @extends CardinalSplineBy2
 *
 * @param {Number} dt
 * @param {Array} points
 *
 * @example
 * var action1 = cc.CatmullRomBy2(3, array);
 */


cc.CatmullRomBy2 = cc.Class({
  name: 'cc.CatmullRomBy2',
  "extends": cc.CardinalSplineBy2,
  ctor: function ctor(dt, points, angleOffset) {
    points && this.initWithDuration(dt, points, angleOffset);
  },
  initWithDuration: function initWithDuration(dt, points, angleOffset) {
    return cc.CardinalSplineTo2.prototype.initWithDuration.call(this, dt, points, angleOffset, 0.5);
  },
  clone: function clone() {
    var action = new cc.CatmullRomBy2();
    action.initWithDuration(this._duration, cloneControlPoints(this._points), this._angleOffset);
    return action;
  }
});
/**
 * !#en Creates an action with a Cardinal Spline array of points and tension.
 * !#zh 按 Catmull Rom 样条曲线轨迹移动指定的距离。
 * @method CatmullRomBy2
 * @param {Number} dt
 * @param {Array} points
 * @param {Number} angleOffset
 * @return {ActionInterval}
 * @example
 * var action1 = cc.CatmullRomBy2(3, array, 90);
 */

cc.catmullRomBy2 = function (dt, points, angleOffset) {
  return new cc.CatmullRomBy2(dt, points, angleOffset);
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxTcGxpbmVFeHRlbmQuanMiXSwibmFtZXMiOlsiY2FyZGluYWxTcGxpbmVBdCIsInAwIiwicDEiLCJwMiIsInAzIiwidGVuc2lvbiIsInQiLCJ0MiIsInQzIiwicyIsImIxIiwiYjIiLCJiMyIsImI0IiwieCIsInkiLCJjYyIsInYyIiwiZ2V0Q29udHJvbFBvaW50QXQiLCJjb250cm9sUG9pbnRzIiwicG9zIiwicCIsIk1hdGgiLCJtaW4iLCJsZW5ndGgiLCJtYXgiLCJyZXZlcnNlQ29udHJvbFBvaW50cyIsIm5ld0FycmF5IiwiaSIsInB1c2giLCJjbG9uZUNvbnRyb2xQb2ludHMiLCJDYXJkaW5hbFNwbGluZVRvMiIsIkNsYXNzIiwibmFtZSIsIkFjdGlvbkludGVydmFsIiwiY2FjaGVMZW5ndGhzIiwiY3RvciIsImR1cmF0aW9uIiwicG9pbnRzIiwiYW5nbGVPZmZzZXQiLCJzcGVlZCIsIl9wb2ludHMiLCJfY2FjaGVMZW5ndGhzIiwiX3RvdGFsTGVuZ3RoIiwiX2RlbHRhVCIsIl90ZW5zaW9uIiwiX2FuZ2xlT2Zmc2V0IiwiX3ByZXZpb3VzUG9zaXRpb24iLCJfYWNjdW11bGF0ZWREaWZmIiwidW5kZWZpbmVkIiwicHJvdG90eXBlIiwiaW5pdFdpdGhEdXJhdGlvbiIsImNhbGwiLCJlcnJvcklEIiwic2V0UG9pbnRzIiwiZ2V0TGVuZ3RoIiwiY2xvbmUiLCJhY3Rpb24iLCJfZHVyYXRpb24iLCJzdGFydFdpdGhUYXJnZXQiLCJ0YXJnZXQiLCJnZXRQIiwiZHQiLCJfY3VtbXV0aWxhdGUiLCJnZXRMdCIsInVwZGF0ZSIsIl9jb21wdXRlRWFzZVRpbWUiLCJsdCIsInBzIiwibG9jRFQiLCJuZXdQb3MiLCJtYWNybyIsIkVOQUJMRV9TVEFDS0FCTEVfQUNUSU9OUyIsInRlbXBYIiwidGVtcFkiLCJsb2NBY2NEaWZmIiwidXBkYXRlUG9zaXRpb24iLCJyZXZlcnNlIiwicmV2ZXJzZVBvaW50cyIsImRlbHRhWCIsImRlbHRhWSIsIl9kaXNhYmxlUm90YXRlIiwiYW5nbGUiLCJhdGFuMiIsIlBJIiwic2V0UG9zaXRpb24iLCJnZXRQb2ludHMiLCJfaW50ZXJwb2xhdGF0ZVBvaW50cyIsIl9zZXRQb2ludCIsInNxcnQiLCJfc2V0UG9pbnQyIiwiZCIsIm4iLCJuZXdQb2ludHMiLCJkaXNhYmxlUm90YXRlIiwiY2FyZGluYWxTcGxpbmVUbzIiLCJjYXJkaW5hbFNwbGluZVRvMl9zcGVlZCIsIkNhcmRpbmFsU3BsaW5lQnkyIiwiX3N0YXJ0UG9zaXRpb24iLCJjb3B5Q29uZmlnIiwic2xpY2UiLCJjdXJyZW50Iiwic3ViIiwicmV2ZXJzZUFycmF5IiwicG9wIiwidW5zaGlmdCIsInBvc1giLCJwb3NZIiwiYSIsImNhcmRpbmFsU3BsaW5lQnkyIiwiQ2F0bXVsbFJvbVRvMiIsImNhdG11bGxSb21UbzIiLCJDYXRtdWxsUm9tQnkyIiwiY2F0bXVsbFJvbUJ5MiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTRCQTs7OztBQUlBOzs7Ozs7Ozs7Ozs7OztBQWNBLFNBQVNBLGdCQUFULENBQTJCQyxFQUEzQixFQUErQkMsRUFBL0IsRUFBbUNDLEVBQW5DLEVBQXVDQyxFQUF2QyxFQUEyQ0MsT0FBM0MsRUFBb0RDLENBQXBELEVBQXVEO0FBQ25ELE1BQUlDLEVBQUUsR0FBR0QsQ0FBQyxHQUFHQSxDQUFiO0FBQ0EsTUFBSUUsRUFBRSxHQUFHRCxFQUFFLEdBQUdELENBQWQ7QUFFQTs7OztBQUdBLE1BQUlHLENBQUMsR0FBRyxDQUFDLElBQUlKLE9BQUwsSUFBZ0IsQ0FBeEI7QUFFQSxNQUFJSyxFQUFFLEdBQUdELENBQUMsSUFBSyxDQUFDRCxFQUFELEdBQU8sSUFBSUQsRUFBWixHQUFtQkQsQ0FBdkIsQ0FBVixDQVRtRCxDQVNPOztBQUMxRCxNQUFJSyxFQUFFLEdBQUdGLENBQUMsSUFBSSxDQUFDRCxFQUFELEdBQU1ELEVBQVYsQ0FBRCxJQUFrQixJQUFJQyxFQUFKLEdBQVMsSUFBSUQsRUFBYixHQUFrQixDQUFwQyxDQUFULENBVm1ELENBVU87O0FBQzFELE1BQUlLLEVBQUUsR0FBR0gsQ0FBQyxJQUFJRCxFQUFFLEdBQUcsSUFBSUQsRUFBVCxHQUFjRCxDQUFsQixDQUFELElBQXlCLENBQUMsQ0FBRCxHQUFLRSxFQUFMLEdBQVUsSUFBSUQsRUFBdkMsQ0FBVCxDQVhtRCxDQVdPOztBQUMxRCxNQUFJTSxFQUFFLEdBQUdKLENBQUMsSUFBSUQsRUFBRSxHQUFHRCxFQUFULENBQVYsQ0FabUQsQ0FZTzs7QUFFMUQsTUFBSU8sQ0FBQyxHQUFJYixFQUFFLENBQUNhLENBQUgsR0FBT0osRUFBUCxHQUFZUixFQUFFLENBQUNZLENBQUgsR0FBT0gsRUFBbkIsR0FBd0JSLEVBQUUsQ0FBQ1csQ0FBSCxHQUFPRixFQUEvQixHQUFvQ1IsRUFBRSxDQUFDVSxDQUFILEdBQU9ELEVBQXBEO0FBQ0EsTUFBSUUsQ0FBQyxHQUFJZCxFQUFFLENBQUNjLENBQUgsR0FBT0wsRUFBUCxHQUFZUixFQUFFLENBQUNhLENBQUgsR0FBT0osRUFBbkIsR0FBd0JSLEVBQUUsQ0FBQ1ksQ0FBSCxHQUFPSCxFQUEvQixHQUFvQ1IsRUFBRSxDQUFDVyxDQUFILEdBQU9GLEVBQXBEO0FBQ0EsU0FBT0csRUFBRSxDQUFDQyxFQUFILENBQU1ILENBQU4sRUFBU0MsQ0FBVCxDQUFQO0FBQ0g7O0FBQUE7QUFFRDs7Ozs7Ozs7QUFPQSxTQUFTRyxpQkFBVCxDQUE0QkMsYUFBNUIsRUFBMkNDLEdBQTNDLEVBQWdEO0FBQzVDLE1BQUlDLENBQUMsR0FBR0MsSUFBSSxDQUFDQyxHQUFMLENBQVNKLGFBQWEsQ0FBQ0ssTUFBZCxHQUF1QixDQUFoQyxFQUFtQ0YsSUFBSSxDQUFDRyxHQUFMLENBQVNMLEdBQVQsRUFBYyxDQUFkLENBQW5DLENBQVI7QUFDQSxTQUFPRCxhQUFhLENBQUNFLENBQUQsQ0FBcEI7QUFDSDs7QUFBQTs7QUFFRCxTQUFTSyxvQkFBVCxDQUErQlAsYUFBL0IsRUFBOEM7QUFDMUMsTUFBSVEsUUFBUSxHQUFHLEVBQWY7O0FBQ0EsT0FBSyxJQUFJQyxDQUFDLEdBQUdULGFBQWEsQ0FBQ0ssTUFBZCxHQUF1QixDQUFwQyxFQUF1Q0ksQ0FBQyxJQUFJLENBQTVDLEVBQStDQSxDQUFDLEVBQWhELEVBQW9EO0FBQ2hERCxJQUFBQSxRQUFRLENBQUNFLElBQVQsQ0FBY2IsRUFBRSxDQUFDQyxFQUFILENBQU1FLGFBQWEsQ0FBQ1MsQ0FBRCxDQUFiLENBQWlCZCxDQUF2QixFQUEwQkssYUFBYSxDQUFDUyxDQUFELENBQWIsQ0FBaUJiLENBQTNDLENBQWQ7QUFDSDs7QUFDRCxTQUFPWSxRQUFQO0FBQ0g7O0FBRUQsU0FBU0csa0JBQVQsQ0FBNkJYLGFBQTdCLEVBQTRDO0FBQ3hDLE1BQUlRLFFBQVEsR0FBRyxFQUFmOztBQUNBLE9BQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR1QsYUFBYSxDQUFDSyxNQUFsQyxFQUEwQ0ksQ0FBQyxFQUEzQztBQUNJRCxJQUFBQSxRQUFRLENBQUNFLElBQVQsQ0FBY2IsRUFBRSxDQUFDQyxFQUFILENBQU1FLGFBQWEsQ0FBQ1MsQ0FBRCxDQUFiLENBQWlCZCxDQUF2QixFQUEwQkssYUFBYSxDQUFDUyxDQUFELENBQWIsQ0FBaUJiLENBQTNDLENBQWQ7QUFESjs7QUFFQSxTQUFPWSxRQUFQO0FBQ0g7QUFFRDs7OztBQUlBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkFYLEVBQUUsQ0FBQ2UsaUJBQUgsR0FBdUJmLEVBQUUsQ0FBQ2dCLEtBQUgsQ0FBUztBQUM1QkMsRUFBQUEsSUFBSSxFQUFFLHNCQURzQjtBQUU1QixhQUFTakIsRUFBRSxDQUFDa0IsY0FGZ0I7QUFHNUJDLEVBQUFBLFlBQVksRUFBRSxJQUhjO0FBSzVCQyxFQUFBQSxJQUFJLEVBQUUsY0FBVUMsUUFBVixFQUFvQkMsTUFBcEIsRUFBNEJDLFdBQTVCLEVBQXlDbEMsT0FBekMsRUFBa0RtQyxLQUFsRCxFQUF5RDtBQUMzRDtBQUNBLFNBQUtDLE9BQUwsR0FBZSxFQUFmO0FBQ0EsU0FBS0MsYUFBTCxHQUFxQixFQUFyQjtBQUNBLFNBQUtDLFlBQUwsR0FBb0IsQ0FBcEI7QUFDQSxTQUFLQyxPQUFMLEdBQWUsQ0FBZjtBQUNBLFNBQUtDLFFBQUwsR0FBZ0IsQ0FBaEI7QUFDQSxTQUFLQyxZQUFMLEdBQW9CLENBQXBCO0FBQ0EsU0FBS0MsaUJBQUwsR0FBeUIsSUFBekI7QUFDQSxTQUFLQyxnQkFBTCxHQUF3QixJQUF4QjtBQUNBM0MsSUFBQUEsT0FBTyxLQUFLNEMsU0FBWixJQUF5QmpDLEVBQUUsQ0FBQ2UsaUJBQUgsQ0FBcUJtQixTQUFyQixDQUErQkMsZ0JBQS9CLENBQWdEQyxJQUFoRCxDQUFxRCxJQUFyRCxFQUEyRGYsUUFBM0QsRUFBcUVDLE1BQXJFLEVBQTZFQyxXQUE3RSxFQUEwRmxDLE9BQTFGLEVBQW1HbUMsS0FBbkcsQ0FBekI7QUFDSCxHQWhCMkI7QUFrQjVCVyxFQUFBQSxnQkFBZ0IsRUFBQywwQkFBVWQsUUFBVixFQUFvQkMsTUFBcEIsRUFBNEJDLFdBQTVCLEVBQXlDbEMsT0FBekMsRUFBa0RtQyxLQUFsRCxFQUF5RDtBQUN0RSxRQUFJLENBQUNGLE1BQUQsSUFBV0EsTUFBTSxDQUFDZCxNQUFQLEtBQWtCLENBQWpDLEVBQW9DO0FBQ2hDUixNQUFBQSxFQUFFLENBQUNxQyxPQUFILENBQVcsSUFBWDtBQUNBLGFBQU8sS0FBUDtBQUNIOztBQUVELFNBQUtSLFFBQUwsR0FBZ0J4QyxPQUFoQjtBQUNBLFNBQUt5QyxZQUFMLEdBQW9CUCxXQUFwQjtBQUNBLFNBQUtlLFNBQUwsQ0FBZWhCLE1BQWY7QUFDQSxRQUFHRSxLQUFILEVBQ0lILFFBQVEsR0FBRyxLQUFLa0IsU0FBTCxLQUFpQmYsS0FBNUI7O0FBRUosUUFBSXhCLEVBQUUsQ0FBQ2tCLGNBQUgsQ0FBa0JnQixTQUFsQixDQUE0QkMsZ0JBQTVCLENBQTZDQyxJQUE3QyxDQUFrRCxJQUFsRCxFQUF3RGYsUUFBeEQsQ0FBSixFQUF1RTtBQUNuRSxhQUFPLElBQVA7QUFDSDs7QUFDRCxXQUFPLEtBQVA7QUFDSCxHQWxDMkI7QUFvQzVCbUIsRUFBQUEsS0FBSyxFQUFDLGlCQUFZO0FBQ2QsUUFBSUMsTUFBTSxHQUFHLElBQUl6QyxFQUFFLENBQUNlLGlCQUFQLEVBQWI7QUFDQTBCLElBQUFBLE1BQU0sQ0FBQ04sZ0JBQVAsQ0FBd0IsS0FBS08sU0FBN0IsRUFBd0M1QixrQkFBa0IsQ0FBQyxLQUFLVyxPQUFOLENBQTFELEVBQTBFLEtBQUtJLFFBQS9FO0FBQ0EsV0FBT1ksTUFBUDtBQUNILEdBeEMyQjtBQTBDNUJFLEVBQUFBLGVBQWUsRUFBQyx5QkFBVUMsTUFBVixFQUFrQjtBQUM5QjVDLElBQUFBLEVBQUUsQ0FBQ2tCLGNBQUgsQ0FBa0JnQixTQUFsQixDQUE0QlMsZUFBNUIsQ0FBNENQLElBQTVDLENBQWlELElBQWpELEVBQXVEUSxNQUF2RCxFQUQ4QixDQUU5Qjs7QUFDQSxTQUFLaEIsT0FBTCxHQUFlLEtBQUssS0FBS0gsT0FBTCxDQUFhakIsTUFBYixHQUFzQixDQUEzQixDQUFmO0FBQ0EsU0FBS3VCLGlCQUFMLEdBQXlCL0IsRUFBRSxDQUFDQyxFQUFILENBQU0sS0FBSzJDLE1BQUwsQ0FBWTlDLENBQWxCLEVBQXFCLEtBQUs4QyxNQUFMLENBQVk3QyxDQUFqQyxDQUF6QjtBQUNBLFNBQUtpQyxnQkFBTCxHQUF3QmhDLEVBQUUsQ0FBQ0MsRUFBSCxDQUFNLENBQU4sRUFBUyxDQUFULENBQXhCO0FBQ0gsR0FoRDJCO0FBa0Q1QjRDLEVBQUFBLElBQUksRUFBRSxjQUFTQyxFQUFULEVBQVk7QUFDZCxTQUFJLElBQUlsQyxDQUFDLEdBQUMsQ0FBVixFQUFhQSxDQUFDLEdBQUMsS0FBS21DLFlBQUwsQ0FBa0J2QyxNQUFqQyxFQUF5Q0ksQ0FBQyxFQUExQyxFQUE4QztBQUMxQyxVQUFHLEtBQUttQyxZQUFMLENBQWtCbkMsQ0FBbEIsSUFBdUJrQyxFQUExQixFQUNJLE9BQU9sQyxDQUFQO0FBQ1A7O0FBQ0QsV0FBTyxLQUFLbUMsWUFBTCxDQUFrQnZDLE1BQXpCO0FBQ0gsR0F4RDJCO0FBMEQ1QndDLEVBQUFBLEtBQUssRUFBRSxlQUFTM0MsQ0FBVCxFQUFZeUMsRUFBWixFQUFlO0FBQ2xCLFFBQUd6QyxDQUFDLElBQUUsQ0FBTixFQUFTO0FBQ0wsYUFBT3lDLEVBQUUsR0FBQyxLQUFLcEIsYUFBTCxDQUFtQnJCLENBQW5CLENBQVY7QUFDSCxLQUZELE1BRU87QUFDSCxhQUFPLENBQUN5QyxFQUFFLEdBQUMsS0FBS0MsWUFBTCxDQUFrQjFDLENBQUMsR0FBQyxDQUFwQixDQUFKLElBQTRCLEtBQUtxQixhQUFMLENBQW1CckIsQ0FBbkIsQ0FBbkM7QUFDSDtBQUNKLEdBaEUyQjtBQWtFNUI0QyxFQUFBQSxNQUFNLEVBQUMsZ0JBQVVILEVBQVYsRUFBYztBQUNqQkEsSUFBQUEsRUFBRSxHQUFHLEtBQUtJLGdCQUFMLENBQXNCSixFQUF0QixDQUFMO0FBQ0EsUUFBSXpDLENBQUosRUFBTzhDLEVBQVA7QUFDQSxRQUFJQyxFQUFFLEdBQUcsS0FBSzNCLE9BQWQsQ0FIaUIsQ0FJakI7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsUUFBSXFCLEVBQUUsS0FBSyxDQUFYLEVBQWM7QUFDVnpDLE1BQUFBLENBQUMsR0FBRytDLEVBQUUsQ0FBQzVDLE1BQUgsR0FBWSxDQUFoQjtBQUNBMkMsTUFBQUEsRUFBRSxHQUFHLENBQUw7QUFDSCxLQUhELE1BR087QUFDSCxVQUFJRSxLQUFLLEdBQUcsS0FBS3pCLE9BQWpCLENBREcsQ0FFSDtBQUNBOztBQUNBdkIsTUFBQUEsQ0FBQyxHQUFHLEtBQUt3QyxJQUFMLENBQVVDLEVBQVYsQ0FBSjtBQUNBSyxNQUFBQSxFQUFFLEdBQUcsS0FBS0gsS0FBTCxDQUFXM0MsQ0FBWCxFQUFjeUMsRUFBZCxDQUFMLENBTEcsQ0FNSDtBQUNIOztBQUVELFFBQUlRLE1BQU0sR0FBR3RFLGdCQUFnQixDQUN6QmtCLGlCQUFpQixDQUFDa0QsRUFBRCxFQUFLL0MsQ0FBQyxHQUFHLENBQVQsQ0FEUSxFQUV6QkgsaUJBQWlCLENBQUNrRCxFQUFELEVBQUsvQyxDQUFDLEdBQUcsQ0FBVCxDQUZRLEVBR3pCSCxpQkFBaUIsQ0FBQ2tELEVBQUQsRUFBSy9DLENBQUMsR0FBRyxDQUFULENBSFEsRUFJekJILGlCQUFpQixDQUFDa0QsRUFBRCxFQUFLL0MsQ0FBQyxHQUFHLENBQVQsQ0FKUSxFQUt6QixLQUFLd0IsUUFMb0IsRUFLVnNCLEVBTFUsQ0FBN0I7O0FBT0EsUUFBSW5ELEVBQUUsQ0FBQ3VELEtBQUgsQ0FBU0Msd0JBQWIsRUFBdUM7QUFDbkMsVUFBSUMsS0FBSixFQUFXQyxLQUFYO0FBQ0FELE1BQUFBLEtBQUssR0FBRyxLQUFLYixNQUFMLENBQVk5QyxDQUFaLEdBQWdCLEtBQUtpQyxpQkFBTCxDQUF1QmpDLENBQS9DO0FBQ0E0RCxNQUFBQSxLQUFLLEdBQUcsS0FBS2QsTUFBTCxDQUFZN0MsQ0FBWixHQUFnQixLQUFLZ0MsaUJBQUwsQ0FBdUJoQyxDQUEvQzs7QUFDQSxVQUFJMEQsS0FBSyxLQUFLLENBQVYsSUFBZUMsS0FBSyxLQUFLLENBQTdCLEVBQWdDO0FBQzVCLFlBQUlDLFVBQVUsR0FBRyxLQUFLM0IsZ0JBQXRCO0FBQ0F5QixRQUFBQSxLQUFLLEdBQUdFLFVBQVUsQ0FBQzdELENBQVgsR0FBZTJELEtBQXZCO0FBQ0FDLFFBQUFBLEtBQUssR0FBR0MsVUFBVSxDQUFDNUQsQ0FBWCxHQUFlMkQsS0FBdkI7QUFDQUMsUUFBQUEsVUFBVSxDQUFDN0QsQ0FBWCxHQUFlMkQsS0FBZjtBQUNBRSxRQUFBQSxVQUFVLENBQUM1RCxDQUFYLEdBQWUyRCxLQUFmO0FBQ0FKLFFBQUFBLE1BQU0sQ0FBQ3hELENBQVAsSUFBWTJELEtBQVo7QUFDQUgsUUFBQUEsTUFBTSxDQUFDdkQsQ0FBUCxJQUFZMkQsS0FBWjtBQUNIO0FBQ0o7O0FBQ0QsU0FBS0UsY0FBTCxDQUFvQk4sTUFBcEI7QUFDSCxHQTVHMkI7QUE4RzVCTyxFQUFBQSxPQUFPLEVBQUMsbUJBQVk7QUFDaEIsUUFBSUMsYUFBYSxHQUFHcEQsb0JBQW9CLENBQUMsS0FBS2UsT0FBTixDQUF4QztBQUNBLFdBQU96QixFQUFFLENBQUNlLGlCQUFILENBQXFCLEtBQUsyQixTQUExQixFQUFxQ29CLGFBQXJDLEVBQW9ELEtBQUtqQyxRQUF6RCxDQUFQO0FBQ0gsR0FqSDJCOztBQW1INUI7Ozs7O0FBS0ErQixFQUFBQSxjQUFjLEVBQUMsd0JBQVVOLE1BQVYsRUFBa0I7QUFDN0IsUUFBSVMsTUFBTSxHQUFHVCxNQUFNLENBQUN4RCxDQUFQLEdBQVMsS0FBS2lDLGlCQUFMLENBQXVCakMsQ0FBN0M7QUFDQSxRQUFJa0UsTUFBTSxHQUFHVixNQUFNLENBQUN2RCxDQUFQLEdBQVMsS0FBS2dDLGlCQUFMLENBQXVCaEMsQ0FBN0M7O0FBQ0EsUUFBRyxDQUFDLEtBQUtrRSxjQUFULEVBQXlCO0FBQ3JCLFVBQUdGLE1BQU0sS0FBSyxDQUFYLElBQWdCQyxNQUFNLEtBQUssQ0FBOUIsRUFDSSxLQUFLcEIsTUFBTCxDQUFZc0IsS0FBWixHQUFvQjVELElBQUksQ0FBQzZELEtBQUwsQ0FBV0gsTUFBWCxFQUFtQkQsTUFBbkIsSUFBMkIsR0FBM0IsR0FBZ0N6RCxJQUFJLENBQUM4RCxFQUFyQyxHQUEyQyxLQUFLdEMsWUFBcEU7QUFDUDs7QUFDRCxTQUFLYyxNQUFMLENBQVl5QixXQUFaLENBQXdCZixNQUF4QjtBQUNBLFNBQUt2QixpQkFBTCxHQUF5QnVCLE1BQXpCO0FBQ0gsR0FqSTJCOztBQW1JNUI7Ozs7O0FBS0FnQixFQUFBQSxTQUFTLEVBQUMscUJBQVk7QUFDbEIsV0FBTyxLQUFLN0MsT0FBWjtBQUNILEdBMUkyQjs7QUE0STVCOzs7OztBQUtBYSxFQUFBQSxTQUFTLEVBQUMsbUJBQVVoQixNQUFWLEVBQWtCO0FBQ3hCQSxJQUFBQSxNQUFNLEdBQUcsS0FBS2lELG9CQUFMLENBQTBCakQsTUFBMUIsRUFBa0MsRUFBbEMsQ0FBVDs7QUFDQSxTQUFLa0QsU0FBTCxDQUFlbEQsTUFBZjtBQUNILEdBcEoyQjtBQXNKNUJrRCxFQUFBQSxTQXRKNEIscUJBc0psQmxELE1BdEprQixFQXNKWDtBQUNiLFNBQUtHLE9BQUwsR0FBZUgsTUFBZjtBQUNBLFNBQUtJLGFBQUwsR0FBcUIsRUFBckI7QUFDQSxTQUFLcUIsWUFBTCxHQUFvQixFQUFwQjs7QUFDQSxTQUFJLElBQUluQyxDQUFDLEdBQUMsQ0FBVixFQUFhQSxDQUFDLEdBQUNVLE1BQU0sQ0FBQ2QsTUFBUCxHQUFjLENBQTdCLEVBQWdDSSxDQUFDLEVBQWpDLEVBQXFDO0FBQ2pDLFVBQUkxQixFQUFFLEdBQUdvQyxNQUFNLENBQUNWLENBQUQsQ0FBZjtBQUNBLFVBQUl6QixFQUFFLEdBQUdtQyxNQUFNLENBQUNWLENBQUMsR0FBQyxDQUFILENBQWY7QUFDQSxVQUFJSixNQUFNLEdBQUdGLElBQUksQ0FBQ21FLElBQUwsQ0FBVSxDQUFDdkYsRUFBRSxDQUFDWSxDQUFILEdBQUtYLEVBQUUsQ0FBQ1csQ0FBVCxLQUFhWixFQUFFLENBQUNZLENBQUgsR0FBS1gsRUFBRSxDQUFDVyxDQUFyQixJQUEwQixDQUFDWixFQUFFLENBQUNhLENBQUgsR0FBS1osRUFBRSxDQUFDWSxDQUFULEtBQWFiLEVBQUUsQ0FBQ2EsQ0FBSCxHQUFLWixFQUFFLENBQUNZLENBQXJCLENBQXBDLENBQWI7O0FBQ0EsV0FBSzJCLGFBQUwsQ0FBbUJiLElBQW5CLENBQXdCTCxNQUF4QjtBQUNIOztBQUNELFNBQUttQixZQUFMLEdBQW9CLENBQXBCOztBQUNBLFNBQUksSUFBSWYsQ0FBQyxHQUFDLENBQVYsRUFBYUEsQ0FBQyxHQUFDLEtBQUtjLGFBQUwsQ0FBbUJsQixNQUFsQyxFQUEwQ0ksQ0FBQyxFQUEzQyxFQUErQztBQUMzQyxXQUFLZSxZQUFMLElBQXFCLEtBQUtELGFBQUwsQ0FBbUJkLENBQW5CLENBQXJCOztBQUNBLFdBQUttQyxZQUFMLENBQWtCbEMsSUFBbEIsQ0FBdUIsS0FBS2MsWUFBNUI7QUFDSDs7QUFDRCxTQUFJLElBQUlmLENBQUMsR0FBQyxDQUFWLEVBQWFBLENBQUMsR0FBQyxLQUFLYyxhQUFMLENBQW1CbEIsTUFBbEMsRUFBMENJLENBQUMsRUFBM0MsRUFBK0M7QUFDM0MsV0FBS2MsYUFBTCxDQUFtQmQsQ0FBbkIsSUFBd0IsS0FBS2MsYUFBTCxDQUFtQmQsQ0FBbkIsSUFBc0IsS0FBS2UsWUFBbkQ7QUFDQSxXQUFLb0IsWUFBTCxDQUFrQm5DLENBQWxCLElBQXVCLEtBQUttQyxZQUFMLENBQWtCbkMsQ0FBbEIsSUFBcUIsS0FBS2UsWUFBakQ7QUFDSDtBQUNKLEdBeksyQjtBQTJLNUIrQyxFQUFBQSxVQTNLNEIsc0JBMktqQnBELE1BM0tpQixFQTJLVjtBQUNkLFNBQUtHLE9BQUwsR0FBZUgsTUFBZjtBQUNBLFNBQUtJLGFBQUwsR0FBcUIsRUFBckI7QUFDQSxTQUFLcUIsWUFBTCxHQUFvQixFQUFwQjs7QUFDQSxTQUFJLElBQUluQyxDQUFDLEdBQUMsQ0FBVixFQUFhQSxDQUFDLEdBQUNVLE1BQU0sQ0FBQ2QsTUFBUCxHQUFjLENBQTdCLEVBQWdDSSxDQUFDLEVBQWpDLEVBQXFDO0FBQ2pDLFVBQUkxQixFQUFFLEdBQUdvQyxNQUFNLENBQUNWLENBQUQsQ0FBZjtBQUNBLFVBQUl6QixFQUFFLEdBQUdtQyxNQUFNLENBQUNWLENBQUMsR0FBQyxDQUFILENBQWY7QUFDQSxVQUFJSixNQUFNLEdBQUcsQ0FBYjs7QUFDQSxXQUFLa0IsYUFBTCxDQUFtQmIsSUFBbkIsQ0FBd0JMLE1BQXhCO0FBQ0g7O0FBQ0QsU0FBS21CLFlBQUwsR0FBb0IsQ0FBcEI7O0FBQ0EsU0FBSSxJQUFJZixDQUFDLEdBQUMsQ0FBVixFQUFhQSxDQUFDLEdBQUMsS0FBS2MsYUFBTCxDQUFtQmxCLE1BQWxDLEVBQTBDSSxDQUFDLEVBQTNDLEVBQStDO0FBQzNDLFdBQUtlLFlBQUwsSUFBcUIsS0FBS0QsYUFBTCxDQUFtQmQsQ0FBbkIsQ0FBckI7O0FBQ0EsV0FBS21DLFlBQUwsQ0FBa0JsQyxJQUFsQixDQUF1QixLQUFLYyxZQUE1QjtBQUNIOztBQUNELFNBQUksSUFBSWYsQ0FBQyxHQUFDLENBQVYsRUFBYUEsQ0FBQyxHQUFDLEtBQUtjLGFBQUwsQ0FBbUJsQixNQUFsQyxFQUEwQ0ksQ0FBQyxFQUEzQyxFQUErQztBQUMzQyxXQUFLYyxhQUFMLENBQW1CZCxDQUFuQixJQUF3QixLQUFLYyxhQUFMLENBQW1CZCxDQUFuQixJQUFzQixLQUFLZSxZQUFuRDtBQUNBLFdBQUtvQixZQUFMLENBQWtCbkMsQ0FBbEIsSUFBdUIsS0FBS21DLFlBQUwsQ0FBa0JuQyxDQUFsQixJQUFxQixLQUFLZSxZQUFqRDtBQUNIO0FBQ0osR0E5TDJCO0FBZ001QjRDLEVBQUFBLG9CQUFvQixFQUFFLDhCQUFTakQsTUFBVCxFQUFpQnFELENBQWpCLEVBQW1CO0FBQ3JDLFNBQUtILFNBQUwsQ0FBZWxELE1BQWY7O0FBQ0EsUUFBSXNELENBQUMsR0FBRyxDQUFDdEQsTUFBTSxDQUFDZCxNQUFQLEdBQWMsQ0FBZixJQUFvQm1FLENBQTVCO0FBQ0EsUUFBSUUsU0FBUyxHQUFHLEVBQWhCO0FBQ0EsUUFBSXhFLENBQUMsR0FBRyxDQUFSO0FBQ0EsUUFBSThDLEVBQUUsR0FBRyxDQUFUO0FBQ0EsUUFBSUMsRUFBRSxHQUFHOUIsTUFBVDs7QUFDQSxTQUFJLElBQUlWLENBQUMsR0FBQyxDQUFWLEVBQWFBLENBQUMsR0FBQ2dFLENBQWYsRUFBa0JoRSxDQUFDLEVBQW5CLEVBQXVCO0FBQ25CLFVBQUlrQyxFQUFFLEdBQUdsQyxDQUFDLEdBQUMsQ0FBRixJQUFLZ0UsQ0FBQyxHQUFDLENBQVAsQ0FBVDs7QUFDQSxVQUFJOUIsRUFBRSxLQUFLLENBQVgsRUFBYztBQUNWekMsUUFBQUEsQ0FBQyxHQUFHK0MsRUFBRSxDQUFDNUMsTUFBSCxHQUFZLENBQWhCO0FBQ0EyQyxRQUFBQSxFQUFFLEdBQUcsQ0FBTDtBQUNILE9BSEQsTUFHTztBQUNILFlBQUlFLEtBQUssR0FBRyxLQUFLL0IsTUFBTSxDQUFDZCxNQUFQLEdBQWdCLENBQXJCLENBQVosQ0FERyxDQUVIO0FBQ0E7O0FBQ0FILFFBQUFBLENBQUMsR0FBRyxLQUFLd0MsSUFBTCxDQUFVQyxFQUFWLENBQUo7QUFDQUssUUFBQUEsRUFBRSxHQUFHLEtBQUtILEtBQUwsQ0FBVzNDLENBQVgsRUFBY3lDLEVBQWQsQ0FBTDtBQUNIOztBQUNELFVBQUlRLE1BQU0sR0FBR3RFLGdCQUFnQixDQUN6QmtCLGlCQUFpQixDQUFDa0QsRUFBRCxFQUFLL0MsQ0FBQyxHQUFHLENBQVQsQ0FEUSxFQUV6QkgsaUJBQWlCLENBQUNrRCxFQUFELEVBQUsvQyxDQUFDLEdBQUcsQ0FBVCxDQUZRLEVBR3pCSCxpQkFBaUIsQ0FBQ2tELEVBQUQsRUFBSy9DLENBQUMsR0FBRyxDQUFULENBSFEsRUFJekJILGlCQUFpQixDQUFDa0QsRUFBRCxFQUFLL0MsQ0FBQyxHQUFHLENBQVQsQ0FKUSxFQUt6QixLQUFLd0IsUUFMb0IsRUFLVnNCLEVBTFUsQ0FBN0I7QUFNQTBCLE1BQUFBLFNBQVMsQ0FBQ2hFLElBQVYsQ0FBZXlDLE1BQWY7QUFDSDs7QUFDRCxXQUFPdUIsU0FBUDtBQUNILEdBNU4yQjtBQThONUJ0QyxFQUFBQSxTQUFTLEVBQUUscUJBQVU7QUFDakIsV0FBTyxLQUFLWixZQUFaO0FBQ0gsR0FoTzJCO0FBa081Qm1ELEVBQUFBLGFBQWEsRUFBRSx5QkFBVTtBQUNyQixTQUFLYixjQUFMLEdBQXNCLElBQXRCO0FBQ0g7QUFwTzJCLENBQVQsQ0FBdkI7QUF3T0E7Ozs7Ozs7Ozs7Ozs7OztBQWNBakUsRUFBRSxDQUFDK0UsaUJBQUgsR0FBdUIsVUFBVTFELFFBQVYsRUFBb0JDLE1BQXBCLEVBQTRCQyxXQUE1QixFQUF5Q2xDLE9BQXpDLEVBQWtEO0FBQ3JFLFNBQU8sSUFBSVcsRUFBRSxDQUFDZSxpQkFBUCxDQUF5Qk0sUUFBekIsRUFBbUNDLE1BQW5DLEVBQTJDQyxXQUEzQyxFQUF3RGxDLE9BQXhELENBQVA7QUFDSCxDQUZEO0FBSUE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFjQVcsRUFBRSxDQUFDZ0YsdUJBQUgsR0FBNkIsVUFBVXhELEtBQVYsRUFBaUJGLE1BQWpCLEVBQXlCQyxXQUF6QixFQUFzQ2xDLE9BQXRDLEVBQStDO0FBQ3hFLFNBQU8sSUFBSVcsRUFBRSxDQUFDZSxpQkFBUCxDQUF5QixDQUF6QixFQUE0Qk8sTUFBNUIsRUFBb0NDLFdBQXBDLEVBQWlEbEMsT0FBakQsRUFBMERtQyxLQUExRCxDQUFQO0FBQ0gsQ0FGRDtBQUlBOzs7Ozs7Ozs7Ozs7Ozs7OztBQWVBeEIsRUFBRSxDQUFDaUYsaUJBQUgsR0FBdUJqRixFQUFFLENBQUNnQixLQUFILENBQVM7QUFDNUJDLEVBQUFBLElBQUksRUFBRSxzQkFEc0I7QUFFNUIsYUFBU2pCLEVBQUUsQ0FBQ2UsaUJBRmdCO0FBSTVCSyxFQUFBQSxJQUFJLEVBQUMsY0FBVUMsUUFBVixFQUFvQkMsTUFBcEIsRUFBNEJDLFdBQTVCLEVBQXlDbEMsT0FBekMsRUFBa0Q7QUFDbkQsU0FBSzZGLGNBQUwsR0FBc0JsRixFQUFFLENBQUNDLEVBQUgsQ0FBTSxDQUFOLEVBQVMsQ0FBVCxDQUF0QjtBQUNBWixJQUFBQSxPQUFPLEtBQUs0QyxTQUFaLElBQXlCLEtBQUtFLGdCQUFMLENBQXNCZCxRQUF0QixFQUFnQ0MsTUFBaEMsRUFBd0NDLFdBQXhDLEVBQXFEbEMsT0FBckQsQ0FBekI7QUFDSCxHQVAyQjtBQVM1QnNELEVBQUFBLGVBQWUsRUFBQyx5QkFBVUMsTUFBVixFQUFrQjtBQUM5QjVDLElBQUFBLEVBQUUsQ0FBQ2UsaUJBQUgsQ0FBcUJtQixTQUFyQixDQUErQlMsZUFBL0IsQ0FBK0NQLElBQS9DLENBQW9ELElBQXBELEVBQTBEUSxNQUExRDtBQUNBLFNBQUtzQyxjQUFMLENBQW9CcEYsQ0FBcEIsR0FBd0I4QyxNQUFNLENBQUM5QyxDQUEvQjtBQUNBLFNBQUtvRixjQUFMLENBQW9CbkYsQ0FBcEIsR0FBd0I2QyxNQUFNLENBQUM3QyxDQUEvQjtBQUNILEdBYjJCO0FBZTVCOEQsRUFBQUEsT0FBTyxFQUFDLG1CQUFZO0FBQ2hCLFFBQUlzQixVQUFVLEdBQUcsS0FBSzFELE9BQUwsQ0FBYTJELEtBQWIsRUFBakI7O0FBQ0EsUUFBSUMsT0FBSixDQUZnQixDQUdoQjtBQUNBO0FBQ0E7O0FBQ0EsUUFBSWhGLENBQUMsR0FBRzhFLFVBQVUsQ0FBQyxDQUFELENBQWxCOztBQUNBLFNBQUssSUFBSXZFLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUd1RSxVQUFVLENBQUMzRSxNQUEvQixFQUF1QyxFQUFFSSxDQUF6QyxFQUE0QztBQUN4Q3lFLE1BQUFBLE9BQU8sR0FBR0YsVUFBVSxDQUFDdkUsQ0FBRCxDQUFwQjtBQUNBdUUsTUFBQUEsVUFBVSxDQUFDdkUsQ0FBRCxDQUFWLEdBQWdCeUUsT0FBTyxDQUFDQyxHQUFSLENBQVlqRixDQUFaLENBQWhCO0FBQ0FBLE1BQUFBLENBQUMsR0FBR2dGLE9BQUo7QUFDSCxLQVhlLENBYWhCOzs7QUFDQSxRQUFJRSxZQUFZLEdBQUc3RSxvQkFBb0IsQ0FBQ3lFLFVBQUQsQ0FBdkMsQ0FkZ0IsQ0FnQmhCOztBQUNBOUUsSUFBQUEsQ0FBQyxHQUFHa0YsWUFBWSxDQUFFQSxZQUFZLENBQUMvRSxNQUFiLEdBQXNCLENBQXhCLENBQWhCO0FBQ0ErRSxJQUFBQSxZQUFZLENBQUNDLEdBQWI7QUFFQW5GLElBQUFBLENBQUMsQ0FBQ1AsQ0FBRixHQUFNLENBQUNPLENBQUMsQ0FBQ1AsQ0FBVDtBQUNBTyxJQUFBQSxDQUFDLENBQUNOLENBQUYsR0FBTSxDQUFDTSxDQUFDLENBQUNOLENBQVQ7QUFFQXdGLElBQUFBLFlBQVksQ0FBQ0UsT0FBYixDQUFxQnBGLENBQXJCOztBQUNBLFNBQUssSUFBSU8sQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBRzJFLFlBQVksQ0FBQy9FLE1BQWpDLEVBQXlDLEVBQUVJLENBQTNDLEVBQThDO0FBQzFDeUUsTUFBQUEsT0FBTyxHQUFHRSxZQUFZLENBQUMzRSxDQUFELENBQXRCO0FBQ0F5RSxNQUFBQSxPQUFPLENBQUN2RixDQUFSLEdBQVksQ0FBQ3VGLE9BQU8sQ0FBQ3ZGLENBQXJCO0FBQ0F1RixNQUFBQSxPQUFPLENBQUN0RixDQUFSLEdBQVksQ0FBQ3NGLE9BQU8sQ0FBQ3RGLENBQXJCO0FBQ0FzRixNQUFBQSxPQUFPLENBQUN2RixDQUFSLElBQWFPLENBQUMsQ0FBQ1AsQ0FBZjtBQUNBdUYsTUFBQUEsT0FBTyxDQUFDdEYsQ0FBUixJQUFhTSxDQUFDLENBQUNOLENBQWY7QUFDQXdGLE1BQUFBLFlBQVksQ0FBQzNFLENBQUQsQ0FBWixHQUFrQnlFLE9BQWxCO0FBQ0FoRixNQUFBQSxDQUFDLEdBQUdnRixPQUFKO0FBQ0g7O0FBQ0QsV0FBT3JGLEVBQUUsQ0FBQ2lGLGlCQUFILENBQXFCLEtBQUt2QyxTQUExQixFQUFxQzZDLFlBQXJDLEVBQW1ELEtBQUt6RCxZQUF4RCxFQUFzRSxLQUFLRCxRQUEzRSxDQUFQO0FBQ0gsR0FqRDJCOztBQW1ENUI7Ozs7O0FBS0ErQixFQUFBQSxjQUFjLEVBQUMsd0JBQVVOLE1BQVYsRUFBa0I7QUFDN0IsUUFBSWxELEdBQUcsR0FBRyxLQUFLOEUsY0FBZjtBQUNBLFFBQUlRLElBQUksR0FBR3BDLE1BQU0sQ0FBQ3hELENBQVAsR0FBV00sR0FBRyxDQUFDTixDQUExQjtBQUNBLFFBQUk2RixJQUFJLEdBQUdyQyxNQUFNLENBQUN2RCxDQUFQLEdBQVdLLEdBQUcsQ0FBQ0wsQ0FBMUI7QUFFQSxRQUFJZ0UsTUFBTSxHQUFHMkIsSUFBSSxHQUFDLEtBQUszRCxpQkFBTCxDQUF1QmpDLENBQXpDO0FBQ0EsUUFBSWtFLE1BQU0sR0FBRzJCLElBQUksR0FBQyxLQUFLNUQsaUJBQUwsQ0FBdUJoQyxDQUF6QztBQUNBLFFBQUdnRSxNQUFNLEtBQUssQ0FBWCxJQUFnQkMsTUFBTSxLQUFLLENBQTlCLEVBQ0ksS0FBS3BCLE1BQUwsQ0FBWXNCLEtBQVosR0FBb0I1RCxJQUFJLENBQUM2RCxLQUFMLENBQVdILE1BQVgsRUFBbUJELE1BQW5CLElBQTJCLEdBQTNCLEdBQWdDekQsSUFBSSxDQUFDOEQsRUFBckMsR0FBMkMsS0FBS3RDLFlBQXBFO0FBRUosU0FBS0MsaUJBQUwsQ0FBdUJqQyxDQUF2QixHQUEyQjRGLElBQTNCO0FBQ0EsU0FBSzNELGlCQUFMLENBQXVCaEMsQ0FBdkIsR0FBMkI0RixJQUEzQjtBQUNBLFNBQUsvQyxNQUFMLENBQVl5QixXQUFaLENBQXdCcUIsSUFBeEIsRUFBOEJDLElBQTlCO0FBQ0gsR0FyRTJCO0FBdUU1Qm5ELEVBQUFBLEtBQUssRUFBQyxpQkFBWTtBQUNkLFFBQUlvRCxDQUFDLEdBQUcsSUFBSTVGLEVBQUUsQ0FBQ2lGLGlCQUFQLEVBQVI7QUFDQVcsSUFBQUEsQ0FBQyxDQUFDekQsZ0JBQUYsQ0FBbUIsS0FBS08sU0FBeEIsRUFBbUM1QixrQkFBa0IsQ0FBQyxLQUFLVyxPQUFOLENBQXJELEVBQXFFLEtBQUtLLFlBQTFFLEVBQXdGLEtBQUtELFFBQTdGO0FBQ0EsV0FBTytELENBQVA7QUFDSDtBQTNFMkIsQ0FBVCxDQUF2QjtBQThFQTs7Ozs7Ozs7Ozs7O0FBV0E1RixFQUFFLENBQUM2RixpQkFBSCxHQUF1QixVQUFVeEUsUUFBVixFQUFvQkMsTUFBcEIsRUFBNEJDLFdBQTVCLEVBQXlDbEMsT0FBekMsRUFBa0Q7QUFDckUsU0FBTyxJQUFJVyxFQUFFLENBQUNpRixpQkFBUCxDQUF5QjVELFFBQXpCLEVBQW1DQyxNQUFuQyxFQUEyQ0MsV0FBM0MsRUFBd0RsQyxPQUF4RCxDQUFQO0FBQ0gsQ0FGRDtBQUlBOzs7Ozs7Ozs7Ozs7Ozs7OztBQWVBVyxFQUFFLENBQUM4RixhQUFILEdBQW1COUYsRUFBRSxDQUFDZ0IsS0FBSCxDQUFTO0FBQ3hCQyxFQUFBQSxJQUFJLEVBQUUsa0JBRGtCO0FBRXhCLGFBQVNqQixFQUFFLENBQUNlLGlCQUZZO0FBSXhCSyxFQUFBQSxJQUFJLEVBQUUsY0FBUzBCLEVBQVQsRUFBYXhCLE1BQWIsRUFBcUJDLFdBQXJCLEVBQWtDO0FBQ3BDLFFBQUdBLFdBQVcsS0FBS1UsU0FBbkIsRUFDSVYsV0FBVyxHQUFHLENBQWQ7QUFDSkQsSUFBQUEsTUFBTSxJQUFJLEtBQUthLGdCQUFMLENBQXNCVyxFQUF0QixFQUEwQnhCLE1BQTFCLEVBQWtDQyxXQUFsQyxDQUFWO0FBQ0gsR0FSdUI7QUFVeEJZLEVBQUFBLGdCQUFnQixFQUFDLDBCQUFVVyxFQUFWLEVBQWN4QixNQUFkLEVBQXNCQyxXQUF0QixFQUFtQztBQUNoRCxXQUFPdkIsRUFBRSxDQUFDZSxpQkFBSCxDQUFxQm1CLFNBQXJCLENBQStCQyxnQkFBL0IsQ0FBZ0RDLElBQWhELENBQXFELElBQXJELEVBQTJEVSxFQUEzRCxFQUErRHhCLE1BQS9ELEVBQXVFQyxXQUF2RSxFQUFvRixHQUFwRixDQUFQO0FBQ0gsR0FadUI7QUFjeEJpQixFQUFBQSxLQUFLLEVBQUMsaUJBQVk7QUFDZCxRQUFJQyxNQUFNLEdBQUcsSUFBSXpDLEVBQUUsQ0FBQzhGLGFBQVAsRUFBYjtBQUNBckQsSUFBQUEsTUFBTSxDQUFDTixnQkFBUCxDQUF3QixLQUFLTyxTQUE3QixFQUF3QzVCLGtCQUFrQixDQUFDLEtBQUtXLE9BQU4sRUFBZSxLQUFLSyxZQUFwQixDQUExRDtBQUNBLFdBQU9XLE1BQVA7QUFDSDtBQWxCdUIsQ0FBVCxDQUFuQjtBQXFCQTs7Ozs7Ozs7Ozs7OztBQVlBekMsRUFBRSxDQUFDK0YsYUFBSCxHQUFtQixVQUFVakQsRUFBVixFQUFjeEIsTUFBZCxFQUFzQkMsV0FBdEIsRUFBbUM7QUFDbEQsU0FBTyxJQUFJdkIsRUFBRSxDQUFDOEYsYUFBUCxDQUFxQmhELEVBQXJCLEVBQXlCeEIsTUFBekIsRUFBaUNDLFdBQWpDLENBQVA7QUFDSCxDQUZEO0FBSUE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZUF2QixFQUFFLENBQUNnRyxhQUFILEdBQW1CaEcsRUFBRSxDQUFDZ0IsS0FBSCxDQUFTO0FBQ3hCQyxFQUFBQSxJQUFJLEVBQUUsa0JBRGtCO0FBRXhCLGFBQVNqQixFQUFFLENBQUNpRixpQkFGWTtBQUl4QjdELEVBQUFBLElBQUksRUFBRSxjQUFTMEIsRUFBVCxFQUFheEIsTUFBYixFQUFxQkMsV0FBckIsRUFBa0M7QUFDcENELElBQUFBLE1BQU0sSUFBSSxLQUFLYSxnQkFBTCxDQUFzQlcsRUFBdEIsRUFBMEJ4QixNQUExQixFQUFrQ0MsV0FBbEMsQ0FBVjtBQUNILEdBTnVCO0FBUXhCWSxFQUFBQSxnQkFBZ0IsRUFBQywwQkFBVVcsRUFBVixFQUFjeEIsTUFBZCxFQUFzQkMsV0FBdEIsRUFBbUM7QUFDaEQsV0FBT3ZCLEVBQUUsQ0FBQ2UsaUJBQUgsQ0FBcUJtQixTQUFyQixDQUErQkMsZ0JBQS9CLENBQWdEQyxJQUFoRCxDQUFxRCxJQUFyRCxFQUEyRFUsRUFBM0QsRUFBK0R4QixNQUEvRCxFQUF1RUMsV0FBdkUsRUFBb0YsR0FBcEYsQ0FBUDtBQUNILEdBVnVCO0FBWXhCaUIsRUFBQUEsS0FBSyxFQUFDLGlCQUFZO0FBQ2QsUUFBSUMsTUFBTSxHQUFHLElBQUl6QyxFQUFFLENBQUNnRyxhQUFQLEVBQWI7QUFDQXZELElBQUFBLE1BQU0sQ0FBQ04sZ0JBQVAsQ0FBd0IsS0FBS08sU0FBN0IsRUFBd0M1QixrQkFBa0IsQ0FBQyxLQUFLVyxPQUFOLENBQTFELEVBQTBFLEtBQUtLLFlBQS9FO0FBQ0EsV0FBT1csTUFBUDtBQUNIO0FBaEJ1QixDQUFULENBQW5CO0FBbUJBOzs7Ozs7Ozs7Ozs7QUFXQXpDLEVBQUUsQ0FBQ2lHLGFBQUgsR0FBbUIsVUFBVW5ELEVBQVYsRUFBY3hCLE1BQWQsRUFBc0JDLFdBQXRCLEVBQW1DO0FBQ2xELFNBQU8sSUFBSXZCLEVBQUUsQ0FBQ2dHLGFBQVAsQ0FBcUJsRCxFQUFyQixFQUF5QnhCLE1BQXpCLEVBQWlDQyxXQUFqQyxDQUFQO0FBQ0gsQ0FGRCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcclxuIENvcHlyaWdodCAoYykgMjAwOCBSYWR1IEdydWlhblxyXG4gQ29weXJpZ2h0IChjKSAyMDA4LTIwMTAgUmljYXJkbyBRdWVzYWRhXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTEgVml0IFZhbGVudGluXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTEtMjAxMiBjb2NvczJkLXgub3JnXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTMtMjAxNiBDaHVrb25nIFRlY2hub2xvZ2llcyBJbmMuXHJcbiBDb3B5cmlnaHQgKGMpIDIwMTctMjAxOCBYaWFtZW4gWWFqaSBTb2Z0d2FyZSBDby4sIEx0ZC5cclxuIGh0dHA6Ly93d3cuY29jb3MyZC14Lm9yZ1xyXG4gUGVybWlzc2lvbiBpcyBoZXJlYnkgZ3JhbnRlZCwgZnJlZSBvZiBjaGFyZ2UsIHRvIGFueSBwZXJzb24gb2J0YWluaW5nIGEgY29weVxyXG4gb2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVzICh0aGUgXCJTb2Z0d2FyZVwiKSwgdG8gZGVhbFxyXG4gaW4gdGhlIFNvZnR3YXJlIHdpdGhvdXQgcmVzdHJpY3Rpb24sIGluY2x1ZGluZyB3aXRob3V0IGxpbWl0YXRpb24gdGhlIHJpZ2h0c1xyXG4gdG8gdXNlLCBjb3B5LCBtb2RpZnksIG1lcmdlLCBwdWJsaXNoLCBkaXN0cmlidXRlLCBzdWJsaWNlbnNlLCBhbmQvb3Igc2VsbFxyXG4gY29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdCBwZXJzb25zIHRvIHdob20gdGhlIFNvZnR3YXJlIGlzXHJcbiBmdXJuaXNoZWQgdG8gZG8gc28sIHN1YmplY3QgdG8gdGhlIGZvbGxvd2luZyBjb25kaXRpb25zOlxyXG4gVGhlIGFib3ZlIGNvcHlyaWdodCBub3RpY2UgYW5kIHRoaXMgcGVybWlzc2lvbiBub3RpY2Ugc2hhbGwgYmUgaW5jbHVkZWQgaW5cclxuIGFsbCBjb3BpZXMgb3Igc3Vic3RhbnRpYWwgcG9ydGlvbnMgb2YgdGhlIFNvZnR3YXJlLlxyXG4gVEhFIFNPRlRXQVJFIElTIFBST1ZJREVEIFwiQVMgSVNcIiwgV0lUSE9VVCBXQVJSQU5UWSBPRiBBTlkgS0lORCwgRVhQUkVTUyBPUlxyXG4gSU1QTElFRCwgSU5DTFVESU5HIEJVVCBOT1QgTElNSVRFRCBUTyBUSEUgV0FSUkFOVElFUyBPRiBNRVJDSEFOVEFCSUxJVFksXHJcbiBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRSBBTkQgTk9OSU5GUklOR0VNRU5ULiBJTiBOTyBFVkVOVCBTSEFMTCBUSEVcclxuIEFVVEhPUlMgT1IgQ09QWVJJR0hUIEhPTERFUlMgQkUgTElBQkxFIEZPUiBBTlkgQ0xBSU0sIERBTUFHRVMgT1IgT1RIRVJcclxuIExJQUJJTElUWSwgV0hFVEhFUiBJTiBBTiBBQ1RJT04gT0YgQ09OVFJBQ1QsIFRPUlQgT1IgT1RIRVJXSVNFLCBBUklTSU5HIEZST00sXHJcbiBPVVQgT0YgT1IgSU4gQ09OTkVDVElPTiBXSVRIIFRIRSBTT0ZUV0FSRSBPUiBUSEUgVVNFIE9SIE9USEVSIERFQUxJTkdTIElOXHJcbiBUSEUgU09GVFdBUkUuXHJcbiBPcmlnbmFsIGNvZGUgYnkgUmFkdSBHcnVpYW46IGh0dHA6Ly93d3cuY29kZXByb2plY3QuY29tL0FydGljbGVzLzMwODM4L092ZXJoYXVzZXItQ2F0bXVsbC1Sb20tU3BsaW5lcy1mb3ItQ2FtZXJhLUFuaW1hdGlvLlNvXHJcbiBBZGFwdGVkIHRvIGNvY29zMmQteCBieSBWaXQgVmFsZW50aW5cclxuIEFkYXB0ZWQgZnJvbSBjb2NvczJkLXggdG8gY29jb3MyZC1pcGhvbmUgYnkgUmljYXJkbyBRdWVzYWRhXHJcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuLyoqXHJcbiAqIEBtb2R1bGUgY2NcclxuICovXHJcblxyXG4vKlxyXG4gKiBSZXR1cm5zIHRoZSBDYXJkaW5hbCBTcGxpbmUgcG9zaXRpb24gZm9yIGEgZ2l2ZW4gc2V0IG9mIGNvbnRyb2wgcG9pbnRzLCB0ZW5zaW9uIGFuZCB0aW1lLiA8YnIgLz5cclxuICogQ2F0bXVsbFJvbSBTcGxpbmUgZm9ybXVsYS4gPGJyIC8+XHJcbiAqIHMoLXR0dCArIDJ0dCAtIHQpUDEgKyBzKC10dHQgKyB0dClQMiArICgydHR0IC0gM3R0ICsgMSlQMiArIHModHR0IC0gMnR0ICsgdClQMyArICgtMnR0dCArIDN0dClQMyArIHModHR0IC0gdHQpUDRcclxuICpcclxuICogQG1ldGhvZCBjYXJkaW5hbFNwbGluZUF0XHJcbiAqIEBwYXJhbSB7VmVjMn0gcDBcclxuICogQHBhcmFtIHtWZWMyfSBwMVxyXG4gKiBAcGFyYW0ge1ZlYzJ9IHAyXHJcbiAqIEBwYXJhbSB7VmVjMn0gcDNcclxuICogQHBhcmFtIHtOdW1iZXJ9IHRlbnNpb25cclxuICogQHBhcmFtIHtOdW1iZXJ9IHRcclxuICogQHJldHVybiB7VmVjMn1cclxuICovXHJcbmZ1bmN0aW9uIGNhcmRpbmFsU3BsaW5lQXQgKHAwLCBwMSwgcDIsIHAzLCB0ZW5zaW9uLCB0KSB7XHJcbiAgICB2YXIgdDIgPSB0ICogdDtcclxuICAgIHZhciB0MyA9IHQyICogdDtcclxuXHJcbiAgICAvKlxyXG4gICAgICogRm9ybXVsYTogcygtdHR0ICsgMnR0IC0gdClQMSArIHMoLXR0dCArIHR0KVAyICsgKDJ0dHQgLSAzdHQgKyAxKVAyICsgcyh0dHQgLSAydHQgKyB0KVAzICsgKC0ydHR0ICsgM3R0KVAzICsgcyh0dHQgLSB0dClQNFxyXG4gICAgICovXHJcbiAgICB2YXIgcyA9ICgxIC0gdGVuc2lvbikgLyAyO1xyXG5cclxuICAgIHZhciBiMSA9IHMgKiAoKC10MyArICgyICogdDIpKSAtIHQpOyAgICAgICAgICAgICAgICAgICAgICAvLyBzKC10MyArIDIgdDIgLSB0KVAxXHJcbiAgICB2YXIgYjIgPSBzICogKC10MyArIHQyKSArICgyICogdDMgLSAzICogdDIgKyAxKTsgICAgICAgICAgLy8gcygtdDMgKyB0MilQMiArICgyIHQzIC0gMyB0MiArIDEpUDJcclxuICAgIHZhciBiMyA9IHMgKiAodDMgLSAyICogdDIgKyB0KSArICgtMiAqIHQzICsgMyAqIHQyKTsgICAgICAvLyBzKHQzIC0gMiB0MiArIHQpUDMgKyAoLTIgdDMgKyAzIHQyKVAzXHJcbiAgICB2YXIgYjQgPSBzICogKHQzIC0gdDIpOyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gcyh0MyAtIHQyKVA0XHJcblxyXG4gICAgdmFyIHggPSAocDAueCAqIGIxICsgcDEueCAqIGIyICsgcDIueCAqIGIzICsgcDMueCAqIGI0KTtcclxuICAgIHZhciB5ID0gKHAwLnkgKiBiMSArIHAxLnkgKiBiMiArIHAyLnkgKiBiMyArIHAzLnkgKiBiNCk7XHJcbiAgICByZXR1cm4gY2MudjIoeCwgeSk7XHJcbn07XHJcblxyXG4vKlxyXG4gKiByZXR1cm5zIGEgcG9pbnQgZnJvbSB0aGUgYXJyYXlcclxuICogQG1ldGhvZCBnZXRDb250cm9sUG9pbnRBdFxyXG4gKiBAcGFyYW0ge0FycmF5fSBjb250cm9sUG9pbnRzXHJcbiAqIEBwYXJhbSB7TnVtYmVyfSBwb3NcclxuICogQHJldHVybiB7QXJyYXl9XHJcbiAqL1xyXG5mdW5jdGlvbiBnZXRDb250cm9sUG9pbnRBdCAoY29udHJvbFBvaW50cywgcG9zKSB7XHJcbiAgICB2YXIgcCA9IE1hdGgubWluKGNvbnRyb2xQb2ludHMubGVuZ3RoIC0gMSwgTWF0aC5tYXgocG9zLCAwKSk7XHJcbiAgICByZXR1cm4gY29udHJvbFBvaW50c1twXTtcclxufTtcclxuXHJcbmZ1bmN0aW9uIHJldmVyc2VDb250cm9sUG9pbnRzIChjb250cm9sUG9pbnRzKSB7XHJcbiAgICB2YXIgbmV3QXJyYXkgPSBbXTtcclxuICAgIGZvciAodmFyIGkgPSBjb250cm9sUG9pbnRzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XHJcbiAgICAgICAgbmV3QXJyYXkucHVzaChjYy52Mihjb250cm9sUG9pbnRzW2ldLngsIGNvbnRyb2xQb2ludHNbaV0ueSkpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG5ld0FycmF5O1xyXG59XHJcblxyXG5mdW5jdGlvbiBjbG9uZUNvbnRyb2xQb2ludHMgKGNvbnRyb2xQb2ludHMpIHtcclxuICAgIHZhciBuZXdBcnJheSA9IFtdO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjb250cm9sUG9pbnRzLmxlbmd0aDsgaSsrKVxyXG4gICAgICAgIG5ld0FycmF5LnB1c2goY2MudjIoY29udHJvbFBvaW50c1tpXS54LCBjb250cm9sUG9pbnRzW2ldLnkpKTtcclxuICAgIHJldHVybiBuZXdBcnJheTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFRPRE8gdOG7kWkgxrB1OiBUaOG7sWMgaGnhu4duIHByZSBpbnRlcnBvbGF0aW9uIHbDoCBwcmVjYWxjdWxhdGUgbGVuZ3RoIGNobyBjw6FjIFNwbGluZSBBY3Rpb24gZ2nhu5FuZyBuaGF1XHJcbiAqL1xyXG5cclxuLypcclxuICogQ2FyZGluYWwgU3BsaW5lIHBhdGguIGh0dHA6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvQ3ViaWNfSGVybWl0ZV9zcGxpbmUjQ2FyZGluYWxfc3BsaW5lXHJcbiAqIEFic29sdXRlIGNvb3JkaW5hdGVzLlxyXG4gKlxyXG4gKiBAY2xhc3MgQ2FyZGluYWxTcGxpbmVUbzJcclxuICogQGV4dGVuZHMgQWN0aW9uSW50ZXJ2YWxcclxuICpcclxuICogQHBhcmFtIHtOdW1iZXJ9IGR1cmF0aW9uXHJcbiAqIEBwYXJhbSB7QXJyYXl9IHBvaW50cyBhcnJheSBvZiBjb250cm9sIHBvaW50c1xyXG4gKiBAcGFyYW0ge051bWJlcn0gdGVuc2lvblxyXG4gKlxyXG4gKiBAZXhhbXBsZVxyXG4gKiAvL2NyZWF0ZSBhIGNjLkNhcmRpbmFsU3BsaW5lVG8yXHJcbiAqIHZhciBhY3Rpb24xID0gY2MuQ2FyZGluYWxTcGxpbmVUbzIoMywgYXJyYXksIDApO1xyXG4gKiBcclxuICovXHJcbmNjLkNhcmRpbmFsU3BsaW5lVG8yID0gY2MuQ2xhc3Moe1xyXG4gICAgbmFtZTogJ2NjLkNhcmRpbmFsU3BsaW5lVG8yJyxcclxuICAgIGV4dGVuZHM6IGNjLkFjdGlvbkludGVydmFsLFxyXG4gICAgY2FjaGVMZW5ndGhzOiBudWxsLFxyXG5cclxuICAgIGN0b3I6IGZ1bmN0aW9uIChkdXJhdGlvbiwgcG9pbnRzLCBhbmdsZU9mZnNldCwgdGVuc2lvbiwgc3BlZWQpIHtcclxuICAgICAgICAvKiBBcnJheSBvZiBjb250cm9sIHBvaW50cyAqL1xyXG4gICAgICAgIHRoaXMuX3BvaW50cyA9IFtdO1xyXG4gICAgICAgIHRoaXMuX2NhY2hlTGVuZ3RocyA9IFtdO1xyXG4gICAgICAgIHRoaXMuX3RvdGFsTGVuZ3RoID0gMDtcclxuICAgICAgICB0aGlzLl9kZWx0YVQgPSAwO1xyXG4gICAgICAgIHRoaXMuX3RlbnNpb24gPSAwO1xyXG4gICAgICAgIHRoaXMuX2FuZ2xlT2Zmc2V0ID0gMDtcclxuICAgICAgICB0aGlzLl9wcmV2aW91c1Bvc2l0aW9uID0gbnVsbDtcclxuICAgICAgICB0aGlzLl9hY2N1bXVsYXRlZERpZmYgPSBudWxsO1xyXG4gICAgICAgIHRlbnNpb24gIT09IHVuZGVmaW5lZCAmJiBjYy5DYXJkaW5hbFNwbGluZVRvMi5wcm90b3R5cGUuaW5pdFdpdGhEdXJhdGlvbi5jYWxsKHRoaXMsIGR1cmF0aW9uLCBwb2ludHMsIGFuZ2xlT2Zmc2V0LCB0ZW5zaW9uLCBzcGVlZCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGluaXRXaXRoRHVyYXRpb246ZnVuY3Rpb24gKGR1cmF0aW9uLCBwb2ludHMsIGFuZ2xlT2Zmc2V0LCB0ZW5zaW9uLCBzcGVlZCkge1xyXG4gICAgICAgIGlmICghcG9pbnRzIHx8IHBvaW50cy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgY2MuZXJyb3JJRCgxMDI0KTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5fdGVuc2lvbiA9IHRlbnNpb247XHJcbiAgICAgICAgdGhpcy5fYW5nbGVPZmZzZXQgPSBhbmdsZU9mZnNldDtcclxuICAgICAgICB0aGlzLnNldFBvaW50cyhwb2ludHMpO1xyXG4gICAgICAgIGlmKHNwZWVkKVxyXG4gICAgICAgICAgICBkdXJhdGlvbiA9IHRoaXMuZ2V0TGVuZ3RoKCkvc3BlZWQ7XHJcblxyXG4gICAgICAgIGlmIChjYy5BY3Rpb25JbnRlcnZhbC5wcm90b3R5cGUuaW5pdFdpdGhEdXJhdGlvbi5jYWxsKHRoaXMsIGR1cmF0aW9uKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfSxcclxuXHJcbiAgICBjbG9uZTpmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIGFjdGlvbiA9IG5ldyBjYy5DYXJkaW5hbFNwbGluZVRvMigpO1xyXG4gICAgICAgIGFjdGlvbi5pbml0V2l0aER1cmF0aW9uKHRoaXMuX2R1cmF0aW9uLCBjbG9uZUNvbnRyb2xQb2ludHModGhpcy5fcG9pbnRzKSwgdGhpcy5fdGVuc2lvbik7XHJcbiAgICAgICAgcmV0dXJuIGFjdGlvbjtcclxuICAgIH0sXHJcblxyXG4gICAgc3RhcnRXaXRoVGFyZ2V0OmZ1bmN0aW9uICh0YXJnZXQpIHtcclxuICAgICAgICBjYy5BY3Rpb25JbnRlcnZhbC5wcm90b3R5cGUuc3RhcnRXaXRoVGFyZ2V0LmNhbGwodGhpcywgdGFyZ2V0KTtcclxuICAgICAgICAvLyBJc3N1ZSAjMTQ0MSBmcm9tIGNvY29zMmQtaXBob25lXHJcbiAgICAgICAgdGhpcy5fZGVsdGFUID0gMSAvICh0aGlzLl9wb2ludHMubGVuZ3RoIC0gMSk7XHJcbiAgICAgICAgdGhpcy5fcHJldmlvdXNQb3NpdGlvbiA9IGNjLnYyKHRoaXMudGFyZ2V0LngsIHRoaXMudGFyZ2V0LnkpO1xyXG4gICAgICAgIHRoaXMuX2FjY3VtdWxhdGVkRGlmZiA9IGNjLnYyKDAsIDApO1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRQOiBmdW5jdGlvbihkdCl7XHJcbiAgICAgICAgZm9yKHZhciBpPTA7IGk8dGhpcy5fY3VtbXV0aWxhdGUubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgaWYodGhpcy5fY3VtbXV0aWxhdGVbaV0gPiBkdClcclxuICAgICAgICAgICAgICAgIHJldHVybiBpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5fY3VtbXV0aWxhdGUubGVuZ3RoO1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRMdDogZnVuY3Rpb24ocCwgZHQpe1xyXG4gICAgICAgIGlmKHA9PTApIHtcclxuICAgICAgICAgICAgcmV0dXJuIGR0L3RoaXMuX2NhY2hlTGVuZ3Roc1twXTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gKGR0LXRoaXMuX2N1bW11dGlsYXRlW3AtMV0pL3RoaXMuX2NhY2hlTGVuZ3Roc1twXTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIHVwZGF0ZTpmdW5jdGlvbiAoZHQpIHtcclxuICAgICAgICBkdCA9IHRoaXMuX2NvbXB1dGVFYXNlVGltZShkdCk7XHJcbiAgICAgICAgdmFyIHAsIGx0O1xyXG4gICAgICAgIHZhciBwcyA9IHRoaXMuX3BvaW50cztcclxuICAgICAgICAvLyBlZy5cclxuICAgICAgICAvLyBwLi5wLi5wLi5wLi5wLi5wLi5wXHJcbiAgICAgICAgLy8gMS4uMi4uMy4uNC4uNS4uNi4uN1xyXG4gICAgICAgIC8vIHdhbnQgcCB0byBiZSAxLCAyLCAzLCA0LCA1LCA2XHJcbiAgICAgICAgaWYgKGR0ID09PSAxKSB7XHJcbiAgICAgICAgICAgIHAgPSBwcy5sZW5ndGggLSAxO1xyXG4gICAgICAgICAgICBsdCA9IDE7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdmFyIGxvY0RUID0gdGhpcy5fZGVsdGFUO1xyXG4gICAgICAgICAgICAvL3AgPSAwIHwgKGR0IC8gbG9jRFQpO1xyXG4gICAgICAgICAgICAvL2x0ID0gKGR0IC0gbG9jRFQgKiBwKSAvIGxvY0RUO1xyXG4gICAgICAgICAgICBwID0gdGhpcy5nZXRQKGR0KTtcclxuICAgICAgICAgICAgbHQgPSB0aGlzLmdldEx0KHAsIGR0KTtcclxuICAgICAgICAgICAgLy8gY29uc29sZS5sb2cobHQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdmFyIG5ld1BvcyA9IGNhcmRpbmFsU3BsaW5lQXQoXHJcbiAgICAgICAgICAgIGdldENvbnRyb2xQb2ludEF0KHBzLCBwIC0gMSksXHJcbiAgICAgICAgICAgIGdldENvbnRyb2xQb2ludEF0KHBzLCBwIC0gMCksXHJcbiAgICAgICAgICAgIGdldENvbnRyb2xQb2ludEF0KHBzLCBwICsgMSksXHJcbiAgICAgICAgICAgIGdldENvbnRyb2xQb2ludEF0KHBzLCBwICsgMiksXHJcbiAgICAgICAgICAgIHRoaXMuX3RlbnNpb24sIGx0KTtcclxuXHJcbiAgICAgICAgaWYgKGNjLm1hY3JvLkVOQUJMRV9TVEFDS0FCTEVfQUNUSU9OUykge1xyXG4gICAgICAgICAgICB2YXIgdGVtcFgsIHRlbXBZO1xyXG4gICAgICAgICAgICB0ZW1wWCA9IHRoaXMudGFyZ2V0LnggLSB0aGlzLl9wcmV2aW91c1Bvc2l0aW9uLng7XHJcbiAgICAgICAgICAgIHRlbXBZID0gdGhpcy50YXJnZXQueSAtIHRoaXMuX3ByZXZpb3VzUG9zaXRpb24ueTtcclxuICAgICAgICAgICAgaWYgKHRlbXBYICE9PSAwIHx8IHRlbXBZICE9PSAwKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgbG9jQWNjRGlmZiA9IHRoaXMuX2FjY3VtdWxhdGVkRGlmZjtcclxuICAgICAgICAgICAgICAgIHRlbXBYID0gbG9jQWNjRGlmZi54ICsgdGVtcFg7XHJcbiAgICAgICAgICAgICAgICB0ZW1wWSA9IGxvY0FjY0RpZmYueSArIHRlbXBZO1xyXG4gICAgICAgICAgICAgICAgbG9jQWNjRGlmZi54ID0gdGVtcFg7XHJcbiAgICAgICAgICAgICAgICBsb2NBY2NEaWZmLnkgPSB0ZW1wWTtcclxuICAgICAgICAgICAgICAgIG5ld1Bvcy54ICs9IHRlbXBYO1xyXG4gICAgICAgICAgICAgICAgbmV3UG9zLnkgKz0gdGVtcFk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy51cGRhdGVQb3NpdGlvbihuZXdQb3MpO1xyXG4gICAgfSxcclxuXHJcbiAgICByZXZlcnNlOmZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgcmV2ZXJzZVBvaW50cyA9IHJldmVyc2VDb250cm9sUG9pbnRzKHRoaXMuX3BvaW50cyk7XHJcbiAgICAgICAgcmV0dXJuIGNjLkNhcmRpbmFsU3BsaW5lVG8yKHRoaXMuX2R1cmF0aW9uLCByZXZlcnNlUG9pbnRzLCB0aGlzLl90ZW5zaW9uKTtcclxuICAgIH0sXHJcblxyXG4gICAgLypcclxuICAgICAqIHVwZGF0ZSBwb3NpdGlvbiBvZiB0YXJnZXRcclxuICAgICAqIEBtZXRob2QgdXBkYXRlUG9zaXRpb25cclxuICAgICAqIEBwYXJhbSB7VmVjMn0gbmV3UG9zXHJcbiAgICAgKi9cclxuICAgIHVwZGF0ZVBvc2l0aW9uOmZ1bmN0aW9uIChuZXdQb3MpIHtcclxuICAgICAgICB2YXIgZGVsdGFYID0gbmV3UG9zLngtdGhpcy5fcHJldmlvdXNQb3NpdGlvbi54O1xyXG4gICAgICAgIHZhciBkZWx0YVkgPSBuZXdQb3MueS10aGlzLl9wcmV2aW91c1Bvc2l0aW9uLnk7XHJcbiAgICAgICAgaWYoIXRoaXMuX2Rpc2FibGVSb3RhdGUpIHtcclxuICAgICAgICAgICAgaWYoZGVsdGFYICE9PSAwICYmIGRlbHRhWSAhPT0gMClcclxuICAgICAgICAgICAgICAgIHRoaXMudGFyZ2V0LmFuZ2xlID0gTWF0aC5hdGFuMihkZWx0YVksIGRlbHRhWCkqMTgwLyhNYXRoLlBJKSArIHRoaXMuX2FuZ2xlT2Zmc2V0O1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnRhcmdldC5zZXRQb3NpdGlvbihuZXdQb3MpO1xyXG4gICAgICAgIHRoaXMuX3ByZXZpb3VzUG9zaXRpb24gPSBuZXdQb3M7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qXHJcbiAgICAgKiBQb2ludHMgZ2V0dGVyXHJcbiAgICAgKiBAbWV0aG9kIGdldFBvaW50c1xyXG4gICAgICogQHJldHVybiB7QXJyYXl9XHJcbiAgICAgKi9cclxuICAgIGdldFBvaW50czpmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3BvaW50cztcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQb2ludHMgc2V0dGVyXHJcbiAgICAgKiBAbWV0aG9kIHNldFBvaW50c1xyXG4gICAgICogQHBhcmFtIHtBcnJheX0gcG9pbnRzXHJcbiAgICAgKi9cclxuICAgIHNldFBvaW50czpmdW5jdGlvbiAocG9pbnRzKSB7XHJcbiAgICAgICAgcG9pbnRzID0gdGhpcy5faW50ZXJwb2xhdGF0ZVBvaW50cyhwb2ludHMsIDEwKTtcclxuICAgICAgICB0aGlzLl9zZXRQb2ludChwb2ludHMpO1xyXG4gICAgfSxcclxuXHJcbiAgICBfc2V0UG9pbnQocG9pbnRzKXtcclxuICAgICAgICB0aGlzLl9wb2ludHMgPSBwb2ludHM7XHJcbiAgICAgICAgdGhpcy5fY2FjaGVMZW5ndGhzID0gW107XHJcbiAgICAgICAgdGhpcy5fY3VtbXV0aWxhdGUgPSBbXTtcclxuICAgICAgICBmb3IodmFyIGk9MDsgaTxwb2ludHMubGVuZ3RoLTE7IGkrKykge1xyXG4gICAgICAgICAgICB2YXIgcDEgPSBwb2ludHNbaV07XHJcbiAgICAgICAgICAgIHZhciBwMiA9IHBvaW50c1tpKzFdO1xyXG4gICAgICAgICAgICB2YXIgbGVuZ3RoID0gTWF0aC5zcXJ0KChwMS54LXAyLngpKihwMS54LXAyLngpICsgKHAxLnktcDIueSkqKHAxLnktcDIueSkpO1xyXG4gICAgICAgICAgICB0aGlzLl9jYWNoZUxlbmd0aHMucHVzaChsZW5ndGgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl90b3RhbExlbmd0aCA9IDA7XHJcbiAgICAgICAgZm9yKHZhciBpPTA7IGk8dGhpcy5fY2FjaGVMZW5ndGhzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3RvdGFsTGVuZ3RoICs9IHRoaXMuX2NhY2hlTGVuZ3Roc1tpXTtcclxuICAgICAgICAgICAgdGhpcy5fY3VtbXV0aWxhdGUucHVzaCh0aGlzLl90b3RhbExlbmd0aCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvcih2YXIgaT0wOyBpPHRoaXMuX2NhY2hlTGVuZ3Rocy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLl9jYWNoZUxlbmd0aHNbaV0gPSB0aGlzLl9jYWNoZUxlbmd0aHNbaV0vdGhpcy5fdG90YWxMZW5ndGg7XHJcbiAgICAgICAgICAgIHRoaXMuX2N1bW11dGlsYXRlW2ldID0gdGhpcy5fY3VtbXV0aWxhdGVbaV0vdGhpcy5fdG90YWxMZW5ndGg7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBfc2V0UG9pbnQyKHBvaW50cyl7XHJcbiAgICAgICAgdGhpcy5fcG9pbnRzID0gcG9pbnRzO1xyXG4gICAgICAgIHRoaXMuX2NhY2hlTGVuZ3RocyA9IFtdO1xyXG4gICAgICAgIHRoaXMuX2N1bW11dGlsYXRlID0gW107XHJcbiAgICAgICAgZm9yKHZhciBpPTA7IGk8cG9pbnRzLmxlbmd0aC0xOyBpKyspIHtcclxuICAgICAgICAgICAgdmFyIHAxID0gcG9pbnRzW2ldO1xyXG4gICAgICAgICAgICB2YXIgcDIgPSBwb2ludHNbaSsxXTtcclxuICAgICAgICAgICAgdmFyIGxlbmd0aCA9IDE7XHJcbiAgICAgICAgICAgIHRoaXMuX2NhY2hlTGVuZ3Rocy5wdXNoKGxlbmd0aCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX3RvdGFsTGVuZ3RoID0gMDtcclxuICAgICAgICBmb3IodmFyIGk9MDsgaTx0aGlzLl9jYWNoZUxlbmd0aHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5fdG90YWxMZW5ndGggKz0gdGhpcy5fY2FjaGVMZW5ndGhzW2ldO1xyXG4gICAgICAgICAgICB0aGlzLl9jdW1tdXRpbGF0ZS5wdXNoKHRoaXMuX3RvdGFsTGVuZ3RoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZm9yKHZhciBpPTA7IGk8dGhpcy5fY2FjaGVMZW5ndGhzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2NhY2hlTGVuZ3Roc1tpXSA9IHRoaXMuX2NhY2hlTGVuZ3Roc1tpXS90aGlzLl90b3RhbExlbmd0aDtcclxuICAgICAgICAgICAgdGhpcy5fY3VtbXV0aWxhdGVbaV0gPSB0aGlzLl9jdW1tdXRpbGF0ZVtpXS90aGlzLl90b3RhbExlbmd0aDtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIF9pbnRlcnBvbGF0YXRlUG9pbnRzOiBmdW5jdGlvbihwb2ludHMsIGQpe1xyXG4gICAgICAgIHRoaXMuX3NldFBvaW50KHBvaW50cyk7XHJcbiAgICAgICAgdmFyIG4gPSAocG9pbnRzLmxlbmd0aC0xKSAqIGQ7XHJcbiAgICAgICAgdmFyIG5ld1BvaW50cyA9IFtdO1xyXG4gICAgICAgIHZhciBwID0gMDtcclxuICAgICAgICB2YXIgbHQgPSAwO1xyXG4gICAgICAgIHZhciBwcyA9IHBvaW50cztcclxuICAgICAgICBmb3IodmFyIGk9MDsgaTxuOyBpKyspIHtcclxuICAgICAgICAgICAgdmFyIGR0ID0gaSoxLyhuLTEpO1xyXG4gICAgICAgICAgICBpZiAoZHQgPT09IDEpIHtcclxuICAgICAgICAgICAgICAgIHAgPSBwcy5sZW5ndGggLSAxO1xyXG4gICAgICAgICAgICAgICAgbHQgPSAxO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdmFyIGxvY0RUID0gMSAvIChwb2ludHMubGVuZ3RoIC0gMSk7XHJcbiAgICAgICAgICAgICAgICAvL3AgPSAwIHwgKGR0IC8gbG9jRFQpO1xyXG4gICAgICAgICAgICAgICAgLy9sdCA9IChkdCAtIGxvY0RUICogcCkgLyBsb2NEVDtcclxuICAgICAgICAgICAgICAgIHAgPSB0aGlzLmdldFAoZHQpO1xyXG4gICAgICAgICAgICAgICAgbHQgPSB0aGlzLmdldEx0KHAsIGR0KTsgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdmFyIG5ld1BvcyA9IGNhcmRpbmFsU3BsaW5lQXQoXHJcbiAgICAgICAgICAgICAgICBnZXRDb250cm9sUG9pbnRBdChwcywgcCAtIDEpLFxyXG4gICAgICAgICAgICAgICAgZ2V0Q29udHJvbFBvaW50QXQocHMsIHAgLSAwKSxcclxuICAgICAgICAgICAgICAgIGdldENvbnRyb2xQb2ludEF0KHBzLCBwICsgMSksXHJcbiAgICAgICAgICAgICAgICBnZXRDb250cm9sUG9pbnRBdChwcywgcCArIDIpLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5fdGVuc2lvbiwgbHQpO1xyXG4gICAgICAgICAgICBuZXdQb2ludHMucHVzaChuZXdQb3MpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbmV3UG9pbnRzO1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRMZW5ndGg6IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3RvdGFsTGVuZ3RoO1xyXG4gICAgfSxcclxuXHJcbiAgICBkaXNhYmxlUm90YXRlOiBmdW5jdGlvbigpe1xyXG4gICAgICAgIHRoaXMuX2Rpc2FibGVSb3RhdGUgPSB0cnVlO1xyXG4gICAgfVxyXG5cclxufSk7XHJcblxyXG4vKipcclxuICogISNlbiBDcmVhdGVzIGFuIGFjdGlvbiB3aXRoIGEgQ2FyZGluYWwgU3BsaW5lIGFycmF5IG9mIHBvaW50cyBhbmQgdGVuc2lvbi5cclxuICogISN6aCDmjInln7rmlbDmoLfmnaHmm7Lnur/ovajov7nnp7vliqjliLDnm67moIfkvY3nva7jgIJcclxuICogQG1ldGhvZCBDYXJkaW5hbFNwbGluZVRvMlxyXG4gKiBAcGFyYW0ge051bWJlcn0gZHVyYXRpb25cclxuICogQHBhcmFtIHtBcnJheX0gcG9pbnRzIGFycmF5IG9mIGNvbnRyb2wgcG9pbnRzXHJcbiAqIEBwYXJhbSB7TnVtYmVyfSBhbmdsZU9mZnNldFxyXG4gKiBAcGFyYW0ge051bWJlcn0gdGVuc2lvblxyXG4gKiBAcmV0dXJuIHtBY3Rpb25JbnRlcnZhbH1cclxuICpcclxuICogQGV4YW1wbGVcclxuICogLy9jcmVhdGUgYSBjYy5DYXJkaW5hbFNwbGluZVRvMlxyXG4gKiB2YXIgYWN0aW9uMSA9IGNjLkNhcmRpbmFsU3BsaW5lVG8yKDMsIGFycmF5LCAwKTtcclxuICovXHJcbmNjLmNhcmRpbmFsU3BsaW5lVG8yID0gZnVuY3Rpb24gKGR1cmF0aW9uLCBwb2ludHMsIGFuZ2xlT2Zmc2V0LCB0ZW5zaW9uKSB7XHJcbiAgICByZXR1cm4gbmV3IGNjLkNhcmRpbmFsU3BsaW5lVG8yKGR1cmF0aW9uLCBwb2ludHMsIGFuZ2xlT2Zmc2V0LCB0ZW5zaW9uKTtcclxufTtcclxuXHJcbi8qKlxyXG4gKiAhI2VuIENyZWF0ZXMgYW4gYWN0aW9uIHdpdGggYSBDYXJkaW5hbCBTcGxpbmUgYXJyYXkgb2YgcG9pbnRzIGFuZCB0ZW5zaW9uLlxyXG4gKiAhI3poIOaMieWfuuaVsOagt+adoeabsue6v+i9qOi/ueenu+WKqOWIsOebruagh+S9jee9ruOAglxyXG4gKiBAbWV0aG9kIENhcmRpbmFsU3BsaW5lVG8yXHJcbiAqIEBwYXJhbSB7TnVtYmVyfSBzcGVlZFxyXG4gKiBAcGFyYW0ge0FycmF5fSBwb2ludHMgYXJyYXkgb2YgY29udHJvbCBwb2ludHNcclxuICogQHBhcmFtIHtOdW1iZXJ9IGFuZ2xlT2Zmc2V0XHJcbiAqIEBwYXJhbSB7TnVtYmVyfSB0ZW5zaW9uXHJcbiAqIEByZXR1cm4ge0FjdGlvbkludGVydmFsfVxyXG4gKlxyXG4gKiBAZXhhbXBsZVxyXG4gKiAvL2NyZWF0ZSBhIGNjLkNhcmRpbmFsU3BsaW5lVG8yXHJcbiAqIHZhciBhY3Rpb24xID0gY2MuQ2FyZGluYWxTcGxpbmVUbzIoMywgYXJyYXksIDApO1xyXG4gKi9cclxuY2MuY2FyZGluYWxTcGxpbmVUbzJfc3BlZWQgPSBmdW5jdGlvbiAoc3BlZWQsIHBvaW50cywgYW5nbGVPZmZzZXQsIHRlbnNpb24pIHtcclxuICAgIHJldHVybiBuZXcgY2MuQ2FyZGluYWxTcGxpbmVUbzIoMSwgcG9pbnRzLCBhbmdsZU9mZnNldCwgdGVuc2lvbiwgc3BlZWQpO1xyXG59O1xyXG5cclxuLypcclxuICogQ2FyZGluYWwgU3BsaW5lIHBhdGguIGh0dHA6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvQ3ViaWNfSGVybWl0ZV9zcGxpbmUjQ2FyZGluYWxfc3BsaW5lXHJcbiAqIFJlbGF0aXZlIGNvb3JkaW5hdGVzLlxyXG4gKlxyXG4gKiBAY2xhc3MgQ2FyZGluYWxTcGxpbmVCeTJcclxuICogQGV4dGVuZHMgQ2FyZGluYWxTcGxpbmVUbzJcclxuICpcclxuICogQHBhcmFtIHtOdW1iZXJ9IGR1cmF0aW9uXHJcbiAqIEBwYXJhbSB7QXJyYXl9IHBvaW50c1xyXG4gKiBAcGFyYW0ge051bWJlcn0gdGVuc2lvblxyXG4gKlxyXG4gKiBAZXhhbXBsZVxyXG4gKiAvL2NyZWF0ZSBhIGNjLkNhcmRpbmFsU3BsaW5lQnkyXHJcbiAqIHZhciBhY3Rpb24xID0gY2MuQ2FyZGluYWxTcGxpbmVCeTIoMywgYXJyYXksIDApO1xyXG4gKi9cclxuY2MuQ2FyZGluYWxTcGxpbmVCeTIgPSBjYy5DbGFzcyh7XHJcbiAgICBuYW1lOiAnY2MuQ2FyZGluYWxTcGxpbmVCeTInLFxyXG4gICAgZXh0ZW5kczogY2MuQ2FyZGluYWxTcGxpbmVUbzIsXHJcblxyXG4gICAgY3RvcjpmdW5jdGlvbiAoZHVyYXRpb24sIHBvaW50cywgYW5nbGVPZmZzZXQsIHRlbnNpb24pIHtcclxuICAgICAgICB0aGlzLl9zdGFydFBvc2l0aW9uID0gY2MudjIoMCwgMCk7XHJcbiAgICAgICAgdGVuc2lvbiAhPT0gdW5kZWZpbmVkICYmIHRoaXMuaW5pdFdpdGhEdXJhdGlvbihkdXJhdGlvbiwgcG9pbnRzLCBhbmdsZU9mZnNldCwgdGVuc2lvbik7XHJcbiAgICB9LFxyXG5cclxuICAgIHN0YXJ0V2l0aFRhcmdldDpmdW5jdGlvbiAodGFyZ2V0KSB7XHJcbiAgICAgICAgY2MuQ2FyZGluYWxTcGxpbmVUbzIucHJvdG90eXBlLnN0YXJ0V2l0aFRhcmdldC5jYWxsKHRoaXMsIHRhcmdldCk7XHJcbiAgICAgICAgdGhpcy5fc3RhcnRQb3NpdGlvbi54ID0gdGFyZ2V0Lng7XHJcbiAgICAgICAgdGhpcy5fc3RhcnRQb3NpdGlvbi55ID0gdGFyZ2V0Lnk7XHJcbiAgICB9LFxyXG5cclxuICAgIHJldmVyc2U6ZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBjb3B5Q29uZmlnID0gdGhpcy5fcG9pbnRzLnNsaWNlKCk7XHJcbiAgICAgICAgdmFyIGN1cnJlbnQ7XHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyBjb252ZXJ0IFwiYWJzb2x1dGVzXCIgdG8gXCJkaWZmc1wiXHJcbiAgICAgICAgLy9cclxuICAgICAgICB2YXIgcCA9IGNvcHlDb25maWdbMF07XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCBjb3B5Q29uZmlnLmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgIGN1cnJlbnQgPSBjb3B5Q29uZmlnW2ldO1xyXG4gICAgICAgICAgICBjb3B5Q29uZmlnW2ldID0gY3VycmVudC5zdWIocCk7XHJcbiAgICAgICAgICAgIHAgPSBjdXJyZW50O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gY29udmVydCB0byBcImRpZmZzXCIgdG8gXCJyZXZlcnNlIGFic29sdXRlXCJcclxuICAgICAgICB2YXIgcmV2ZXJzZUFycmF5ID0gcmV2ZXJzZUNvbnRyb2xQb2ludHMoY29weUNvbmZpZyk7XHJcblxyXG4gICAgICAgIC8vIDFzdCBlbGVtZW50ICh3aGljaCBzaG91bGQgYmUgMCwwKSBzaG91bGQgYmUgaGVyZSB0b29cclxuICAgICAgICBwID0gcmV2ZXJzZUFycmF5WyByZXZlcnNlQXJyYXkubGVuZ3RoIC0gMSBdO1xyXG4gICAgICAgIHJldmVyc2VBcnJheS5wb3AoKTtcclxuXHJcbiAgICAgICAgcC54ID0gLXAueDtcclxuICAgICAgICBwLnkgPSAtcC55O1xyXG5cclxuICAgICAgICByZXZlcnNlQXJyYXkudW5zaGlmdChwKTtcclxuICAgICAgICBmb3IgKHZhciBpID0gMTsgaSA8IHJldmVyc2VBcnJheS5sZW5ndGg7ICsraSkge1xyXG4gICAgICAgICAgICBjdXJyZW50ID0gcmV2ZXJzZUFycmF5W2ldO1xyXG4gICAgICAgICAgICBjdXJyZW50LnggPSAtY3VycmVudC54O1xyXG4gICAgICAgICAgICBjdXJyZW50LnkgPSAtY3VycmVudC55O1xyXG4gICAgICAgICAgICBjdXJyZW50LnggKz0gcC54O1xyXG4gICAgICAgICAgICBjdXJyZW50LnkgKz0gcC55O1xyXG4gICAgICAgICAgICByZXZlcnNlQXJyYXlbaV0gPSBjdXJyZW50O1xyXG4gICAgICAgICAgICBwID0gY3VycmVudDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGNjLkNhcmRpbmFsU3BsaW5lQnkyKHRoaXMuX2R1cmF0aW9uLCByZXZlcnNlQXJyYXksIHRoaXMuX2FuZ2xlT2Zmc2V0LCB0aGlzLl90ZW5zaW9uKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiB1cGRhdGUgcG9zaXRpb24gb2YgdGFyZ2V0XHJcbiAgICAgKiBAbWV0aG9kIHVwZGF0ZVBvc2l0aW9uXHJcbiAgICAgKiBAcGFyYW0ge1ZlYzJ9IG5ld1Bvc1xyXG4gICAgICovXHJcbiAgICB1cGRhdGVQb3NpdGlvbjpmdW5jdGlvbiAobmV3UG9zKSB7XHJcbiAgICAgICAgdmFyIHBvcyA9IHRoaXMuX3N0YXJ0UG9zaXRpb247XHJcbiAgICAgICAgdmFyIHBvc1ggPSBuZXdQb3MueCArIHBvcy54O1xyXG4gICAgICAgIHZhciBwb3NZID0gbmV3UG9zLnkgKyBwb3MueTtcclxuXHJcbiAgICAgICAgdmFyIGRlbHRhWCA9IHBvc1gtdGhpcy5fcHJldmlvdXNQb3NpdGlvbi54O1xyXG4gICAgICAgIHZhciBkZWx0YVkgPSBwb3NZLXRoaXMuX3ByZXZpb3VzUG9zaXRpb24ueTtcclxuICAgICAgICBpZihkZWx0YVggIT09IDAgJiYgZGVsdGFZICE9PSAwKVxyXG4gICAgICAgICAgICB0aGlzLnRhcmdldC5hbmdsZSA9IE1hdGguYXRhbjIoZGVsdGFZLCBkZWx0YVgpKjE4MC8oTWF0aC5QSSkgKyB0aGlzLl9hbmdsZU9mZnNldDtcclxuXHJcbiAgICAgICAgdGhpcy5fcHJldmlvdXNQb3NpdGlvbi54ID0gcG9zWDtcclxuICAgICAgICB0aGlzLl9wcmV2aW91c1Bvc2l0aW9uLnkgPSBwb3NZO1xyXG4gICAgICAgIHRoaXMudGFyZ2V0LnNldFBvc2l0aW9uKHBvc1gsIHBvc1kpO1xyXG4gICAgfSxcclxuXHJcbiAgICBjbG9uZTpmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIGEgPSBuZXcgY2MuQ2FyZGluYWxTcGxpbmVCeTIoKTtcclxuICAgICAgICBhLmluaXRXaXRoRHVyYXRpb24odGhpcy5fZHVyYXRpb24sIGNsb25lQ29udHJvbFBvaW50cyh0aGlzLl9wb2ludHMpLCB0aGlzLl9hbmdsZU9mZnNldCwgdGhpcy5fdGVuc2lvbik7XHJcbiAgICAgICAgcmV0dXJuIGE7XHJcbiAgICB9XHJcbn0pO1xyXG5cclxuLyoqXHJcbiAqICEjZW4gQ3JlYXRlcyBhbiBhY3Rpb24gd2l0aCBhIENhcmRpbmFsIFNwbGluZSBhcnJheSBvZiBwb2ludHMgYW5kIHRlbnNpb24uXHJcbiAqICEjemgg5oyJ5Z+65pWw5qC35p2h5puy57q/6L2o6L+556e75Yqo5oyH5a6a55qE6Led56a744CCXHJcbiAqIEBtZXRob2QgQ2FyZGluYWxTcGxpbmVCeTJcclxuICogQHBhcmFtIHtOdW1iZXJ9IGR1cmF0aW9uXHJcbiAqIEBwYXJhbSB7QXJyYXl9IHBvaW50c1xyXG4gKiBAcGFyYW0ge051bWJlcn0gYW5nbGVPZmZzZXRcclxuICogQHBhcmFtIHtOdW1iZXJ9IHRlbnNpb25cclxuICpcclxuICogQHJldHVybiB7QWN0aW9uSW50ZXJ2YWx9XHJcbiAqL1xyXG5jYy5jYXJkaW5hbFNwbGluZUJ5MiA9IGZ1bmN0aW9uIChkdXJhdGlvbiwgcG9pbnRzLCBhbmdsZU9mZnNldCwgdGVuc2lvbikge1xyXG4gICAgcmV0dXJuIG5ldyBjYy5DYXJkaW5hbFNwbGluZUJ5MihkdXJhdGlvbiwgcG9pbnRzLCBhbmdsZU9mZnNldCwgdGVuc2lvbik7XHJcbn07XHJcblxyXG4vKlxyXG4gKiBBbiBhY3Rpb24gdGhhdCBtb3ZlcyB0aGUgdGFyZ2V0IHdpdGggYSBDYXRtdWxsUm9tIGN1cnZlIHRvIGEgZGVzdGluYXRpb24gcG9pbnQuPGJyLz5cclxuICogQSBDYXRtdWxsIFJvbSBpcyBhIENhcmRpbmFsIFNwbGluZSB3aXRoIGEgdGVuc2lvbiBvZiAwLjUuICA8YnIvPlxyXG4gKiBodHRwOi8vZW4ud2lraXBlZGlhLm9yZy93aWtpL0N1YmljX0hlcm1pdGVfc3BsaW5lI0NhdG11bGwuRTIuODAuOTNSb21fc3BsaW5lXHJcbiAqIEFic29sdXRlIGNvb3JkaW5hdGVzLlxyXG4gKlxyXG4gKiBAY2xhc3MgQ2F0bXVsbFJvbVRvMlxyXG4gKiBAZXh0ZW5kcyBDYXJkaW5hbFNwbGluZVRvMlxyXG4gKlxyXG4gKiBAcGFyYW0ge051bWJlcn0gZHRcclxuICogQHBhcmFtIHtBcnJheX0gcG9pbnRzXHJcbiAqXHJcbiAqIEBleGFtcGxlXHJcbiAqIHZhciBhY3Rpb24xID0gY2MuQ2F0bXVsbFJvbVRvMigzLCBhcnJheSk7XHJcbiAqL1xyXG5jYy5DYXRtdWxsUm9tVG8yID0gY2MuQ2xhc3Moe1xyXG4gICAgbmFtZTogJ2NjLkNhdG11bGxSb21UbzInLFxyXG4gICAgZXh0ZW5kczogY2MuQ2FyZGluYWxTcGxpbmVUbzIsXHJcblxyXG4gICAgY3RvcjogZnVuY3Rpb24oZHQsIHBvaW50cywgYW5nbGVPZmZzZXQpIHtcclxuICAgICAgICBpZihhbmdsZU9mZnNldCA9PT0gdW5kZWZpbmVkKVxyXG4gICAgICAgICAgICBhbmdsZU9mZnNldCA9IDA7XHJcbiAgICAgICAgcG9pbnRzICYmIHRoaXMuaW5pdFdpdGhEdXJhdGlvbihkdCwgcG9pbnRzLCBhbmdsZU9mZnNldCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGluaXRXaXRoRHVyYXRpb246ZnVuY3Rpb24gKGR0LCBwb2ludHMsIGFuZ2xlT2Zmc2V0KSB7XHJcbiAgICAgICAgcmV0dXJuIGNjLkNhcmRpbmFsU3BsaW5lVG8yLnByb3RvdHlwZS5pbml0V2l0aER1cmF0aW9uLmNhbGwodGhpcywgZHQsIHBvaW50cywgYW5nbGVPZmZzZXQsIDAuNSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNsb25lOmZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgYWN0aW9uID0gbmV3IGNjLkNhdG11bGxSb21UbzIoKTtcclxuICAgICAgICBhY3Rpb24uaW5pdFdpdGhEdXJhdGlvbih0aGlzLl9kdXJhdGlvbiwgY2xvbmVDb250cm9sUG9pbnRzKHRoaXMuX3BvaW50cywgdGhpcy5fYW5nbGVPZmZzZXQpKTtcclxuICAgICAgICByZXR1cm4gYWN0aW9uO1xyXG4gICAgfVxyXG59KTtcclxuXHJcbi8qKlxyXG4gKiAhI2VuIENyZWF0ZXMgYW4gYWN0aW9uIHdpdGggYSBDYXJkaW5hbCBTcGxpbmUgYXJyYXkgb2YgcG9pbnRzIGFuZCB0ZW5zaW9uLlxyXG4gKiAhI3poIOaMiSBDYXRtdWxsIFJvbSDmoLfmnaHmm7Lnur/ovajov7nnp7vliqjliLDnm67moIfkvY3nva7jgIJcclxuICogQG1ldGhvZCBDYXRtdWxsUm9tVG8yXHJcbiAqIEBwYXJhbSB7TnVtYmVyfSBkdFxyXG4gKiBAcGFyYW0ge0FycmF5fSBwb2ludHNcclxuICogQHBhcmFtIHtOdW1iZXJ9IGFuZ2xlT2Zmc2V0XHJcbiAqIEByZXR1cm4ge0FjdGlvbkludGVydmFsfVxyXG4gKlxyXG4gKiBAZXhhbXBsZVxyXG4gKiB2YXIgYWN0aW9uMSA9IGNjLkNhdG11bGxSb21UbzIoMywgYXJyYXkpO1xyXG4gKi9cclxuY2MuY2F0bXVsbFJvbVRvMiA9IGZ1bmN0aW9uIChkdCwgcG9pbnRzLCBhbmdsZU9mZnNldCkge1xyXG4gICAgcmV0dXJuIG5ldyBjYy5DYXRtdWxsUm9tVG8yKGR0LCBwb2ludHMsIGFuZ2xlT2Zmc2V0KTtcclxufTtcclxuXHJcbi8qXHJcbiAqIEFuIGFjdGlvbiB0aGF0IG1vdmVzIHRoZSB0YXJnZXQgd2l0aCBhIENhdG11bGxSb20gY3VydmUgYnkgYSBjZXJ0YWluIGRpc3RhbmNlLiAgPGJyLz5cclxuICogQSBDYXRtdWxsIFJvbSBpcyBhIENhcmRpbmFsIFNwbGluZSB3aXRoIGEgdGVuc2lvbiBvZiAwLjUuPGJyLz5cclxuICogaHR0cDovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9DdWJpY19IZXJtaXRlX3NwbGluZSNDYXRtdWxsLkUyLjgwLjkzUm9tX3NwbGluZVxyXG4gKiBSZWxhdGl2ZSBjb29yZGluYXRlcy5cclxuICpcclxuICogQGNsYXNzIENhdG11bGxSb21CeTJcclxuICogQGV4dGVuZHMgQ2FyZGluYWxTcGxpbmVCeTJcclxuICpcclxuICogQHBhcmFtIHtOdW1iZXJ9IGR0XHJcbiAqIEBwYXJhbSB7QXJyYXl9IHBvaW50c1xyXG4gKlxyXG4gKiBAZXhhbXBsZVxyXG4gKiB2YXIgYWN0aW9uMSA9IGNjLkNhdG11bGxSb21CeTIoMywgYXJyYXkpO1xyXG4gKi9cclxuY2MuQ2F0bXVsbFJvbUJ5MiA9IGNjLkNsYXNzKHtcclxuICAgIG5hbWU6ICdjYy5DYXRtdWxsUm9tQnkyJyxcclxuICAgIGV4dGVuZHM6IGNjLkNhcmRpbmFsU3BsaW5lQnkyLFxyXG5cclxuICAgIGN0b3I6IGZ1bmN0aW9uKGR0LCBwb2ludHMsIGFuZ2xlT2Zmc2V0KSB7XHJcbiAgICAgICAgcG9pbnRzICYmIHRoaXMuaW5pdFdpdGhEdXJhdGlvbihkdCwgcG9pbnRzLCBhbmdsZU9mZnNldCk7XHJcbiAgICB9LFxyXG5cclxuICAgIGluaXRXaXRoRHVyYXRpb246ZnVuY3Rpb24gKGR0LCBwb2ludHMsIGFuZ2xlT2Zmc2V0KSB7XHJcbiAgICAgICAgcmV0dXJuIGNjLkNhcmRpbmFsU3BsaW5lVG8yLnByb3RvdHlwZS5pbml0V2l0aER1cmF0aW9uLmNhbGwodGhpcywgZHQsIHBvaW50cywgYW5nbGVPZmZzZXQsIDAuNSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNsb25lOmZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgYWN0aW9uID0gbmV3IGNjLkNhdG11bGxSb21CeTIoKTtcclxuICAgICAgICBhY3Rpb24uaW5pdFdpdGhEdXJhdGlvbih0aGlzLl9kdXJhdGlvbiwgY2xvbmVDb250cm9sUG9pbnRzKHRoaXMuX3BvaW50cyksIHRoaXMuX2FuZ2xlT2Zmc2V0KTtcclxuICAgICAgICByZXR1cm4gYWN0aW9uO1xyXG4gICAgfVxyXG59KTtcclxuXHJcbi8qKlxyXG4gKiAhI2VuIENyZWF0ZXMgYW4gYWN0aW9uIHdpdGggYSBDYXJkaW5hbCBTcGxpbmUgYXJyYXkgb2YgcG9pbnRzIGFuZCB0ZW5zaW9uLlxyXG4gKiAhI3poIOaMiSBDYXRtdWxsIFJvbSDmoLfmnaHmm7Lnur/ovajov7nnp7vliqjmjIflrprnmoTot53nprvjgIJcclxuICogQG1ldGhvZCBDYXRtdWxsUm9tQnkyXHJcbiAqIEBwYXJhbSB7TnVtYmVyfSBkdFxyXG4gKiBAcGFyYW0ge0FycmF5fSBwb2ludHNcclxuICogQHBhcmFtIHtOdW1iZXJ9IGFuZ2xlT2Zmc2V0XHJcbiAqIEByZXR1cm4ge0FjdGlvbkludGVydmFsfVxyXG4gKiBAZXhhbXBsZVxyXG4gKiB2YXIgYWN0aW9uMSA9IGNjLkNhdG11bGxSb21CeTIoMywgYXJyYXksIDkwKTtcclxuICovXHJcbmNjLmNhdG11bGxSb21CeTIgPSBmdW5jdGlvbiAoZHQsIHBvaW50cywgYW5nbGVPZmZzZXQpIHtcclxuICAgIHJldHVybiBuZXcgY2MuQ2F0bXVsbFJvbUJ5MihkdCwgcG9pbnRzLCBhbmdsZU9mZnNldCk7XHJcbn07Il19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/BR1/BR1.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9a459Pv04ZAzJrDsCZSGkoc', 'BR1');
// Scripts/GamePlay/BR1/BR1.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Global_1 = require("../../Common/Global");
var Singleton_1 = require("../../Common/Singleton");
var AIController_1 = require("../../Controller/AIController");
var CharacterController_1 = require("../../Controller/CharacterController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BR1 = /** @class */ (function (_super) {
    __extends(BR1, _super);
    function BR1() {
        var _this = _super.call(this) || this;
        _this.Guide = null;
        _this.txtCollectBlock = null;
        _this.myCharacter = null;
        _this.AIParent = null;
        _this.BoxParent = null;
        _this.txtClimb = null;
        _this.joyStick = null;
        _this.effectCongra = null;
        _this.btnDownLoad = null;
        _this.endGame = null;
        _this.boolEndGameOne = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        _this.boolcheckInteraction = false;
        BR1_1._instance = _this;
        return _this;
    }
    BR1_1 = BR1;
    BR1.prototype.start = function () {
        Global_1.default.boolStartPlay = true;
    };
    BR1.prototype.update = function () {
        var _this = this;
        if (Global_1.default.boolEnableTouch && !this.boolcheckInteraction) {
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
            for (var i = 0; i < this.AIParent.childrenCount; i++) {
                this.AIParent.children[i].getComponent(AIController_1.default).StartMove();
            }
            this.btnDownLoad.active = true;
            this.boolcheckInteraction = true;
        }
        if (this.myCharacter.y >= 70 && !this.boolEndGameOne) {
            this.boolEndGameOne = true;
            Global_1.default.boolEndGame = true;
            Global_1.default.boolEnableTouch = false;
            for (var i = 0; i < this.myCharacter.children[2].childrenCount; i++) {
                if (this.myCharacter.children[2].children[i].name == "BrickStick") {
                    this.myCharacter.children[2].children[i].active = false;
                }
            }
            for (var i = 0; i < this.AIParent.childrenCount; i++) {
                this.AIParent.children[i].active = false;
            }
            this.myCharacter.stopAllActions();
            this.joyStick.active = false;
            this.ScaleCongra();
            this.myCharacter.getComponent(cc.RigidBody3D).setLinearVelocity(new cc.Vec3(0, 0, 0));
            this.myCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = false;
            this.myCharacter.getComponent(cc.SkeletonAnimation).play("Win");
            cc.audioEngine.playEffect(Global_1.default.soundWin, false);
            this.scheduleOnce(function () {
                _this.EndGame();
            }, 1.2);
        }
    };
    BR1.prototype.ScaleCongra = function () {
        var _this = this;
        this.effectCongra.scale = 0;
        this.effectCongra.opacity = 255;
        this.effectCongra.runAction(cc.sequence(cc.scaleTo(0.3, 1.3).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.effectCongra.runAction(cc.fadeOut(0.3));
            }, 0.7);
        })));
    };
    BR1.prototype.EndGame = function () {
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
        this.scheduleOnce(function () {
            cc.audioEngine.stopAllEffects();
        }, 2);
        this.endGame.active = true;
    };
    var BR1_1;
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "txtCollectBlock", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "myCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "AIParent", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "BoxParent", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "txtClimb", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "joyStick", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "effectCongra", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "btnDownLoad", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "endGame", void 0);
    BR1 = BR1_1 = __decorate([
        ccclass
    ], BR1);
    return BR1;
}(Singleton_1.default));
exports.default = BR1;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXEJSMVxcQlIxLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLDhDQUF5QztBQUN6QyxvREFBK0M7QUFDL0MsOERBQXlEO0FBQ3pELDRFQUF1RTtBQUVqRSxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRzVDO0lBQWlDLHVCQUFjO0lBMEIzQztRQUFBLFlBQ0ksaUJBQU8sU0FFVjtRQTNCRCxXQUFLLEdBQVksSUFBSSxDQUFDO1FBRXRCLHFCQUFlLEdBQVksSUFBSSxDQUFDO1FBRWhDLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRTVCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFFekIsZUFBUyxHQUFZLElBQUksQ0FBQztRQUUxQixjQUFRLEdBQVksSUFBSSxDQUFDO1FBRXpCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFFekIsa0JBQVksR0FBWSxJQUFJLENBQUM7UUFFN0IsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFFNUIsYUFBTyxHQUFZLElBQUksQ0FBQztRQUN4QixvQkFBYyxHQUFZLEtBQUssQ0FBQztRQUNoQyxnQkFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixlQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLFlBQU0sR0FBWSxLQUFLLENBQUM7UUFDeEIsMEJBQW9CLEdBQVksS0FBSyxDQUFDO1FBR2xDLEtBQUcsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDOztJQUN6QixDQUFDO1lBN0JnQixHQUFHO0lBOEJwQixtQkFBSyxHQUFMO1FBQ0ksZ0JBQU0sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO0lBQ2hDLENBQUM7SUFDRCxvQkFBTSxHQUFOO1FBQUEsaUJBbUNDO1FBbENHLElBQUksZ0JBQU0sQ0FBQyxlQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDdEQsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUNwQztZQUNELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDbEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLHNCQUFZLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQzthQUNwRTtZQUNELElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUMvQixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO1NBQ3BDO1FBQ0QsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ2xELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1lBQzNCLGdCQUFNLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztZQUMxQixnQkFBTSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7WUFDL0IsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDakUsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLFlBQVksRUFBRTtvQkFDL0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7aUJBQzNEO2FBQ0o7WUFDRCxLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQ25EO2dCQUNJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7YUFDNUM7WUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ2xDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUM3QixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDbkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdEYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNqRixJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDaEUsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDbEQsSUFBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxLQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDbkIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ1g7SUFDTCxDQUFDO0lBQ0QseUJBQVcsR0FBWDtRQUFBLGlCQVFDO1FBUEcsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQzVCLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUNoQyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsUUFBUSxDQUFDO1lBQ2pHLEtBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ2QsS0FBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ2pELENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNaLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNULENBQUM7SUFDRCxxQkFBTyxHQUFQO1FBQ0ksSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLE1BQU0sQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ3RDO1FBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtTQUNwQztRQUNELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNiLE1BQU0sQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ3ZDO1FBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNkLEVBQUUsQ0FBQyxXQUFXLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDcEMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ04sSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQy9CLENBQUM7O0lBMUZEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7c0NBQ0k7SUFFdEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnREFDYztJQUVoQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzRDQUNVO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7eUNBQ087SUFFekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzswQ0FDUTtJQUUxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3lDQUNPO0lBRXpCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7eUNBQ087SUFFekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs2Q0FDVztJQUU3QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzRDQUNVO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7d0NBQ007SUFwQlAsR0FBRztRQUR2QixPQUFPO09BQ2EsR0FBRyxDQTZGdkI7SUFBRCxVQUFDO0NBN0ZELEFBNkZDLENBN0ZnQyxtQkFBUyxHQTZGekM7a0JBN0ZvQixHQUFHIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEdsb2JhbCBmcm9tIFwiLi4vLi4vQ29tbW9uL0dsb2JhbFwiO1xyXG5pbXBvcnQgU2luZ2xldG9uIGZyb20gXCIuLi8uLi9Db21tb24vU2luZ2xldG9uXCI7XHJcbmltcG9ydCBBSUNvbnRyb2xsZXIgZnJvbSBcIi4uLy4uL0NvbnRyb2xsZXIvQUlDb250cm9sbGVyXCI7XHJcbmltcG9ydCBDaGFyYWN0ZXJDb250cm9sbGVyIGZyb20gXCIuLi8uLi9Db250cm9sbGVyL0NoYXJhY3RlckNvbnRyb2xsZXJcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcbmRlY2xhcmUgY29uc3Qgd2luZG93OiBhbnk7XHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEJSMSBleHRlbmRzIFNpbmdsZXRvbjxCUjE+IHtcclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgR3VpZGU6IGNjLk5vZGUgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICB0eHRDb2xsZWN0QmxvY2s6IGNjLk5vZGUgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBteUNoYXJhY3RlcjogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEFJUGFyZW50OiBjYy5Ob2RlID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgQm94UGFyZW50OiBjYy5Ob2RlID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgdHh0Q2xpbWI6IGNjLk5vZGUgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBqb3lTdGljazogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGVmZmVjdENvbmdyYTogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGJ0bkRvd25Mb2FkOiBjYy5Ob2RlID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgZW5kR2FtZTogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBib29sRW5kR2FtZU9uZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgaXJvbnNvdXJjZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgbWluZHdvcmtzOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICB2dW5nbGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGJvb2xjaGVja0ludGVyYWN0aW9uOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIEJSMS5faW5zdGFuY2UgPSB0aGlzO1xyXG4gICAgfVxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgR2xvYmFsLmJvb2xTdGFydFBsYXkgPSB0cnVlO1xyXG4gICAgfVxyXG4gICAgdXBkYXRlKCkge1xyXG4gICAgICAgIGlmIChHbG9iYWwuYm9vbEVuYWJsZVRvdWNoICYmICF0aGlzLmJvb2xjaGVja0ludGVyYWN0aW9uKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmlyb25zb3VyY2UpIHtcclxuICAgICAgICAgICAgICAgIHdpbmRvdy5OVUMudHJpZ2dlci5pbnRlcmFjdGlvbigpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5BSVBhcmVudC5jaGlsZHJlbkNvdW50OyBpKyspIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuQUlQYXJlbnQuY2hpbGRyZW5baV0uZ2V0Q29tcG9uZW50KEFJQ29udHJvbGxlcikuU3RhcnRNb3ZlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5idG5Eb3duTG9hZC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLmJvb2xjaGVja0ludGVyYWN0aW9uID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMubXlDaGFyYWN0ZXIueSA+PSA3MCAmJiAhdGhpcy5ib29sRW5kR2FtZU9uZSkge1xyXG4gICAgICAgICAgICB0aGlzLmJvb2xFbmRHYW1lT25lID0gdHJ1ZTtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xFbmRHYW1lID0gdHJ1ZTtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xFbmFibGVUb3VjaCA9IGZhbHNlO1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMubXlDaGFyYWN0ZXIuY2hpbGRyZW5bMl0uY2hpbGRyZW5Db3VudDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5teUNoYXJhY3Rlci5jaGlsZHJlblsyXS5jaGlsZHJlbltpXS5uYW1lID09IFwiQnJpY2tTdGlja1wiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5teUNoYXJhY3Rlci5jaGlsZHJlblsyXS5jaGlsZHJlbltpXS5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdGhpcy5BSVBhcmVudC5jaGlsZHJlbkNvdW50OyBpKyspXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuQUlQYXJlbnQuY2hpbGRyZW5baV0uYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5teUNoYXJhY3Rlci5zdG9wQWxsQWN0aW9ucygpO1xyXG4gICAgICAgICAgICB0aGlzLmpveVN0aWNrLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLlNjYWxlQ29uZ3JhKCk7XHJcbiAgICAgICAgICAgIHRoaXMubXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KGNjLlJpZ2lkQm9keTNEKS5zZXRMaW5lYXJWZWxvY2l0eShuZXcgY2MuVmVjMygwLCAwLCAwKSk7XHJcbiAgICAgICAgICAgIHRoaXMubXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkFycm93RGlyZWN0aW9uLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLm15Q2hhcmFjdGVyLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIldpblwiKTtcclxuICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdChHbG9iYWwuc291bmRXaW4sIGZhbHNlKTtcclxuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5FbmRHYW1lKCk7XHJcbiAgICAgICAgICAgIH0sIDEuMik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgU2NhbGVDb25ncmEoKSB7XHJcbiAgICAgICAgdGhpcy5lZmZlY3RDb25ncmEuc2NhbGUgPSAwO1xyXG4gICAgICAgIHRoaXMuZWZmZWN0Q29uZ3JhLm9wYWNpdHkgPSAyNTU7XHJcbiAgICAgICAgdGhpcy5lZmZlY3RDb25ncmEucnVuQWN0aW9uKGNjLnNlcXVlbmNlKGNjLnNjYWxlVG8oMC4zLCAxLjMpLmVhc2luZyhjYy5lYXNlQm91bmNlT3V0KCkpLCBjYy5jYWxsRnVuYygoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZWZmZWN0Q29uZ3JhLnJ1bkFjdGlvbihjYy5mYWRlT3V0KDAuMykpO1xyXG4gICAgICAgICAgICB9LCAwLjcpO1xyXG4gICAgICAgIH0pKSk7XHJcbiAgICB9XHJcbiAgICBFbmRHYW1lKCkge1xyXG4gICAgICAgIGlmICh0aGlzLm1pbmR3b3Jrcykge1xyXG4gICAgICAgICAgICB3aW5kb3cuZ2FtZUVuZCAmJiB3aW5kb3cuZ2FtZUVuZCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5pcm9uc291cmNlKSB7XHJcbiAgICAgICAgICAgIHdpbmRvdy5OVUMudHJpZ2dlci5lbmRHYW1lKCd3aW4nKVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy52dW5nbGUpIHtcclxuICAgICAgICAgICAgcGFyZW50LnBvc3RNZXNzYWdlKCdjb21wbGV0ZScsICcqJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUuc3RvcEFsbEVmZmVjdHMoKTtcclxuICAgICAgICB9LCAyKTtcclxuICAgICAgICB0aGlzLmVuZEdhbWUuYWN0aXZlID0gdHJ1ZTtcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Camera/CameraFollow.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '92881+lvetMI6WKBsqilrji', 'CameraFollow');
// Scripts/Camera/CameraFollow.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var CharacterController_1 = require("../Controller/CharacterController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CameraFollow = /** @class */ (function (_super) {
    __extends(CameraFollow, _super);
    function CameraFollow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.cameraOffsetX = 0;
        _this.cameraOffsetY = 0;
        _this.cameraOffsetZ = 0;
        return _this;
    }
    CameraFollow.prototype.start = function () {
        this.cameraOffsetX = this.node.x;
        this.cameraOffsetY = this.node.y + 15;
        this.cameraOffsetZ = this.node.z;
    };
    CameraFollow.prototype.update = function () {
        if (Global_1.default.boolStartPlay) {
            if (!Global_1.default.boolEndGame) {
                if (GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.getComponent(CharacterController_1.default).level == 0) {
                    this.node.x = cc.misc.lerp(this.node.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
                    this.node.y = cc.misc.lerp(this.node.y, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y + this.cameraOffsetY, 0.2);
                    this.node.z = cc.misc.lerp(this.node.z, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.z + this.cameraOffsetZ, 0.2);
                }
                else if (GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.getComponent(CharacterController_1.default).level >= 1) {
                    this.node.x = cc.misc.lerp(this.node.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
                    this.node.y = cc.misc.lerp(this.node.y, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y + this.cameraOffsetY - (1 * GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.getComponent(CharacterController_1.default).level), 0.2);
                    this.node.z = cc.misc.lerp(this.node.z, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.z + this.cameraOffsetZ + (0.7 * GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.getComponent(CharacterController_1.default).level), 0.2);
                }
            }
            else {
                this.node.x = cc.misc.lerp(this.node.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y + this.cameraOffsetY, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.z + this.cameraOffsetZ, 0.2);
            }
        }
    };
    CameraFollow = __decorate([
        ccclass
    ], CameraFollow);
    return CameraFollow;
}(cc.Component));
exports.default = CameraFollow;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ2FtZXJhXFxDYW1lcmFGb2xsb3cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsK0RBQTBEO0FBQzFELDJDQUFzQztBQUN0Qyx5RUFBb0U7QUFFOUQsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUc1QztJQUEwQyxnQ0FBWTtJQUR0RDtRQUFBLHFFQStCQztRQTdCRyxtQkFBYSxHQUFXLENBQUMsQ0FBQztRQUMxQixtQkFBYSxHQUFXLENBQUMsQ0FBQztRQUMxQixtQkFBYSxHQUFXLENBQUMsQ0FBQzs7SUEyQjlCLENBQUM7SUExQkcsNEJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDakMsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDdEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBQ0QsNkJBQU0sR0FBTjtRQUNJLElBQUksZ0JBQU0sQ0FBQyxhQUFhLEVBQUU7WUFDdEIsSUFBSSxDQUFDLGdCQUFNLENBQUMsV0FBVyxFQUFFO2dCQUNyQixJQUFJLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsRUFBRTtvQkFDL0csSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxHQUFHLENBQUMsQ0FBQztvQkFDdEksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxHQUFHLENBQUMsQ0FBQztvQkFDdEksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxHQUFHLENBQUMsQ0FBQztpQkFDekk7cUJBQ0ksSUFBSSwwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUU7b0JBQ3BILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7b0JBQ3RJLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztvQkFDdlAsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLEdBQUcsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2lCQUM1UDthQUNKO2lCQUNJO2dCQUNELElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3RJLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3RJLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7YUFDekk7U0FDSjtJQUNMLENBQUM7SUE3QmdCLFlBQVk7UUFEaEMsT0FBTztPQUNhLFlBQVksQ0E4QmhDO0lBQUQsbUJBQUM7Q0E5QkQsQUE4QkMsQ0E5QnlDLEVBQUUsQ0FBQyxTQUFTLEdBOEJyRDtrQkE5Qm9CLFlBQVkiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgR2FtZVBsYXlDb250cm9sbGVyIGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlDb250cm9sbGVyXCI7XHJcbmltcG9ydCBHYW1lUGxheUluc3RhbmNlIGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi9Db21tb24vR2xvYmFsXCI7XHJcbmltcG9ydCBDaGFyYWN0ZXJDb250cm9sbGVyIGZyb20gXCIuLi9Db250cm9sbGVyL0NoYXJhY3RlckNvbnRyb2xsZXJcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDYW1lcmFGb2xsb3cgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG4gICAgY2FtZXJhT2Zmc2V0WDogbnVtYmVyID0gMDtcclxuICAgIGNhbWVyYU9mZnNldFk6IG51bWJlciA9IDA7XHJcbiAgICBjYW1lcmFPZmZzZXRaOiBudW1iZXIgPSAwO1xyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5jYW1lcmFPZmZzZXRYID0gdGhpcy5ub2RlLng7XHJcbiAgICAgICAgdGhpcy5jYW1lcmFPZmZzZXRZID0gdGhpcy5ub2RlLnkgKyAxNTtcclxuICAgICAgICB0aGlzLmNhbWVyYU9mZnNldFogPSB0aGlzLm5vZGUuejtcclxuICAgIH1cclxuICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICBpZiAoR2xvYmFsLmJvb2xTdGFydFBsYXkpIHtcclxuICAgICAgICAgICAgaWYgKCFHbG9iYWwuYm9vbEVuZEdhbWUpIHtcclxuICAgICAgICAgICAgICAgIGlmIChHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5sZXZlbCA9PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCwgMC4yKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vZGUueSA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueSwgR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZS56ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS56LCBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnogKyB0aGlzLmNhbWVyYU9mZnNldFosIDAuMik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNlIGlmIChHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5sZXZlbCA+PSAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCwgMC4yKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vZGUueSA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueSwgR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZIC0gKDEgKiBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5sZXZlbCksIDAuMik7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueiArIHRoaXMuY2FtZXJhT2Zmc2V0WiArICgwLjcgKiBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5sZXZlbCksIDAuMik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueCwgR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueSArIHRoaXMuY2FtZXJhT2Zmc2V0WSwgMC4yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS56ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS56LCBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnogKyB0aGlzLmNhbWVyYU9mZnNldFosIDAuMik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/EnumDefine.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'f512aadc+dGGrR/NUnmdokG', 'EnumDefine');
// Scripts/Common/EnumDefine.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BrickType;
(function (BrickType) {
    BrickType[BrickType["BrickCharacter"] = 0] = "BrickCharacter";
    BrickType[BrickType["BrickAI1"] = 1] = "BrickAI1";
    BrickType[BrickType["BrickAI2"] = 2] = "BrickAI2";
})(BrickType = exports.BrickType || (exports.BrickType = {}));

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxFbnVtRGVmaW5lLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsSUFBWSxTQUlYO0FBSkQsV0FBWSxTQUFTO0lBQ2pCLDZEQUFjLENBQUE7SUFDZCxpREFBUSxDQUFBO0lBQ1IsaURBQVEsQ0FBQTtBQUNaLENBQUMsRUFKVyxTQUFTLEdBQVQsaUJBQVMsS0FBVCxpQkFBUyxRQUlwQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBlbnVtIEJyaWNrVHlwZSB7XHJcbiAgICBCcmlja0NoYXJhY3RlcixcclxuICAgIEJyaWNrQUkxLFxyXG4gICAgQnJpY2tBSTIsICBcclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/EnableEngine.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b20182Zp0dJcrXp4wkKiAlB', 'EnableEngine');
// Scripts/Common/EnableEngine.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var EnableEngine = /** @class */ (function (_super) {
    __extends(EnableEngine, _super);
    function EnableEngine() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    EnableEngine.prototype.onLoad = function () {
        cc.view.enableAutoFullScreen(false);
        cc.director.getCollisionManager().enabled = true;
        cc.director.getPhysics3DManager().enabled = true;
        cc.director.getPhysics3DManager().gravity = cc.v3(0, 0, -1);
    };
    EnableEngine = __decorate([
        ccclass
    ], EnableEngine);
    return EnableEngine;
}(cc.Component));
exports.default = EnableEngine;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxFbmFibGVFbmdpbmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQU0sSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUc1QztJQUEwQyxnQ0FBWTtJQUF0RDs7SUFTQSxDQUFDO0lBUEcsNkJBQU0sR0FBTjtRQUNJLEVBQUUsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDcEMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDakQsRUFBRSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDakQsRUFBRSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBUGdCLFlBQVk7UUFEaEMsT0FBTztPQUNhLFlBQVksQ0FTaEM7SUFBRCxtQkFBQztDQVRELEFBU0MsQ0FUeUMsRUFBRSxDQUFDLFNBQVMsR0FTckQ7a0JBVG9CLFlBQVkiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRW5hYmxlRW5naW5lIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuXHJcbiAgICBvbkxvYWQoKSB7XHJcbiAgICAgICAgY2Mudmlldy5lbmFibGVBdXRvRnVsbFNjcmVlbihmYWxzZSk7XHJcbiAgICAgICAgY2MuZGlyZWN0b3IuZ2V0Q29sbGlzaW9uTWFuYWdlcigpLmVuYWJsZWQgPSB0cnVlO1xyXG4gICAgICAgIGNjLmRpcmVjdG9yLmdldFBoeXNpY3MzRE1hbmFnZXIoKS5lbmFibGVkID0gdHJ1ZTtcclxuICAgICAgICBjYy5kaXJlY3Rvci5nZXRQaHlzaWNzM0RNYW5hZ2VyKCkuZ3Jhdml0eSA9IGNjLnYzKDAsMCwtMSk7XHJcbiAgICB9XHJcblxyXG59Il19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/GamePlayInstance.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '4c8358ev8RCn6trwZVq6/92', 'GamePlayInstance');
// Scripts/Common/GamePlayInstance.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var BR1_1 = require("../GamePlay/BR1/BR1");
var Singleton_1 = require("./Singleton");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
exports.eventDispatcher = new cc.EventTarget();
var GamePlayInstance = /** @class */ (function (_super) {
    __extends(GamePlayInstance, _super);
    function GamePlayInstance() {
        var _this = _super.call(this) || this;
        _this.gameplay = BR1_1.default.Instance(BR1_1.default);
        GamePlayInstance_1._instance = _this;
        return _this;
    }
    GamePlayInstance_1 = GamePlayInstance;
    var GamePlayInstance_1;
    GamePlayInstance = GamePlayInstance_1 = __decorate([
        ccclass
    ], GamePlayInstance);
    return GamePlayInstance;
}(Singleton_1.default));
exports.default = GamePlayInstance;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxHYW1lUGxheUluc3RhbmNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLDJDQUFzQztBQUN0Qyx5Q0FBb0M7QUFDOUIsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUMvQixRQUFBLGVBQWUsR0FBRyxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztBQUVwRDtJQUE4QyxvQ0FBMkI7SUFFckU7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUFKRCxjQUFRLEdBQVEsYUFBRyxDQUFDLFFBQVEsQ0FBQyxhQUFHLENBQUMsQ0FBQztRQUc5QixrQkFBZ0IsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDOztJQUN0QyxDQUFDO3lCQUxnQixnQkFBZ0I7O0lBQWhCLGdCQUFnQjtRQURwQyxPQUFPO09BQ2EsZ0JBQWdCLENBTXBDO0lBQUQsdUJBQUM7Q0FORCxBQU1DLENBTjZDLG1CQUFTLEdBTXREO2tCQU5vQixnQkFBZ0IiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQlIxIGZyb20gXCIuLi9HYW1lUGxheS9CUjEvQlIxXCI7XHJcbmltcG9ydCBTaW5nbGV0b24gZnJvbSBcIi4vU2luZ2xldG9uXCI7XHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcbmV4cG9ydCBjb25zdCBldmVudERpc3BhdGNoZXIgPSBuZXcgY2MuRXZlbnRUYXJnZXQoKTtcclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgR2FtZVBsYXlJbnN0YW5jZSBleHRlbmRzIFNpbmdsZXRvbjxHYW1lUGxheUluc3RhbmNlPiB7XHJcbiAgICBnYW1lcGxheTogQlIxID0gQlIxLkluc3RhbmNlKEJSMSk7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIEdhbWVQbGF5SW5zdGFuY2UuX2luc3RhbmNlID0gdGhpcztcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/GamePlayController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '91971i5LkpN2r+KQEdz49a3', 'GamePlayController');
// Scripts/Common/GamePlayController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Singleton_1 = require("./Singleton");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GamePlayController = /** @class */ (function (_super) {
    __extends(GamePlayController, _super);
    function GamePlayController() {
        var _this = _super.call(this) || this;
        _this.materialBrickCharacter = null;
        _this.materialBrickAI = [];
        GamePlayController_1._instance = _this;
        return _this;
    }
    GamePlayController_1 = GamePlayController;
    var GamePlayController_1;
    __decorate([
        property(cc.Material)
    ], GamePlayController.prototype, "materialBrickCharacter", void 0);
    __decorate([
        property(cc.Material)
    ], GamePlayController.prototype, "materialBrickAI", void 0);
    GamePlayController = GamePlayController_1 = __decorate([
        ccclass
    ], GamePlayController);
    return GamePlayController;
}(Singleton_1.default));
exports.default = GamePlayController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxHYW1lUGxheUNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EseUNBQW9DO0FBQzlCLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFFNUM7SUFBZ0Qsc0NBQTZCO0lBS3pFO1FBQUEsWUFDSSxpQkFBTyxTQUVWO1FBTkQsNEJBQXNCLEdBQWdCLElBQUksQ0FBQztRQUUzQyxxQkFBZSxHQUFrQixFQUFFLENBQUM7UUFHaEMsb0JBQWtCLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQzs7SUFDeEMsQ0FBQzsyQkFSZ0Isa0JBQWtCOztJQUVuQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDO3NFQUNxQjtJQUUzQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDOytEQUNjO0lBSm5CLGtCQUFrQjtRQUR0QyxPQUFPO09BQ2Esa0JBQWtCLENBU3RDO0lBQUQseUJBQUM7Q0FURCxBQVNDLENBVCtDLG1CQUFTLEdBU3hEO2tCQVRvQixrQkFBa0IiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQlIxIGZyb20gXCIuLi9HYW1lUGxheS9CUjEvQlIxXCI7XHJcbmltcG9ydCBTaW5nbGV0b24gZnJvbSBcIi4vU2luZ2xldG9uXCI7XHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEdhbWVQbGF5Q29udHJvbGxlciBleHRlbmRzIFNpbmdsZXRvbjxHYW1lUGxheUNvbnRyb2xsZXI+IHtcclxuICAgIEBwcm9wZXJ0eShjYy5NYXRlcmlhbClcclxuICAgIG1hdGVyaWFsQnJpY2tDaGFyYWN0ZXI6IGNjLk1hdGVyaWFsID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5NYXRlcmlhbClcclxuICAgIG1hdGVyaWFsQnJpY2tBSTogY2MuTWF0ZXJpYWxbXSA9IFtdO1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBHYW1lUGxheUNvbnRyb2xsZXIuX2luc3RhbmNlID0gdGhpcztcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/PlatformBtn.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'e53faTo9E5NVrjkskVH0ayI', 'PlatformBtn');
// Scripts/Common/PlatformBtn.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var PlatformBtn = /** @class */ (function (_super) {
    __extends(PlatformBtn, _super);
    function PlatformBtn() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.androidSprites = null;
        _this.iosSprites = null;
        return _this;
    }
    PlatformBtn.prototype.start = function () {
        if (cc.sys.os == cc.sys.OS_ANDROID)
            this.getComponent(cc.Sprite).spriteFrame = this.androidSprites;
        else if (cc.sys.os == cc.sys.OS_IOS)
            this.getComponent(cc.Sprite).spriteFrame = this.iosSprites;
    };
    __decorate([
        property(cc.SpriteFrame)
    ], PlatformBtn.prototype, "androidSprites", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], PlatformBtn.prototype, "iosSprites", void 0);
    PlatformBtn = __decorate([
        ccclass
    ], PlatformBtn);
    return PlatformBtn;
}(cc.Component));
exports.default = PlatformBtn;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxQbGF0Zm9ybUJ0bi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBTSxJQUFBLGtCQUFtQyxFQUFsQyxvQkFBTyxFQUFFLHNCQUF5QixDQUFDO0FBRzFDO0lBQXlDLCtCQUFZO0lBRHJEO1FBQUEscUVBY0M7UUFYRyxvQkFBYyxHQUFtQixJQUFJLENBQUM7UUFFdEMsZ0JBQVUsR0FBbUIsSUFBSSxDQUFDOztJQVN0QyxDQUFDO0lBUkcsMkJBQUssR0FBTDtRQUVJLElBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFVO1lBQzdCLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO2FBQzlELElBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNO1lBQzlCLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ25FLENBQUM7SUFURDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDO3VEQUNhO0lBRXRDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7bURBQ1M7SUFKakIsV0FBVztRQUQvQixPQUFPO09BQ2EsV0FBVyxDQWEvQjtJQUFELGtCQUFDO0NBYkQsQUFhQyxDQWJ3QyxFQUFFLENBQUMsU0FBUyxHQWFwRDtrQkFib0IsV0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUGxhdGZvcm1CdG4gZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxyXG4gICAgYW5kcm9pZFNwcml0ZXM6IGNjLlNwcml0ZUZyYW1lID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGVGcmFtZSlcclxuICAgIGlvc1Nwcml0ZXM6IGNjLlNwcml0ZUZyYW1lID0gbnVsbDtcclxuICAgIHN0YXJ0KClcclxuICAgIHtcclxuICAgICAgICBpZihjYy5zeXMub3MgPT0gY2Muc3lzLk9TX0FORFJPSUQpXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSB0aGlzLmFuZHJvaWRTcHJpdGVzO1xyXG4gICAgICAgIGVsc2UgaWYoY2Muc3lzLm9zID09IGNjLnN5cy5PU19JT1MpXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSB0aGlzLmlvc1Nwcml0ZXM7XHJcbiAgICB9XHJcbiAgICBcclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/Global.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '46e6fxcur1NFZbnIGcspwN3', 'Global');
// Scripts/Common/Global.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Global = {
    touchPos: null,
    boolEnableTouch: false,
    boolFirstTouchJoyStick: false,
    boolStartPlay: false,
    boolStartAttacking: false,
    boolCheckAttacking: false,
    boolCheckAttacked: false,
    boolCharacterFall: false,
    boolEndGame: false,
    soundBG: null,
    soundIntro: null,
    soundAttack: null,
    soundFootStep: null,
    soundCollect: null,
    soundClickBtn: null,
    soundBoxBroken: null,
    soundUpStairs: null,
    soundWin: null
};
exports.default = Global;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxHbG9iYWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQkEsSUFBSSxNQUFNLEdBQVc7SUFDakIsUUFBUSxFQUFFLElBQUk7SUFDZCxlQUFlLEVBQUUsS0FBSztJQUN0QixzQkFBc0IsRUFBRSxLQUFLO0lBQzdCLGFBQWEsRUFBRSxLQUFLO0lBQ3BCLGtCQUFrQixFQUFFLEtBQUs7SUFDekIsa0JBQWtCLEVBQUUsS0FBSztJQUN6QixpQkFBaUIsRUFBRSxLQUFLO0lBQ3hCLGlCQUFpQixFQUFFLEtBQUs7SUFDeEIsV0FBVyxFQUFFLEtBQUs7SUFDbEIsT0FBTyxFQUFFLElBQUk7SUFDYixVQUFVLEVBQUUsSUFBSTtJQUNoQixXQUFXLEVBQUUsSUFBSTtJQUNqQixhQUFhLEVBQUUsSUFBSTtJQUNuQixZQUFZLEVBQUUsSUFBSTtJQUNsQixhQUFhLEVBQUUsSUFBSTtJQUNuQixjQUFjLEVBQUUsSUFBSTtJQUNwQixhQUFhLEVBQUUsSUFBSTtJQUNuQixRQUFRLEVBQUUsSUFBSTtDQUNqQixDQUFDO0FBQ0Ysa0JBQWUsTUFBTSxDQUFDIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW50ZXJmYWNlIEdsb2JhbCB7XHJcbiAgICB0b3VjaFBvczogY2MuVmVjMixcclxuICAgIGJvb2xFbmFibGVUb3VjaDogYm9vbGVhbixcclxuICAgIGJvb2xGaXJzdFRvdWNoSm95U3RpY2s6IGJvb2xlYW4sXHJcbiAgICBib29sU3RhcnRQbGF5OiBib29sZWFuLFxyXG4gICAgYm9vbFN0YXJ0QXR0YWNraW5nOiBib29sZWFuLFxyXG4gICAgYm9vbENoZWNrQXR0YWNraW5nOiBib29sZWFuLFxyXG4gICAgYm9vbENoZWNrQXR0YWNrZWQ6IGJvb2xlYW4sXHJcbiAgICBib29sQ2hhcmFjdGVyRmFsbDogYm9vbGVhbixcclxuICAgIGJvb2xFbmRHYW1lOiBib29sZWFuLFxyXG4gICAgc291bmRCRzogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRJbnRybzogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRBdHRhY2s6IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kRm9vdFN0ZXA6IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kQ29sbGVjdDogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRDbGlja0J0bjogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRCb3hCcm9rZW46IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kVXBTdGFpcnM6IGNjLkF1ZGlvQ2xpcFxyXG4gICAgc291bmRXaW46IGNjLkF1ZGlvQ2xpcFxyXG59XHJcbmxldCBHbG9iYWw6IEdsb2JhbCA9IHtcclxuICAgIHRvdWNoUG9zOiBudWxsLFxyXG4gICAgYm9vbEVuYWJsZVRvdWNoOiBmYWxzZSxcclxuICAgIGJvb2xGaXJzdFRvdWNoSm95U3RpY2s6IGZhbHNlLFxyXG4gICAgYm9vbFN0YXJ0UGxheTogZmFsc2UsXHJcbiAgICBib29sU3RhcnRBdHRhY2tpbmc6IGZhbHNlLFxyXG4gICAgYm9vbENoZWNrQXR0YWNraW5nOiBmYWxzZSxcclxuICAgIGJvb2xDaGVja0F0dGFja2VkOiBmYWxzZSxcclxuICAgIGJvb2xDaGFyYWN0ZXJGYWxsOiBmYWxzZSxcclxuICAgIGJvb2xFbmRHYW1lOiBmYWxzZSxcclxuICAgIHNvdW5kQkc6IG51bGwsXHJcbiAgICBzb3VuZEludHJvOiBudWxsLFxyXG4gICAgc291bmRBdHRhY2s6IG51bGwsXHJcbiAgICBzb3VuZEZvb3RTdGVwOiBudWxsLFxyXG4gICAgc291bmRDb2xsZWN0OiBudWxsLFxyXG4gICAgc291bmRDbGlja0J0bjogbnVsbCxcclxuICAgIHNvdW5kQm94QnJva2VuOiBudWxsLFxyXG4gICAgc291bmRVcFN0YWlyczogbnVsbCxcclxuICAgIHNvdW5kV2luOiBudWxsXHJcbn07XHJcbmV4cG9ydCBkZWZhdWx0IEdsb2JhbDsiXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/KeyEvent.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'e49c6ej8eJKEZmMHqGedjtb', 'KeyEvent');
// Scripts/Common/KeyEvent.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var KeyEvent = {
    increaseStickMyCharacter: "increaseStickMyCharacter",
    increaseStickAI1: "increaseStickAI1",
    increaseStickAI2: "increaseStickAI2"
};
exports.default = KeyEvent;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxLZXlFdmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUtBLElBQUksUUFBUSxHQUNaO0lBQ0ksd0JBQXdCLEVBQUUsMEJBQTBCO0lBQ3BELGdCQUFnQixFQUFFLGtCQUFrQjtJQUNwQyxnQkFBZ0IsRUFBRSxrQkFBa0I7Q0FDdkMsQ0FBQTtBQUNELGtCQUFlLFFBQVEsQ0FBQSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImludGVyZmFjZSBLZXlFdmVudCB7XHJcbiAgICBpbmNyZWFzZVN0aWNrTXlDaGFyYWN0ZXI6IHN0cmluZyxcclxuICAgIGluY3JlYXNlU3RpY2tBSTE6IHN0cmluZyxcclxuICAgIGluY3JlYXNlU3RpY2tBSTI6IHN0cmluZyxcclxufVxyXG5sZXQgS2V5RXZlbnQ6IEtleUV2ZW50ID1cclxue1xyXG4gICAgaW5jcmVhc2VTdGlja015Q2hhcmFjdGVyOiBcImluY3JlYXNlU3RpY2tNeUNoYXJhY3RlclwiLFxyXG4gICAgaW5jcmVhc2VTdGlja0FJMTogXCJpbmNyZWFzZVN0aWNrQUkxXCIsXHJcbiAgICBpbmNyZWFzZVN0aWNrQUkyOiBcImluY3JlYXNlU3RpY2tBSTJcIlxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IEtleUV2ZW50Il19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/Singleton.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '4bd7doXpcZJkaYWd5ceCR/g', 'Singleton');
// Scripts/Common/Singleton.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Singleton = /** @class */ (function (_super) {
    __extends(Singleton, _super);
    function Singleton() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Singleton.Instance = function (c) {
        if (this._instance == null) {
            this._instance = new c();
        }
        return this._instance;
    };
    Singleton._instance = null;
    return Singleton;
}(cc.Component));
exports.default = Singleton;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxTaW5nbGV0b24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0E7SUFBMEMsNkJBQVk7SUFBdEQ7O0lBUUEsQ0FBQztJQVBpQixrQkFBUSxHQUF0QixVQUEwQixDQUFlO1FBQ3JDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLEVBQUM7WUFDdkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1NBQzVCO1FBQ0QsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7SUFDYSxtQkFBUyxHQUFHLElBQUksQ0FBQztJQUNuQyxnQkFBQztDQVJELEFBUUMsQ0FSeUMsRUFBRSxDQUFDLFNBQVMsR0FRckQ7a0JBUm9CLFNBQVMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2luZ2xldG9uPFQ+IGV4dGVuZHMgY2MuQ29tcG9uZW50e1xyXG4gICAgcHVibGljIHN0YXRpYyBJbnN0YW5jZTxUPihjOiB7bmV3KCk6IFQ7IH0pIDogVHtcclxuICAgICAgICBpZiAodGhpcy5faW5zdGFuY2UgPT0gbnVsbCl7XHJcbiAgICAgICAgICAgIHRoaXMuX2luc3RhbmNlID0gbmV3IGMoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2luc3RhbmNlO1xyXG4gICAgfVxyXG4gICAgcHVibGljIHN0YXRpYyBfaW5zdGFuY2UgPSBudWxsO1xyXG59XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/ScrollNew.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b1b8bobEddLp5s23+RJ6V0W', 'ScrollNew');
// Scripts/Common/ScrollNew.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ScrollNew = /** @class */ (function (_super) {
    __extends(ScrollNew, _super);
    function ScrollNew() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.speed = 80;
        _this.resetY = -1138;
        _this.YStart = 1138;
        _this.Bg1 = null;
        _this.Bg2 = null;
        _this.Temp1 = 0;
        _this.Temp2 = 0;
        return _this;
    }
    ScrollNew.prototype.update = function (dt) {
        var y = this.node.y;
        y -= this.speed * dt;
        if (y <= this.resetY) {
            if (this.Bg2.y > this.Bg1.y) {
                this.Bg1.y = this.YStart;
                this.Bg2.y = 0;
            }
            else {
                this.Bg2.y = this.YStart;
                this.Bg1.y = 0;
            }
            y = 0;
        }
        this.node.y = y;
    };
    __decorate([
        property()
    ], ScrollNew.prototype, "speed", void 0);
    __decorate([
        property()
    ], ScrollNew.prototype, "resetY", void 0);
    __decorate([
        property()
    ], ScrollNew.prototype, "YStart", void 0);
    __decorate([
        property(cc.Node)
    ], ScrollNew.prototype, "Bg1", void 0);
    __decorate([
        property(cc.Node)
    ], ScrollNew.prototype, "Bg2", void 0);
    ScrollNew = __decorate([
        ccclass
    ], ScrollNew);
    return ScrollNew;
}(cc.Component));
exports.default = ScrollNew;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxTY3JvbGxOZXcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQU0sSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUc1QztJQUF1Qyw2QkFBWTtJQURuRDtRQUFBLHFFQThCQztRQTNCRyxXQUFLLEdBQUcsRUFBRSxDQUFDO1FBRVgsWUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDO1FBRWYsWUFBTSxHQUFHLElBQUksQ0FBQztRQUVkLFNBQUcsR0FBWSxJQUFJLENBQUM7UUFFcEIsU0FBRyxHQUFZLElBQUksQ0FBQztRQUNwQixXQUFLLEdBQVcsQ0FBQyxDQUFDO1FBQ2xCLFdBQUssR0FBVyxDQUFDLENBQUM7O0lBaUJ0QixDQUFDO0lBaEJHLDBCQUFNLEdBQU4sVUFBTyxFQUFFO1FBQ0wsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDcEIsQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDdkIsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDekIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztnQkFDekIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2xCO2lCQUNJO2dCQUNELElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNsQjtZQUNELENBQUMsR0FBRyxDQUFDLENBQUM7U0FDTDtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNwQixDQUFDO0lBMUJEO1FBREMsUUFBUSxFQUFFOzRDQUNBO0lBRVg7UUFEQyxRQUFRLEVBQUU7NkNBQ0k7SUFFZjtRQURDLFFBQVEsRUFBRTs2Q0FDRztJQUVkO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7MENBQ0U7SUFFcEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzswQ0FDRTtJQVZILFNBQVM7UUFEN0IsT0FBTztPQUNhLFNBQVMsQ0E2QjdCO0lBQUQsZ0JBQUM7Q0E3QkQsQUE2QkMsQ0E3QnNDLEVBQUUsQ0FBQyxTQUFTLEdBNkJsRDtrQkE3Qm9CLFNBQVMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2Nyb2xsTmV3IGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICAgIEBwcm9wZXJ0eSgpXHJcbiAgICBzcGVlZCA9IDgwO1xyXG4gICAgQHByb3BlcnR5KClcclxuICAgIHJlc2V0WSA9IC0xMTM4O1xyXG4gICAgQHByb3BlcnR5KClcclxuICAgIFlTdGFydCA9IDExMzg7XHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEJnMTogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEJnMjogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBUZW1wMTogbnVtYmVyID0gMDtcclxuICAgIFRlbXAyOiBudW1iZXIgPSAwO1xyXG4gICAgdXBkYXRlKGR0KSB7XHJcbiAgICAgICAgdmFyIHkgPSB0aGlzLm5vZGUueTtcclxuICAgICAgICB5IC09IHRoaXMuc3BlZWQgKiBkdDtcclxuICAgICAgICAgaWYgKHkgPD0gdGhpcy5yZXNldFkpIHtcclxuICAgICAgICBpZiAodGhpcy5CZzIueSA+IHRoaXMuQmcxLnkpIHtcclxuICAgICAgICAgICAgdGhpcy5CZzEueSA9IHRoaXMuWVN0YXJ0O1xyXG4gICAgICAgICAgICB0aGlzLkJnMi55ID0gMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuQmcyLnkgPSB0aGlzLllTdGFydDtcclxuICAgICAgICAgICAgdGhpcy5CZzEueSA9IDA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHkgPSAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm5vZGUueSA9IHk7XHJcbiAgICB9XHJcbn0iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/SoundManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a9858zk9XFHs4LhnNGJENNI', 'SoundManager');
// Scripts/Common/SoundManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Global_1 = require("./Global");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SoundManager = /** @class */ (function (_super) {
    __extends(SoundManager, _super);
    function SoundManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Bg = null;
        _this.footStep = null;
        _this.Intro = null;
        _this.Attack = null;
        _this.collect = null;
        _this.boxBroken = null;
        _this.upStairs = null;
        _this.win = null;
        return _this;
    }
    SoundManager.prototype.onLoad = function () {
        Global_1.default.soundBG = this.Bg;
        Global_1.default.soundIntro = this.Intro;
        Global_1.default.soundFootStep = this.footStep;
        Global_1.default.soundAttack = this.Attack;
        Global_1.default.soundCollect = this.collect;
        Global_1.default.soundBoxBroken = this.boxBroken;
        Global_1.default.soundUpStairs = this.upStairs;
        Global_1.default.soundWin = this.win;
    };
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Bg", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "footStep", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Intro", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Attack", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "collect", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "boxBroken", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "upStairs", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "win", void 0);
    SoundManager = __decorate([
        ccclass
    ], SoundManager);
    return SoundManager;
}(cc.Component));
exports.default = SoundManager;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxTb3VuZE1hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsbUNBQThCO0FBRXhCLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFHNUM7SUFBMEMsZ0NBQVk7SUFEdEQ7UUFBQSxxRUE0Q0M7UUF2Q0csUUFBRSxHQUFpQixJQUFJLENBQUM7UUFJeEIsY0FBUSxHQUFpQixJQUFJLENBQUM7UUFJOUIsV0FBSyxHQUFpQixJQUFJLENBQUM7UUFJM0IsWUFBTSxHQUFpQixJQUFJLENBQUM7UUFJNUIsYUFBTyxHQUFpQixJQUFJLENBQUM7UUFJN0IsZUFBUyxHQUFpQixJQUFJLENBQUM7UUFJL0IsY0FBUSxHQUFpQixJQUFJLENBQUM7UUFJOUIsU0FBRyxHQUFpQixJQUFJLENBQUM7O0lBVzdCLENBQUM7SUFWRyw2QkFBTSxHQUFOO1FBQ0ksZ0JBQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUN6QixnQkFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQy9CLGdCQUFNLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDckMsZ0JBQU0sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNqQyxnQkFBTSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ25DLGdCQUFNLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDdkMsZ0JBQU0sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUNyQyxnQkFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQy9CLENBQUM7SUF0Q0Q7UUFIQyxRQUFRLENBQUM7WUFDTixJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVM7U0FDckIsQ0FBQzs0Q0FDc0I7SUFJeEI7UUFIQyxRQUFRLENBQUM7WUFDTixJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVM7U0FDckIsQ0FBQztrREFDNEI7SUFJOUI7UUFIQyxRQUFRLENBQUM7WUFDTixJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVM7U0FDckIsQ0FBQzsrQ0FDeUI7SUFJM0I7UUFIQyxRQUFRLENBQUM7WUFDTixJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVM7U0FDckIsQ0FBQztnREFDMEI7SUFJNUI7UUFIQyxRQUFRLENBQUM7WUFDTixJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVM7U0FDckIsQ0FBQztpREFDMkI7SUFJN0I7UUFIQyxRQUFRLENBQUM7WUFDTixJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVM7U0FDckIsQ0FBQzttREFDNkI7SUFJL0I7UUFIQyxRQUFRLENBQUM7WUFDTixJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVM7U0FDckIsQ0FBQztrREFDNEI7SUFJOUI7UUFIQyxRQUFRLENBQUM7WUFDTixJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQVM7U0FDckIsQ0FBQzs2Q0FDdUI7SUFoQ1IsWUFBWTtRQURoQyxPQUFPO09BQ2EsWUFBWSxDQTJDaEM7SUFBRCxtQkFBQztDQTNDRCxBQTJDQyxDQTNDeUMsRUFBRSxDQUFDLFNBQVMsR0EyQ3JEO2tCQTNDb0IsWUFBWSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBHbG9iYWwgZnJvbSBcIi4vR2xvYmFsXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU291bmRNYW5hZ2VyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICAgIEBwcm9wZXJ0eSh7XHJcbiAgICAgICAgdHlwZTogY2MuQXVkaW9DbGlwXHJcbiAgICB9KVxyXG4gICAgQmc6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoe1xyXG4gICAgICAgIHR5cGU6IGNjLkF1ZGlvQ2xpcFxyXG4gICAgfSlcclxuICAgIGZvb3RTdGVwOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KHtcclxuICAgICAgICB0eXBlOiBjYy5BdWRpb0NsaXBcclxuICAgIH0pXHJcbiAgICBJbnRybzogY2MuQXVkaW9DbGlwID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eSh7XHJcbiAgICAgICAgdHlwZTogY2MuQXVkaW9DbGlwXHJcbiAgICB9KVxyXG4gICAgQXR0YWNrOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KHtcclxuICAgICAgICB0eXBlOiBjYy5BdWRpb0NsaXBcclxuICAgIH0pXHJcbiAgICBjb2xsZWN0OiBjYy5BdWRpb0NsaXAgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KHtcclxuICAgICAgICB0eXBlOiBjYy5BdWRpb0NsaXBcclxuICAgIH0pXHJcbiAgICBib3hCcm9rZW46IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoe1xyXG4gICAgICAgIHR5cGU6IGNjLkF1ZGlvQ2xpcFxyXG4gICAgfSlcclxuICAgIHVwU3RhaXJzOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KHtcclxuICAgICAgICB0eXBlOiBjYy5BdWRpb0NsaXBcclxuICAgIH0pXHJcbiAgICB3aW46IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XHJcbiAgICBvbkxvYWQoKSB7XHJcbiAgICAgICAgR2xvYmFsLnNvdW5kQkcgPSB0aGlzLkJnO1xyXG4gICAgICAgIEdsb2JhbC5zb3VuZEludHJvID0gdGhpcy5JbnRybztcclxuICAgICAgICBHbG9iYWwuc291bmRGb290U3RlcCA9IHRoaXMuZm9vdFN0ZXA7XHJcbiAgICAgICAgR2xvYmFsLnNvdW5kQXR0YWNrID0gdGhpcy5BdHRhY2s7XHJcbiAgICAgICAgR2xvYmFsLnNvdW5kQ29sbGVjdCA9IHRoaXMuY29sbGVjdDtcclxuICAgICAgICBHbG9iYWwuc291bmRCb3hCcm9rZW4gPSB0aGlzLmJveEJyb2tlbjtcclxuICAgICAgICBHbG9iYWwuc291bmRVcFN0YWlycyA9IHRoaXMudXBTdGFpcnM7XHJcbiAgICAgICAgR2xvYmFsLnNvdW5kV2luID0gdGhpcy53aW47XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Controller/CheckMoveDownStairs.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9b8d8xvinNMPYvAQa26/D07', 'CheckMoveDownStairs');
// Scripts/Controller/CheckMoveDownStairs.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CheckMoveDownStairs = /** @class */ (function (_super) {
    __extends(CheckMoveDownStairs, _super);
    function CheckMoveDownStairs() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.boolMoveDown = false;
        return _this;
    }
    CheckMoveDownStairs.prototype.start = function () {
        var collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-stay', this.onTriggerStay, this);
        collider.on('trigger-exit', this.onTriggerExit, this);
    };
    CheckMoveDownStairs.prototype.onTriggerStay = function (event) {
        if (event.otherCollider.node.group == "Model") {
            if (event.otherCollider.node.name == "CheckPlayerMoveBack") {
                this.boolMoveDown = true;
            }
        }
    };
    CheckMoveDownStairs.prototype.onTriggerExit = function (event) {
        this.boolMoveDown = false;
    };
    CheckMoveDownStairs = __decorate([
        ccclass
    ], CheckMoveDownStairs);
    return CheckMoveDownStairs;
}(cc.Component));
exports.default = CheckMoveDownStairs;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29udHJvbGxlclxcQ2hlY2tNb3ZlRG93blN0YWlycy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBTSxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRzVDO0lBQWlELHVDQUFZO0lBRDdEO1FBQUEscUVBa0JDO1FBaEJHLGtCQUFZLEdBQVksS0FBSyxDQUFDOztJQWdCbEMsQ0FBQztJQWZHLG1DQUFLLEdBQUw7UUFDSSxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNoRCxRQUFRLENBQUMsRUFBRSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3RELFFBQVEsQ0FBQyxFQUFFLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUNELDJDQUFhLEdBQWIsVUFBYyxLQUFLO1FBQ2YsSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksT0FBTyxFQUFFO1lBQzNDLElBQUksS0FBSyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLHFCQUFxQixFQUFFO2dCQUN4RCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQzthQUM1QjtTQUNKO0lBQ0wsQ0FBQztJQUNELDJDQUFhLEdBQWIsVUFBYyxLQUFLO1FBQ2YsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7SUFDOUIsQ0FBQztJQWhCZ0IsbUJBQW1CO1FBRHZDLE9BQU87T0FDYSxtQkFBbUIsQ0FpQnZDO0lBQUQsMEJBQUM7Q0FqQkQsQUFpQkMsQ0FqQmdELEVBQUUsQ0FBQyxTQUFTLEdBaUI1RDtrQkFqQm9CLG1CQUFtQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDaGVja01vdmVEb3duU3RhaXJzIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICAgIGJvb2xNb3ZlRG93bjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgbGV0IGNvbGxpZGVyID0gdGhpcy5nZXRDb21wb25lbnQoY2MuQ29sbGlkZXIzRCk7XHJcbiAgICAgICAgY29sbGlkZXIub24oJ3RyaWdnZXItc3RheScsIHRoaXMub25UcmlnZ2VyU3RheSwgdGhpcyk7XHJcbiAgICAgICAgY29sbGlkZXIub24oJ3RyaWdnZXItZXhpdCcsIHRoaXMub25UcmlnZ2VyRXhpdCwgdGhpcyk7XHJcbiAgICB9XHJcbiAgICBvblRyaWdnZXJTdGF5KGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50Lm90aGVyQ29sbGlkZXIubm9kZS5ncm91cCA9PSBcIk1vZGVsXCIpIHtcclxuICAgICAgICAgICAgaWYgKGV2ZW50Lm90aGVyQ29sbGlkZXIubm9kZS5uYW1lID09IFwiQ2hlY2tQbGF5ZXJNb3ZlQmFja1wiKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmJvb2xNb3ZlRG93biA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBvblRyaWdnZXJFeGl0KGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5ib29sTW92ZURvd24gPSBmYWxzZTtcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Controller/BoxController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6ea4aqpj3hKabEYvQVlqxyC', 'BoxController');
// Scripts/Controller/BoxController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var Utility_1 = require("../Common/Utility");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BoxController = /** @class */ (function (_super) {
    __extends(BoxController, _super);
    function BoxController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.brickHom = null;
        _this.timeBroken = 1.06;
        _this.boolCheckDeathOne = false;
        _this.boolCheckDeath = false;
        _this.boolCheckdelay = false;
        return _this;
    }
    BoxController.prototype.update = function () {
        var _this = this;
        if (!this.boolCheckDeath) {
            if (Global_1.default.boolStartAttacking && Global_1.default.boolCheckAttacking) {
                var pos1 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[0].getPosition()));
                var pos2 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[1].getPosition()));
                var pos3 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[2].getPosition()));
                var direction1 = cc.v2(pos1.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, pos1.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var direction2 = cc.v2(pos2.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, pos2.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var degree = direction1.signAngle(direction2);
                degree = cc.misc.radiansToDegrees(degree);
                var posEnemy = cc.v2(this.node.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, this.node.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var degreeWithPos1 = posEnemy.signAngle(direction1);
                degreeWithPos1 = cc.misc.radiansToDegrees(degreeWithPos1);
                var degreeWithPos2 = posEnemy.signAngle(direction2);
                degreeWithPos2 = cc.misc.radiansToDegrees(degreeWithPos2);
                var realNeed = 0;
                degreeWithPos1 = Math.abs(degreeWithPos1);
                degreeWithPos2 = Math.abs(degreeWithPos2);
                if (degreeWithPos1 > degreeWithPos2) {
                    realNeed = degreeWithPos1;
                }
                else {
                    realNeed = degreeWithPos2;
                }
                var distance = Utility_1.default.Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y));
                var maxDistance = Utility_1.default.Distance(cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y), cc.v2(pos3.x, pos3.y));
                if (Math.abs(realNeed) < degree) {
                    if (distance < maxDistance) {
                        if (Global_1.default.boolCheckAttacked && !this.boolCheckDeathOne) {
                            this.BrokenBox();
                            this.boolCheckDeath = true;
                            this.boolCheckDeathOne = true;
                            //Global.boolCheckAttacked = false;
                            //Global.boolCheckAttacking = false;
                        }
                    }
                }
                if (!this.boolCheckdelay) {
                    this.boolCheckdelay = true;
                    this.scheduleOnce(function () {
                        Global_1.default.boolCheckAttacking = false;
                        _this.boolCheckdelay = false;
                    }, 0.003);
                }
            }
        }
    };
    BoxController.prototype.DieWhenEnemyAttack = function () {
        this.boolCheckDeath = true;
        this.BrokenBox();
    };
    BoxController.prototype.BrokenBox = function () {
        var _this = this;
        this.node.getComponent(cc.SkeletonAnimation).play("Broken");
        cc.audioEngine.playEffect(Global_1.default.soundBoxBroken, false);
        this.node.getComponent(cc.BoxCollider3D).enabled = false;
        this.scheduleOnce(function () {
            // for( let i = 0; i < 20; i++)
            // {
            //     this.SpawnerBrick(this.node.x - 2, this.node.y + 2);
            // }
            _this.SpawnerBrick(_this.node.x - 2, _this.node.y + 2);
            _this.SpawnerBrick(_this.node.x + 2, _this.node.y + 2);
            _this.SpawnerBrick(_this.node.x - 2, _this.node.y - 2);
            _this.SpawnerBrick(_this.node.x + 2, _this.node.y - 2);
        }, this.timeBroken / 7);
        this.scheduleOnce(function () {
            _this.node.getComponent(cc.SkeletonAnimation).stop();
        }, this.timeBroken);
    };
    BoxController.prototype.SpawnerBrick = function (x, y) {
        var Brick = cc.instantiate(this.brickHom);
        Brick.parent = cc.Canvas.instance.node;
        Brick.x = this.node.x;
        Brick.y = this.node.y;
        Brick.z = 1;
        var t = new cc.Tween().to(0.3, { position: new cc.Vec3(x, y, 0.5) })
            .call(function () {
            Brick.getComponent(cc.BoxCollider3D).enabled = true;
        })
            .target(Brick).start();
    };
    __decorate([
        property(cc.Prefab)
    ], BoxController.prototype, "brickHom", void 0);
    BoxController = __decorate([
        ccclass
    ], BoxController);
    return BoxController;
}(cc.Component));
exports.default = BoxController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29udHJvbGxlclxcQm94Q29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSwrREFBMEQ7QUFDMUQsMkNBQXNDO0FBRXRDLDZDQUF3QztBQUVsQyxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRzVDO0lBQTJDLGlDQUFZO0lBRHZEO1FBQUEscUVBMEZDO1FBdkZHLGNBQVEsR0FBYyxJQUFJLENBQUM7UUFDM0IsZ0JBQVUsR0FBVyxJQUFJLENBQUM7UUFDMUIsdUJBQWlCLEdBQVksS0FBSyxDQUFDO1FBQ25DLG9CQUFjLEdBQVksS0FBSyxDQUFDO1FBQ2hDLG9CQUFjLEdBQVksS0FBSyxDQUFDOztJQW1GcEMsQ0FBQztJQWxGRyw4QkFBTSxHQUFOO1FBQUEsaUJBK0NDO1FBOUNHLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3RCLElBQUksZ0JBQU0sQ0FBQyxrQkFBa0IsSUFBSSxnQkFBTSxDQUFDLGtCQUFrQixFQUFFO2dCQUN4RCxJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDcFEsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BRLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUVwUSxJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqTCxJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqTCxJQUFJLE1BQU0sR0FBRyxVQUFVLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUM5QyxNQUFNLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDMUMsSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN6TCxJQUFJLGNBQWMsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNwRCxjQUFjLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDMUQsSUFBSSxjQUFjLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDcEQsY0FBYyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzFELElBQUksUUFBUSxHQUFHLENBQUMsQ0FBQztnQkFDakIsY0FBYyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzFDLGNBQWMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUMxQyxJQUFJLGNBQWMsR0FBRyxjQUFjLEVBQUU7b0JBQ2pDLFFBQVEsR0FBRyxjQUFjLENBQUM7aUJBQzdCO3FCQUNJO29CQUNELFFBQVEsR0FBRyxjQUFjLENBQUM7aUJBQzdCO2dCQUNELElBQUksUUFBUSxHQUFHLGlCQUFPLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDaE4sSUFBSSxXQUFXLEdBQUcsaUJBQU8sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSwwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDek0sSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHLE1BQU0sRUFBRTtvQkFDN0IsSUFBSSxRQUFRLEdBQUcsV0FBVyxFQUFFO3dCQUN4QixJQUFJLGdCQUFNLENBQUMsaUJBQWlCLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7NEJBQ3JELElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQzs0QkFDakIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7NEJBQzNCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7NEJBQzlCLG1DQUFtQzs0QkFDbkMsb0NBQW9DO3lCQUN2QztxQkFDSjtpQkFDSjtnQkFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtvQkFDdEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7b0JBQzNCLElBQUksQ0FBQyxZQUFZLENBQUM7d0JBQ2QsZ0JBQU0sQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7d0JBQ2xDLEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO29CQUNoQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7aUJBQ2I7YUFDSjtTQUNKO0lBQ0wsQ0FBQztJQUNELDBDQUFrQixHQUFsQjtRQUNJLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1FBQzNCLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBQ0QsaUNBQVMsR0FBVDtRQUFBLGlCQWlCQztRQWhCRyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDNUQsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxjQUFjLEVBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDekQsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNkLCtCQUErQjtZQUMvQixJQUFJO1lBQ0osMkRBQTJEO1lBQzNELElBQUk7WUFDSixLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNwRCxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNwRCxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNwRCxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUN4RCxDQUFDLEVBQUUsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUN4QixJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDeEQsQ0FBQyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBQ0Qsb0NBQVksR0FBWixVQUFhLENBQVMsRUFBRSxDQUFTO1FBQzdCLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO1FBQ3ZDLEtBQUssQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDdEIsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUN0QixLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNaLElBQUksQ0FBQyxHQUFHLElBQUksRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsR0FBRyxDQUFDLEVBQUUsQ0FBQzthQUMvRCxJQUFJLENBQUM7WUFDRixLQUFLLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3hELENBQUMsQ0FBQzthQUNELE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBdEZEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7bURBQ087SUFGVixhQUFhO1FBRGpDLE9BQU87T0FDYSxhQUFhLENBeUZqQztJQUFELG9CQUFDO0NBekZELEFBeUZDLENBekYwQyxFQUFFLENBQUMsU0FBUyxHQXlGdEQ7a0JBekZvQixhQUFhIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEdhbWVQbGF5SW5zdGFuY2UgZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFV0aWxpdHkgZnJvbSBcIi4uL0NvbW1vbi9VdGlsaXR5XCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQm94Q29udHJvbGxlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgYnJpY2tIb206IGNjLlByZWZhYiA9IG51bGw7XHJcbiAgICB0aW1lQnJva2VuOiBudW1iZXIgPSAxLjA2O1xyXG4gICAgYm9vbENoZWNrRGVhdGhPbmU6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGJvb2xDaGVja0RlYXRoOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBib29sQ2hlY2tkZWxheTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgdXBkYXRlKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5ib29sQ2hlY2tEZWF0aCkge1xyXG4gICAgICAgICAgICBpZiAoR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZyAmJiBHbG9iYWwuYm9vbENoZWNrQXR0YWNraW5nKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgcG9zMSA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuY2hpbGRyZW5bMF0uY29udmVydFRvV29ybGRTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuY2hpbGRyZW5bMF0uY2hpbGRyZW5bMF0uZ2V0UG9zaXRpb24oKSkpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHBvczIgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmNoaWxkcmVuWzBdLmNvbnZlcnRUb1dvcmxkU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmNoaWxkcmVuWzBdLmNoaWxkcmVuWzFdLmdldFBvc2l0aW9uKCkpKTtcclxuICAgICAgICAgICAgICAgIGxldCBwb3MzID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGUuY29udmVydFRvTm9kZVNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci5jaGlsZHJlblswXS5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci5jaGlsZHJlblswXS5jaGlsZHJlblsyXS5nZXRQb3NpdGlvbigpKSk7XHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IGRpcmVjdGlvbjEgPSBjYy52Mihwb3MxLnggLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLngsIHBvczEueSAtIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGlyZWN0aW9uMiA9IGNjLnYyKHBvczIueCAtIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueCwgcG9zMi55IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci55KTtcclxuICAgICAgICAgICAgICAgIGxldCBkZWdyZWUgPSBkaXJlY3Rpb24xLnNpZ25BbmdsZShkaXJlY3Rpb24yKTtcclxuICAgICAgICAgICAgICAgIGRlZ3JlZSA9IGNjLm1pc2MucmFkaWFuc1RvRGVncmVlcyhkZWdyZWUpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHBvc0VuZW15ID0gY2MudjIodGhpcy5ub2RlLnggLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLngsIHRoaXMubm9kZS55IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci55KTtcclxuICAgICAgICAgICAgICAgIGxldCBkZWdyZWVXaXRoUG9zMSA9IHBvc0VuZW15LnNpZ25BbmdsZShkaXJlY3Rpb24xKTtcclxuICAgICAgICAgICAgICAgIGRlZ3JlZVdpdGhQb3MxID0gY2MubWlzYy5yYWRpYW5zVG9EZWdyZWVzKGRlZ3JlZVdpdGhQb3MxKTtcclxuICAgICAgICAgICAgICAgIGxldCBkZWdyZWVXaXRoUG9zMiA9IHBvc0VuZW15LnNpZ25BbmdsZShkaXJlY3Rpb24yKTtcclxuICAgICAgICAgICAgICAgIGRlZ3JlZVdpdGhQb3MyID0gY2MubWlzYy5yYWRpYW5zVG9EZWdyZWVzKGRlZ3JlZVdpdGhQb3MyKTtcclxuICAgICAgICAgICAgICAgIGxldCByZWFsTmVlZCA9IDA7XHJcbiAgICAgICAgICAgICAgICBkZWdyZWVXaXRoUG9zMSA9IE1hdGguYWJzKGRlZ3JlZVdpdGhQb3MxKTtcclxuICAgICAgICAgICAgICAgIGRlZ3JlZVdpdGhQb3MyID0gTWF0aC5hYnMoZGVncmVlV2l0aFBvczIpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGRlZ3JlZVdpdGhQb3MxID4gZGVncmVlV2l0aFBvczIpIHtcclxuICAgICAgICAgICAgICAgICAgICByZWFsTmVlZCA9IGRlZ3JlZVdpdGhQb3MxO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVhbE5lZWQgPSBkZWdyZWVXaXRoUG9zMjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGxldCBkaXN0YW5jZSA9IFV0aWxpdHkuRGlzdGFuY2UoY2MudjIodGhpcy5ub2RlLngsIHRoaXMubm9kZS55KSwgY2MudjIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci54LCBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnkpKTtcclxuICAgICAgICAgICAgICAgIGxldCBtYXhEaXN0YW5jZSA9IFV0aWxpdHkuRGlzdGFuY2UoY2MudjIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5teUNoYXJhY3Rlci54LCBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnkpLCBjYy52Mihwb3MzLngsIHBvczMueSkpO1xyXG4gICAgICAgICAgICAgICAgaWYgKE1hdGguYWJzKHJlYWxOZWVkKSA8IGRlZ3JlZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChkaXN0YW5jZSA8IG1heERpc3RhbmNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChHbG9iYWwuYm9vbENoZWNrQXR0YWNrZWQgJiYgIXRoaXMuYm9vbENoZWNrRGVhdGhPbmUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuQnJva2VuQm94KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmJvb2xDaGVja0RlYXRoID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYm9vbENoZWNrRGVhdGhPbmUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9HbG9iYWwuYm9vbENoZWNrQXR0YWNrZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vR2xvYmFsLmJvb2xDaGVja0F0dGFja2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLmJvb2xDaGVja2RlbGF5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ib29sQ2hlY2tkZWxheSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBHbG9iYWwuYm9vbENoZWNrQXR0YWNraW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYm9vbENoZWNrZGVsYXkgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB9LCAwLjAwMyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBEaWVXaGVuRW5lbXlBdHRhY2soKSB7XHJcbiAgICAgICAgdGhpcy5ib29sQ2hlY2tEZWF0aCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5Ccm9rZW5Cb3goKTtcclxuICAgIH1cclxuICAgIEJyb2tlbkJveCgpIHtcclxuICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiQnJva2VuXCIpO1xyXG4gICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kQm94QnJva2VuLGZhbHNlKTtcclxuICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLkJveENvbGxpZGVyM0QpLmVuYWJsZWQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIGZvciggbGV0IGkgPSAwOyBpIDwgMjA7IGkrKylcclxuICAgICAgICAgICAgLy8ge1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5TcGF3bmVyQnJpY2sodGhpcy5ub2RlLnggLSAyLCB0aGlzLm5vZGUueSArIDIpO1xyXG4gICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgIHRoaXMuU3Bhd25lckJyaWNrKHRoaXMubm9kZS54IC0gMiwgdGhpcy5ub2RlLnkgKyAyKTtcclxuICAgICAgICAgICAgdGhpcy5TcGF3bmVyQnJpY2sodGhpcy5ub2RlLnggKyAyLCB0aGlzLm5vZGUueSArIDIpO1xyXG4gICAgICAgICAgICB0aGlzLlNwYXduZXJCcmljayh0aGlzLm5vZGUueCAtIDIsIHRoaXMubm9kZS55IC0gMik7XHJcbiAgICAgICAgICAgIHRoaXMuU3Bhd25lckJyaWNrKHRoaXMubm9kZS54ICsgMiwgdGhpcy5ub2RlLnkgLSAyKTtcclxuICAgICAgICB9LCB0aGlzLnRpbWVCcm9rZW4gLyA3KTtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnN0b3AoKTtcclxuICAgICAgICB9LCB0aGlzLnRpbWVCcm9rZW4pO1xyXG4gICAgfVxyXG4gICAgU3Bhd25lckJyaWNrKHg6IG51bWJlciwgeTogbnVtYmVyKSB7XHJcbiAgICAgICAgbGV0IEJyaWNrID0gY2MuaW5zdGFudGlhdGUodGhpcy5icmlja0hvbSk7XHJcbiAgICAgICAgQnJpY2sucGFyZW50ID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGU7XHJcbiAgICAgICAgQnJpY2sueCA9IHRoaXMubm9kZS54O1xyXG4gICAgICAgIEJyaWNrLnkgPSB0aGlzLm5vZGUueTtcclxuICAgICAgICBCcmljay56ID0gMTtcclxuICAgICAgICBsZXQgdCA9IG5ldyBjYy5Ud2VlbigpLnRvKDAuMywgeyBwb3NpdGlvbjogbmV3IGNjLlZlYzMoeCwgeSwgMC41KSB9KVxyXG4gICAgICAgICAgICAuY2FsbCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBCcmljay5nZXRDb21wb25lbnQoY2MuQm94Q29sbGlkZXIzRCkuZW5hYmxlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC50YXJnZXQoQnJpY2spLnN0YXJ0KCk7XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/AdManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'f9322uTpHhOfrvMpTn8sIdn', 'AdManager');
// Scripts/Common/AdManager.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    androidLink: {
      "default": ''
    },
    iosLink: {
      "default": ''
    },
    defaultLink: {
      "default": ''
    }
  },
  openAdUrl: function openAdUrl() {
    var clickTag = '';
    window.androidLink = this.androidLink;
    window.iosLink = this.iosLink;
    window.defaultLink = this.defaultLink;

    if (window.openAdUrl) {
      window.openAdUrl();
    } else {
      window.open();
    }
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxBZE1hbmFnZXIuanMiXSwibmFtZXMiOlsiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJhbmRyb2lkTGluayIsImlvc0xpbmsiLCJkZWZhdWx0TGluayIsIm9wZW5BZFVybCIsImNsaWNrVGFnIiwid2luZG93Iiwib3BlbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1JDLElBQUFBLFdBQVcsRUFBRTtBQUNULGlCQUFTO0FBREEsS0FETDtBQUlSQyxJQUFBQSxPQUFPLEVBQUU7QUFDTCxpQkFBUztBQURKLEtBSkQ7QUFPUkMsSUFBQUEsV0FBVyxFQUFFO0FBQ1QsaUJBQVM7QUFEQTtBQVBMLEdBSFA7QUFlTEMsRUFBQUEsU0FBUyxFQUFFLHFCQUFZO0FBQ25CLFFBQUlDLFFBQVEsR0FBRyxFQUFmO0FBQ0FDLElBQUFBLE1BQU0sQ0FBQ0wsV0FBUCxHQUFxQixLQUFLQSxXQUExQjtBQUNBSyxJQUFBQSxNQUFNLENBQUNKLE9BQVAsR0FBaUIsS0FBS0EsT0FBdEI7QUFDQUksSUFBQUEsTUFBTSxDQUFDSCxXQUFQLEdBQXFCLEtBQUtBLFdBQTFCOztBQUNBLFFBQUlHLE1BQU0sQ0FBQ0YsU0FBWCxFQUFzQjtBQUNsQkUsTUFBQUEsTUFBTSxDQUFDRixTQUFQO0FBQ0gsS0FGRCxNQUVPO0FBQ0hFLE1BQUFBLE1BQU0sQ0FBQ0MsSUFBUDtBQUNIO0FBQ0o7QUF6QkksQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG5cclxuICAgIHByb3BlcnRpZXM6IHtcclxuICAgICAgICBhbmRyb2lkTGluazoge1xyXG4gICAgICAgICAgICBkZWZhdWx0OiAnJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgaW9zTGluazoge1xyXG4gICAgICAgICAgICBkZWZhdWx0OiAnJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZGVmYXVsdExpbms6IHtcclxuICAgICAgICAgICAgZGVmYXVsdDogJydcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9wZW5BZFVybDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBjbGlja1RhZyA9ICcnO1xyXG4gICAgICAgIHdpbmRvdy5hbmRyb2lkTGluayA9IHRoaXMuYW5kcm9pZExpbms7XHJcbiAgICAgICAgd2luZG93Lmlvc0xpbmsgPSB0aGlzLmlvc0xpbms7XHJcbiAgICAgICAgd2luZG93LmRlZmF1bHRMaW5rID0gdGhpcy5kZWZhdWx0TGluaztcclxuICAgICAgICBpZiAod2luZG93Lm9wZW5BZFVybCkge1xyXG4gICAgICAgICAgICB3aW5kb3cub3BlbkFkVXJsKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgd2luZG93Lm9wZW4oKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pOyJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Brick/BrickHom.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '532c3wlv65DxqpVK0UVc+yj', 'BrickHom');
// Scripts/Brick/BrickHom.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var AIController_1 = require("../Controller/AIController");
var CharacterController_1 = require("../Controller/CharacterController");
var BrickData_1 = require("./BrickData");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BrickHom = /** @class */ (function (_super) {
    __extends(BrickHom, _super);
    function BrickHom() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.x = 0.003;
        _this.z = -0.002;
        return _this;
    }
    BrickHom.prototype.start = function () {
        var _this = this;
        var collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-enter', this.onTriggerEnter, this);
        this.scheduleOnce(function () {
            _this.node.getComponent(cc.BoxCollider3D).enabled = true;
        }, 1);
    };
    BrickHom.prototype.onTriggerEnter = function (event) {
        var _this = this;
        if (event.otherCollider.node.group == "Player") {
            var otherNode = event.otherCollider.node;
            cc.audioEngine.playEffect(Global_1.default.soundCollect, false);
            var pos1 = cc.Canvas.instance.node.convertToWorldSpaceAR((new cc.Vec3(this.node.x, this.node.y, this.node.z)));
            var pos = otherNode.children[2].convertToNodeSpaceAR(pos1);
            this.node.getComponent(BrickData_1.default).setmaterial(otherNode.getComponent(CharacterController_1.default).brickType);
            this.node.parent = otherNode.children[2];
            this.node.x = pos.x;
            this.node.y = pos.y;
            this.node.z = pos.z;
            this.node.scale = 1;
            var t = new cc.Tween();
            t.to(0.2, { scale: 0, position: new cc.Vec3(this.x, otherNode.getComponent(CharacterController_1.default).lastPositionStickY + 0.004, this.z) })
                .call(function () {
                GamePlayInstance_1.eventDispatcher.emit(KeyEvent_1.default.increaseStickMyCharacter);
                _this.node.destroy();
            }).target(this.node).start();
        }
        if (event.otherCollider.node.group == "Enemy") {
            var otherNode_1 = event.otherCollider.node;
            cc.audioEngine.playEffect(Global_1.default.soundCollect, false);
            var pos1 = cc.Canvas.instance.node.convertToWorldSpaceAR((new cc.Vec3(this.node.x, this.node.y, this.node.z)));
            var pos = otherNode_1.children[2].convertToNodeSpaceAR(pos1);
            this.node.getComponent(BrickData_1.default).setmaterial(otherNode_1.getComponent(AIController_1.default).brickType);
            this.node.parent = otherNode_1.children[2];
            this.node.x = pos.x;
            this.node.y = pos.y;
            this.node.z = pos.z;
            this.node.scale = 1;
            var t = new cc.Tween();
            t.to(0.2, { scale: 0, position: new cc.Vec3(this.x, otherNode_1.getComponent(AIController_1.default).lastPositionStickY + 0.004, this.z) })
                .call(function () {
                GamePlayInstance_1.eventDispatcher.emit(otherNode_1.getComponent(AIController_1.default).stringIncrease);
                _this.node.destroy();
            }).target(this.node).start();
        }
    };
    BrickHom = __decorate([
        ccclass
    ], BrickHom);
    return BrickHom;
}(cc.Component));
exports.default = BrickHom;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQnJpY2tcXEJyaWNrSG9tLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLCtEQUE2RDtBQUM3RCwyQ0FBc0M7QUFDdEMsK0NBQTBDO0FBQzFDLDJEQUFzRDtBQUN0RCx5RUFBb0U7QUFDcEUseUNBQW9DO0FBRTlCLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFHNUM7SUFBc0MsNEJBQVk7SUFEbEQ7UUFBQSxxRUFpREM7UUEvQ0csT0FBQyxHQUFXLEtBQUssQ0FBQztRQUNsQixPQUFDLEdBQVcsQ0FBQyxLQUFLLENBQUM7O0lBOEN2QixDQUFDO0lBN0NHLHdCQUFLLEdBQUw7UUFBQSxpQkFNQztRQUxHLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2hELFFBQVEsQ0FBQyxFQUFFLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNkLEtBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQzVELENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQztJQUNULENBQUM7SUFDRCxpQ0FBYyxHQUFkLFVBQWUsS0FBSztRQUFwQixpQkFxQ0M7UUFwQ0csSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksUUFBUSxFQUFFO1lBQzVDLElBQUksU0FBUyxHQUFHLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO1lBQ3pDLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3RELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvRyxJQUFJLEdBQUcsR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNELElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLG1CQUFTLENBQUMsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3JHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxHQUFHLElBQUksRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLGtCQUFrQixHQUFHLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDakksSUFBSSxDQUFDO2dCQUNGLGtDQUFlLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsd0JBQXdCLENBQUMsQ0FBQztnQkFDeEQsS0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ3BDO1FBQ0QsSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksT0FBTyxFQUFFO1lBQzNDLElBQUksV0FBUyxHQUFHLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO1lBQ3pDLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3RELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvRyxJQUFJLEdBQUcsR0FBRyxXQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNELElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLG1CQUFTLENBQUMsQ0FBQyxXQUFXLENBQUMsV0FBUyxDQUFDLFlBQVksQ0FBQyxzQkFBWSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDOUYsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsV0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDcEIsSUFBSSxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDdkIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxXQUFTLENBQUMsWUFBWSxDQUFDLHNCQUFZLENBQUMsQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7aUJBQzFILElBQUksQ0FBQztnQkFDRixrQ0FBZSxDQUFDLElBQUksQ0FBQyxXQUFTLENBQUMsWUFBWSxDQUFDLHNCQUFZLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDMUUsS0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ3BDO0lBQ0wsQ0FBQztJQS9DZ0IsUUFBUTtRQUQ1QixPQUFPO09BQ2EsUUFBUSxDQWdENUI7SUFBRCxlQUFDO0NBaERELEFBZ0RDLENBaERxQyxFQUFFLENBQUMsU0FBUyxHQWdEakQ7a0JBaERvQixRQUFRIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQnJpY2tUeXBlIH0gZnJvbSBcIi4uL0NvbW1vbi9FbnVtRGVmaW5lXCI7XHJcbmltcG9ydCB7IGV2ZW50RGlzcGF0Y2hlciB9IGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi9Db21tb24vR2xvYmFsXCI7XHJcbmltcG9ydCBLZXlFdmVudCBmcm9tIFwiLi4vQ29tbW9uL0tleUV2ZW50XCI7XHJcbmltcG9ydCBBSUNvbnRyb2xsZXIgZnJvbSBcIi4uL0NvbnRyb2xsZXIvQUlDb250cm9sbGVyXCI7XHJcbmltcG9ydCBDaGFyYWN0ZXJDb250cm9sbGVyIGZyb20gXCIuLi9Db250cm9sbGVyL0NoYXJhY3RlckNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IEJyaWNrRGF0YSBmcm9tIFwiLi9Ccmlja0RhdGFcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBCcmlja0hvbSBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcbiAgICB4OiBudW1iZXIgPSAwLjAwMztcclxuICAgIHo6IG51bWJlciA9IC0wLjAwMjtcclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIGxldCBjb2xsaWRlciA9IHRoaXMuZ2V0Q29tcG9uZW50KGNjLkNvbGxpZGVyM0QpO1xyXG4gICAgICAgIGNvbGxpZGVyLm9uKCd0cmlnZ2VyLWVudGVyJywgdGhpcy5vblRyaWdnZXJFbnRlciwgdGhpcyk7XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLkJveENvbGxpZGVyM0QpLmVuYWJsZWQgPSB0cnVlO1xyXG4gICAgICAgIH0sMSk7XHJcbiAgICB9XHJcbiAgICBvblRyaWdnZXJFbnRlcihldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudC5vdGhlckNvbGxpZGVyLm5vZGUuZ3JvdXAgPT0gXCJQbGF5ZXJcIikge1xyXG4gICAgICAgICAgICBsZXQgb3RoZXJOb2RlID0gZXZlbnQub3RoZXJDb2xsaWRlci5ub2RlO1xyXG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KEdsb2JhbC5zb3VuZENvbGxlY3QsIGZhbHNlKTtcclxuICAgICAgICAgICAgbGV0IHBvczEgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZS5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoKG5ldyBjYy5WZWMzKHRoaXMubm9kZS54LCB0aGlzLm5vZGUueSwgdGhpcy5ub2RlLnopKSk7XHJcbiAgICAgICAgICAgIGxldCBwb3MgPSBvdGhlck5vZGUuY2hpbGRyZW5bMl0uY29udmVydFRvTm9kZVNwYWNlQVIocG9zMSk7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoQnJpY2tEYXRhKS5zZXRtYXRlcmlhbChvdGhlck5vZGUuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLmJyaWNrVHlwZSk7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5wYXJlbnQgPSBvdGhlck5vZGUuY2hpbGRyZW5bMl07XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS54ID0gcG9zLng7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS55ID0gcG9zLnk7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS56ID0gcG9zLno7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5zY2FsZSA9IDE7XHJcbiAgICAgICAgICAgIHZhciB0ID0gbmV3IGNjLlR3ZWVuKCk7XHJcbiAgICAgICAgICAgIHQudG8oMC4yLCB7IHNjYWxlOiAwLCBwb3NpdGlvbjogbmV3IGNjLlZlYzModGhpcy54LCBvdGhlck5vZGUuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLmxhc3RQb3NpdGlvblN0aWNrWSArIDAuMDA0LCB0aGlzLnopIH0pXHJcbiAgICAgICAgICAgICAgICAuY2FsbCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnREaXNwYXRjaGVyLmVtaXQoS2V5RXZlbnQuaW5jcmVhc2VTdGlja015Q2hhcmFjdGVyKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vZGUuZGVzdHJveSgpO1xyXG4gICAgICAgICAgICAgICAgfSkudGFyZ2V0KHRoaXMubm9kZSkuc3RhcnQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGV2ZW50Lm90aGVyQ29sbGlkZXIubm9kZS5ncm91cCA9PSBcIkVuZW15XCIpIHtcclxuICAgICAgICAgICAgbGV0IG90aGVyTm9kZSA9IGV2ZW50Lm90aGVyQ29sbGlkZXIubm9kZTtcclxuICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdChHbG9iYWwuc291bmRDb2xsZWN0LCBmYWxzZSk7XHJcbiAgICAgICAgICAgIGxldCBwb3MxID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGUuY29udmVydFRvV29ybGRTcGFjZUFSKChuZXcgY2MuVmVjMyh0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnksIHRoaXMubm9kZS56KSkpO1xyXG4gICAgICAgICAgICBsZXQgcG9zID0gb3RoZXJOb2RlLmNoaWxkcmVuWzJdLmNvbnZlcnRUb05vZGVTcGFjZUFSKHBvczEpO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KEJyaWNrRGF0YSkuc2V0bWF0ZXJpYWwob3RoZXJOb2RlLmdldENvbXBvbmVudChBSUNvbnRyb2xsZXIpLmJyaWNrVHlwZSk7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5wYXJlbnQgPSBvdGhlck5vZGUuY2hpbGRyZW5bMl07XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS54ID0gcG9zLng7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS55ID0gcG9zLnk7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS56ID0gcG9zLno7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5zY2FsZSA9IDE7XHJcbiAgICAgICAgICAgIHZhciB0ID0gbmV3IGNjLlR3ZWVuKCk7XHJcbiAgICAgICAgICAgIHQudG8oMC4yLCB7IHNjYWxlOiAwLCBwb3NpdGlvbjogbmV3IGNjLlZlYzModGhpcy54LCBvdGhlck5vZGUuZ2V0Q29tcG9uZW50KEFJQ29udHJvbGxlcikubGFzdFBvc2l0aW9uU3RpY2tZICsgMC4wMDQsIHRoaXMueikgfSlcclxuICAgICAgICAgICAgICAgIC5jYWxsKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBldmVudERpc3BhdGNoZXIuZW1pdChvdGhlck5vZGUuZ2V0Q29tcG9uZW50KEFJQ29udHJvbGxlcikuc3RyaW5nSW5jcmVhc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZS5kZXN0cm95KCk7XHJcbiAgICAgICAgICAgICAgICB9KS50YXJnZXQodGhpcy5ub2RlKS5zdGFydCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Brick/BrickStairs.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6e770L2b9VC3KZmrEF0oM/r', 'BrickStairs');
// Scripts/Brick/BrickStairs.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var CharacterController_1 = require("../Controller/CharacterController");
var BrickData_1 = require("./BrickData");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BrickStairs = /** @class */ (function (_super) {
    __extends(BrickStairs, _super);
    function BrickStairs() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.boolAppeared = false;
        return _this;
    }
    BrickStairs.prototype.start = function () {
        var collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-enter', this.onTriggerEnter, this);
    };
    BrickStairs.prototype.onTriggerEnter = function (event) {
        if (event.otherCollider.node.group == "Player") {
            if (!this.boolAppeared) {
                cc.audioEngine.playEffect(Global_1.default.soundUpStairs, false);
                GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.txtClimb.runAction(cc.fadeOut(0.3));
                this.boolAppeared = true;
                this.node.opacity = 255;
                this.node.getComponent(BrickData_1.default).setmaterial(EnumDefine_1.BrickType.BrickCharacter);
                event.otherCollider.node.getComponent(CharacterController_1.default).DecreaseStick();
            }
        }
    };
    BrickStairs = __decorate([
        ccclass
    ], BrickStairs);
    return BrickStairs;
}(cc.Component));
exports.default = BrickStairs;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQnJpY2tcXEJyaWNrU3RhaXJzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG1EQUFpRDtBQUVqRCwrREFBMEQ7QUFDMUQsMkNBQXNDO0FBQ3RDLHlFQUFvRTtBQUNwRSx5Q0FBb0M7QUFFOUIsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUc1QztJQUF5QywrQkFBWTtJQURyRDtRQUFBLHFFQW1CQztRQWpCRyxrQkFBWSxHQUFZLEtBQUssQ0FBQzs7SUFpQmxDLENBQUM7SUFoQkcsMkJBQUssR0FBTDtRQUNJLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2hELFFBQVEsQ0FBQyxFQUFFLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDNUQsQ0FBQztJQUNELG9DQUFjLEdBQWQsVUFBZSxLQUFLO1FBQ2hCLElBQUksS0FBSyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLFFBQVEsRUFBRTtZQUM1QyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDcEIsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxhQUFhLEVBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3RELDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDekYsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztnQkFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsbUJBQVMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxzQkFBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUN4RSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxhQUFhLEVBQUUsQ0FBQzthQUM5RTtTQUNKO0lBQ0wsQ0FBQztJQWpCZ0IsV0FBVztRQUQvQixPQUFPO09BQ2EsV0FBVyxDQWtCL0I7SUFBRCxrQkFBQztDQWxCRCxBQWtCQyxDQWxCd0MsRUFBRSxDQUFDLFNBQVMsR0FrQnBEO2tCQWxCb0IsV0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJyaWNrVHlwZSB9IGZyb20gXCIuLi9Db21tb24vRW51bURlZmluZVwiO1xyXG5pbXBvcnQgR2FtZVBsYXlDb250cm9sbGVyIGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlDb250cm9sbGVyXCI7XHJcbmltcG9ydCBHYW1lUGxheUluc3RhbmNlIGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi9Db21tb24vR2xvYmFsXCI7XHJcbmltcG9ydCBDaGFyYWN0ZXJDb250cm9sbGVyIGZyb20gXCIuLi9Db250cm9sbGVyL0NoYXJhY3RlckNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IEJyaWNrRGF0YSBmcm9tIFwiLi9Ccmlja0RhdGFcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBCcmlja1N0YWlycyBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcbiAgICBib29sQXBwZWFyZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIGxldCBjb2xsaWRlciA9IHRoaXMuZ2V0Q29tcG9uZW50KGNjLkNvbGxpZGVyM0QpO1xyXG4gICAgICAgIGNvbGxpZGVyLm9uKCd0cmlnZ2VyLWVudGVyJywgdGhpcy5vblRyaWdnZXJFbnRlciwgdGhpcyk7XHJcbiAgICB9XHJcbiAgICBvblRyaWdnZXJFbnRlcihldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudC5vdGhlckNvbGxpZGVyLm5vZGUuZ3JvdXAgPT0gXCJQbGF5ZXJcIikge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuYm9vbEFwcGVhcmVkKSB7XHJcbiAgICAgICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KEdsb2JhbC5zb3VuZFVwU3RhaXJzLGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkudHh0Q2xpbWIucnVuQWN0aW9uKGNjLmZhZGVPdXQoMC4zKSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmJvb2xBcHBlYXJlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUub3BhY2l0eSA9IDI1NTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoQnJpY2tEYXRhKS5zZXRtYXRlcmlhbChCcmlja1R5cGUuQnJpY2tDaGFyYWN0ZXIpO1xyXG4gICAgICAgICAgICAgICAgZXZlbnQub3RoZXJDb2xsaWRlci5ub2RlLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5EZWNyZWFzZVN0aWNrKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/Utility.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '5445fCIuP1HJLq2hkCm2zpL', 'Utility');
// Scripts/Common/Utility.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Singleton_1 = require("./Singleton");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Utility = /** @class */ (function (_super) {
    __extends(Utility, _super);
    function Utility() {
        var _this = _super.call(this) || this;
        Utility_1._instance = _this;
        return _this;
    }
    Utility_1 = Utility;
    Utility.RandomRangeFloat = function (lower, upper) {
        return Math.random() * (upper - lower) + lower;
        //return Math.floor(Math.random() * (lower - lower)) + lower;
    };
    Utility.RandomRangeInteger = function (lower, upper) {
        return Math.round(Math.random() * (upper - lower) + lower);
    };
    Utility.Distance = function (vec1, vec2) {
        var Distance = Math.sqrt(Math.pow(vec1.x - vec2.x, 2) +
            Math.pow(vec1.y - vec2.y, 2));
        return Distance;
    };
    Utility.BetweenDegree = function (comVec, dirVec) {
        var angleDegree = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDegree - 90;
    };
    Utility.CaculatorDegree = function (Target) {
        var r = Math.atan2(Target.y, Target.x);
        var degree = r * 180 / (Math.PI);
        degree = 360 - degree + 90;
        return degree;
    };
    var Utility_1;
    Utility = Utility_1 = __decorate([
        ccclass
    ], Utility);
    return Utility;
}(Singleton_1.default));
exports.default = Utility;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxVdGlsaXR5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHlDQUFvQztBQUU5QixJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRzVDO0lBQXFDLDJCQUFrQjtJQUNuRDtRQUFBLFlBQ0ksaUJBQU8sU0FFVjtRQURHLFNBQU8sQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDOztJQUM3QixDQUFDO2dCQUpnQixPQUFPO0lBS2pCLHdCQUFnQixHQUF2QixVQUF3QixLQUFhLEVBQUUsS0FBYTtRQUNoRCxPQUFPLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUM7UUFDL0MsNkRBQTZEO0lBQ2pFLENBQUM7SUFDTSwwQkFBa0IsR0FBekIsVUFBMEIsS0FBYSxFQUFFLEtBQWE7UUFDbEQsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBQ00sZ0JBQVEsR0FBZixVQUFnQixJQUFhLEVBQUUsSUFBYTtRQUN4QyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xDLE9BQU8sUUFBUSxDQUFDO0lBQ3BCLENBQUM7SUFDTSxxQkFBYSxHQUFwQixVQUFxQixNQUFlLEVBQUUsTUFBZTtRQUNqRCxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUN2RixPQUFPLFdBQVcsR0FBRyxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUNNLHVCQUFlLEdBQXRCLFVBQXVCLE1BQWU7UUFDbEMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2QyxJQUFJLE1BQU0sR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2pDLE1BQU0sR0FBRyxHQUFHLEdBQUcsTUFBTSxHQUFHLEVBQUUsQ0FBQztRQUMzQixPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDOztJQTFCZ0IsT0FBTztRQUQzQixPQUFPO09BQ2EsT0FBTyxDQTJCM0I7SUFBRCxjQUFDO0NBM0JELEFBMkJDLENBM0JvQyxtQkFBUyxHQTJCN0M7a0JBM0JvQixPQUFPIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFNpbmdsZXRvbiBmcm9tIFwiLi9TaW5nbGV0b25cIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVdGlsaXR5IGV4dGVuZHMgU2luZ2xldG9uPFV0aWxpdHk+IHtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgVXRpbGl0eS5faW5zdGFuY2UgPSB0aGlzO1xyXG4gICAgfVxyXG4gICAgc3RhdGljIFJhbmRvbVJhbmdlRmxvYXQobG93ZXI6IG51bWJlciwgdXBwZXI6IG51bWJlcikge1xyXG4gICAgICAgIHJldHVybiBNYXRoLnJhbmRvbSgpICogKHVwcGVyIC0gbG93ZXIpICsgbG93ZXI7XHJcbiAgICAgICAgLy9yZXR1cm4gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogKGxvd2VyIC0gbG93ZXIpKSArIGxvd2VyO1xyXG4gICAgfVxyXG4gICAgc3RhdGljIFJhbmRvbVJhbmdlSW50ZWdlcihsb3dlcjogbnVtYmVyLCB1cHBlcjogbnVtYmVyKSB7XHJcbiAgICAgICAgcmV0dXJuIE1hdGgucm91bmQoTWF0aC5yYW5kb20oKSAqICh1cHBlciAtIGxvd2VyKSArIGxvd2VyKTtcclxuICAgIH1cclxuICAgIHN0YXRpYyBEaXN0YW5jZSh2ZWMxOiBjYy5WZWMyLCB2ZWMyOiBjYy5WZWMyKSB7XHJcbiAgICAgICAgbGV0IERpc3RhbmNlID0gTWF0aC5zcXJ0KE1hdGgucG93KHZlYzEueCAtIHZlYzIueCwgMikgK1xyXG4gICAgICAgICAgICBNYXRoLnBvdyh2ZWMxLnkgLSB2ZWMyLnksIDIpKTtcclxuICAgICAgICByZXR1cm4gRGlzdGFuY2U7XHJcbiAgICB9XHJcbiAgICBzdGF0aWMgQmV0d2VlbkRlZ3JlZShjb21WZWM6IGNjLlZlYzIsIGRpclZlYzogY2MuVmVjMikge1xyXG4gICAgICAgIGxldCBhbmdsZURlZ3JlZSA9IE1hdGguYXRhbjIoZGlyVmVjLnkgLSBjb21WZWMueSwgZGlyVmVjLnggLSBjb21WZWMueCkgKiAxODAgLyBNYXRoLlBJO1xyXG4gICAgICAgIHJldHVybiBhbmdsZURlZ3JlZSAtIDkwOyBcclxuICAgIH1cclxuICAgIHN0YXRpYyBDYWN1bGF0b3JEZWdyZWUoVGFyZ2V0OiBjYy5WZWMyKSB7XHJcbiAgICAgICAgdmFyIHIgPSBNYXRoLmF0YW4yKFRhcmdldC55LCBUYXJnZXQueCk7XHJcbiAgICAgICAgdmFyIGRlZ3JlZSA9IHIgKiAxODAgLyAoTWF0aC5QSSk7XHJcbiAgICAgICAgZGVncmVlID0gMzYwIC0gZGVncmVlICsgOTA7XHJcbiAgICAgICAgcmV0dXJuIGRlZ3JlZTtcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Controller/JoystickFollow.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'f6068GFEGlHn7TrO7hQLo8v', 'JoystickFollow');
// Scripts/Controller/JoystickFollow.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var CharacterController_1 = require("./CharacterController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var JoysticFollow = /** @class */ (function (_super) {
    __extends(JoysticFollow, _super);
    function JoysticFollow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.joyRing = null;
        _this.joyDot = null;
        _this.stickPos = null;
        _this.touchLocation = null;
        _this.radius = 0;
        return _this;
    }
    JoysticFollow.prototype.onLoad = function () {
        this.radius = this.joyRing.width / 2;
    };
    JoysticFollow.prototype.start = function () {
        this.gamePlayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        this.node.on(cc.Node.EventType.TOUCH_START, this.TouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.TouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.TouchCancel, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.TouchCancel, this);
    };
    JoysticFollow.prototype.TouchStart = function (event) {
        if (!Global_1.default.boolEndGame && Global_1.default.boolStartPlay && !Global_1.default.boolCharacterFall) {
            var mousePosition = event.getLocation();
            var localMousePosition = this.node.convertToNodeSpaceAR(mousePosition);
            this.node.opacity = 255;
            this.stickPos = localMousePosition;
            this.touchLocation = event.getLocation();
            this.joyRing.setPosition(localMousePosition);
            this.joyDot.setPosition(localMousePosition);
            this.gamePlayInstance.gameplay.Guide.active = false;
            this.gamePlayInstance.gameplay.txtCollectBlock.active = false;
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = true;
        }
    };
    JoysticFollow.prototype.TouchMove = function (event) {
        if (!Global_1.default.boolEndGame && Global_1.default.boolStartPlay && !Global_1.default.boolCharacterFall) {
            this.node.opacity = 255;
            Global_1.default.boolEnableTouch = true;
            if (!Global_1.default.boolFirstTouchJoyStick) {
                Global_1.default.boolFirstTouchJoyStick = true;
                this.gamePlayInstance.gameplay.myCharacter.getComponent(cc.SkeletonAnimation).play("Run");
            }
            if (this.touchLocation === event.getLocation()) {
                return false;
            }
            this.gamePlayInstance.gameplay.Guide.active = false;
            var touchPos = this.joyRing.convertToNodeSpaceAR(event.getLocation());
            var distance = touchPos.mag();
            var posX = this.stickPos.x + touchPos.x;
            var posY = this.stickPos.y + touchPos.y;
            var p = cc.v2(posX, posY).sub(this.joyRing.getPosition()).normalize();
            Global_1.default.touchPos = p;
            if (this.radius > distance) {
                this.joyDot.setPosition(cc.v2(posX, posY));
            }
            else {
                var x = this.stickPos.x + p.x * this.radius;
                var y = this.stickPos.y + p.y * this.radius;
                this.joyDot.setPosition(cc.v2(x, y));
            }
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = true;
            this.gamePlayInstance.gameplay.Guide.active = false;
            this.gamePlayInstance.gameplay.txtCollectBlock.active = false;
        }
    };
    JoysticFollow.prototype.TouchCancel = function () {
        if (!Global_1.default.boolEndGame && Global_1.default.boolStartPlay && !Global_1.default.boolCharacterFall) {
            this.joyDot.setPosition(this.joyRing.getPosition());
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).Attacking();
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).StopMove();
            Global_1.default.boolEnableTouch = false;
            this.node.opacity = 0;
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).boolCheckTypeRotate = true;
        }
    };
    __decorate([
        property(cc.Node)
    ], JoysticFollow.prototype, "joyRing", void 0);
    __decorate([
        property(cc.Node)
    ], JoysticFollow.prototype, "joyDot", void 0);
    JoysticFollow = __decorate([
        ccclass
    ], JoysticFollow);
    return JoysticFollow;
}(cc.Component));
exports.default = JoysticFollow;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29udHJvbGxlclxcSm95c3RpY2tGb2xsb3cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsK0RBQTBEO0FBQzFELDJDQUFzQztBQUN0Qyw2REFBd0Q7QUFDbEQsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUU1QztJQUEyQyxpQ0FBWTtJQUR2RDtRQUFBLHFFQTBFQztRQXZFRyxhQUFPLEdBQVksSUFBSSxDQUFDO1FBRXhCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFDdkIsY0FBUSxHQUFZLElBQUksQ0FBQztRQUN6QixtQkFBYSxHQUFZLElBQUksQ0FBQztRQUM5QixZQUFNLEdBQVcsQ0FBQyxDQUFDOztJQWtFdkIsQ0FBQztJQWhFRyw4QkFBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUNELDZCQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDekUsQ0FBQztJQUNELGtDQUFVLEdBQVYsVUFBVyxLQUFLO1FBQ1osSUFBSSxDQUFDLGdCQUFNLENBQUMsV0FBVyxJQUFJLGdCQUFNLENBQUMsYUFBYSxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxpQkFBaUIsRUFBRTtZQUMxRSxJQUFJLGFBQWEsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDeEMsSUFBSSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3ZFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztZQUN4QixJQUFJLENBQUMsUUFBUSxHQUFHLGtCQUFrQixDQUFDO1lBQ25DLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3BELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDOUQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7U0FDN0c7SUFDTCxDQUFDO0lBQ0QsaUNBQVMsR0FBVCxVQUFVLEtBQUs7UUFDWCxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLElBQUksZ0JBQU0sQ0FBQyxhQUFhLElBQUssQ0FBQyxnQkFBTSxDQUFDLGlCQUFpQixFQUFFO1lBQzNFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztZQUN4QixnQkFBTSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7WUFDOUIsSUFBSSxDQUFDLGdCQUFNLENBQUMsc0JBQXNCLEVBQUU7Z0JBQ2hDLGdCQUFNLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDO2dCQUNyQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzdGO1lBQ0QsSUFBSSxJQUFJLENBQUMsYUFBYSxLQUFLLEtBQUssQ0FBQyxXQUFXLEVBQUUsRUFBRTtnQkFDNUMsT0FBTyxLQUFLLENBQUM7YUFDaEI7WUFDRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3BELElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7WUFDdEUsSUFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQzlCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDeEMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN4QyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ3RFLGdCQUFNLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQztZQUNwQixJQUFJLElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxFQUFFO2dCQUN4QixJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO2FBQzlDO2lCQUFNO2dCQUNILElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztnQkFDNUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO2dCQUM1QyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3hDO1lBQ0QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDMUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNwRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1NBQ2pFO0lBQ0wsQ0FBQztJQUNELG1DQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLElBQUksZ0JBQU0sQ0FBQyxhQUFhLElBQUssQ0FBQyxnQkFBTSxDQUFDLGlCQUFpQixFQUFFO1lBQzNFLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUNwRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUN6RixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4RixnQkFBTSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7WUFDL0IsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztTQUMzRztJQUNMLENBQUM7SUF0RUQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDTTtJQUV4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2lEQUNLO0lBSk4sYUFBYTtRQURqQyxPQUFPO09BQ2EsYUFBYSxDQXlFakM7SUFBRCxvQkFBQztDQXpFRCxBQXlFQyxDQXpFMEMsRUFBRSxDQUFDLFNBQVMsR0F5RXREO2tCQXpFb0IsYUFBYSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBHYW1lUGxheUluc3RhbmNlIGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi9Db21tb24vR2xvYmFsXCI7XHJcbmltcG9ydCBDaGFyYWN0ZXJDb250cm9sbGVyIGZyb20gXCIuL0NoYXJhY3RlckNvbnRyb2xsZXJcIjtcclxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSm95c3RpY0ZvbGxvdyBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGpveVJpbmc6IGNjLk5vZGUgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBqb3lEb3Q6IGNjLk5vZGUgPSBudWxsO1xyXG4gICAgc3RpY2tQb3M6IGNjLlZlYzIgPSBudWxsO1xyXG4gICAgdG91Y2hMb2NhdGlvbjogY2MuVmVjMiA9IG51bGw7XHJcbiAgICByYWRpdXM6IG51bWJlciA9IDA7XHJcbiAgICBnYW1lUGxheUluc3RhbmNlOiBHYW1lUGxheUluc3RhbmNlO1xyXG4gICAgb25Mb2FkKCkge1xyXG4gICAgICAgIHRoaXMucmFkaXVzID0gdGhpcy5qb3lSaW5nLndpZHRoIC8gMjtcclxuICAgIH1cclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZSA9IEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSk7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX1NUQVJULCB0aGlzLlRvdWNoU3RhcnQsIHRoaXMpO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbihjYy5Ob2RlLkV2ZW50VHlwZS5UT1VDSF9NT1ZFLCB0aGlzLlRvdWNoTW92ZSwgdGhpcyk7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX0VORCwgdGhpcy5Ub3VjaENhbmNlbCwgdGhpcyk7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX0NBTkNFTCwgdGhpcy5Ub3VjaENhbmNlbCwgdGhpcyk7XHJcbiAgICB9XHJcbiAgICBUb3VjaFN0YXJ0KGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKCFHbG9iYWwuYm9vbEVuZEdhbWUgJiYgR2xvYmFsLmJvb2xTdGFydFBsYXkgJiYgIUdsb2JhbC5ib29sQ2hhcmFjdGVyRmFsbCkge1xyXG4gICAgICAgICAgICB2YXIgbW91c2VQb3NpdGlvbiA9IGV2ZW50LmdldExvY2F0aW9uKCk7XHJcbiAgICAgICAgICAgIGxldCBsb2NhbE1vdXNlUG9zaXRpb24gPSB0aGlzLm5vZGUuY29udmVydFRvTm9kZVNwYWNlQVIobW91c2VQb3NpdGlvbik7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gMjU1O1xyXG4gICAgICAgICAgICB0aGlzLnN0aWNrUG9zID0gbG9jYWxNb3VzZVBvc2l0aW9uO1xyXG4gICAgICAgICAgICB0aGlzLnRvdWNoTG9jYXRpb24gPSBldmVudC5nZXRMb2NhdGlvbigpO1xyXG4gICAgICAgICAgICB0aGlzLmpveVJpbmcuc2V0UG9zaXRpb24obG9jYWxNb3VzZVBvc2l0aW9uKTtcclxuICAgICAgICAgICAgdGhpcy5qb3lEb3Quc2V0UG9zaXRpb24obG9jYWxNb3VzZVBvc2l0aW9uKTtcclxuICAgICAgICAgICAgdGhpcy5nYW1lUGxheUluc3RhbmNlLmdhbWVwbGF5Lkd1aWRlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmdhbWVQbGF5SW5zdGFuY2UuZ2FtZXBsYXkudHh0Q29sbGVjdEJsb2NrLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmdhbWVQbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkFycm93RGlyZWN0aW9uLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgVG91Y2hNb3ZlKGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKCFHbG9iYWwuYm9vbEVuZEdhbWUgJiYgR2xvYmFsLmJvb2xTdGFydFBsYXkgICYmICFHbG9iYWwuYm9vbENoYXJhY3RlckZhbGwpIHtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLm9wYWNpdHkgPSAyNTU7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sRW5hYmxlVG91Y2ggPSB0cnVlO1xyXG4gICAgICAgICAgICBpZiAoIUdsb2JhbC5ib29sRmlyc3RUb3VjaEpveVN0aWNrKSB7XHJcbiAgICAgICAgICAgICAgICBHbG9iYWwuYm9vbEZpcnN0VG91Y2hKb3lTdGljayA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdhbWVQbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiUnVuXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRvdWNoTG9jYXRpb24gPT09IGV2ZW50LmdldExvY2F0aW9uKCkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmdhbWVQbGF5SW5zdGFuY2UuZ2FtZXBsYXkuR3VpZGUuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGxldCB0b3VjaFBvcyA9IHRoaXMuam95UmluZy5jb252ZXJ0VG9Ob2RlU3BhY2VBUihldmVudC5nZXRMb2NhdGlvbigpKTtcclxuICAgICAgICAgICAgbGV0IGRpc3RhbmNlID0gdG91Y2hQb3MubWFnKCk7XHJcbiAgICAgICAgICAgIGxldCBwb3NYID0gdGhpcy5zdGlja1Bvcy54ICsgdG91Y2hQb3MueDtcclxuICAgICAgICAgICAgbGV0IHBvc1kgPSB0aGlzLnN0aWNrUG9zLnkgKyB0b3VjaFBvcy55O1xyXG4gICAgICAgICAgICBsZXQgcCA9IGNjLnYyKHBvc1gsIHBvc1kpLnN1Yih0aGlzLmpveVJpbmcuZ2V0UG9zaXRpb24oKSkubm9ybWFsaXplKCk7XHJcbiAgICAgICAgICAgIEdsb2JhbC50b3VjaFBvcyA9IHA7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnJhZGl1cyA+IGRpc3RhbmNlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmpveURvdC5zZXRQb3NpdGlvbihjYy52Mihwb3NYLCBwb3NZKSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgeCA9IHRoaXMuc3RpY2tQb3MueCArIHAueCAqIHRoaXMucmFkaXVzO1xyXG4gICAgICAgICAgICAgICAgbGV0IHkgPSB0aGlzLnN0aWNrUG9zLnkgKyBwLnkgKiB0aGlzLnJhZGl1cztcclxuICAgICAgICAgICAgICAgIHRoaXMuam95RG90LnNldFBvc2l0aW9uKGNjLnYyKHgsIHkpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmdhbWVQbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkFycm93RGlyZWN0aW9uLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZS5nYW1lcGxheS5HdWlkZS5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5nYW1lUGxheUluc3RhbmNlLmdhbWVwbGF5LnR4dENvbGxlY3RCbG9jay5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBUb3VjaENhbmNlbCgpIHtcclxuICAgICAgICBpZiAoIUdsb2JhbC5ib29sRW5kR2FtZSAmJiBHbG9iYWwuYm9vbFN0YXJ0UGxheSAgJiYgIUdsb2JhbC5ib29sQ2hhcmFjdGVyRmFsbCkge1xyXG4gICAgICAgICAgICB0aGlzLmpveURvdC5zZXRQb3NpdGlvbih0aGlzLmpveVJpbmcuZ2V0UG9zaXRpb24oKSk7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuQXR0YWNraW5nKCk7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuU3RvcE1vdmUoKTtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xFbmFibGVUb3VjaCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUub3BhY2l0eSA9IDA7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuYm9vbENoZWNrVHlwZVJvdGF0ZSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Controller/ColliderAttack.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ede90aP+upFTbO0wWH/S0jG', 'ColliderAttack');
// Scripts/Controller/ColliderAttack.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Global_1 = require("../Common/Global");
var Utility_1 = require("../Common/Utility");
var AIController_1 = require("./AIController");
var BoxController_1 = require("./BoxController");
var CharacterController_1 = require("./CharacterController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ColliderAttack = /** @class */ (function (_super) {
    __extends(ColliderAttack, _super);
    function ColliderAttack() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.myAI = null;
        return _this;
    }
    ColliderAttack.prototype.start = function () {
        var collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-enter', this.onTriggerEnter, this);
    };
    ColliderAttack.prototype.onTriggerEnter = function (event) {
        var _this = this;
        if (event.otherCollider.node.group == "Model2") {
            if (event.otherCollider.node.name == "Box" && !this.myAI.getComponent(AIController_1.default).boolCheckDeath) {
                this.myAI.getComponent(AIController_1.default).unscheduleAllCallbacks();
                this.myAI.stopAllActions();
                this.myAI.getComponent(cc.SkeletonAnimation).play("Attack");
                cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
                event.otherCollider.node.getComponent(BoxController_1.default).DieWhenEnemyAttack();
                this.scheduleOnce(function () {
                    _this.myAI.getComponent(cc.SkeletonAnimation).play("Idle");
                }, 0.708);
                this.scheduleOnce(function () {
                    _this.myAI.getComponent(AIController_1.default).EnemyRunIdle();
                }, 1.3);
            }
        }
        else if (event.otherCollider.node.group == "Player" && !this.myAI.getComponent(AIController_1.default).boolCheckDeath) {
            var Ran = Utility_1.default.RandomRangeInteger(1, 5);
            if (Ran == 2) {
                this.myAI.getComponent(AIController_1.default).unscheduleAllCallbacks();
                this.myAI.stopAllActions();
                this.myAI.getComponent(cc.SkeletonAnimation).play("Attack");
                event.otherCollider.node.getComponent(CharacterController_1.default).CharacterFall();
                cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
                this.scheduleOnce(function () {
                    _this.myAI.getComponent(cc.SkeletonAnimation).play("Idle");
                }, 0.708);
                this.scheduleOnce(function () {
                    _this.myAI.getComponent(AIController_1.default).EnemyRunIdle();
                }, 0.85);
            }
        }
    };
    __decorate([
        property(cc.Node)
    ], ColliderAttack.prototype, "myAI", void 0);
    ColliderAttack = __decorate([
        ccclass
    ], ColliderAttack);
    return ColliderAttack;
}(cc.Component));
exports.default = ColliderAttack;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29udHJvbGxlclxcQ29sbGlkZXJBdHRhY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsMkNBQXNDO0FBQ3RDLDZDQUF3QztBQUN4QywrQ0FBMEM7QUFDMUMsaURBQTRDO0FBQzVDLDZEQUF3RDtBQUVsRCxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRzVDO0lBQTRDLGtDQUFZO0lBRHhEO1FBQUEscUVBeUNDO1FBdENHLFVBQUksR0FBWSxJQUFJLENBQUM7O0lBc0N6QixDQUFDO0lBckNHLDhCQUFLLEdBQUw7UUFDSSxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNoRCxRQUFRLENBQUMsRUFBRSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFDRCx1Q0FBYyxHQUFkLFVBQWUsS0FBSztRQUFwQixpQkFnQ0M7UUEvQkcsSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksUUFBUSxFQUFFO1lBQzVDLElBQUksS0FBSyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLHNCQUFZLENBQUMsQ0FBQyxjQUFjLEVBQUU7Z0JBQ2hHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLHNCQUFZLENBQUMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dCQUM5RCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzVELEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUNyRCxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsdUJBQWEsQ0FBQyxDQUFDLGtCQUFrQixFQUFFLENBQUM7Z0JBQzFFLElBQUksQ0FBQyxZQUFZLENBQUM7b0JBQ2QsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO2dCQUM3RCxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ1YsSUFBSSxDQUFDLFlBQVksQ0FBQztvQkFDZCxLQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxzQkFBWSxDQUFDLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQ3hELENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUNYO1NBQ0o7YUFDSSxJQUFJLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxRQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxzQkFBWSxDQUFDLENBQUMsY0FBYyxFQUFFO1lBQ3pHLElBQUksR0FBRyxHQUFHLGlCQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNDLElBQUksR0FBRyxJQUFJLENBQUMsRUFBRTtnQkFDVixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxzQkFBWSxDQUFDLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDOUQsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUM1RCxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDM0UsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ3JELElBQUksQ0FBQyxZQUFZLENBQUM7b0JBQ2QsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO2dCQUM3RCxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ1YsSUFBSSxDQUFDLFlBQVksQ0FBQztvQkFDZCxLQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxzQkFBWSxDQUFDLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQ3hELENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQzthQUNaO1NBQ0o7SUFDTCxDQUFDO0lBckNEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0RBQ0c7SUFGSixjQUFjO1FBRGxDLE9BQU87T0FDYSxjQUFjLENBd0NsQztJQUFELHFCQUFDO0NBeENELEFBd0NDLENBeEMyQyxFQUFFLENBQUMsU0FBUyxHQXdDdkQ7a0JBeENvQixjQUFjIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEdsb2JhbCBmcm9tIFwiLi4vQ29tbW9uL0dsb2JhbFwiO1xyXG5pbXBvcnQgVXRpbGl0eSBmcm9tIFwiLi4vQ29tbW9uL1V0aWxpdHlcIjtcclxuaW1wb3J0IEFJQ29udHJvbGxlciBmcm9tIFwiLi9BSUNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IEJveENvbnRyb2xsZXIgZnJvbSBcIi4vQm94Q29udHJvbGxlclwiO1xyXG5pbXBvcnQgQ2hhcmFjdGVyQ29udHJvbGxlciBmcm9tIFwiLi9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29sbGlkZXJBdHRhY2sgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBteUFJOiBjYy5Ob2RlID0gbnVsbDtcclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIGxldCBjb2xsaWRlciA9IHRoaXMuZ2V0Q29tcG9uZW50KGNjLkNvbGxpZGVyM0QpO1xyXG4gICAgICAgIGNvbGxpZGVyLm9uKCd0cmlnZ2VyLWVudGVyJywgdGhpcy5vblRyaWdnZXJFbnRlciwgdGhpcyk7XHJcbiAgICB9XHJcbiAgICBvblRyaWdnZXJFbnRlcihldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudC5vdGhlckNvbGxpZGVyLm5vZGUuZ3JvdXAgPT0gXCJNb2RlbDJcIikge1xyXG4gICAgICAgICAgICBpZiAoZXZlbnQub3RoZXJDb2xsaWRlci5ub2RlLm5hbWUgPT0gXCJCb3hcIiAmJiAhdGhpcy5teUFJLmdldENvbXBvbmVudChBSUNvbnRyb2xsZXIpLmJvb2xDaGVja0RlYXRoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm15QUkuZ2V0Q29tcG9uZW50KEFJQ29udHJvbGxlcikudW5zY2hlZHVsZUFsbENhbGxiYWNrcygpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5teUFJLnN0b3BBbGxBY3Rpb25zKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm15QUkuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiQXR0YWNrXCIpO1xyXG4gICAgICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdChHbG9iYWwuc291bmRBdHRhY2ssIGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIGV2ZW50Lm90aGVyQ29sbGlkZXIubm9kZS5nZXRDb21wb25lbnQoQm94Q29udHJvbGxlcikuRGllV2hlbkVuZW15QXR0YWNrKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5teUFJLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIklkbGVcIilcclxuICAgICAgICAgICAgICAgIH0sIDAuNzA4KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm15QUkuZ2V0Q29tcG9uZW50KEFJQ29udHJvbGxlcikuRW5lbXlSdW5JZGxlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCAxLjMpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYgKGV2ZW50Lm90aGVyQ29sbGlkZXIubm9kZS5ncm91cCA9PSBcIlBsYXllclwiICYmICF0aGlzLm15QUkuZ2V0Q29tcG9uZW50KEFJQ29udHJvbGxlcikuYm9vbENoZWNrRGVhdGgpIHtcclxuICAgICAgICAgICAgbGV0IFJhbiA9IFV0aWxpdHkuUmFuZG9tUmFuZ2VJbnRlZ2VyKDEsIDUpO1xyXG4gICAgICAgICAgICBpZiAoUmFuID09IDIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubXlBSS5nZXRDb21wb25lbnQoQUlDb250cm9sbGVyKS51bnNjaGVkdWxlQWxsQ2FsbGJhY2tzKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm15QUkuc3RvcEFsbEFjdGlvbnMoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubXlBSS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJBdHRhY2tcIik7XHJcbiAgICAgICAgICAgICAgICBldmVudC5vdGhlckNvbGxpZGVyLm5vZGUuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkNoYXJhY3RlckZhbGwoKTtcclxuICAgICAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kQXR0YWNrLCBmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5teUFJLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIklkbGVcIilcclxuICAgICAgICAgICAgICAgIH0sIDAuNzA4KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm15QUkuZ2V0Q29tcG9uZW50KEFJQ29udHJvbGxlcikuRW5lbXlSdW5JZGxlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCAwLjg1KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------
