"use strict";
cc._RF.push(module, 'b3890qhPopIIpBUBGAYxovj', 'SplineExtend');
// Scripts/Common/SplineExtend.js

"use strict";

/****************************************************************************
 Copyright (c) 2008 Radu Gruian
 Copyright (c) 2008-2010 Ricardo Quesada
 Copyright (c) 2011 Vit Valentin
 Copyright (c) 2011-2012 cocos2d-x.org
 Copyright (c) 2013-2016 Chukong Technologies Inc.
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 http://www.cocos2d-x.org
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 Orignal code by Radu Gruian: http://www.codeproject.com/Articles/30838/Overhauser-Catmull-Rom-Splines-for-Camera-Animatio.So
 Adapted to cocos2d-x by Vit Valentin
 Adapted from cocos2d-x to cocos2d-iphone by Ricardo Quesada
 ****************************************************************************/

/**
 * @module cc
 */

/*
 * Returns the Cardinal Spline position for a given set of control points, tension and time. <br />
 * CatmullRom Spline formula. <br />
 * s(-ttt + 2tt - t)P1 + s(-ttt + tt)P2 + (2ttt - 3tt + 1)P2 + s(ttt - 2tt + t)P3 + (-2ttt + 3tt)P3 + s(ttt - tt)P4
 *
 * @method cardinalSplineAt
 * @param {Vec2} p0
 * @param {Vec2} p1
 * @param {Vec2} p2
 * @param {Vec2} p3
 * @param {Number} tension
 * @param {Number} t
 * @return {Vec2}
 */
function cardinalSplineAt(p0, p1, p2, p3, tension, t) {
  var t2 = t * t;
  var t3 = t2 * t;
  /*
   * Formula: s(-ttt + 2tt - t)P1 + s(-ttt + tt)P2 + (2ttt - 3tt + 1)P2 + s(ttt - 2tt + t)P3 + (-2ttt + 3tt)P3 + s(ttt - tt)P4
   */

  var s = (1 - tension) / 2;
  var b1 = s * (-t3 + 2 * t2 - t); // s(-t3 + 2 t2 - t)P1

  var b2 = s * (-t3 + t2) + (2 * t3 - 3 * t2 + 1); // s(-t3 + t2)P2 + (2 t3 - 3 t2 + 1)P2

  var b3 = s * (t3 - 2 * t2 + t) + (-2 * t3 + 3 * t2); // s(t3 - 2 t2 + t)P3 + (-2 t3 + 3 t2)P3

  var b4 = s * (t3 - t2); // s(t3 - t2)P4

  var x = p0.x * b1 + p1.x * b2 + p2.x * b3 + p3.x * b4;
  var y = p0.y * b1 + p1.y * b2 + p2.y * b3 + p3.y * b4;
  return cc.v2(x, y);
}

;
/*
 * returns a point from the array
 * @method getControlPointAt
 * @param {Array} controlPoints
 * @param {Number} pos
 * @return {Array}
 */

function getControlPointAt(controlPoints, pos) {
  var p = Math.min(controlPoints.length - 1, Math.max(pos, 0));
  return controlPoints[p];
}

;

function reverseControlPoints(controlPoints) {
  var newArray = [];

  for (var i = controlPoints.length - 1; i >= 0; i--) {
    newArray.push(cc.v2(controlPoints[i].x, controlPoints[i].y));
  }

  return newArray;
}

function cloneControlPoints(controlPoints) {
  var newArray = [];

  for (var i = 0; i < controlPoints.length; i++) {
    newArray.push(cc.v2(controlPoints[i].x, controlPoints[i].y));
  }

  return newArray;
}
/**
 * TODO tối ưu: Thực hiện pre interpolation và precalculate length cho các Spline Action giống nhau
 */

/*
 * Cardinal Spline path. http://en.wikipedia.org/wiki/Cubic_Hermite_spline#Cardinal_spline
 * Absolute coordinates.
 *
 * @class CardinalSplineTo2
 * @extends ActionInterval
 *
 * @param {Number} duration
 * @param {Array} points array of control points
 * @param {Number} tension
 *
 * @example
 * //create a cc.CardinalSplineTo2
 * var action1 = cc.CardinalSplineTo2(3, array, 0);
 * 
 */


cc.CardinalSplineTo2 = cc.Class({
  name: 'cc.CardinalSplineTo2',
  "extends": cc.ActionInterval,
  cacheLengths: null,
  ctor: function ctor(duration, points, angleOffset, tension, speed) {
    /* Array of control points */
    this._points = [];
    this._cacheLengths = [];
    this._totalLength = 0;
    this._deltaT = 0;
    this._tension = 0;
    this._angleOffset = 0;
    this._previousPosition = null;
    this._accumulatedDiff = null;
    tension !== undefined && cc.CardinalSplineTo2.prototype.initWithDuration.call(this, duration, points, angleOffset, tension, speed);
  },
  initWithDuration: function initWithDuration(duration, points, angleOffset, tension, speed) {
    if (!points || points.length === 0) {
      cc.errorID(1024);
      return false;
    }

    this._tension = tension;
    this._angleOffset = angleOffset;
    this.setPoints(points);
    if (speed) duration = this.getLength() / speed;

    if (cc.ActionInterval.prototype.initWithDuration.call(this, duration)) {
      return true;
    }

    return false;
  },
  clone: function clone() {
    var action = new cc.CardinalSplineTo2();
    action.initWithDuration(this._duration, cloneControlPoints(this._points), this._tension);
    return action;
  },
  startWithTarget: function startWithTarget(target) {
    cc.ActionInterval.prototype.startWithTarget.call(this, target); // Issue #1441 from cocos2d-iphone

    this._deltaT = 1 / (this._points.length - 1);
    this._previousPosition = cc.v2(this.target.x, this.target.y);
    this._accumulatedDiff = cc.v2(0, 0);
  },
  getP: function getP(dt) {
    for (var i = 0; i < this._cummutilate.length; i++) {
      if (this._cummutilate[i] > dt) return i;
    }

    return this._cummutilate.length;
  },
  getLt: function getLt(p, dt) {
    if (p == 0) {
      return dt / this._cacheLengths[p];
    } else {
      return (dt - this._cummutilate[p - 1]) / this._cacheLengths[p];
    }
  },
  update: function update(dt) {
    dt = this._computeEaseTime(dt);
    var p, lt;
    var ps = this._points; // eg.
    // p..p..p..p..p..p..p
    // 1..2..3..4..5..6..7
    // want p to be 1, 2, 3, 4, 5, 6

    if (dt === 1) {
      p = ps.length - 1;
      lt = 1;
    } else {
      var locDT = this._deltaT; //p = 0 | (dt / locDT);
      //lt = (dt - locDT * p) / locDT;

      p = this.getP(dt);
      lt = this.getLt(p, dt); // console.log(lt);
    }

    var newPos = cardinalSplineAt(getControlPointAt(ps, p - 1), getControlPointAt(ps, p - 0), getControlPointAt(ps, p + 1), getControlPointAt(ps, p + 2), this._tension, lt);

    if (cc.macro.ENABLE_STACKABLE_ACTIONS) {
      var tempX, tempY;
      tempX = this.target.x - this._previousPosition.x;
      tempY = this.target.y - this._previousPosition.y;

      if (tempX !== 0 || tempY !== 0) {
        var locAccDiff = this._accumulatedDiff;
        tempX = locAccDiff.x + tempX;
        tempY = locAccDiff.y + tempY;
        locAccDiff.x = tempX;
        locAccDiff.y = tempY;
        newPos.x += tempX;
        newPos.y += tempY;
      }
    }

    this.updatePosition(newPos);
  },
  reverse: function reverse() {
    var reversePoints = reverseControlPoints(this._points);
    return cc.CardinalSplineTo2(this._duration, reversePoints, this._tension);
  },

  /*
   * update position of target
   * @method updatePosition
   * @param {Vec2} newPos
   */
  updatePosition: function updatePosition(newPos) {
    var deltaX = newPos.x - this._previousPosition.x;
    var deltaY = newPos.y - this._previousPosition.y;

    if (!this._disableRotate) {
      if (deltaX !== 0 && deltaY !== 0) this.target.angle = Math.atan2(deltaY, deltaX) * 180 / Math.PI + this._angleOffset;
    }

    this.target.setPosition(newPos);
    this._previousPosition = newPos;
  },

  /*
   * Points getter
   * @method getPoints
   * @return {Array}
   */
  getPoints: function getPoints() {
    return this._points;
  },

  /**
   * Points setter
   * @method setPoints
   * @param {Array} points
   */
  setPoints: function setPoints(points) {
    points = this._interpolatatePoints(points, 10);

    this._setPoint(points);
  },
  _setPoint: function _setPoint(points) {
    this._points = points;
    this._cacheLengths = [];
    this._cummutilate = [];

    for (var i = 0; i < points.length - 1; i++) {
      var p1 = points[i];
      var p2 = points[i + 1];
      var length = Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));

      this._cacheLengths.push(length);
    }

    this._totalLength = 0;

    for (var i = 0; i < this._cacheLengths.length; i++) {
      this._totalLength += this._cacheLengths[i];

      this._cummutilate.push(this._totalLength);
    }

    for (var i = 0; i < this._cacheLengths.length; i++) {
      this._cacheLengths[i] = this._cacheLengths[i] / this._totalLength;
      this._cummutilate[i] = this._cummutilate[i] / this._totalLength;
    }
  },
  _setPoint2: function _setPoint2(points) {
    this._points = points;
    this._cacheLengths = [];
    this._cummutilate = [];

    for (var i = 0; i < points.length - 1; i++) {
      var p1 = points[i];
      var p2 = points[i + 1];
      var length = 1;

      this._cacheLengths.push(length);
    }

    this._totalLength = 0;

    for (var i = 0; i < this._cacheLengths.length; i++) {
      this._totalLength += this._cacheLengths[i];

      this._cummutilate.push(this._totalLength);
    }

    for (var i = 0; i < this._cacheLengths.length; i++) {
      this._cacheLengths[i] = this._cacheLengths[i] / this._totalLength;
      this._cummutilate[i] = this._cummutilate[i] / this._totalLength;
    }
  },
  _interpolatatePoints: function _interpolatatePoints(points, d) {
    this._setPoint(points);

    var n = (points.length - 1) * d;
    var newPoints = [];
    var p = 0;
    var lt = 0;
    var ps = points;

    for (var i = 0; i < n; i++) {
      var dt = i * 1 / (n - 1);

      if (dt === 1) {
        p = ps.length - 1;
        lt = 1;
      } else {
        var locDT = 1 / (points.length - 1); //p = 0 | (dt / locDT);
        //lt = (dt - locDT * p) / locDT;

        p = this.getP(dt);
        lt = this.getLt(p, dt);
      }

      var newPos = cardinalSplineAt(getControlPointAt(ps, p - 1), getControlPointAt(ps, p - 0), getControlPointAt(ps, p + 1), getControlPointAt(ps, p + 2), this._tension, lt);
      newPoints.push(newPos);
    }

    return newPoints;
  },
  getLength: function getLength() {
    return this._totalLength;
  },
  disableRotate: function disableRotate() {
    this._disableRotate = true;
  }
});
/**
 * !#en Creates an action with a Cardinal Spline array of points and tension.
 * !#zh 按基数样条曲线轨迹移动到目标位置。
 * @method CardinalSplineTo2
 * @param {Number} duration
 * @param {Array} points array of control points
 * @param {Number} angleOffset
 * @param {Number} tension
 * @return {ActionInterval}
 *
 * @example
 * //create a cc.CardinalSplineTo2
 * var action1 = cc.CardinalSplineTo2(3, array, 0);
 */

cc.cardinalSplineTo2 = function (duration, points, angleOffset, tension) {
  return new cc.CardinalSplineTo2(duration, points, angleOffset, tension);
};
/**
 * !#en Creates an action with a Cardinal Spline array of points and tension.
 * !#zh 按基数样条曲线轨迹移动到目标位置。
 * @method CardinalSplineTo2
 * @param {Number} speed
 * @param {Array} points array of control points
 * @param {Number} angleOffset
 * @param {Number} tension
 * @return {ActionInterval}
 *
 * @example
 * //create a cc.CardinalSplineTo2
 * var action1 = cc.CardinalSplineTo2(3, array, 0);
 */


cc.cardinalSplineTo2_speed = function (speed, points, angleOffset, tension) {
  return new cc.CardinalSplineTo2(1, points, angleOffset, tension, speed);
};
/*
 * Cardinal Spline path. http://en.wikipedia.org/wiki/Cubic_Hermite_spline#Cardinal_spline
 * Relative coordinates.
 *
 * @class CardinalSplineBy2
 * @extends CardinalSplineTo2
 *
 * @param {Number} duration
 * @param {Array} points
 * @param {Number} tension
 *
 * @example
 * //create a cc.CardinalSplineBy2
 * var action1 = cc.CardinalSplineBy2(3, array, 0);
 */


cc.CardinalSplineBy2 = cc.Class({
  name: 'cc.CardinalSplineBy2',
  "extends": cc.CardinalSplineTo2,
  ctor: function ctor(duration, points, angleOffset, tension) {
    this._startPosition = cc.v2(0, 0);
    tension !== undefined && this.initWithDuration(duration, points, angleOffset, tension);
  },
  startWithTarget: function startWithTarget(target) {
    cc.CardinalSplineTo2.prototype.startWithTarget.call(this, target);
    this._startPosition.x = target.x;
    this._startPosition.y = target.y;
  },
  reverse: function reverse() {
    var copyConfig = this._points.slice();

    var current; //
    // convert "absolutes" to "diffs"
    //

    var p = copyConfig[0];

    for (var i = 1; i < copyConfig.length; ++i) {
      current = copyConfig[i];
      copyConfig[i] = current.sub(p);
      p = current;
    } // convert to "diffs" to "reverse absolute"


    var reverseArray = reverseControlPoints(copyConfig); // 1st element (which should be 0,0) should be here too

    p = reverseArray[reverseArray.length - 1];
    reverseArray.pop();
    p.x = -p.x;
    p.y = -p.y;
    reverseArray.unshift(p);

    for (var i = 1; i < reverseArray.length; ++i) {
      current = reverseArray[i];
      current.x = -current.x;
      current.y = -current.y;
      current.x += p.x;
      current.y += p.y;
      reverseArray[i] = current;
      p = current;
    }

    return cc.CardinalSplineBy2(this._duration, reverseArray, this._angleOffset, this._tension);
  },

  /**
   * update position of target
   * @method updatePosition
   * @param {Vec2} newPos
   */
  updatePosition: function updatePosition(newPos) {
    var pos = this._startPosition;
    var posX = newPos.x + pos.x;
    var posY = newPos.y + pos.y;
    var deltaX = posX - this._previousPosition.x;
    var deltaY = posY - this._previousPosition.y;
    if (deltaX !== 0 && deltaY !== 0) this.target.angle = Math.atan2(deltaY, deltaX) * 180 / Math.PI + this._angleOffset;
    this._previousPosition.x = posX;
    this._previousPosition.y = posY;
    this.target.setPosition(posX, posY);
  },
  clone: function clone() {
    var a = new cc.CardinalSplineBy2();
    a.initWithDuration(this._duration, cloneControlPoints(this._points), this._angleOffset, this._tension);
    return a;
  }
});
/**
 * !#en Creates an action with a Cardinal Spline array of points and tension.
 * !#zh 按基数样条曲线轨迹移动指定的距离。
 * @method CardinalSplineBy2
 * @param {Number} duration
 * @param {Array} points
 * @param {Number} angleOffset
 * @param {Number} tension
 *
 * @return {ActionInterval}
 */

cc.cardinalSplineBy2 = function (duration, points, angleOffset, tension) {
  return new cc.CardinalSplineBy2(duration, points, angleOffset, tension);
};
/*
 * An action that moves the target with a CatmullRom curve to a destination point.<br/>
 * A Catmull Rom is a Cardinal Spline with a tension of 0.5.  <br/>
 * http://en.wikipedia.org/wiki/Cubic_Hermite_spline#Catmull.E2.80.93Rom_spline
 * Absolute coordinates.
 *
 * @class CatmullRomTo2
 * @extends CardinalSplineTo2
 *
 * @param {Number} dt
 * @param {Array} points
 *
 * @example
 * var action1 = cc.CatmullRomTo2(3, array);
 */


cc.CatmullRomTo2 = cc.Class({
  name: 'cc.CatmullRomTo2',
  "extends": cc.CardinalSplineTo2,
  ctor: function ctor(dt, points, angleOffset) {
    if (angleOffset === undefined) angleOffset = 0;
    points && this.initWithDuration(dt, points, angleOffset);
  },
  initWithDuration: function initWithDuration(dt, points, angleOffset) {
    return cc.CardinalSplineTo2.prototype.initWithDuration.call(this, dt, points, angleOffset, 0.5);
  },
  clone: function clone() {
    var action = new cc.CatmullRomTo2();
    action.initWithDuration(this._duration, cloneControlPoints(this._points, this._angleOffset));
    return action;
  }
});
/**
 * !#en Creates an action with a Cardinal Spline array of points and tension.
 * !#zh 按 Catmull Rom 样条曲线轨迹移动到目标位置。
 * @method CatmullRomTo2
 * @param {Number} dt
 * @param {Array} points
 * @param {Number} angleOffset
 * @return {ActionInterval}
 *
 * @example
 * var action1 = cc.CatmullRomTo2(3, array);
 */

cc.catmullRomTo2 = function (dt, points, angleOffset) {
  return new cc.CatmullRomTo2(dt, points, angleOffset);
};
/*
 * An action that moves the target with a CatmullRom curve by a certain distance.  <br/>
 * A Catmull Rom is a Cardinal Spline with a tension of 0.5.<br/>
 * http://en.wikipedia.org/wiki/Cubic_Hermite_spline#Catmull.E2.80.93Rom_spline
 * Relative coordinates.
 *
 * @class CatmullRomBy2
 * @extends CardinalSplineBy2
 *
 * @param {Number} dt
 * @param {Array} points
 *
 * @example
 * var action1 = cc.CatmullRomBy2(3, array);
 */


cc.CatmullRomBy2 = cc.Class({
  name: 'cc.CatmullRomBy2',
  "extends": cc.CardinalSplineBy2,
  ctor: function ctor(dt, points, angleOffset) {
    points && this.initWithDuration(dt, points, angleOffset);
  },
  initWithDuration: function initWithDuration(dt, points, angleOffset) {
    return cc.CardinalSplineTo2.prototype.initWithDuration.call(this, dt, points, angleOffset, 0.5);
  },
  clone: function clone() {
    var action = new cc.CatmullRomBy2();
    action.initWithDuration(this._duration, cloneControlPoints(this._points), this._angleOffset);
    return action;
  }
});
/**
 * !#en Creates an action with a Cardinal Spline array of points and tension.
 * !#zh 按 Catmull Rom 样条曲线轨迹移动指定的距离。
 * @method CatmullRomBy2
 * @param {Number} dt
 * @param {Array} points
 * @param {Number} angleOffset
 * @return {ActionInterval}
 * @example
 * var action1 = cc.CatmullRomBy2(3, array, 90);
 */

cc.catmullRomBy2 = function (dt, points, angleOffset) {
  return new cc.CatmullRomBy2(dt, points, angleOffset);
};

cc._RF.pop();