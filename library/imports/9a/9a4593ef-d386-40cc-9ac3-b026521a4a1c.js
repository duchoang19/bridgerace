"use strict";
cc._RF.push(module, '9a459Pv04ZAzJrDsCZSGkoc', 'BR1');
// Scripts/GamePlay/BR1/BR1.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Global_1 = require("../../Common/Global");
var Singleton_1 = require("../../Common/Singleton");
var AIController_1 = require("../../Controller/AIController");
var CharacterController_1 = require("../../Controller/CharacterController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BR1 = /** @class */ (function (_super) {
    __extends(BR1, _super);
    function BR1() {
        var _this = _super.call(this) || this;
        _this.Guide = null;
        _this.txtCollectBlock = null;
        _this.myCharacter = null;
        _this.AIParent = null;
        _this.BoxParent = null;
        _this.txtClimb = null;
        _this.joyStick = null;
        _this.effectCongra = null;
        _this.btnDownLoad = null;
        _this.endGame = null;
        _this.boolEndGameOne = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        _this.boolcheckInteraction = false;
        BR1_1._instance = _this;
        return _this;
    }
    BR1_1 = BR1;
    BR1.prototype.start = function () {
        Global_1.default.boolStartPlay = true;
    };
    BR1.prototype.update = function () {
        var _this = this;
        if (Global_1.default.boolEnableTouch && !this.boolcheckInteraction) {
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
            for (var i = 0; i < this.AIParent.childrenCount; i++) {
                this.AIParent.children[i].getComponent(AIController_1.default).StartMove();
            }
            this.btnDownLoad.active = true;
            this.boolcheckInteraction = true;
        }
        if (this.myCharacter.y >= 70 && !this.boolEndGameOne) {
            this.boolEndGameOne = true;
            Global_1.default.boolEndGame = true;
            Global_1.default.boolEnableTouch = false;
            for (var i = 0; i < this.myCharacter.children[2].childrenCount; i++) {
                if (this.myCharacter.children[2].children[i].name == "BrickStick") {
                    this.myCharacter.children[2].children[i].active = false;
                }
            }
            for (var i = 0; i < this.AIParent.childrenCount; i++) {
                this.AIParent.children[i].active = false;
            }
            this.myCharacter.stopAllActions();
            this.joyStick.active = false;
            this.ScaleCongra();
            this.myCharacter.getComponent(cc.RigidBody3D).setLinearVelocity(new cc.Vec3(0, 0, 0));
            this.myCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = false;
            this.myCharacter.getComponent(cc.SkeletonAnimation).play("Win");
            cc.audioEngine.playEffect(Global_1.default.soundWin, false);
            this.scheduleOnce(function () {
                _this.EndGame();
            }, 1.2);
        }
    };
    BR1.prototype.ScaleCongra = function () {
        var _this = this;
        this.effectCongra.scale = 0;
        this.effectCongra.opacity = 255;
        this.effectCongra.runAction(cc.sequence(cc.scaleTo(0.3, 1.3).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.effectCongra.runAction(cc.fadeOut(0.3));
            }, 0.7);
        })));
    };
    BR1.prototype.EndGame = function () {
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
        this.scheduleOnce(function () {
            cc.audioEngine.stopAllEffects();
        }, 2);
        this.endGame.active = true;
    };
    var BR1_1;
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "txtCollectBlock", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "myCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "AIParent", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "BoxParent", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "txtClimb", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "joyStick", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "effectCongra", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "btnDownLoad", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "endGame", void 0);
    BR1 = BR1_1 = __decorate([
        ccclass
    ], BR1);
    return BR1;
}(Singleton_1.default));
exports.default = BR1;

cc._RF.pop();