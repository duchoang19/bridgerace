"use strict";
cc._RF.push(module, 'f6068GFEGlHn7TrO7hQLo8v', 'JoystickFollow');
// Scripts/Controller/JoystickFollow.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var CharacterController_1 = require("./CharacterController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var JoysticFollow = /** @class */ (function (_super) {
    __extends(JoysticFollow, _super);
    function JoysticFollow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.joyRing = null;
        _this.joyDot = null;
        _this.stickPos = null;
        _this.touchLocation = null;
        _this.radius = 0;
        return _this;
    }
    JoysticFollow.prototype.onLoad = function () {
        this.radius = this.joyRing.width / 2;
    };
    JoysticFollow.prototype.start = function () {
        this.gamePlayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        this.node.on(cc.Node.EventType.TOUCH_START, this.TouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.TouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.TouchCancel, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.TouchCancel, this);
    };
    JoysticFollow.prototype.TouchStart = function (event) {
        if (!Global_1.default.boolEndGame && Global_1.default.boolStartPlay && !Global_1.default.boolCharacterFall) {
            var mousePosition = event.getLocation();
            var localMousePosition = this.node.convertToNodeSpaceAR(mousePosition);
            this.node.opacity = 255;
            this.stickPos = localMousePosition;
            this.touchLocation = event.getLocation();
            this.joyRing.setPosition(localMousePosition);
            this.joyDot.setPosition(localMousePosition);
            this.gamePlayInstance.gameplay.Guide.active = false;
            this.gamePlayInstance.gameplay.txtCollectBlock.active = false;
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = true;
        }
    };
    JoysticFollow.prototype.TouchMove = function (event) {
        if (!Global_1.default.boolEndGame && Global_1.default.boolStartPlay && !Global_1.default.boolCharacterFall) {
            this.node.opacity = 255;
            Global_1.default.boolEnableTouch = true;
            if (!Global_1.default.boolFirstTouchJoyStick) {
                Global_1.default.boolFirstTouchJoyStick = true;
                this.gamePlayInstance.gameplay.myCharacter.getComponent(cc.SkeletonAnimation).play("Run");
            }
            if (this.touchLocation === event.getLocation()) {
                return false;
            }
            this.gamePlayInstance.gameplay.Guide.active = false;
            var touchPos = this.joyRing.convertToNodeSpaceAR(event.getLocation());
            var distance = touchPos.mag();
            var posX = this.stickPos.x + touchPos.x;
            var posY = this.stickPos.y + touchPos.y;
            var p = cc.v2(posX, posY).sub(this.joyRing.getPosition()).normalize();
            Global_1.default.touchPos = p;
            if (this.radius > distance) {
                this.joyDot.setPosition(cc.v2(posX, posY));
            }
            else {
                var x = this.stickPos.x + p.x * this.radius;
                var y = this.stickPos.y + p.y * this.radius;
                this.joyDot.setPosition(cc.v2(x, y));
            }
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = true;
            this.gamePlayInstance.gameplay.Guide.active = false;
            this.gamePlayInstance.gameplay.txtCollectBlock.active = false;
        }
    };
    JoysticFollow.prototype.TouchCancel = function () {
        if (!Global_1.default.boolEndGame && Global_1.default.boolStartPlay && !Global_1.default.boolCharacterFall) {
            this.joyDot.setPosition(this.joyRing.getPosition());
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).Attacking();
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).StopMove();
            Global_1.default.boolEnableTouch = false;
            this.node.opacity = 0;
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).boolCheckTypeRotate = true;
        }
    };
    __decorate([
        property(cc.Node)
    ], JoysticFollow.prototype, "joyRing", void 0);
    __decorate([
        property(cc.Node)
    ], JoysticFollow.prototype, "joyDot", void 0);
    JoysticFollow = __decorate([
        ccclass
    ], JoysticFollow);
    return JoysticFollow;
}(cc.Component));
exports.default = JoysticFollow;

cc._RF.pop();