import Singleton from "./Singleton";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Utility extends Singleton<Utility> {
    constructor() {
        super();
        Utility._instance = this;
    }
    static RandomRangeFloat(lower: number, upper: number) {
        return Math.random() * (upper - lower) + lower;
        //return Math.floor(Math.random() * (lower - lower)) + lower;
    }
    static RandomRangeInteger(lower: number, upper: number) {
        return Math.round(Math.random() * (upper - lower) + lower);
    }
    static Distance(vec1: cc.Vec2, vec2: cc.Vec2) {
        let Distance = Math.sqrt(Math.pow(vec1.x - vec2.x, 2) +
            Math.pow(vec1.y - vec2.y, 2));
        return Distance;
    }
    static BetweenDegree(comVec: cc.Vec2, dirVec: cc.Vec2) {
        let angleDegree = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDegree - 90; 
    }
    static CaculatorDegree(Target: cc.Vec2) {
        var r = Math.atan2(Target.y, Target.x);
        var degree = r * 180 / (Math.PI);
        degree = 360 - degree + 90;
        return degree;
    }
}
