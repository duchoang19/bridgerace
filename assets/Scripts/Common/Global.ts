interface Global {
    touchPos: cc.Vec2,
    boolEnableTouch: boolean,
    boolFirstTouchJoyStick: boolean,
    boolStartPlay: boolean,
    boolStartAttacking: boolean,
    boolCheckAttacking: boolean,
    boolCheckAttacked: boolean,
    boolCharacterFall: boolean,
    boolEndGame: boolean,
    soundBG: cc.AudioClip,
    soundIntro: cc.AudioClip,
    soundAttack: cc.AudioClip,
    soundFootStep: cc.AudioClip,
    soundCollect: cc.AudioClip,
    soundClickBtn: cc.AudioClip,
    soundBoxBroken: cc.AudioClip,
    soundUpStairs: cc.AudioClip
    soundWin: cc.AudioClip
}
let Global: Global = {
    touchPos: null,
    boolEnableTouch: false,
    boolFirstTouchJoyStick: false,
    boolStartPlay: false,
    boolStartAttacking: false,
    boolCheckAttacking: false,
    boolCheckAttacked: false,
    boolCharacterFall: false,
    boolEndGame: false,
    soundBG: null,
    soundIntro: null,
    soundAttack: null,
    soundFootStep: null,
    soundCollect: null,
    soundClickBtn: null,
    soundBoxBroken: null,
    soundUpStairs: null,
    soundWin: null
};
export default Global;