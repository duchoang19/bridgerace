import BR1 from "../GamePlay/BR1/BR1";
import Singleton from "./Singleton";
const { ccclass, property } = cc._decorator;
@ccclass
export default class GamePlayController extends Singleton<GamePlayController> {
    @property(cc.Material)
    materialBrickCharacter: cc.Material = null;
    @property(cc.Material)
    materialBrickAI: cc.Material[] = [];
    constructor() {
        super();
        GamePlayController._instance = this;
    }
}
