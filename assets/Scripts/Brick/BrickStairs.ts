import { BrickType } from "../Common/EnumDefine";
import GamePlayController from "../Common/GamePlayController";
import GamePlayInstance from "../Common/GamePlayInstance";
import Global from "../Common/Global";
import CharacterController from "../Controller/CharacterController";
import BrickData from "./BrickData";

const { ccclass, property } = cc._decorator;

@ccclass
export default class BrickStairs extends cc.Component {
    boolAppeared: boolean = false;
    start() {
        let collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-enter', this.onTriggerEnter, this);
    }
    onTriggerEnter(event) {
        if (event.otherCollider.node.group == "Player") {
            if (!this.boolAppeared) {
                cc.audioEngine.playEffect(Global.soundUpStairs,false);
                GamePlayInstance.Instance(GamePlayInstance).gameplay.txtClimb.runAction(cc.fadeOut(0.3));
                this.boolAppeared = true;
                this.node.opacity = 255;
                this.node.getComponent(BrickData).setmaterial(BrickType.BrickCharacter);
                event.otherCollider.node.getComponent(CharacterController).DecreaseStick();
            }
        }
    }
}
