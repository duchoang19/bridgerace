const { ccclass, property } = cc._decorator;

@ccclass
export default class CheckMoveDownStairs extends cc.Component {
    boolMoveDown: boolean = false;
    start() {
        let collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-stay', this.onTriggerStay, this);
        collider.on('trigger-exit', this.onTriggerExit, this);
    }
    onTriggerStay(event) {
        if (event.otherCollider.node.group == "Model") {
            if (event.otherCollider.node.name == "CheckPlayerMoveBack") {
                this.boolMoveDown = true;
            }
        }
    }
    onTriggerExit(event) {
        this.boolMoveDown = false;
    }
}
