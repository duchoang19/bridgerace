import BrickData from "../Brick/BrickData";
import { BrickType } from "../Common/EnumDefine";
import GamePlayInstance, { eventDispatcher } from "../Common/GamePlayInstance";
import Global from "../Common/Global";
import KeyEvent from "../Common/KeyEvent";
import Utility from "../Common/Utility";
import BoxController from "./BoxController";
import ColliderAttack from "./ColliderAttack";

const { ccclass, property } = cc._decorator;

@ccclass
export default class AIController extends cc.Component {
    @property(cc.Integer)
    clampXLeft: number = -33;
    @property(cc.Integer)
    clampXRight: number = 33;
    @property(cc.Integer)
    clampYTop: number = 21;
    @property(cc.Integer)
    clampYBottom: number = -18;
    @property(cc.Integer)
    speedMove: number = 20;
    @property(cc.Node)
    colliderAttack: cc.Node = null;
    @property(cc.Prefab)
    brickHom: cc.Prefab = null;
    @property(cc.Prefab)
    brickStick: cc.Prefab = null;
    @property({ type: cc.Enum(BrickType) })
    public brickType: BrickType = BrickType.BrickAI1;
    @property(cc.String)
    stringIncrease: string = "";
    moveX: number = 0;
    moveY: number = 0;
    level: number = 0;
    listBrickAdd: cc.Node[] = [];
    x: number = 0.003;
    lastPositionStickY: number = 0.013;
    z: number = -0.002;
    boolCheckDeath: boolean = false;
    boolCheckDeathOne: boolean = false;
    boolCheckdelay: boolean = false;
    boolEnemyRunningIdle: boolean = false;
    boolEnemyFollowBox: boolean = false;
    boolEnemyAttacking: boolean = false;
    //rigidbody: cc.RigidBody3D;
    onEnable() {
        eventDispatcher.on(this.stringIncrease, this.IncreaseStick, this);
    }
    onDisable() {
        eventDispatcher.off(this.stringIncrease, this.IncreaseStick, this);
    }
    // start() {
    //     this.rigidbody = this.node.getComponent(cc.RigidBody3D);
    // }
    update() {
        //this.CheckingAttack();
        if (!this.boolCheckDeath) {
            if (Global.boolStartAttacking && Global.boolCheckAttacking) {
                let pos1 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.children[0].children[0].getPosition()));
                let pos2 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.children[0].children[1].getPosition()));
                let pos3 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.children[0].children[2].getPosition()));

                let direction1 = cc.v2(pos1.x - GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.x, pos1.y - GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.y);
                let direction2 = cc.v2(pos2.x - GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.x, pos2.y - GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.y);
                let degree = direction1.signAngle(direction2);
                degree = cc.misc.radiansToDegrees(degree);
                let posEnemy = cc.v2(this.node.x - GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.x, this.node.y - GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.y);
                let degreeWithPos1 = posEnemy.signAngle(direction1);
                degreeWithPos1 = cc.misc.radiansToDegrees(degreeWithPos1);
                let degreeWithPos2 = posEnemy.signAngle(direction2);
                degreeWithPos2 = cc.misc.radiansToDegrees(degreeWithPos2);
                let realNeed = 0;
                degreeWithPos1 = Math.abs(degreeWithPos1);
                degreeWithPos2 = Math.abs(degreeWithPos2);
                if (degreeWithPos1 > degreeWithPos2) {
                    realNeed = degreeWithPos1;
                }
                else {
                    realNeed = degreeWithPos2;
                }
                let distance = Utility.Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.x, GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.y));
                let maxDistance = Utility.Distance(cc.v2(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.x, GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.y), cc.v2(pos3.x, pos3.y));
                if (Math.abs(realNeed) < degree) {
                    if (distance < maxDistance) {
                        if (Global.boolCheckAttacked && !this.boolCheckDeathOne) {
                            this.EnemyFail();
                            this.boolCheckDeath = true;
                            this.boolCheckDeathOne = true;
                        }
                    }
                }
            }
        }
    }
    EnemyFail() {
        this.unscheduleAllCallbacks();
        this.node.stopAllActions();
        this.colliderAttack.getComponent(ColliderAttack).unscheduleAllCallbacks();
        this.node.getComponent(cc.SkeletonAnimation).play("Fall");
        this.SpawnerBrickWhenFall();
        this.scheduleOnce(() => {
            this.node.getComponent(cc.SkeletonAnimation).play("Idle");
        }, 1.28);
        this.scheduleOnce(() => {
            this.boolCheckDeath = false;
            this.boolCheckDeathOne = false;
            this.EnemyRunIdle();
        }, 1.6);
    }
    StartMove() {
        this.EnemyRunIdle();
    }
    EnemyRunIdle() {
        this.node.getComponent(cc.SkeletonAnimation).play("Run");
        this.boolEnemyRunningIdle = true;
        this.boolEnemyFollowBox = false;
        while (Utility.Distance(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) < 30) {
            this.moveX = Utility.RandomRangeFloat(this.clampXLeft, this.clampXRight);
            this.moveY = Utility.RandomRangeFloat(this.clampYBottom, this.clampYTop);
        }
        this.moveX = Utility.RandomRangeFloat(this.clampXLeft, this.clampXRight);
        this.moveY = Utility.RandomRangeFloat(this.clampYBottom, this.clampYTop);
        //let Direction = new cc.Vec2(this.moveX - this.node.x, this.moveY - this.node.y).normalize();
        //this.rigidbody.setLinearVelocity(new cc.Vec3(this.speedMove * Direction.x, this.speedMove * Direction.y, -10));
        //let degree = Utility.CaculatorDegree(Direction);
        //this.node.runAction(cc.rotate3DTo(0.2, cc.v3(-90, 180, degree)));

        var tween = new cc.Tween().to(2, { position: cc.v3(this.moveX, this.moveY, 0) }).call(() => {
            this.EnemyRunIdle();
        });
        tween.target(this.node).start();
        //let degree = Utility.CaculatorDegree(cc.v2(this.moveX, this.moveY));
        let degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) - 90;
        this.node.runAction(cc.rotate3DTo(0.2, cc.v3(-90, -180, -degree)));
        this.scheduleOnce(() => {
            this.node.stopAllActions();
            this.node.getComponent(cc.SkeletonAnimation).play("Run");
            this.EnemyRunIdle();
        }, 2.5);
    }
    SpawnerBrickWhenFall() {
        if (this.level > 0) {
            for (let i = 0; i < this.listBrickAdd.length; i++) {
                let x = Utility.RandomRangeFloat(-7, 7);
                let y = Utility.RandomRangeFloat(-7, 7);
                this.SpawnerBrick(x, y)
            }
            this.DecreaseStickAll();
        }
    }
    SpawnerBrick(x: number, y: number) {
        let Brick = cc.instantiate(this.brickHom);
        Brick.parent = cc.Canvas.instance.node;
        Brick.x = this.node.x;
        Brick.y = this.node.y;
        Brick.z = 1;
        let t = new cc.Tween().to(0.3, { position: new cc.Vec3(this.node.x + x, this.node.y + y, 0.5) })
            .target(Brick).start();
    }
    IncreaseStick() {
        let brick = cc.instantiate(this.brickStick);
        brick.parent = this.node.children[2];
        brick.x = this.x;
        brick.y = this.lastPositionStickY + 0.004;
        brick.z = this.z;
        brick.getComponent(BrickData).setmaterial(this.brickType);
        this.IncreaseLevel();
        this.lastPositionStickY = this.lastPositionStickY + 0.004;
        this.listBrickAdd.push(brick);
    }
    DecreaseStickAll() {
        for (let i = 0; i < this.listBrickAdd.length; i++) {
            this.listBrickAdd[i].destroy();
        }
        this.listBrickAdd.splice(0, this.listBrickAdd.length);
        this.lastPositionStickY = 0.013;
        this.node.children[0].scale = 1;
        this.DecreaseLevel();
    }
    IncreaseLevel() {
        this.node.children[0].scale += 0.1;
        this.level++;
    }
    DecreaseLevel() {
        this.level = 0;
    }
    betweenDegree(comVec, dirVec) {
        let angleDeg = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDeg;
    }
}
