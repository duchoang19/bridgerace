import BrickData from "../Brick/BrickData";
import { BrickType } from "../Common/EnumDefine";
import GamePlayInstance, { eventDispatcher } from "../Common/GamePlayInstance";
import Global from "../Common/Global";
import KeyEvent from "../Common/KeyEvent";
import Utility from "../Common/Utility";
import CheckMoveDownStairs from "./CheckMoveDownStairs";
import JoysticFollow from "./JoystickFollow";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CharacterController extends cc.Component {
    @property(cc.Node)
    ArrowDirection: cc.Node = null;
    @property(cc.Integer)
    speedMove: number = 20;
    @property(cc.Integer)
    timeAnim: number = 0;
    @property(cc.Node)
    placeVfxWeapon: cc.Node = null;
    @property(cc.Prefab)
    brickHom: cc.Prefab = null;
    @property(cc.Prefab)
    brickStick: cc.Prefab = null;
    @property(cc.Node)
    checkMoveDownStairs: cc.Node = null;
    @property({ type: cc.Enum(BrickType) })
    public brickType: BrickType = BrickType.BrickCharacter;
    level: number = 0;
    x: number = 0.003;
    lastPositionStickY: number = 0.013;
    z: number = -0.002;
    clampTopY: number = 0;
    listBrickAdd: cc.Node[] = [];
    boolCanAttack: boolean = true;
    boolMoveInFloor: boolean = false;
    boolPlaySoundFoot: boolean = false;
    boolCheckTypeRotate: boolean = false;
    public rigidbody: cc.RigidBody3D = null;
    onEnable() {
        eventDispatcher.on(KeyEvent.increaseStickMyCharacter, this.IncreaseStick, this);
    }
    onDisable() {
        eventDispatcher.off(KeyEvent.increaseStickMyCharacter, this.IncreaseStick, this);
    }
    start() {
        this.rigidbody = this.node.getComponent(cc.RigidBody3D);
        let collider = this.getComponent(cc.Collider3D);
        collider.on('collision-stay', this.onCollisionStay, this);
        this.listBrickAdd.splice(0, this.listBrickAdd.length);
    }
    onCollisionStay(event) {
        if (event.otherCollider.node.group == "Floor") {
            this.boolMoveInFloor = true;
            this.boolCanAttack = true;
        }
        if (event.otherCollider.node.group == "Model") {
            if (event.otherCollider.node.name == "StairsBoxCollider") {
                this.boolCanAttack = false;
            }
        }
    }
    onCollisionExit(event) {
        this.boolMoveInFloor = false;
    }
    update(dt) {
        if (Global.boolEnableTouch) {
            if (!this.checkMoveDownStairs.getComponent(CheckMoveDownStairs).boolMoveDown && this.boolMoveInFloor)
                this.MovePhyics(0);
            else
                this.MovePhyics(-20);
        }
        this.ClampTopYByLevel();
    }
    Attacking() {
        if (!Global.boolStartAttacking) {
            if (this.boolCanAttack) {
                Global.boolStartAttacking = true;
                Global.boolCheckAttacking = false;
                this.node.getComponent(cc.SkeletonAnimation).play("Attack");
                cc.audioEngine.playEffect(Global.soundAttack, false);
                this.scheduleOnce(() => {
                    Global.boolCheckAttacking = true;
                    Global.boolCheckAttacked = true;
                }, 0.18);
                this.scheduleOnce(() => {
                    Global.boolStartAttacking = false;
                    Global.boolFirstTouchJoyStick = false;
                    this.node.getComponent(cc.SkeletonAnimation).play("Idle");
                }, this.timeAnim);
            }
            else {
                this.node.getComponent(cc.SkeletonAnimation).play("Idle");
                Global.boolFirstTouchJoyStick = false;
            }
        }
    }
    MovePhyics(z: number) {
        this.node.y = cc.misc.clampf(this.node.y, -26, this.clampTopY);
        if (!this.boolPlaySoundFoot) {
            this.boolPlaySoundFoot = true;
            cc.audioEngine.playEffect(Global.soundFootStep, false);
            this.scheduleOnce(() => {
                this.boolPlaySoundFoot = false;
            }, 0.3);
        }
        let degree = Utility.CaculatorDegree(Global.touchPos);
        this.node.is3DNode = true;
        if (!this.boolCheckTypeRotate) {
            this.node.eulerAngles = new cc.Vec3(-90, 180, degree);
        }
        else {
            this.node.runAction(cc.sequence(cc.rotate3DTo(0.1, cc.v3(-90, 180, degree)), cc.callFunc(() => {
                this.boolCheckTypeRotate = false;
            })));
        }
        this.rigidbody.setLinearVelocity(new cc.Vec3(this.speedMove * Global.touchPos.x, this.speedMove * Global.touchPos.y, z));
    }
    StopMove() {
        this.rigidbody.setLinearVelocity(new cc.Vec3(0, 0, 0));
    }
    SpawnerEffectSmoke(smoke: cc.Prefab) {
        let Smoke = cc.instantiate(smoke);
        Smoke.parent = cc.Canvas.instance.node;
        let pos = this.node.convertToWorldSpaceAR(this.placeVfxWeapon.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        Smoke.x = pos.x;
        Smoke.y = pos.y;
        Smoke.z = 0;
    }
    IncreaseStick() {
        let brick = cc.instantiate(this.brickStick);
        brick.parent = this.node.children[2];
        brick.x = this.x;
        brick.y = this.lastPositionStickY + 0.004;
        brick.z = this.z;
        brick.getComponent(BrickData).setmaterial(BrickType.BrickCharacter);
        this.IncreaseLevel();
        this.lastPositionStickY = this.lastPositionStickY + 0.004;
        this.listBrickAdd.push(brick);
    }
    DecreaseStick() {
        this.listBrickAdd[this.listBrickAdd.length - 1].destroy();
        this.lastPositionStickY = this.lastPositionStickY - 0.004;
        this.listBrickAdd.pop();
    }
    IncreaseLevel() {
        this.node.children[0].scale += 0.1;
        this.level++;
        if (this.level >= 5)
            GamePlayInstance.Instance(GamePlayInstance).gameplay.txtClimb.runAction(cc.fadeIn(0.3));
    }
    DecreaseLevel() {
        this.level--;
    }
    SpawnerBrickWhenFall() {
        if (this.level > 0) {
            for (let i = 0; i < this.listBrickAdd.length; i++) {
                let x = Utility.RandomRangeFloat(-7, 7);
                let y = Utility.RandomRangeFloat(-7, 7);
                this.SpawnerBrick(x, y)
            }
            this.DecreaseStickAll();
        }
    }
    SpawnerBrick(x: number, y: number) {
        let Brick = cc.instantiate(this.brickHom);
        Brick.parent = cc.Canvas.instance.node;
        Brick.x = this.node.x;
        Brick.y = this.node.y;
        Brick.z = 1;
        let t = new cc.Tween().to(0.3, { position: new cc.Vec3(this.node.x + x, this.node.y + y, 0.5) })
            .target(Brick).start();
    }
    DecreaseStickAll() {
        for (let i = 0; i < this.listBrickAdd.length; i++) {
            this.listBrickAdd[i].destroy();
        }
        this.listBrickAdd.splice(0, this.listBrickAdd.length);
        this.lastPositionStickY = 0.013;
        this.node.children[0].scale = 1;
        this.level = 0;
    }
    ClampTopYByLevel() {
        switch (this.level) {
            case 0: this.clampTopY = 32;
                break;
            case 1: this.clampTopY = 36;
                break;
            case 2: this.clampTopY = 40;
                break;
            case 3: this.clampTopY = 44;
                break;
            case 4: this.clampTopY = 48;
                break;
            case 5: this.clampTopY = 52;
                break;
            case 6: this.clampTopY = 56;
                break;
            case 7: this.clampTopY = 60;
                break;
            case 8: this.clampTopY = 64;
                break;
            default:
                this.clampTopY = 200;
        }
    }
    CharacterFall() {
        this.node.getComponent(cc.SkeletonAnimation).play("Fall");
        GamePlayInstance.Instance(GamePlayInstance).gameplay.joyStick.opacity = 0;
        this.SpawnerBrickWhenFall();
        Global.boolEnableTouch = false;
        this.boolCheckTypeRotate = true;
        Global.boolCharacterFall = true;
        GamePlayInstance.Instance(GamePlayInstance).gameplay.joyStick.getComponent(JoysticFollow).joyDot.setPosition(GamePlayInstance.Instance(GamePlayInstance).gameplay.joyStick.getComponent(JoysticFollow).joyRing.getPosition());
        this.StopMove();
        this.scheduleOnce(() => {
            Global.boolCharacterFall = false;
            this.node.getComponent(cc.SkeletonAnimation).play("Idle");
            Global.boolFirstTouchJoyStick = false;
        }, 1.28);
    }
}
