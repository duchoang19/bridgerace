import GamePlayInstance from "../Common/GamePlayInstance";
import Global from "../Common/Global";
import CharacterController from "./CharacterController";
const { ccclass, property } = cc._decorator;
@ccclass
export default class JoysticFollow extends cc.Component {
    @property(cc.Node)
    joyRing: cc.Node = null;
    @property(cc.Node)
    joyDot: cc.Node = null;
    stickPos: cc.Vec2 = null;
    touchLocation: cc.Vec2 = null;
    radius: number = 0;
    gamePlayInstance: GamePlayInstance;
    onLoad() {
        this.radius = this.joyRing.width / 2;
    }
    start() {
        this.gamePlayInstance = GamePlayInstance.Instance(GamePlayInstance);
        this.node.on(cc.Node.EventType.TOUCH_START, this.TouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.TouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.TouchCancel, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.TouchCancel, this);
    }
    TouchStart(event) {
        if (!Global.boolEndGame && Global.boolStartPlay && !Global.boolCharacterFall) {
            var mousePosition = event.getLocation();
            let localMousePosition = this.node.convertToNodeSpaceAR(mousePosition);
            this.node.opacity = 255;
            this.stickPos = localMousePosition;
            this.touchLocation = event.getLocation();
            this.joyRing.setPosition(localMousePosition);
            this.joyDot.setPosition(localMousePosition);
            this.gamePlayInstance.gameplay.Guide.active = false;
            this.gamePlayInstance.gameplay.txtCollectBlock.active = false;
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController).ArrowDirection.active = true;
        }
    }
    TouchMove(event) {
        if (!Global.boolEndGame && Global.boolStartPlay  && !Global.boolCharacterFall) {
            this.node.opacity = 255;
            Global.boolEnableTouch = true;
            if (!Global.boolFirstTouchJoyStick) {
                Global.boolFirstTouchJoyStick = true;
                this.gamePlayInstance.gameplay.myCharacter.getComponent(cc.SkeletonAnimation).play("Run");
            }
            if (this.touchLocation === event.getLocation()) {
                return false;
            }
            this.gamePlayInstance.gameplay.Guide.active = false;
            let touchPos = this.joyRing.convertToNodeSpaceAR(event.getLocation());
            let distance = touchPos.mag();
            let posX = this.stickPos.x + touchPos.x;
            let posY = this.stickPos.y + touchPos.y;
            let p = cc.v2(posX, posY).sub(this.joyRing.getPosition()).normalize();
            Global.touchPos = p;
            if (this.radius > distance) {
                this.joyDot.setPosition(cc.v2(posX, posY));
            } else {
                let x = this.stickPos.x + p.x * this.radius;
                let y = this.stickPos.y + p.y * this.radius;
                this.joyDot.setPosition(cc.v2(x, y));
            }
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController).ArrowDirection.active = true;
            this.gamePlayInstance.gameplay.Guide.active = false;
            this.gamePlayInstance.gameplay.txtCollectBlock.active = false;
        }
    }
    TouchCancel() {
        if (!Global.boolEndGame && Global.boolStartPlay  && !Global.boolCharacterFall) {
            this.joyDot.setPosition(this.joyRing.getPosition());
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController).Attacking();
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController).StopMove();
            Global.boolEnableTouch = false;
            this.node.opacity = 0;
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController).boolCheckTypeRotate = true;
        }
    }
}
