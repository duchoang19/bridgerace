import GamePlayInstance from "../Common/GamePlayInstance";
import Global from "../Common/Global";
import KeyEvent from "../Common/KeyEvent";
import Utility from "../Common/Utility";

const { ccclass, property } = cc._decorator;

@ccclass
export default class BoxController extends cc.Component {
    @property(cc.Prefab)
    brickHom: cc.Prefab = null;
    timeBroken: number = 1.06;
    boolCheckDeathOne: boolean = false;
    boolCheckDeath: boolean = false;
    boolCheckdelay: boolean = false;
    update() {
        if (!this.boolCheckDeath) {
            if (Global.boolStartAttacking && Global.boolCheckAttacking) {
                let pos1 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.children[0].children[0].getPosition()));
                let pos2 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.children[0].children[1].getPosition()));
                let pos3 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.children[0].children[2].getPosition()));

                let direction1 = cc.v2(pos1.x - GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.x, pos1.y - GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.y);
                let direction2 = cc.v2(pos2.x - GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.x, pos2.y - GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.y);
                let degree = direction1.signAngle(direction2);
                degree = cc.misc.radiansToDegrees(degree);
                let posEnemy = cc.v2(this.node.x - GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.x, this.node.y - GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.y);
                let degreeWithPos1 = posEnemy.signAngle(direction1);
                degreeWithPos1 = cc.misc.radiansToDegrees(degreeWithPos1);
                let degreeWithPos2 = posEnemy.signAngle(direction2);
                degreeWithPos2 = cc.misc.radiansToDegrees(degreeWithPos2);
                let realNeed = 0;
                degreeWithPos1 = Math.abs(degreeWithPos1);
                degreeWithPos2 = Math.abs(degreeWithPos2);
                if (degreeWithPos1 > degreeWithPos2) {
                    realNeed = degreeWithPos1;
                }
                else {
                    realNeed = degreeWithPos2;
                }
                let distance = Utility.Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.x, GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.y));
                let maxDistance = Utility.Distance(cc.v2(GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.x, GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.y), cc.v2(pos3.x, pos3.y));
                if (Math.abs(realNeed) < degree) {
                    if (distance < maxDistance) {
                        if (Global.boolCheckAttacked && !this.boolCheckDeathOne) {
                            this.BrokenBox();
                            this.boolCheckDeath = true;
                            this.boolCheckDeathOne = true;
                            //Global.boolCheckAttacked = false;
                            //Global.boolCheckAttacking = false;
                        }
                    }
                }
                if (!this.boolCheckdelay) {
                    this.boolCheckdelay = true;
                    this.scheduleOnce(() => {
                        Global.boolCheckAttacking = false;
                        this.boolCheckdelay = false;
                    }, 0.003);
                }
            }
        }
    }
    DieWhenEnemyAttack() {
        this.boolCheckDeath = true;
        this.BrokenBox();
    }
    BrokenBox() {
        this.node.getComponent(cc.SkeletonAnimation).play("Broken");
        cc.audioEngine.playEffect(Global.soundBoxBroken,false);
        this.node.getComponent(cc.BoxCollider3D).enabled = false;
        this.scheduleOnce(() => {
            // for( let i = 0; i < 20; i++)
            // {
            //     this.SpawnerBrick(this.node.x - 2, this.node.y + 2);
            // }
            this.SpawnerBrick(this.node.x - 2, this.node.y + 2);
            this.SpawnerBrick(this.node.x + 2, this.node.y + 2);
            this.SpawnerBrick(this.node.x - 2, this.node.y - 2);
            this.SpawnerBrick(this.node.x + 2, this.node.y - 2);
        }, this.timeBroken / 7);
        this.scheduleOnce(() => {
            this.node.getComponent(cc.SkeletonAnimation).stop();
        }, this.timeBroken);
    }
    SpawnerBrick(x: number, y: number) {
        let Brick = cc.instantiate(this.brickHom);
        Brick.parent = cc.Canvas.instance.node;
        Brick.x = this.node.x;
        Brick.y = this.node.y;
        Brick.z = 1;
        let t = new cc.Tween().to(0.3, { position: new cc.Vec3(x, y, 0.5) })
            .call(() => {
                Brick.getComponent(cc.BoxCollider3D).enabled = true;
            })
            .target(Brick).start();
    }
}
