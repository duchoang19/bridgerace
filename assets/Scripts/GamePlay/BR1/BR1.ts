import Global from "../../Common/Global";
import Singleton from "../../Common/Singleton";
import AIController from "../../Controller/AIController";
import CharacterController from "../../Controller/CharacterController";

const { ccclass, property } = cc._decorator;
declare const window: any;
@ccclass
export default class BR1 extends Singleton<BR1> {
    @property(cc.Node)
    Guide: cc.Node = null;
    @property(cc.Node)
    txtCollectBlock: cc.Node = null;
    @property(cc.Node)
    myCharacter: cc.Node = null;
    @property(cc.Node)
    AIParent: cc.Node = null;
    @property(cc.Node)
    BoxParent: cc.Node = null;
    @property(cc.Node)
    txtClimb: cc.Node = null;
    @property(cc.Node)
    joyStick: cc.Node = null;
    @property(cc.Node)
    effectCongra: cc.Node = null;
    @property(cc.Node)
    btnDownLoad: cc.Node = null;
    @property(cc.Node)
    endGame: cc.Node = null;
    boolEndGameOne: boolean = false;
    ironsource: boolean = false;
    mindworks: boolean = false;
    vungle: boolean = false;
    boolcheckInteraction: boolean = false;
    constructor() {
        super();
        BR1._instance = this;
    }
    start() {
        Global.boolStartPlay = true;
    }
    update() {
        if (Global.boolEnableTouch && !this.boolcheckInteraction) {
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
            for (let i = 0; i < this.AIParent.childrenCount; i++) {
                this.AIParent.children[i].getComponent(AIController).StartMove();
            }
            this.btnDownLoad.active = true;
            this.boolcheckInteraction = true;
        }
        if (this.myCharacter.y >= 70 && !this.boolEndGameOne) {
            this.boolEndGameOne = true;
            Global.boolEndGame = true;
            Global.boolEnableTouch = false;
            for (let i = 0; i < this.myCharacter.children[2].childrenCount; i++) {
                if (this.myCharacter.children[2].children[i].name == "BrickStick") {
                    this.myCharacter.children[2].children[i].active = false;
                }
            }
            for(let i = 0; i < this.AIParent.childrenCount; i++)
            {
                this.AIParent.children[i].active = false;
            }
            this.myCharacter.stopAllActions();
            this.joyStick.active = false;
            this.ScaleCongra();
            this.myCharacter.getComponent(cc.RigidBody3D).setLinearVelocity(new cc.Vec3(0, 0, 0));
            this.myCharacter.getComponent(CharacterController).ArrowDirection.active = false;
            this.myCharacter.getComponent(cc.SkeletonAnimation).play("Win");
            cc.audioEngine.playEffect(Global.soundWin, false);
            this.scheduleOnce(() => {
                this.EndGame();
            }, 1.2);
        }
    }
    ScaleCongra() {
        this.effectCongra.scale = 0;
        this.effectCongra.opacity = 255;
        this.effectCongra.runAction(cc.sequence(cc.scaleTo(0.3, 1.3).easing(cc.easeBounceOut()), cc.callFunc(() => {
            this.scheduleOnce(() => {
                this.effectCongra.runAction(cc.fadeOut(0.3));
            }, 0.7);
        })));
    }
    EndGame() {
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
        this.scheduleOnce(() => {
            cc.audioEngine.stopAllEffects();
        }, 2);
        this.endGame.active = true;
    }
}
